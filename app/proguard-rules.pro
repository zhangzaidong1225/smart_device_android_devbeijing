# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Users\lilos\AppData\Local\Android\Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
#-dontwarn com.tencent.bugly.**
#-keep public class com.tencent.bugly.**{*;}

-keep class org.openudid.**{*;}
#
#-keep class com.lianluo.qingfeng.module.**{*;}
#-keep class no.nordicsemi.android.support.v18.scanner.ScanCallback{*;}
#-keep class no.nordicsemi.android.dfu.**{*;}
#
#-keep class  android.support.v4.**{*;}
#-keep class  android.support.v7.**{*;}
