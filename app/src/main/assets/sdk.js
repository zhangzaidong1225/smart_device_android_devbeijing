(function(name, definition, context){
	if (typeof module != 'undefined' && module.exports) {
		module.exports = definition();
	}
	else if (typeof context['define'] == 'function' && (context['define']['amd'] || context['define']['cmd']) ) {
		// amd或cmd环境
		define(definition);
	}
	else {
		// 正常环境
		context[name] = definition();
	}

	LL.ble.initial();

})('LL',function(){


	/**
	* 判断手机类型
	*
	* Windows Phone 8.1 fakes user agent string to look like Android and iPhone.
	*
	* @type boolean
	*/
	var deviceIsWindowsPhone = navigator.userAgent.indexOf("Windows Phone") >= 0;
	var deviceIsAndroid = navigator.userAgent.indexOf('Android') > 0 && !deviceIsWindowsPhone;
	var deviceIsIOS = /iP(ad|hone|od)/.test(navigator.userAgent) && !deviceIsWindowsPhone;

	sdk = {

		/**
		 * sdk最低支持app的版本号，如果app的版本号低于lowestAppVersion，sdk不能使用
		 */
		lowestIOSVersion : '1.0.0',
		lowestAndroidVersion : '1',

		/** 
		*  保存JS回调
		*/
		callBackFuncs : {},

		/** 
		* 保存回调
		*/
		saveCallBackFunc : function(key, callBackFunc) {
			this.callBackFuncs[key] = callBackFunc || function(){};
			return key;
		},

        /**
		 * 调用native方法
         * @param  {String} ctrl   native类
         * @param  {String} method 类的方法
         * @param  {Object} params JSON参数
         * @return {undefined}
         */
        call : function(ctrl, method, params, cb) {
        	// 保存回调
			var callBackFuncName = sdk.saveCallBackFunc(ctrl + method, cb);
			var lowestAppVersion = {
				"iOS":this.lowestIOSVersion,
				"android":this.lowestAndroidVersion
			};

            params = params || {};
            // params.sdkVersion = this.version;
            LL.debug && console.log('sdk接口调用: ' + ctrl + '.' + method + ' & 入参:' + JSON.stringify(params));

			setTimeout(function() {
				// 调用iOS
		        window.webkit && window.webkit.messageHandlers && window.webkit.messageHandlers.iOS_obj && window.webkit.messageHandlers.iOS_obj.postMessage({
		        	'iOS_obj_target':ctrl,
		        	'iOS_obj_selector':method,
		        	'iOS_obj_callBackFuncName':callBackFuncName,
		        	'iOS_obj_lowestAppVersion':lowestAppVersion,
		        	'iOS_obj_paramters': params
		        });
	            // window.iOS_obj && window.iOS_obj.execute(ctrl,method, JSON.stringify(params),callBackFuncName,JSON.stringify(lowestAppVersion));
	            // 调用android
	            window.android_obj && window.android_obj.execute(ctrl,method,JSON.stringify(params),callBackFuncName,JSON.stringify(lowestAppVersion));
			}, 0);
        },
	};

	LL = {
		// 调试模式
		debug : 1,

		prevTime : new Date().getTime(),

		/**
		 * 一秒执行一次
		 */
		canDo : function(){
			var now = new Date().getTime();
			if(now - this.prevTime > 1000){
				this.prevTime = now;
				return true;
			}
			return false;
		},

		/**
		 * 调试用的alert
		 */
		alert : function(params){
			if (this.debug) {
				if (typeof params == "object") {
					var message = JSON.stringify(params);
				}else{
					var message = params;
				}

				var type = typeof params;
				message += ' {{type=' + type + '}}';

			    // alert(message);
				setTimeout(function() {
					if (window.webkit && window.webkit.messageHandlers && window.webkit.messageHandlers.iOS_obj) {
						alert(message);
					}
				    window.android_obj && android_obj.alert(message);
				}, 0);
			}
		},

		/**
		 * 播放系统声音
		 *
		 * @param  {[type]}
		 */
		playSystemSound : function(params, cb){
			return sdk.call('Audio','playSystemSound',params || {}, cb || function(){});
		},

		/**
		*  是否连接wifi
		*/
		hasNetWork : function(cb){
			return sdk.call('NetWork','hasNetWork',{},cb || function(){});
		},

		/**
		*  存储数据到联络服务器
		*/
		backupAppData : function(params, cb){
		    return sdk.call('Storage','setServerItem',params || {}, cb || function(){});
		},

		/**
		*  从联络服务器获取数据
		*/
		retrieveAppData : function(params, cb){
		    return sdk.call('Storage','getServerItem',params || {}, cb || function(){});
		},

		/**
		*  通过手机GPS，获取当前的地理位置
		*/
		getLocation : function(params, cb){
			return sdk.call('Location','getLocation',params || {}, cb || function(){});
		},

		/**
		*  搜索给定地址的地理位置经纬度
		*/
		searchLocation : function(params, cb){
			return sdk.call('Location','searchLocation',params || {}, cb || function(){});
		}

	};

	/**
	* native回调
	*/
	LL.callBack = function(result){
		setTimeout(function() {
			var callBackParamsObj;
			if (typeof result.callBackParams == 'object') {
				callBackParamsObj = result.callBackParams;
			}else{
				callBackParamsObj = JSON.parse(result.callBackParams);
			}
			var func = sdk.callBackFuncs[result.callBackName];
			func && func(callBackParamsObj);
			delete sdk.callBackFuncs[result.callBackName];
		}, 0);
	};

	LL.ble = {

		/**
		*  蓝牙订阅通知事件
		*/
		bleSubscribeNotify : document.createEvent('HTMLEvents'),

		/**
		*  蓝牙状态改变事件
		*/
		bleStateDidChangedNotify : document.createEvent('HTMLEvents'),

		/**
		*  设备连接状态发生改变事件
		*/
		deviceStateDidChangedNotify : document.createEvent('HTMLEvents'),

		/**
		*  初始化方法
		*/
		initial : function() {
			this.bleSubscribeNotify.initEvent('bleSubscribeNotify', false, false);
			this.bleStateDidChangedNotify.initEvent('bleStateDidChangedNotify', false, false);
			this.deviceStateDidChangedNotify.initEvent('deviceStateDidChangedNotify', false, false);
		},

		/**
		*  获取当前设备的deviceId
		*/
		getDeviceId : function(cb){
			return sdk.call('BlueTooth','getDeviceId',{}, cb || function(){});
		},

		/**
		*  蓝牙是否可用
		*/
		isEnabled : function(cb){
			return sdk.call('BlueTooth','isEnabled',{}, cb || function(){});
		},

		/**
		*  是否已连接
		*/
		isConnect : function(params, cb) {
			return sdk.call('BlueTooth','isConnect',params || {}, cb || function(){});
		},

		bleEnableDict : {
			0 : "Unknown",
			1 : "Unsupported",
			2 : "PoweredOff",
			3 : "PoweredOn"
		},

		/**
		*  蓝牙状态发生改变
		*/
		stateDidChanged : function(state) {
			setTimeout(function() {
				if (state > 3) {
					state = 0;
				}
				var stateStr = LL.ble.bleEnableDict[state];
				var params = {};
				params.state = stateStr;
				LL.ble.bleStateDidChangedNotify.params = params;
				document.dispatchEvent(LL.ble.bleStateDidChangedNotify);
			}, 0);
		},

		/**
		*  设备连接状态发生改变
		*/
		deviceStateDidChanged : function(result) {
			setTimeout(function() {
				var params = {};
				params.state = result;
				LL.ble.deviceStateDidChangedNotify.params = params;
				document.dispatchEvent(LL.ble.deviceStateDidChangedNotify);
			}, 0);
		},

		/**
		*  通知第三方订阅消息
		*/
		subScribeCallBack : function(params) {
			// 在js->native->js->...频繁调用的时候，js和native会处在资源竞争的环境下，解决方法是在js代码外面包装一层setTimeout()，在js线程去调度执行
			// 参考 http://stackoverflow.com/questions/19859414/unreproducible-webcore-crashes
			setTimeout(function() {
				LL.ble.bleSubscribeNotify.params = params;
				var bytes = LL.tool.hexStr2Bytes(params.value);
				params.data = bytes;
				document.dispatchEvent(LL.ble.bleSubscribeNotify);
			}, 0);
		},

		/**
		 * 连接设备
		 *
		 * @param  {object}
		 * @param  {Function}
		 * @return {[type]}
		 */
		connect : function(params, cb) {
			return sdk.call('BlueTooth','connect',params || {}, cb || function(){});
		},

		/**
		 * 断开连接
		 *
		 * @param  {object}
		 * @param  {Function}
		 * @return {[type]}
		 */
		disconnect : function(params, cb) {
			return sdk.call('BlueTooth','disconnect',params || {}, cb || function(){});
		},

		/**
		 * 订阅设备
		 *
		 * @param  {object}
		 * @param  {Function}
		 * @return {[type]}
		 */
		subscribe : function(params, cb) {
			return sdk.call('BlueTooth','subscribe',params || {}, cb || function(){});
		},

		/**
		 * 取消订阅
		 *
		 * @param  {[type]}
		 * @param  {Function}
		 * @return {[type]}
		 */
		unsubscribe : function(params, cb) {
			return sdk.call('BlueTooth','unsubscribe',params || {}, cb || function(){});
		},

		/**
		 * 写数据
		 *
		 * @param  {[type]}
		 * @param  {Function}
		 * @return {[type]}
		 */
		write : function(params, cb) {
			params = params || {};
			var dataBytes = params.data || "";
			var data = LL.tool.bytes2HexStr(dataBytes);
			params.data = data;
			return sdk.call('BlueTooth','write',params, cb || function(){});
		},
	}

	/**
	 * 存储模块
	 */
    LL.nativeStorage = {
        setStorage: function(params, cb) {
            return sdk.call('Storage', 'setItem', params || {}, cb || function(){});
        },
        getStorage: function(params, cb) {
            return sdk.call('Storage', 'getItem', params || {}, cb || function(){});
        },
        removeStorage: function(params, cb) {
            return sdk.call('Storage', 'removeItem', params || {}, cb || function(){});
        },
    }

	/**
	 * 工具
	 */
	LL.tool = {

		/**
		 * utf8字符串转成十六进制字符串
		 *
		 * @param  {[type]}
		 * @return {[type]}
		 */
		utf8StrToHexString:function(str){
		    var ret = "";
		    var sa = str.split("%");
		    for(i = 0; i < sa.length; i++){
		        if(sa[i].length == 0){
		            continue;
		        }

		        // i = 0 的时候，如果sa[i]有值，则肯定不是中文，这时候当做普通字符串处理
		        if (i == 0) {
		        	ret += this.cstringToBytes(sa[i]);
		        	continue;
		        }

		        else if(sa[i].length == 1){
		            ret += this.cstringToBytes(sa[i]);
		        }
		        else if(sa[i].length == 2){
		            ret += sa[i].substring(0,2);
		        }
		        else{
		            ret += sa[i].substring(0,2);
		            ret += this.cstringToBytes(sa[i].substring(2,sa[i].length));
		        }
		    }
		    return ret;
		},

		/**
		 * 字符串转byte数组
		 *
		 * @param  {[type]}
		 * @return {[type]}
		 */
		stringToBytes:function(str){
			var value = encodeURIComponent(str);
		    var nameEncodedBytes = LL.tool.utf8StrToHexString(value);
		    return LL.tool.hexStr2Bytes(nameEncodedBytes);
		},

		/**
		 * 普通字符串转byte数组(不能转中文字符，中文字符应该先uft8再由utf8StrToHexString转HexString)
		 *
		 * @param  {[type]}
		 * @return {[type]}
		 */
		cstringToBytes:function(str){
		    var bytes = new ArrayBuffer(str.length * 2);
		    var uint16Bytes = new Uint16Array(bytes);
		    for (var i = 0; i < str.length; i++) {
		        uint16Bytes[i] = str.charCodeAt(i);
		    }
		    return this.bytes2HexStr(uint16Bytes);
		},

		/**
		 * 十六进制字符串转byte数组
		 *
		 * @param  {[type]}
		 * @return {[type]}
		 */
		hexStr2Bytes:function(str){
		    var pos = 0;
		    var len = str.length;

		    if(len % 2 != 0) {
		        return null;
		    }

		    len /= 2;
		    var uint8Bytes = new Uint8Array(len);

		    for(var i = 0; i < len; i++) {
		        var s = str.substr(pos, 2);
		        uint8Bytes[i] = parseInt(s, 16);
		        pos += 2;
		    }

		    return uint8Bytes;
		},

		/**
		 * bytes数组转十六进制字符串
		 *
		 * @param  {[type]}
		 * @return {[type]}
		 */
		bytes2HexStr:function(arr)
		{
		    var str = "";
		    for(var i=0; i<arr.length; i++) {
		        var tmp = arr[i].toString(16);
		        if(tmp.length == 1) {
		            tmp = "0" + tmp;
		        }
		        str += tmp;
		    }

		    return str;
		}
	}

	return LL;

}, this);