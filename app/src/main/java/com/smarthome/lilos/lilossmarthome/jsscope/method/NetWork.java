package com.smarthome.lilos.lilossmarthome.jsscope.method;

//import android.webkit.WebView;

import com.smarthome.lilos.lilossmarthome.jsscope.bean.CallbackBean;
import com.smarthome.lilos.lilossmarthome.jsscope.holder.NetworkHolder;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.tencent.smtt.sdk.WebView;

import java.util.HashMap;
import java.util.Map;

/**
 * 对应JSSDK中NetWork集指令
 */
public class NetWork
{
    private static CallbackBean bean = new CallbackBean();
    private static Map<String, Object> resutlMap = new HashMap<>();

    public static boolean hasNetWork(WebView webView, Map<String, String> params,
                                     String callBackFuncName)
    {
        resutlMap.clear();
        bean.callBackName = callBackFuncName;

        resutlMap.put("hasNetWork", NetworkHolder.isNetworkAvailable(webView.getContext()));

        bean.callBackParams = JsonUtils.objectToJson(resutlMap);
        webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");

        return true;
    }
}
