package com.smarthome.lilos.lilossmarthome.network.bean.mops;

/**
 * Created by Joker on 2016/12/30.
 */

public class DeviceTypeEntity
{

    /**
     * id : 1
     * name : 忻风
     * icon : http://139.198.9.141:81/uploads/20161221/585a0853466b7.png
     * description : 忻风便携式智能空气净化器
     * conn_type : 2
     * status : 1
     * sn : A1
     * control_url : http://139.198.9.141:81/uploads/html/W1/app.html
     * control_model : html5
     * control_version : html5
     * wifi_config_model :
     * wifi_config_desc : 请打开设备的开关,确认设备处于待连接状态
     * wifi_config_gif : /uploads/20161221/585a085348376.gif
     * hotspot_ssid_prefix : lianluo-smart-
     */

    private int id;
    private String name;
    private String icon;
    private String description;
    private int conn_type;
    private int status;
    private String sn;
    private String control_url;
    private String control_model;
    private String control_version;
    private String wifi_config_model;
    private String wifi_config_desc;
    private String wifi_config_gif;
    private String hotspot_ssid_prefix;
    private String bluetooth_server_id;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getIcon()
    {
        return icon;
    }

    public void setIcon(String icon)
    {
        this.icon = icon;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public int getConn_type()
    {
        return conn_type;
    }

    public void setConn_type(int conn_type)
    {
        this.conn_type = conn_type;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public String getSn()
    {
        return sn;
    }

    public void setSn(String sn)
    {
        this.sn = sn;
    }

    public String getControl_url()
    {
        return control_url;
    }

    public void setControl_url(String control_url)
    {
        this.control_url = control_url;
    }

    public String getControl_model()
    {
        return control_model;
    }

    public void setControl_model(String control_model)
    {
        this.control_model = control_model;
    }

    public String getControl_version()
    {
        return control_version;
    }

    public void setControl_version(String control_version)
    {
        this.control_version = control_version;
    }

    public String getWifi_config_model()
    {
        return wifi_config_model;
    }

    public void setWifi_config_model(String wifi_config_model)
    {
        this.wifi_config_model = wifi_config_model;
    }

    public String getWifi_config_desc()
    {
        return wifi_config_desc;
    }

    public void setWifi_config_desc(String wifi_config_desc)
    {
        this.wifi_config_desc = wifi_config_desc;
    }

    public String getWifi_config_gif()
    {
        return wifi_config_gif;
    }

    public void setWifi_config_gif(String wifi_config_gif)
    {
        this.wifi_config_gif = wifi_config_gif;
    }

    public String getHotspot_ssid_prefix()
    {
        return hotspot_ssid_prefix;
    }

    public void setHotspot_ssid_prefix(String hotspot_ssid_prefix)
    {
        this.hotspot_ssid_prefix = hotspot_ssid_prefix;
    }

    public String getBluetooth_server_id()
    {
        return bluetooth_server_id;
    }

    public void setBluetooth_server_id(String bluetooth_server_id)
    {
        this.bluetooth_server_id = bluetooth_server_id;
    }
}
