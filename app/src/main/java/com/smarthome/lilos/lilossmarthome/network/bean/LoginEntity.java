package com.smarthome.lilos.lilossmarthome.network.bean;

/**
 * Created by Kevin on 2016/7/11.
 */
public class LoginEntity extends ResponseEntity
{

    private LoginData data;

    public LoginData getData()
    {
        return data;
    }

    public void setData(LoginData data)
    {
        this.data = data;
    }

    public class LoginData
    {
        private String key;

        public String getKey()
        {
            return key;
        }

        public void setKey(String key)
        {
            this.key = key;
        }
    }

}
