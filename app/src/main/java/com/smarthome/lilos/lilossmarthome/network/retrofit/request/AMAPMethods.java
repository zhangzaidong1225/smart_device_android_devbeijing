package com.smarthome.lilos.lilossmarthome.network.retrofit.request;

import android.util.Log;

import com.smarthome.lilos.lilossmarthome.network.bean.amap.AMAPAddress;
import com.smarthome.lilos.lilossmarthome.network.bean.amap.AMAPGeo;
import com.smarthome.lilos.lilossmarthome.network.bean.amap.AMAPLocation;
import com.smarthome.lilos.lilossmarthome.network.retrofit.SchedulersTransformer;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;

/**
 * 高德地图对应网络操作
 * Created by Joker on 2016/12/23.
 */

public class AMAPMethods
{
    private static final String BASE_URL = "http://restapi.amap.com/v3/";

    private static final String KEY = "db50eba84b76f8ccccb87a0c2c0b034a";
    private static final String COORDSYS = "gps";
    private static final int RADIUS = 0;
    private static final String EXTENSIONS = "base";
    private static final String BATCH = "false";
    private static final int ROADLEVEL = 1;

    private static final int DEFAULT_TIMEOUT = 10;

    private Retrofit retrofit;
    private IAMAPService service;

    private AMAPMethods()
    {
    }

    public static AMAPMethods getInstance()
    {
        return SingletonHolder.INSTANCE;
    }

    public void init()
    {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);

        HttpLoggingInterceptor.Level level = HttpLoggingInterceptor.Level.BODY;
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(message ->
                Log.d("AMAPMethods", message));
        loggingInterceptor.setLevel(level);

        client.addInterceptor(loggingInterceptor);

        retrofit = new Retrofit.Builder()
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .build();

        service = retrofit.create(IAMAPService.class);
    }

    public Observable<AMAPLocation> locationToAmap(String location)
    {
        return service.locationsToAmap(KEY, location, COORDSYS)
                .compose(new SchedulersTransformer<>());
    }

    public Observable<AMAPAddress> address(String locations)
    {
        return service.address(KEY, locations, RADIUS, EXTENSIONS, BATCH, ROADLEVEL)
                .compose(new SchedulersTransformer<>());
    }

    public void search(Subscriber<AMAPGeo> subscriber, String address)
    {
        service.search(KEY, address)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    private static class SingletonHolder
    {
        private static final AMAPMethods INSTANCE = new AMAPMethods();
    }
}
