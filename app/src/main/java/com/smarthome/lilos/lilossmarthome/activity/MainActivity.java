package com.smarthome.lilos.lilossmarthome.activity;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.RadioGroup;

import com.orhanobut.logger.Logger;
import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.bluetooth.BLEService;
import com.smarthome.lilos.lilossmarthome.bluetooth.BluetoothScanner;
import com.smarthome.lilos.lilossmarthome.device.BaseBluetoothDevice;
import com.smarthome.lilos.lilossmarthome.device.BaseDevice;
import com.smarthome.lilos.lilossmarthome.device.DeviceManager;
import com.smarthome.lilos.lilossmarthome.fragment.FragmentTabAdapter;
import com.smarthome.lilos.lilossmarthome.fragment.TabHomeFragment;
import com.smarthome.lilos.lilossmarthome.fragment.TabInfoFragment;
import com.smarthome.lilos.lilossmarthome.fragment.TabMarketFragment;
import com.smarthome.lilos.lilossmarthome.notify.Notify;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.umeng.analytics.MobclickAgent;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 主界面
 * 用于展示"设备列表" "商城" "我的"三个Fragment
 */
public class MainActivity extends BaseActivity
{
    public static MainActivity mContext;
    private TabMarketFragment mTabMarketFragment;
    //双击返回退出标识符
    private static boolean isExit = false;
    private RadioGroup mRgTabs;
    private List<Fragment> fragments = new ArrayList<>();
    private FragmentTabAdapter tabAdapter;
    private Queue<String> waitForConAdrQueue = new LinkedBlockingQueue<>();
    private DeviceManager myDManager = null;


    private static boolean requestStatus = false;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        listenClose = false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        init();
    }

    @Override
    protected void onDestroy() {
        Notify.clearAllNotify();
        super.onDestroy();
    }
    @Override
    public void onAttachFragment(Fragment fragment)
    {
        super.onAttachFragment(fragment);
    }

    private void initView()
    {
        mRgTabs = (RadioGroup) findViewById(R.id.rg_tabs);
    }

    private void init()
    {
        mContext = this;
        String url = getIntent().getStringExtra("url");
        if (url != null && !url.equals(""))
        {
            if (url.startsWith("http"))
            {
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
            else
            {
                // TODO: 2016/8/18 后期处理
            }
        }

        mTabMarketFragment = new TabMarketFragment();
        fragments.add(new TabHomeFragment());
        fragments.add(mTabMarketFragment);
        fragments.add(new TabInfoFragment());
        tabAdapter = new FragmentTabAdapter(this, fragments, mRgTabs, R.id.tab_container);
//        tabAdapter.setOnRgsExtraCheckedChangedListener((radioGroup, checkedId, index) -> {});

        myDManager = DeviceManager.getInstance();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        Log.d("MainActivity", "MainActivity onKey");
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            if (1 == tabAdapter.getCurrentTab()) {
                if(mTabMarketFragment != null && mTabMarketFragment.onKeyDown(keyCode,event)) {
                    return true;
                }
            }
            exitBy2Click();
        }
        return false;
    }

    private void exitBy2Click()
    {
        Timer tExit = null;
        if (isExit == false)
        {
            isExit = true;
            this.refreshList();
            AppToast.showToast(R.string.double_click_exit);
            tExit = new Timer();
            tExit.schedule(new TimerTask()
            {
                @Override
                public void run()
                {
                    isExit = false;
                }
            }, 2000);
        }
        else
        {
            finish();
            System.exit(0);
        }
    }

    public void refreshList()
    {
        Logger.d("刷新主页面设备列表");
        for (Fragment fragment : fragments)
        {
            if (fragment instanceof TabHomeFragment)
            {
                TabHomeFragment thf = (TabHomeFragment) fragment;
                Log.e("8888===","getLeDeviceList1");
                thf.getLeDeviceList();
            }
        }
    }

    public void refreshView()
    {
        mRgTabs.postInvalidate();
    }

    public void requestBluetooth(int requestCode, String address)
    {
        Logger.d("请求开启蓝牙" + requestCode + " 地址为" + address);
        waitForConAdrQueue.add(address);
        if (!requestStatus){
            requestStatus = true;
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, requestCode);
        }

    }


    @Override
    protected void onActivityResult(final int requestCode, int resultCode, final Intent data)
    {
        if (waitForConAdrQueue.size() != 0)
        {
            new BluetoothWatcher(requestCode, resultCode, waitForConAdrQueue.remove()).start();
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    class BluetoothWatcher extends Thread
    {
        int i;
        int requestCode;
        int resultCode;
        String waitForConnectingAddress;


        public BluetoothWatcher(int requestCode, int resultCode, String waitForConnectingAddress)
        {
            this.requestCode = requestCode;
            this.resultCode = resultCode;
            this.waitForConnectingAddress = waitForConnectingAddress;
            i = 0;
        }

        @Override
        public void run()
        {
            while (i < 1)
            {
                Logger.e("查询蓝牙是否开始，当前尝试第" + i + "次" + "  尝试的设备为" + waitForConnectingAddress);
                if (BluetoothAdapter.getDefaultAdapter().isEnabled())
                {
                    requestStatus = false;
                    Logger.d(BluetoothAdapter.getDefaultAdapter().getState() + "");
                    BluetoothScanner.scan(1);
//                    try
//                    {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e)
//                    {
//                        e.printStackTrace();
//                    }
                    if (requestCode == BLEService.REQUEST_CONNECT)
                    {
                        if (myDManager.mData.containsKey(waitForConnectingAddress))
                        {
                            myDManager.mData.get(waitForConnectingAddress).connect();
                        }
                    }
                    else if (requestCode == BLEService.REQUEST_SCAN)
                    {
                        BluetoothScanner.scan(1);
                    }
                    return;
                }
                else
                {
                    requestStatus = false;
                    try
                    {
                        i++;
                        Thread.sleep(1000);
                    } catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
            }

            if (requestCode == BLEService.REQUEST_CONNECT)
            {
                if (myDManager.mData.containsKey(waitForConnectingAddress))
                {
                    BaseDevice baseDevice = myDManager.mData.get(waitForConnectingAddress);
                    if (baseDevice instanceof BaseBluetoothDevice)
                    {
                        BaseBluetoothDevice device = (BaseBluetoothDevice) baseDevice;
                        if (device.getIsconnectd() != BLEService.STATE_CONNECTED)
                        {
                            Logger.d("设备未连接成功，连接超时");
                            device.setIsconnectd(BLEService.STATE_DISCONNECTED);
                            SmartHomeApplication.mBluetoothService.close(device.getAddress());
                            SmartHomeApplication.mBluetoothService.mListener.connectTimeOut(device.getAddress());
                            myDManager.mData.get(waitForConnectingAddress).setIsconnectd(BLEService.STATE_DISCONNECTED);
                        }
                    }
                }
                else
                {
                    Logger.json(JsonUtils.objectToJson(myDManager.mData.keySet()));
                    Logger.d(waitForConnectingAddress);
                }
            }
        }
    }
}
