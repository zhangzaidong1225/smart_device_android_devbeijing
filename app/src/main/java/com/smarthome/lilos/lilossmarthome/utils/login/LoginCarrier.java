package com.smarthome.lilos.lilossmarthome.utils.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * 登录信息载体
 * Created by Joker on 2016/7/19.
 */
public class LoginCarrier implements Parcelable
{
    public static final Parcelable.Creator<LoginCarrier> CREATOR = new Parcelable.Creator<LoginCarrier>()
    {
        @Override
        public LoginCarrier createFromParcel(Parcel source)
        {
            return new LoginCarrier(source);
        }

        @Override
        public LoginCarrier[] newArray(int size)
        {
            return new LoginCarrier[size];
        }
    };
    public String mTargetAction;
    public Bundle mBundle;

    public LoginCarrier(String target, Bundle bundle)
    {
        this.mTargetAction = target;
        this.mBundle = bundle;
    }

    protected LoginCarrier(Parcel in)
    {
        this.mTargetAction = in.readString();
        this.mBundle = in.readBundle();
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(this.mTargetAction);
        dest.writeBundle(this.mBundle);
    }

    public void invoke(Context ctx, int[] flags)
    {
        Intent intent = new Intent(mTargetAction);
        if (mBundle != null)
        {
            intent.putExtras(mBundle);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        if (flags != null && flags.length > 0)
        {
            for (int flag : flags)
            {
                intent.addFlags(flag);
            }
        }

        ctx.startActivity(intent);
    }
}
