package com.smarthome.lilos.lilossmarthome.APConfig;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * 网络交互数据工具类
 * <p>
 * Created by Tristan on 2016/11/23.
 */

public class Downloader extends Thread
{
    public static final int MESSAGE_BIND_STATUS_BACK = 0x120002;
    private static final String TAG = "DOWNLOADER";
    Handler mHandler = null;
    String downloadUrl = null;

    public Downloader(String url, Handler hanndler)
    {
        downloadUrl = url;
        mHandler = hanndler;
    }

    @Override
    public void run()
    {
        String result = addTask(downloadUrl, 5000, null);
        if (result == null)
        {
//                sendOutputMessage(mHandler, mDataType, LDNLD_MESSAGE_DOWNLOAD_FAILED, mOutArg2);
            Log.e(TAG, "Download String File Failed![" + downloadUrl + "]");
        }
        else
        {
            Message msg = new Message();
            msg.what = MESSAGE_BIND_STATUS_BACK;
            msg.obj = result;
            mHandler.sendMessage(msg);

        }
    }


    public String addTask(String link, int timeoutConnection, String encode)
    {
        try
        {
            URL url = new URL(link);
            HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
            httpConnection.setConnectTimeout(timeoutConnection);
            httpConnection.setReadTimeout(timeoutConnection);

            if (HttpURLConnection.HTTP_OK != httpConnection.getResponseCode())
            {
                Log.e(TAG, "downLoadLink---> http not ok");
                return null;
            }

            String askEncode = "UTF-8";
            if (encode != null && encode.length() > 0)
            {
                askEncode = encode;
            }
            StringBuilder Builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream(), askEncode));
            for (String s = reader.readLine(); s != null; s = reader.readLine())
            {
                Builder.append(s);
                s = null;//add by wxl, out of memory
            }
            reader.close();
            httpConnection.disconnect();
            httpConnection = null;
            return Builder.toString();
        } catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG, "Download Link File Error : " + e.getMessage());
        }
        return null;
    }
};