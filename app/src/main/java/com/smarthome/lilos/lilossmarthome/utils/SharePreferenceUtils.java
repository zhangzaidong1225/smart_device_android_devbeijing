package com.smarthome.lilos.lilossmarthome.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceTypeEntity;
import com.zxing.decoding.Intents;

import java.util.List;

/**
 * SharePreference工具类
 * Created by Joker on 2016/7/19.
 */
public class SharePreferenceUtils
{
    private static Context mContext;

    public static void init(Context context)
    {
        mContext = context;
    }

    private static SharedPreferences get()
    {
        return mContext.getSharedPreferences("SmartHomeInfo", Activity.MODE_PRIVATE);
    }

    public static boolean getLoginStatus()
    {
        return SharePreferenceUtils.get().getBoolean("Is_login", false);
    }

    public static void setLoginStatus(boolean status)
    {
        SharePreferenceUtils.get().edit().putBoolean("Is_login", status).apply();
    }

    public static String getMobileNum()
    {
        return SharePreferenceUtils.get().getString("Mobile", "");
    }

    public static void setMobileNum(String mobile)
    {
        SharePreferenceUtils.get().edit().putString("Mobile", mobile).apply();
    }

    public static String getAccessToken()
    {
        return SharePreferenceUtils.get().getString("AccessToken", "");
    }

    public static void setAccessToken(String token)
    {
        SharePreferenceUtils.get().edit().putString("AccessToken", token).apply();
    }

    public static String getRefreshToken()
    {
        return SharePreferenceUtils.get().getString("RefreshToken", "");
    }

    public static void setRefreshToken(String token)
    {
        SharePreferenceUtils.get().edit().putString("RefreshToken", token).apply();
    }

    public static int getOldVersion()
    {
        return SharePreferenceUtils.get().getInt("version", 0);
    }

    public static void setOldVersion(int versionCode)
    {
        SharePreferenceUtils.get().edit().putInt("version", versionCode).apply();
    }

    public static String getADImage()
    {
        return SharePreferenceUtils.get().getString("ad_image", "");
    }

    public static void setADImage(String imagePath)
    {
        SharePreferenceUtils.get().edit().putString("ad_image", imagePath).apply();
    }

    public static String getADUrl()
    {
        return SharePreferenceUtils.get().getString("ad_url", "");
    }

    public static void setADUrl(String url)
    {
        SharePreferenceUtils.get().edit().putString("ad_url", url).apply();
    }

    public static List<DeviceTypeEntity> getTypeList()
    {
        String jsonStr = SharePreferenceUtils.get().getString("dev_type", "");
        java.lang.reflect.Type type = new com.google.gson.reflect.TypeToken<List<DeviceTypeEntity>>()
        {
        }.getType();
        Gson gson = new Gson();
        return gson.fromJson(jsonStr, type);
    }

    public static void setTypeList(List<DeviceTypeEntity> devList)
    {
        SharePreferenceUtils.get().edit().putString("dev_type", JsonUtils.objectToJson(devList)).apply();
    }

    public static void setLatestAddr(String address){

        SharePreferenceUtils.get().edit().putString("pm_addr",address).apply();
    }

    public static void setMqttClient(String client){
        SharePreferenceUtils.get().edit().putString("mqtt_client", client).apply();
    }
    public static String getMqttClient(){
        return SharePreferenceUtils.get().getString("mqtt_client", "");
    }

    public static String getLatestAddr(){

        return SharePreferenceUtils.get().getString("pm_addr", "");
    }

    public static String getString(String key) {
        return SharePreferenceUtils.get().getString(key, "");

    }

    public static void setString(String key, String value) {
        SharePreferenceUtils.get().edit().putString(key, value).apply();
    }

    public static void setAreaIdList(String listType, List<String> locationList){
        Log.d("test",JsonUtils.objectToJson(locationList));

        SharePreferenceUtils.get().edit().putString(listType, JsonUtils.objectToJson(locationList)).apply();
    }

    public static List<String> getAreaIdList(String listType)
    {
        String jsonStr = SharePreferenceUtils.get().getString(listType, "");
        java.lang.reflect.Type type = new com.google.gson.reflect.TypeToken<List<String>>()
        {
        }.getType();
        Gson gson = new Gson();
        return gson.fromJson(jsonStr, type);
    }



    public static void setWeatherData(String weatherData) {
        SharePreferenceUtils.get().edit().putString("weather_data", weatherData).apply();
    }

    public static String getWeatherData() {
        return SharePreferenceUtils.get().getString("weather_data", "");
    }


}
