package com.smarthome.lilos.lilossmarthome.wigdet;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Created by Joker on 2017/1/6.
 */

public class PassowrdWatcher implements TextWatcher
{
    private EditText editText;

    public PassowrdWatcher(EditText editText)
    {
        this.editText = editText;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        String editable = editText.getText().toString();
        String str = stringFilter(editable.toUpperCase());
        if(!editable.equals(str))
        {
            editText.setText(str);
            editText.setSelection(str.length());
        }
    }

    @Override
    public void afterTextChanged(Editable s)
    {

    }

    private String stringFilter(String str) throws PatternSyntaxException
    {
        String regEx = "[\\x00-\\xff]+";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        return m.replaceAll("").trim();
    }
}
