package com.smarthome.lilos.lilossmarthome.network;

import com.orhanobut.logger.Logger;
import com.smarthome.lilos.lilossmarthome.BuildConfig;
import com.smarthome.lilos.lilossmarthome.utils.ConfigUtils;

/**
 * URL管理
 * Created by Kevin on 2016/7/11.
 */
public class URLManager
{
    //MOPS对应API Server
    private final String ONLINE_SERVER_MOPSAPI_URL = "https://mops-xinfeng.lianluo.com:883/api/v2/";
    private final String TEST_SERVER_MOPSAPI_URL = "https://mops-xinfeng-develop.lianluo.com:883/api/v2/";

    //用户中心 Server
    private final String ONLINE_SERVER_USERCENTER_URL = "https://mops-api.lianluo.com/account/v1/";
    private final String TEST_SERVER_USERCENTER_URL = "https://mops-staging.lianluo.com/account/v1/";
    private final String CLIENT_ID = "5c72f5fc-82cc-31a7-bb9e-42de8586e29f";
    private final String TEST_CLIENT_SECRET = "_rXe29BIwLs-4nfvGCXFpCNQuvBYMdJp";
    private static final String ONLINE_CLIENT_SECRET = "wPulIC2cBELEuscIbwCmAmVQWocpG29L";

    //发现模块 URL
    private final String ONLINE_FOUND_URL = "https://mops-xinfeng.lianluo.com:889";
    private final String TEST_FOUND_URL = "https://mops-xinfeng-develop.lianluo.com:889";

    //消息中心 URL
    private final String ONLINE_MSG_URL = " https://mops-xinfeng.lianluo.com:889/#/message";
    private final String TEST_MSG_URL = "https://mops-xinfeng-develop.lianluo.com:889/#/message";

    //用户协议 URL
    private final String ONLINE_PROTOCOL_URL = "https://mops-xinfeng.lianluo.com:883/page/view?view=user-agreement";
    private final String TEST_PROTOCOL_URL = "https://mops-xinfeng-develop.lianluo.com:883/page/view?view=user-agreement";


    public String getProtocolUrl()
    {
        return buildUrl(getServer(TEST_PROTOCOL_URL, ONLINE_PROTOCOL_URL));
    }

    public String getMsgUrl() {
        return buildUrl(getServer(TEST_MSG_URL, ONLINE_MSG_URL));
    }

    public String getFoundUrl() {
        return buildUrl(getServer(TEST_FOUND_URL, ONLINE_FOUND_URL));
    }


    public String getUserCenterClientId() {
        return CLIENT_ID;
    }

    public String getUserCenterClientSecret() {
        switch (ConfigUtils.getServer())
        {
            case ConfigUtils.ONLINE_SERVER:
                return ONLINE_CLIENT_SECRET;

            case ConfigUtils.TEST_SERVER:
                return TEST_CLIENT_SECRET;

            default:
                return TEST_CLIENT_SECRET;
        }
    }

    public String getUserCenterServerUrl() {
        return buildUrl(getServer(TEST_SERVER_USERCENTER_URL, ONLINE_SERVER_USERCENTER_URL));

    }

    public String getMopsApiServerUrl() {
        return buildUrl(getServer(TEST_SERVER_MOPSAPI_URL, ONLINE_SERVER_MOPSAPI_URL));
    }

    private String buildUrl(String serverUrl)
    {
        StringBuilder builder = new StringBuilder();

        builder.append(serverUrl);
//        builder.append("?");
//        builder.append("os" + "=" + "0");

        if (BuildConfig.DEBUG)
        {
            Logger.d("请求的url为： " + builder.toString());
        }
        return builder.toString();
    }

    private String getServer(String testUrl, String onlineUrl)
    {
        StringBuilder builder = new StringBuilder();
        switch (ConfigUtils.getServer())
        {
            case ConfigUtils.ONLINE_SERVER:
                return onlineUrl;

            case ConfigUtils.TEST_SERVER:
                return testUrl;

            default:
                return testUrl;
        }

    }

//    private final String ONLINE_SERVER_URL = "https://mops-smart.lianluo.com:883/page/view?view=product";
//    private final String TEST_SERVER_URL = "https://mops-dev.lianluo.com:883";//https://mops-dev.lianluo.com:883/page/view?view=product
//
//    private final String PROTOCOL_URL = "/page/view?view=user-agreement";//"/web/agreement";
//    private final String MARKET_URL = "/page/view?view=product";//"/web/goods_list";
//
//    //private final String ARTICLE = "http://139.198.9.141:904/community.html";
//    private final String ARTICLE = "http://139.198.9.141:904";
//    private final String NOTIFY = "http://139.198.9.141:904/#/message";
//
//
//
//    public String getMarketUrl()
//    {
//        String url = "";
//        switch (ConfigUtils.getServer())
//        {
//            case ConfigUtils.ONLINE_SERVER:
//                url = ONLINE_SERVER_URL;
//                break;
//            case ConfigUtils.TEST_SERVER:
//                url = TEST_SERVER_URL;
//                break;
//        }
//        return url + MARKET_URL;
//    }
//
//    public String getArticleUrl() {
//        String url = "";
//        switch (ConfigUtils.getServer())
//        {
//            case ConfigUtils.ONLINE_SERVER:
//                url = ARTICLE;
//                break;
//            case ConfigUtils.TEST_SERVER:
//                url = ARTICLE;
//                break;
//        }
//        return url;
//    }
//    public String getNotifyUrl() {
//        String url = "";
//        switch (ConfigUtils.getServer())
//        {
//            case ConfigUtils.ONLINE_SERVER:
//                url = NOTIFY;
//                break;
//            case ConfigUtils.TEST_SERVER:
//                url = NOTIFY;
//                break;
//        }
//        return url;
//    }




}
