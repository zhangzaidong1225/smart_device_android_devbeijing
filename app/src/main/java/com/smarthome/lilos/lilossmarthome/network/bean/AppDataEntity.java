package com.smarthome.lilos.lilossmarthome.network.bean;

/**
 * Created by Joker on 2016/8/30.
 */
public class AppDataEntity extends ResponseEntity
{
    private Data data;

    public Data getData()
    {
        return data;
    }

    public void setData(
            Data data)
    {
        this.data = data;
    }

    public class Data
    {
        private String dataKey;
        private String dataString;

        public String getDataKey()
        {
            return dataKey;
        }

        public void setDataKey(String dataKey)
        {
            this.dataKey = dataKey;
        }

        public String getDataString()
        {
            return dataString;
        }

        public void setDataString(String dataString)
        {
            this.dataString = dataString;
        }
    }
}
