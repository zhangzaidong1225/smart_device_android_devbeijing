package com.smarthome.lilos.lilossmarthome.bluetooth.analysis;

import android.bluetooth.BluetoothGattCharacteristic;

import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.bean.CSC_CrankMeasurement;
import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.bean.CSC_Record;
import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.bean.CSC_WheelMeasurement;

import java.util.HashMap;
import java.util.Map;

/**
 * 自行车速度与里程服务数据解析类
 * Created by Joker on 2016/11/21.
 */

public class CyclingSpeedCadenceUtil
{
    private static final byte WHEEL_REVOLUTIONS_DATA_PRESENT = 0x01; // 1 bit
    private static final byte CRANK_REVOLUTION_DATA_PRESENT = 0x02; // 1 bit

    private static Map<String, CyclingSpeedCadenceListener> listenerMap = new HashMap<>();
    private static Map<String, CSC_Record> recordMap = new HashMap<>();
    private static Map<String, Integer> circumferenceMap = new HashMap<>();

    public static void parseCyclingSpeedCadenceMeasurement(String address,
                                                           BluetoothGattCharacteristic characteristic)
    {
        if (!recordMap.containsKey(address))
        {
            recordMap.put(address, new CSC_Record());
            circumferenceMap.put(address, 20); // TODO: 2016/11/22 后期数值20需要改为某一个传入值，不同车的轮胎大小不同
        }

        int offset = 0;
        int flags = characteristic.getValue()[offset]; // 1 byte
        offset += 1;

        boolean wheelRevPresent = (flags & WHEEL_REVOLUTIONS_DATA_PRESENT) > 0;
        boolean crankRevPreset = (flags & CRANK_REVOLUTION_DATA_PRESENT) > 0;

        if (wheelRevPresent)
        {
            int wheelRevolutions = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, offset);
            offset += 4;

            int lastWheelEventTime = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset); // 1/1024 s
            offset += 2;
            parseWheelMeasurement(address, wheelRevolutions, lastWheelEventTime);
        }

        if (crankRevPreset)
        {
            int crankRevolutions = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
            offset += 2;

            int lastCrankEventTime = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
            parseCrankMeasurement(address, crankRevolutions, lastCrankEventTime);
        }
    }

    private static void parseWheelMeasurement(String address, int wheelRevolutions,
                                              int lastWheelEventTime)
    {
        if (!circumferenceMap.containsKey(address))
        {
            circumferenceMap.put(address, 20);// TODO: 2016/11/22 需要设置默认值
        }

        if (!recordMap.containsKey(address))
        {
            recordMap.put(address, new CSC_Record());
        }

        CSC_Record cscRecord = recordMap.get(address);

        if (cscRecord.getFirstWheelRevolutions() < 0)
        {
            cscRecord.setFirstWheelRevolutions(wheelRevolutions);
        }

        if (cscRecord.getLastWheelEventTime() == lastWheelEventTime)
        {
            return;
        }

        if (cscRecord.getLastWheelRevolutions() >= 0)
        {
            float timeDifference;
            if (lastWheelEventTime < cscRecord.getLastWheelEventTime())
            {
                timeDifference = (65535 + lastWheelEventTime - cscRecord.getLastWheelEventTime()) / 1024.0f;
            }
            else
            {
                timeDifference = (lastWheelEventTime - cscRecord.getLastWheelEventTime()) / 1024.f;
            }

            CSC_WheelMeasurement csc = new CSC_WheelMeasurement();

            float distanceDifference = (wheelRevolutions - cscRecord.getLastWheelRevolutions()) * circumferenceMap.get(address) / 1000.0f;
            csc.totlaDistance = (float) wheelRevolutions * (float) circumferenceMap.get(address) / 1000.0f;
            csc.distance = (float) (wheelRevolutions - cscRecord.getFirstWheelRevolutions()) * (float) circumferenceMap.get(address) / 1000.0f;
            csc.speed = distanceDifference / timeDifference;

            cscRecord.setWheelCadence((wheelRevolutions - cscRecord.getLastWheelRevolutions()) * 60.f / timeDifference);

            if (listenerMap.containsKey(address))
            {
                listenerMap.get(address).onCSCWheelMeasurementChanged(csc);
            }
        }

        cscRecord.setLastWheelRevolutions(wheelRevolutions);
        cscRecord.setLastWheelEventTime(lastWheelEventTime);
    }

    private static void parseCrankMeasurement(String address, int crankRevolutions,
                                              int lastCrankEventTime)
    {
        if (!circumferenceMap.containsKey(address))
        {
            circumferenceMap.put(address, 20);// TODO: 2016/11/22 需要设置默认值
        }

        if (!recordMap.containsKey(address))
        {
            recordMap.put(address, new CSC_Record());
        }

        CSC_Record cscRecord = recordMap.get(address);

        if (cscRecord.getLastCrankRevolutions() >= 0)
        {
            float timeDifference;
            if (lastCrankEventTime < cscRecord.getLastCrankEventTime())
            {
                timeDifference = (65535 + lastCrankEventTime - cscRecord.getLastCrankEventTime()) / 1024.0f;
            }
            else
            {
                timeDifference = (lastCrankEventTime - cscRecord.getLastCrankEventTime()) / 1024.f;
            }

            CSC_CrankMeasurement csc = new CSC_CrankMeasurement();

            float crankCadence = (crankRevolutions - cscRecord.getLastCrankRevolutions()) * 60.f / timeDifference;
            if (crankCadence > 0)
            {
                csc.gearRatio = cscRecord.getWheelCadence() / crankCadence;

                if (listenerMap.containsKey(address))
                {
                    listenerMap.get(address).onCSCCrankMeasurementChanged(csc);
                }
            }
        }

        cscRecord.setLastCrankRevolutions(crankRevolutions);
        cscRecord.setLastCrankEventTime(lastCrankEventTime);
    }

    public static void registered(CyclingSpeedCadenceListener cscListener, String address)
    {
        if (listenerMap.containsKey(address))
        {
            listenerMap.remove(address);
        }
        listenerMap.put(address, cscListener);
    }

    public static void unregistered(String address)
    {
        if (listenerMap.containsKey(address))
        {
            listenerMap.remove(address);
        }
    }

    public static void setCircumference(int circumference, String address)
    {
        if (circumferenceMap.containsKey(address))
        {
            circumferenceMap.remove(address);
        }
        circumferenceMap.put(address, circumference);
    }

    public static int getCircumference(String address)
    {
        if (circumferenceMap.containsKey(address))
        {
            return circumferenceMap.get(address);
        }
        else
        {
            return -1;
        }
    }

    public interface CyclingSpeedCadenceListener
    {
        void onCSCWheelMeasurementChanged(CSC_WheelMeasurement cscm);

        void onCSCCrankMeasurementChanged(CSC_CrankMeasurement cscc);
    }
}
