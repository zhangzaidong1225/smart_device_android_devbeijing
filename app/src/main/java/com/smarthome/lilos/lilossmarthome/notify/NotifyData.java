package com.smarthome.lilos.lilossmarthome.notify;

import org.json.JSONObject;

/**
 * Created by songyu on 2017/4/27.
 */
public class NotifyData {
    String titile;
    String description;
    int notification_builder_id;
    int notification_basic_style;
    int open_type;
    String url;
    String pkg_content;

    String customContent_titile;
    int customContent_is_notify;
    String customContent_type;
    String customContent_action;
    String customContent_content;
    String customContent_params;

    boolean isParseMqttMessageError = false;

    public String getTitile() {
        return titile;
    }

    public void setTitile(String titile) {
        this.titile = titile;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNotification_builder_id() {
        return notification_builder_id;
    }

    public void setNotification_builder_id(int notification_builder_id) {
        this.notification_builder_id = notification_builder_id;
    }

    public int getNotification_basic_style() {
        return notification_basic_style;
    }

    public void setNotification_basic_style(int notification_basic_style) {
        this.notification_basic_style = notification_basic_style;
    }

    public int getOpen_type() {
        return open_type;
    }

    public void setOpen_type(int open_type) {
        this.open_type = open_type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPkg_content() {
        return pkg_content;
    }

    public void setPkg_content(String pkg_content) {
        this.pkg_content = pkg_content;
    }

    public String getCustomContent_titile() {
        return customContent_titile;
    }

    public void setCustomContent_titile(String customContent_titile) {
        this.customContent_titile = customContent_titile;
    }

    public int getCustomContent_is_notify() {
        return customContent_is_notify;
    }

    public void setCustomContent_is_notify(int customContent_is_notify) {
        this.customContent_is_notify = customContent_is_notify;
    }

    public String getCustomContent_type() {
        return customContent_type;
    }

    public void setCustomContent_type(String customContent_type) {
        this.customContent_type = customContent_type;
    }

    public String getCustomContent_action() {
        return customContent_action;
    }

    public void setCustomContent_action(String customContent_action) {
        this.customContent_action = customContent_action;
    }

    public String getCustomContent_content() {
        return customContent_content;
    }

    public void setCustomContent_content(String customContent_content) {
        this.customContent_content = customContent_content;
    }

    public String getCustomContent_params() {
        return customContent_params;
    }

    public void setCustomContent_params(String customContent_params) {
        this.customContent_params = customContent_params;
    }

    public boolean isParseMqttMessageError() {
        return isParseMqttMessageError;
    }

    public void setParseMqttMessageError(boolean parseMqttMessageError) {
        isParseMqttMessageError = parseMqttMessageError;
    }

    @Override
    public String toString() {
        return "NotifyData{" +
                "titile='" + titile + '\'' +
                ", description='" + description + '\'' +
                ", notification_builder_id=" + notification_builder_id +
                ", notification_basic_style=" + notification_basic_style +
                ", open_type=" + open_type +
                ", url='" + url + '\'' +
                ", pkg_content='" + pkg_content + '\'' +
                ", customContent_titile='" + customContent_titile + '\'' +
                ", customContent_is_notify=" + customContent_is_notify +
                ", customContent_type='" + customContent_type + '\'' +
                ", customContent_action='" + customContent_action + '\'' +
                ", customContent_content='" + customContent_content + '\'' +
                ", customContent_params='" + customContent_params + '\'' +
                '}';
    }
}
