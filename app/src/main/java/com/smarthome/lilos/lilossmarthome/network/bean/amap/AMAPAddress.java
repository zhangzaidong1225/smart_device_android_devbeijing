/**
 * Copyright 2016 aTool.org
 */
package com.smarthome.lilos.lilossmarthome.network.bean.amap;

/**
 * Auto-generated: 2016-08-29 10:38:47
 *
 * @author aTool.org (i@aTool.org)
 * @website http://www.atool.org/json2javabean.php
 */
public class AMAPAddress
{

    private String status;
    private String info;
    private String infocode;
    private Regeocode regeocode;

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getInfo()
    {
        return info;
    }

    public void setInfo(String info)
    {
        this.info = info;
    }

    public String getInfocode()
    {
        return infocode;
    }

    public void setInfocode(String infocode)
    {
        this.infocode = infocode;
    }

    public Regeocode getRegeocode()
    {
        return regeocode;
    }

    public void setRegeocode(Regeocode regeocode)
    {
        this.regeocode = regeocode;
    }

}