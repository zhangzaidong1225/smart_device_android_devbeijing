package com.smarthome.lilos.lilossmarthome.network.bean;

/**
 * Created by Kevin on 2016/7/12.
 */
public class FeedbackBody
{
    private int code;
    private int type;
    private String contact;
    private String content;

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }

    public String getAccount()
    {
        return contact;
    }

    public void setAccount(String contact)
    {
        this.contact = contact;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }
}
