/**
 * Copyright 2016 aTool.org
 */
package com.smarthome.lilos.lilossmarthome.network.bean.amap;

import java.util.List;

/**
 * Auto-generated: 2016-08-29 10:38:47
 *
 * @author aTool.org (i@aTool.org)
 * @website http://www.atool.org/json2javabean.php
 */
public class Neighborhood
{

    private List<String> name;
    private List<String> type;

    public List<String> getName()
    {
        return name;
    }

    public void setName(List<String> name)
    {
        this.name = name;
    }

    public List<String> getType()
    {
        return type;
    }

    public void setType(List<String> type)
    {
        this.type = type;
    }

}