package com.smarthome.lilos.lilossmarthome.activity.tool;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.fragment.TabMarketFragment;
import com.smarthome.lilos.lilossmarthome.jsscope.BleJsScope;
import com.smarthome.lilos.lilossmarthome.jsscope.util.InjectedChromeClient;
import com.smarthome.lilos.lilossmarthome.network.URLManager;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.tencent.smtt.export.external.interfaces.WebResourceError;
import com.tencent.smtt.export.external.interfaces.WebResourceRequest;
import com.tencent.smtt.sdk.CookieManager;
import com.tencent.smtt.sdk.CookieSyncManager;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

/**
 * Created by Administrator on 2017/4/26.
 * 文章／发现
 */

public class WebArticleActivity extends BaseActivity {

    private WebView webView;
    private String realUrl = null;//new URLManager().getFoundUrl();
    private boolean isSetToken = false;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        Log.d("MainActivity", "MainActivity onKey");
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            if (webView != null && webView.canGoBack())
            {
                webView.goBack();
                return true;
            } else {
                finish();
                return true;
            }
        }
        return false;
    }
    View.OnKeyListener listener = new View.OnKeyListener()
    {

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event)
        {
            if (event.getAction() == KeyEvent.ACTION_DOWN)
            {
                if (keyCode == KeyEvent.KEYCODE_BACK)
                {  //表示按返回键 时的操作
                    if (webView.canGoBack())
                    {
                        webView.goBack();
                        return true;
                    }
                    else
                    {
                        finish();
                        return true;
                    }
                }
            }
            return false; //已处理
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_web);
        realUrl = getIntent().getStringExtra("url");
        Log.d("WebArticleActivity", "url:" + realUrl);
        webView = (WebView) findViewById(R.id.article_webview);

        webView.getSettings().setTextZoom(100);//屏蔽系统字体
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webView.getSettings().setDomStorageEnabled(true);
        } else {
            webView.getSettings().setDomStorageEnabled(true);
            webView.getSettings().setAppCacheMaxSize(1024*1024*100);
            String appCachePath = SmartHomeApplication.mContext.getCacheDir().getAbsolutePath();
            webView.getSettings().setAppCachePath(appCachePath);
            webView.getSettings().setAllowFileAccess(true);
            webView.getSettings().setAppCacheEnabled(true);
        }
        webView.getSettings().setJavaScriptEnabled(true);

        webView.loadUrl(realUrl);

        webView.setWebViewClient(new ArticleWebViewClient());
//        webView.setOnKeyListener(listener);

        webView.setWebChromeClient(new InjectedChromeClient("android_obj", BleJsScope.class));
//        webView.setOnKeyListener(listener);

        RxBus.with(this)
                .setEvent(Events.LOGIN_SUCCESS)
                .onNext(events -> {
                    setTokenToLocalStorage(webView);
//                    getTokenFromLocalStorage(webView);

                })
                .create();

        RxBus.with(this)
                .setEvent(Events.LOG_OUT)
                .onNext(events -> {
                    setTokenToLocalStorage(webView);
//                    getTokenFromLocalStorage(webView);

                })
                .create();
    }

    public void syncCookie(Context context,String url){
        String token = SharePreferenceUtils.getAccessToken();
        String cookie = "authorization=Bearer " + token;

        CookieSyncManager.createInstance(context);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);

        cookieManager.setCookie(url, cookie);
        CookieSyncManager.getInstance().sync();
    }
    private class ArticleWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
//            syncCookie(webView.getContext(),url);
            if(url.startsWith(realUrl))
            {
                webView.loadUrl(url);
                return false;
            }
            else
            {
                if(url.startsWith("file:///android_asset/undefined")){
                    return false;
                }
//
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                return true;
            }
        }

        @Override
        public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
            super.onReceivedError(webView, webResourceRequest, webResourceError);
            webView.loadUrl("file:///android_asset/nowifi.html");
        }

        @Override
        public void onPageFinished(WebView webView, String s) {
            webView.loadUrl("javascript:setRelaodUrl('" + realUrl + "')");
            if (!isSetToken) {
                isSetToken = true;
                setTokenToLocalStorage(webView);
            }
//            getTokenFromLocalStorage(webView);
            super.onPageFinished(webView, s);

        }
    }

    private void setTokenToLocalStorage(WebView webview) {

        String token = SharePreferenceUtils.getAccessToken();
        if (token == null) {
            return;
        }
        Log.d("TabMarketFragment", "TabMarketFragment settokenToLocalStorage:" + token);
        //1.拼接 JavaScript 代码
        String js = "window.localStorage.setItem('authorization','Bearer " + token + "');";
        String jsUrl = "javascript:(function({ var localStorage = window.localStorage; localStorage.setItem('authorization','Bearer " + token + "')})()";

        if (webview == null) {
            return;
        }
        //2.根据不同版本，使用不同的 API 执行 Js
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        webview.evaluateJavascript(js, null);

//        } else {
//            webview.loadUrl(jsUrl);
//            webview.reload();
//        }
    }

    private void getTokenFromLocalStorage(WebView webView) {
        if (webView != null ) {
            webView.loadUrl(
                    "javascript:(function(){ var localStorage = window.localStorage; var authorization =  localStorage.getItem('authorization'); console.log('authorization = ' + authorization);})()");
        }
    }
}
