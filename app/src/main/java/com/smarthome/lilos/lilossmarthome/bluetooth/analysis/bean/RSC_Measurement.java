package com.smarthome.lilos.lilossmarthome.bluetooth.analysis.bean;

/**
 * Created by Joker on 2016/11/23.
 */

public class RSC_Measurement
{
    public static final int NOT_AVAILABLE = -1;
    public static final int ACTIVITY_WALKING = 0;
    public static final int ACTIVITY_RUNNING = 1;

    public float speed;
    public int cadence;
    public float distance;
    public float stridenLen;
    public int activity;
}
