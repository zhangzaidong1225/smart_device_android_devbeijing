package com.smarthome.lilos.lilossmarthome.activity.splash;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.android.slip.SwipeViewPager;
import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.activity.MainActivity;
import com.smarthome.lilos.lilossmarthome.fragment.CustomViewPageAdapter;
import com.smarthome.lilos.lilossmarthome.jsscope.BleJsScope;
import com.smarthome.lilos.lilossmarthome.jsscope.util.InjectedChromeClient;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.SystemUtils;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

/**
 * 引导页面
 * 用于首次启动APP或者更新APP后,展示新的内容
 */
public class GuidanceActivity extends BaseActivity
{
//    private SwipeViewPager mSvpGuidance;
//    private CustomViewPageAdapter mCustomViewPageAdapter;
    private Button btnStart;
//    private int mPosition = -1;
//    private boolean isInLastPage = false;
//    private float xStart = 0;
//    private List<Integer> guidanceImageList;


    private WebView mGuidanceWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_guidance);

        initViews();
        getData();
    }

    private void initViews()
    {
//        mSvpGuidance = (SwipeViewPager) findViewById(R.id.svp_guidance);
        btnStart = (Button) findViewById(R.id.btn_start);
        btnStart.setVisibility(View.GONE);
        btnStart.setOnClickListener(v -> gotoMainActivity());

        mGuidanceWebView = (WebView) findViewById(R.id.guidance_webview);
    }

    private void getData()
    {
        mGuidanceWebView.getSettings().setJavaScriptEnabled(true);
//        mGuidanceWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);

        mGuidanceWebView.getSettings().setUseWideViewPort(true);
        mGuidanceWebView.getSettings().setLoadWithOverviewMode(true);

        mGuidanceWebView.getSettings().setDisplayZoomControls(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            //透明状态栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//            //透明导航栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

            mGuidanceWebView.loadUrl("file:///android_asset/guide_normal/guide_normal.html");
        } else {
            mGuidanceWebView.loadUrl("file:///android_asset/guide_no_normal/guide_no_normal.html");
        }
        mGuidanceWebView.setVisibility(View.VISIBLE);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                gotoMainActivity();
////                btnStart.setVisibility(View.VISIBLE);
//            }
//        }, 19500);
        mGuidanceWebView.setWebChromeClient(new InjectedChromeClient("android_obj", BleJsScope.class));

//        guidanceImageList = new ArrayList<>();
//        guidanceImageList.add(R.drawable.guidance_1);
//        guidanceImageList.add(R.drawable.guidance_2);
//        guidanceImageList.add(R.drawable.guidance_3);
//
//        mCustomViewPageAdapter = new CustomViewPageAdapter(getBaseContext(), guidanceImageList);
//        mSvpGuidance.updateIndicatorView(guidanceImageList.size(), (size, position) -> mPosition = position);
//        mSvpGuidance.setAdapter(mCustomViewPageAdapter);
    }

    private void gotoMainActivity()
    {
        MobclickAgent.onEvent(this, "GuidanceActivity_gotoMainActivity");
        SharePreferenceUtils.setOldVersion(SystemUtils.getVersionCode(getBaseContext()));
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev)
    {
//        switch (ev.getAction())
//        {
//            case MotionEvent.ACTION_DOWN:
//                if (mPosition == (guidanceImageList.size() - 1))
//                {
//                    isInLastPage = true;
//                    xStart = ev.getX();
//                }
//                else
//                {
//                    isInLastPage = false;
//                    xStart = 0;
//                }
//                break;
//            case MotionEvent.ACTION_UP:
//                if (isInLastPage)
//                {
//                    float xEnd = ev.getX();
//                    if (xStart - xEnd > SystemUtils.dip2px(getBaseContext(), 80))
//                    {
//                        gotoMainActivity();
//                    }
//                }
//                break;
//        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
