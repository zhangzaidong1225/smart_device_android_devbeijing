package com.smarthome.lilos.lilossmarthome.network.bean;

/**
 * Created by Kevin on 2016/7/20.
 */
public class WeatherEntity extends ResponseEntity
{
    private WeatherData data;

    public WeatherData getData()
    {
        return data;
    }

    public void setData(WeatherData data)
    {
        this.data = data;
    }

    public class WeatherData
    {

        private String city_name;
        private String text;
        private String temperature;
        private String wind_direction;
        private String wind_scale;
        private String pm25;

        public String getCity_name()
        {
            return city_name;
        }

        public void setCity_name(String city_name)
        {
            this.city_name = city_name;
        }

        public String getText()
        {
            return text;
        }

        public void setText(String text)
        {
            this.text = text;
        }

        public String getTemperature()
        {
            return temperature;
        }

        public void setTemperature(String temperature)
        {
            this.temperature = temperature;
        }

        public String getWind_direction()
        {
            return wind_direction;
        }

        public void setWind_direction(String wind_direction)
        {
            this.wind_direction = wind_direction;
        }

        public String getWind_scale()
        {
            return wind_scale;
        }

        public void setWind_scale(String wind_scale)
        {
            this.wind_scale = wind_scale;
        }

        public String getPm25()
        {
            return pm25;
        }

        public void setPm25(String pm25)
        {
            this.pm25 = pm25;
        }

    }
}
