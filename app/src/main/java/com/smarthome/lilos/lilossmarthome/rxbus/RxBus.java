package com.smarthome.lilos.lilossmarthome.rxbus;

import android.util.Log;

import com.trello.rxlifecycle.LifecycleProvider;
import com.trello.rxlifecycle.android.ActivityEvent;
import com.trello.rxlifecycle.android.FragmentEvent;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;
import com.trello.rxlifecycle.components.support.RxFragment;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

/**
 * RxBus工具类
 * 用于代替系统广播
 * Created by Joker on 2017/1/11.
 */

public class RxBus
{
    private static RxBus rxBus;
    private final Subject<Events<?>, Events<?>> _bus = new SerializedSubject<>(PublishSubject.<Events<?>>create());

    private RxBus()
    {
    }

    public static RxBus getInstance()
    {
        if (rxBus == null)
        {
            synchronized (RxBus.class)
            {
                if (rxBus == null)
                {
                    rxBus = new RxBus();
                }
            }
        }
        return rxBus;
    }

    public void send(Events<?> o)
    {
        _bus.onNext(o);
    }

    public void send(@Events.EventCode int code)
    {
        Events<Object> events = new Events<>();
        events.code = code;
        send(events);
    }

    public void send(@Events.EventCode int code, Object content)
    {
//        Log.e("**send","code: "+code);
        Events<Object> events = new Events<>();
        events.code = code;
        events.content = content;
        send(events);
    }

    public Observable<Events<?>> toObservable()
    {
        return _bus;
    }

    public static SubscriberBuilder with(LifecycleProvider<?> provider)
    {
        return new SubscriberBuilder(provider);
    }

    public static class SubscriberBuilder
    {
        private LifecycleProvider<ActivityEvent> mActivityProvider;
        private LifecycleProvider<FragmentEvent> mFragmentProvider;
        private FragmentEvent mFragmentEndEvent;
        private ActivityEvent mActivityEndEvent;
        private int event;
        private Action1<? super Events<?>> onNext;
        private Action1<Throwable> onError;

        public SubscriberBuilder(LifecycleProvider<?> provider)
        {
            if (provider instanceof RxAppCompatActivity)
            {
                mActivityProvider = (LifecycleProvider<ActivityEvent>) provider;
            }
            else if (provider instanceof RxFragment)
            {
                mFragmentProvider = (LifecycleProvider<FragmentEvent>) provider;
            }
        }

        public SubscriberBuilder setEvent(@Events.EventCode int event)
        {
            this.event = event;
            return this;
        }

        public SubscriberBuilder onNext(Action1<? super Events<?>> action)
        {
            this.onNext = action;
            return this;
        }

        public SubscriberBuilder onError(Action1<Throwable> action)
        {
            Log.e("**onError"," :"+action);
            this.onError = action;
            return this;
        }

        public void create()
        {
            mActivityEndEvent = ActivityEvent.DESTROY;
            mFragmentEndEvent = FragmentEvent.DESTROY;
            _create();
        }

        public void createBindLC()
        {
            mActivityEndEvent = null;
            mFragmentEndEvent = null;
            _create();
        }

        public void create(ActivityEvent event)
        {
            mActivityEndEvent = event;
            _create();
        }

        public void create(FragmentEvent event)
        {
            mFragmentEndEvent = event;
            _create();
        }

        private Subscription _create()
        {
            if (mActivityProvider != null)
            {
                return RxBus.getInstance().toObservable()
                        .compose(mActivityEndEvent == null ? mActivityProvider.bindToLifecycle() : mActivityProvider.bindUntilEvent(mActivityEndEvent))
                        .filter(events -> events.code == event)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe(onNext, onError == null ? throwable -> throwable.printStackTrace() : onError);
            }
            if (mFragmentProvider != null)
            {
                return RxBus.getInstance().toObservable()
                        .compose(mFragmentEndEvent == null ? mFragmentProvider.bindToLifecycle() : mFragmentProvider.bindUntilEvent(mFragmentEndEvent))
                        .filter(events -> events.code == event)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe(onNext, onError == null ? throwable -> throwable.printStackTrace() : onError);
            }
            return null;
        }
    }
}
