package com.smarthome.lilos.lilossmarthome.bluetooth.analysis.bean;

import java.util.Calendar;

/**
 * Created by Joker on 2016/11/21.
 */

public class WS_Measurement
{
    public int units;
    public float weight;
    public int userId;
    public Calendar calendar;
    public int bmi;
    public float height;
}
