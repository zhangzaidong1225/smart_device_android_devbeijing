package com.smarthome.lilos.lilossmarthome.wigdet;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import com.smarthome.lilos.lilossmarthome.R;

/**
 * 圆角进度条
 * Created by Joker on 2016/2/2.
 */
public class RoundProgressBar extends View
{
    public static final int STROKE = 0;
    public static final int FILL = 1;
    public static final int POSITIVE = 2;
    public static final int DESCEND = 3;
    private Paint paint;
    private int roundColor;
    private int roundProgressColor;
    private int textColor;
    private float textSize;
    private float roundWidth;
    private int max;
    private float progress;
    private float realProgress;
    private boolean textIsDisplayable;
    private int style;
    private int order;

    public RoundProgressBar(Context context)
    {
        this(context, null);
    }

    public RoundProgressBar(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public RoundProgressBar(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);

        paint = new Paint();

        TypedArray mTypedArray = context.obtainStyledAttributes(attrs, R.styleable.RoundProgressBar);
        //获取自定义属性和默认值
        roundColor = mTypedArray.getColor(R.styleable.RoundProgressBar_roundColor, Color.RED);
        roundProgressColor = mTypedArray.getColor(R.styleable.RoundProgressBar_roundProgressColor, Color.GREEN);
        textColor = mTypedArray.getColor(R.styleable.RoundProgressBar_textColorR, Color.GREEN);
        textSize = mTypedArray.getDimension(R.styleable.RoundProgressBar_textSizeR, 15);
        roundWidth = mTypedArray.getDimension(R.styleable.RoundProgressBar_roundWidth, 5);
        max = mTypedArray.getInteger(R.styleable.RoundProgressBar_maxValue, 100);
        textIsDisplayable = mTypedArray.getBoolean(R.styleable.RoundProgressBar_textIsDisplayable, true);

        mTypedArray.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        int centerX, centerY;
        int radius;

        centerX = getWidth() / 2;
        centerY = getHeight() / 2;

        if (getWidth() > getHeight())
        {
            radius = (int) (centerY - roundWidth / 2);
        }
        else
        {
            radius = (int) (centerX - roundWidth / 2);
        }

        paint.setColor(roundColor); //设置圆环的颜色
        paint.setStyle(Paint.Style.STROKE); //设置空心
        paint.setStrokeWidth(roundWidth); //设置圆环的宽度
        paint.setAntiAlias(true);  //消除锯齿
        canvas.drawCircle(centerX, centerY, radius, paint); //画出圆环

        paint.setStrokeWidth(0);
        paint.setColor(textColor);
        paint.setTextSize(textSize);
        paint.setTypeface(Typeface.DEFAULT_BOLD); //设置字体
        float textWidth = paint.measureText((int) Math.ceil(progress) + "%");   //测量字体宽度，我们需要根据字体的宽度设置在圆环中间

        if (textIsDisplayable)
        {
            if (progress != 0)
            {
                canvas.drawText((int) Math.ceil(progress) + "%", centerX - textWidth / 2, centerY + textSize / 2, paint); //画出进度百分比
            }
        }

        paint.setStrokeWidth(roundWidth); //设置圆环的宽度
        paint.setColor(roundProgressColor);  //设置进度的颜色
        RectF oval = new RectF(centerX - radius, centerY - radius, centerX + radius, centerY + radius);  //用于定义的圆弧的形状和大小的界限

        paint.setStyle(Paint.Style.STROKE);
        canvas.drawArc(oval, -90, 360 * progress / max, false, paint);  //根据进度画圆弧
    }

    public synchronized int getMax()
    {
        return max;
    }

    public synchronized void setMax(int max)
    {
        if (max < 0)
        {
            throw new IllegalArgumentException("max not less than 0");
        }
        this.max = max;
    }

    public synchronized float getProgress()
    {
        return progress;
    }

    public synchronized void setProgress(float progress)
    {
        if (progress < 0)
        {
            throw new IllegalArgumentException("progress not less than 0");
        }
        if (progress > max)
        {
            progress = max;
        }
        if (progress <= max)
        {
            this.progress = progress;
            postInvalidate();
        }
    }

    public int getCircleColor()
    {
        return roundColor;
    }

    public void setCircleColor(int cricleColor)
    {
        this.roundColor = cricleColor;
    }

    public int getCircleProgressColor()
    {
        return roundProgressColor;
    }

    public void setCircleProgressColor(int cricleProgressColor)
    {
        this.roundProgressColor = cricleProgressColor;
    }

    public int getTextColor()
    {
        return textColor;
    }

    public void setTextColor(int textColor)
    {
        this.textColor = textColor;
    }

    public float getTextSize()
    {
        return textSize;
    }

    public void setTextSize(float textSize)
    {
        this.textSize = textSize;
    }

    public float getRoundWidth()
    {
        return roundWidth;
    }

    public void setRoundWidth(float roundWidth)
    {
        this.roundWidth = roundWidth;
    }

    public int getStyle()
    {
        return style;
    }

    public void setStyle(int style)
    {
        this.style = style;
    }

    public int getOrder()
    {
        return order;
    }

    public void setOrder(int order)
    {
        this.order = order;
    }
}
