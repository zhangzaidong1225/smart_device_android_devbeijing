package com.smarthome.lilos.lilossmarthome.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.EventLog;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.logger.Logger;
import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.activity.MainActivity;
import com.smarthome.lilos.lilossmarthome.activity.device.DeviceScanActivity;
import com.smarthome.lilos.lilossmarthome.activity.location.LocationManagerActivity;
import com.smarthome.lilos.lilossmarthome.activity.user.LoginActivity;
import com.smarthome.lilos.lilossmarthome.activity.weather.WeatherDetailActivity;
import com.smarthome.lilos.lilossmarthome.adapter.LeDeviceListAdapter;
import com.smarthome.lilos.lilossmarthome.bluetooth.BLEService;
import com.smarthome.lilos.lilossmarthome.bluetooth.BluetoothScanner;
import com.smarthome.lilos.lilossmarthome.device.BaseBluetoothDevice;
import com.smarthome.lilos.lilossmarthome.device.BaseDevice;
import com.smarthome.lilos.lilossmarthome.device.DeviceFactory;
import com.smarthome.lilos.lilossmarthome.device.DeviceManager;
import com.smarthome.lilos.lilossmarthome.greendao.GreenDaoHelper;
import com.smarthome.lilos.lilossmarthome.greendao.entity.Area;
import com.smarthome.lilos.lilossmarthome.greendao.gen.AreaDao;
import com.smarthome.lilos.lilossmarthome.greendao.utils.GreenDaoUtils;
import com.smarthome.lilos.lilossmarthome.jsscope.holder.LocationHolder;
import com.smarthome.lilos.lilossmarthome.network.bean.FirstPageEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.amap.AMAPAddress;
import com.smarthome.lilos.lilossmarthome.network.bean.amap.AMAPLocation;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.UpdateDeviceEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.UserDeviceEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.pm25.PMDayData;
import com.smarthome.lilos.lilossmarthome.network.bean.pm25.PMHoursData;
import com.smarthome.lilos.lilossmarthome.network.bean.pm25.PmWeatherData;
import com.smarthome.lilos.lilossmarthome.network.bean.pm25.PmWeatherTimeData;
import com.smarthome.lilos.lilossmarthome.network.bean.pm25.WeatherData;
import com.smarthome.lilos.lilossmarthome.network.retrofit.BaseSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.WaitingSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.AMAPMethods;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.FirstPageMethods;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.MopsV2Methods;
import com.smarthome.lilos.lilossmarthome.rxbus.EventKey;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.utils.ConvertUtils;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.control.ControlManager;
import com.smarthome.lilos.lilossmarthome.utils.control.OfflinePackageManage;
import com.smarthome.lilos.lilossmarthome.utils.login.LoginInterceptor;
import com.smarthome.lilos.lilossmarthome.wigdet.DialogConfirmListener;
import com.smarthome.lilos.lilossmarthome.wigdet.LongPressDialog;
import com.trello.rxlifecycle.components.support.RxFragment;
import com.umeng.analytics.MobclickAgent;
import com.wevey.selector.dialog.MDAlertDialog;
import com.zxing.decoding.Intents;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;

import static com.smarthome.lilos.lilossmarthome.activity.location.LocationManagerActivity.SP_SELECTED_AREA_ID;

/**
 * 设备列表Fragment
 * 用于展示当前账户绑定的设备列表
 */
public class TabHomeFragment extends RxFragment implements LongPressDialog.IOnClickListener
{
    private static final int MSG_CONNECT_TIME_OUT = 100;
    DeviceManager myDeviceManager = null;
    private LinearLayout mOfficialDelection;
    private ListView mLvDevices;
    private TextView mTvPrompt;
    private ImageView mBtnAddDevice;
    private LeDeviceListAdapter mLeDeviceAdapter;
    private LongPressDialog mLongPressDialog;
    private SwipeRefreshLayout mSrlDeviceList;
    private boolean needSynDevice = true;
    private String mDeviceName;
    private String mDeviceAddress;
    private BaseDevice mDevice,reName,firstDevice,lastDevice,mDeleteDevice;
    private Map<String, UserDeviceEntity> newMap = new HashMap<>();//用户设备map

    private ImageView mIconUser,mIconDelete,mDevice_delete;
    //private CheckBox mDelCheckbox;
    private TextView mUsername,mPm25,mAirQuality,mOutdoor,mLocation,single_pm25;
    private View single_delection,location;
    private View mTransparence,mSkyBackground;
    public static boolean editMode = false;
    public static boolean isDelete = false;
    public static boolean hasSingle = false;
    public static int num = 0;
    public static int pm25 = 0;
    public static int width,height,imageHeight,imageWidth;
    public static boolean isNetgetLocation = true;
    MDAlertDialog  dialog4;
    Timer timer;
    private Subscriber<FirstPageEntity> subscriber;


    private String areaID;//地理位置
    private TextView tv_home_temp, tv_home_rain;


    private AreaDao mLocationAreaDao = null;
    private static int LOCATION_STEP_DEFAULT = 0xF0;
    private static int LOCATION_STEP_ING = 0xF1;
//    private static int LOCATION_STEP_IP = 0xF2;
    private static int LOCATION_STEP_COMPLETED = 0xF3;
    private int locationStep = LOCATION_STEP_DEFAULT;


    private DialogConfirmListener mDialogConfirmListener = new DialogConfirmListener()
    {
        @Override
        public void postMessage(String message)
        {
            if (message == null || message.trim().equals(""))
            {
                AppToast.showToast(R.string.dev_name_cannot_empty);
            }
            else
            {
                mDeviceName = message;
                renameDevice(reName.getAddress(), mDeviceName);
            }
        }

        @Override
        public void postDeviceInfo(BaseDevice device)
        {
            mDevice = device;
        }
    };
    private android.os.Handler mHanlder = new android.os.Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case MSG_CONNECT_TIME_OUT:
                    final String addr = msg.getData().getString(BLEService.EXTRA_DEVICE_ADDR);
                    updateConnectStatus(addr);
                    break;
                default:
                    break;
            }
        }
    };

    private ConnectTimeOutListener mConnectTimeOutListener = address -> {
        Message msg = mHanlder.obtainMessage(MSG_CONNECT_TIME_OUT);
        final Bundle bundle = new Bundle();
        Log.e("zidong  :","  "+address);
        bundle.putString(BLEService.EXTRA_DEVICE_ADDR,address);
        msg.setData(bundle);
        mHanlder.sendMessage(msg);
    };

    private BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(intent.getAction()))
            {
                int btState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.STATE_OFF);
                printBTState(btState);
            }
        }
    };

    private void updateWifiOnlineStatus(String deviceID, boolean onlienstatus)
    {
        BaseDevice mDevice = myDeviceManager.getDevice(deviceID);
        if ((mDevice != null) && mDevice.getTypeId() == 10)
        {
            if (onlienstatus == true)
            {
                mDevice.setIsconnectd(BLEService.STATE_CONNECTED);
            }
            else
            {
                mDevice.setIsconnectd(BLEService.STATE_DISCONNECTED);
            }
            myDeviceManager.updateData(deviceID, mDevice);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d("===","onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        Log.d("===","onCreate");
        super.onCreate(savedInstanceState);
        if (GreenDaoUtils.isInitDataBase(GreenDaoHelper.DB_VERSION)) {
            mLocationAreaDao = GreenDaoHelper.getLocationDaoSession().getAreaDao();

        }
        myDeviceManager = DeviceManager.getInstance();
        myDeviceManager.refreshWiFiDevices();
        FirstPageMethods.getInstance().init();

        isNetgetLocation = true;

        //十分钟刷新一次 TODO 启动后，首次不执行
        timer = new Timer();
        timer.schedule( new TimerTask()
        {
            @Override
            public void run() {
                getPMData(areaID);

            }
        }, 0, 10 * 60 * 1000 );

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        Log.d("===","onCreateView");

        View v = inflater.inflate(R.layout.fragment_tab_home, container, false);
        mSrlDeviceList = (SwipeRefreshLayout) v.findViewById(R.id.srl_device_list);
        mLvDevices = (ListView) v.findViewById(R.id.lv_device);
        mBtnAddDevice = (ImageView) v.findViewById(R.id.btn_add_device);
        mTvPrompt = (TextView) v.findViewById(R.id.tv_prompt);

        //mIconUser = (ImageView) v.findViewById(R.id.ic_user);
        mUsername = (TextView) v.findViewById(R.id.username);
        mPm25 = (TextView) v.findViewById(R.id.pm25);
        mAirQuality = (TextView) v.findViewById(R.id.ic_air_quality);
        mOutdoor = (TextView) v.findViewById(R.id.text_outdoor);
        mLocation = (TextView)v.findViewById(R.id.ic_location);
        single_pm25 = (TextView) v.findViewById(R.id.single_pm25);

        //tv_home_temp = (TextView) v.findViewById(R.id.tv_home_temp);
        //tv_home_rain = (TextView) v.findViewById(R.id.tv_home_rain);

        mIconDelete = (ImageView)v.findViewById(R.id.btn_delete_device);
        mDevice_delete = (ImageView)v.findViewById(R.id.device_delete);
        //mDelCheckbox = (CheckBox)v.findViewById(R.id.delete_checkbox);

        mOfficialDelection = (LinearLayout) v.findViewById(R.id.official_delection);
        location = (View) v.findViewById(R.id.location);
        single_delection = (View) v.findViewById(R.id.single_delection);
        single_delection.setVisibility(View.GONE);

        WindowManager wm = getActivity().getWindowManager();
        width = wm.getDefaultDisplay().getWidth();
        height = wm.getDefaultDisplay().getHeight();
        mSkyBackground = (View)v.findViewById(R.id.skg_background);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_skg_background);
        mSkyBackground.setBackground(getResources().getDrawable(R.drawable.ic_skg_background));
        imageHeight = bitmap.getHeight();
        imageWidth = bitmap.getWidth();
        ViewGroup.LayoutParams params = mSkyBackground.getLayoutParams();
        params.width =width;
        params.height=imageHeight*width/imageWidth;
        mSkyBackground.setLayoutParams(params);
        location.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if (GreenDaoUtils.isInitDataBase(GreenDaoHelper.DB_VERSION)) {
                    gotoLocationManagerActivity();
                }
            }
        });
        mOfficialDelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoWeatherDetailActivity();
            }
        });


        mBtnAddDevice.setOnClickListener(v1 -> gotoDeviceScanActivity());
        mIconDelete.setOnClickListener(v1 -> isShowCheckbox());


        mLvDevices.setOnItemClickListener((parent, view, position, id) -> {
            MobclickAgent.onEvent(getActivity(),"TabHomeFragment_mLvDevices_ItemClick");
            mDevice = mLeDeviceAdapter.getItem(position);
            refreshView();
            if(editMode){

                if(mDevice.getIsconnectd() == BLEService.STATE_CONNECTING){
                    AppToast.showToast("设备连接中不可删除");
                } else {
                    firstDevice = lastDevice;
                    lastDevice = mDevice;
                    if (firstDevice != lastDevice || mDevice.getIsconnectd() == BLEService.STATE_DISCONNECTED) {
                        Log.e("**^===1","===="+mDevice.getName()+mDevice.getAddress()+"   mLeDeviceAdapter  :"+mLeDeviceAdapter);
                        mLeDeviceAdapter.updateStatus(mDevice);
                    } else {
                        return;
                    }
                }
            } else {
                if (mDevice instanceof BaseBluetoothDevice) {
                    if(mDevice.getIs_demo() == 1){
                        gotoDeviceControlActivity(mDevice);
                    } else {

                        if (mDevice.getIsconnectd() == BLEService.STATE_CONNECTED) {
                            gotoDeviceControlActivity(mDevice);
                        } else {
                            Logger.v("尝试手动连接设备" + mDevice.getName() + "(" + mDevice.getName() + ")", mDevice);
                            mLeDeviceAdapter.getItem(position).setIsconnectd(BLEService.STATE_CONNECTING);
                            mLeDeviceAdapter.notifyDataSetChanged();
                            mDevice.setIsconnectd(BLEService.STATE_CONNECTING);
                            updateDataSet(mDevice);
                            //连接函数
                            if (LoginInterceptor.getLogin()) {
                                Log.d("===","login");
                                deviceList();
                                connect(myDeviceManager.mData.get(mDevice.getAddress()));
                            } else {
                                Log.d("===","loginOut");
                                connect(myDeviceManager.mDataOff.get(mDevice.getAddress()));
                            }
                        }
                    }

                } else if (mDevice.getTypeId() == 10) {
                    //wifi设备 。
                    gotoDeviceControlActivity(mDevice);
                }
            }

        });
        mLvDevices.setOnItemLongClickListener((parent, view, position, id) -> {
            MobclickAgent.onEvent(getActivity(),"TabHomeFragment_mLvDevices_ItemLongClick");
            if(editMode){
                return true;
            }else{
                mDevice = mLeDeviceAdapter.getItem(position);
                showLongPressDialog(position, view,mDevice.getIs_demo());
                reName = mDevice;
                return true;
            }
        });
        mLvDevices.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                boolean enable = false;
                if(mLvDevices != null && mLvDevices.getChildCount() > 0){

                    // check if the first item of the list is visible
                    boolean firstItemVisible = mLvDevices.getFirstVisiblePosition() == 0;
                    // check if the top of the first item is visible
                    boolean topOfFirstItemVisible = mLvDevices.getChildAt(0).getTop() == 0;
                    // enabling or disabling the refresh layout
                    //enable = firstItemVisible && topOfFirstItemVisible;
                }
                //mSrlDeviceList.setEnabled(enable);
            }});
        mSrlDeviceList.setEnabled(true);
        mSrlDeviceList.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        mSrlDeviceList.setOnRefreshListener(this::refreshData);
//        mSrlDeviceList.setOnRefreshListener(this::getLeDeviceList);
        init();
        initRxBus();
        initScan();

        return v;
    }

    //只获取地理位置
    private void getLocation(String param){

        Log.e("test","-pm-");


            //系统定位
            Subscriber<WeatherData> locationSubscriber = new Subscriber<WeatherData>() {
                @Override
                public void onCompleted() {

                }
                @Override
                public void onError(Throwable e) {
                    //定位失败，改为未定位状态
                    locationStep = TabHomeFragment.LOCATION_STEP_DEFAULT;
                    Toast.makeText(getActivity(), "请检查网络连接", Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onNext(WeatherData weatherData) {
                    locationStep = LOCATION_STEP_COMPLETED;
                    handleLocation(weatherData);
//                    if (locationStep == LOCATION_STEP_DEFAULT) {
//
//                        handleWeatherData(weatherData, true);
//                    } else {
                    Log.e("TabHomeFragment", "weatherData onNext:" + JsonUtils.objectToJson(weatherData));

//                        SharePreferenceUtils.setWeatherData(JsonUtils.objectToJson(weatherData));
//                        handleWeatherData(weatherData, false);
//                    }

                }
            };
            Log.e("TabHomeFragment", "param:" + param);
            FirstPageMethods.getInstance().getPMData(locationSubscriber, (param == null ? "" : param));

    }

    //获取天气数据
    private void getPMData(String param){
        if (param == null) {
            return;
        }


        Subscriber<WeatherData> subscriber = new Subscriber<WeatherData>() {
            @Override
            public void onCompleted() {

            }
            @Override
            public void onError(Throwable e) {

                Log.e("TabHomeFragment", "请检查网络连接getPMData");
				Toast.makeText(getActivity(), "请检查网络连接", Toast.LENGTH_SHORT).show();
                String weatherData = SharePreferenceUtils.getWeatherData();
                Log.d("TabHomeFragment", "weatherData:" + weatherData);
                if (!"".equals(weatherData)) {
                    handleWeatherData((WeatherData) JsonUtils.jsonToBean(weatherData,WeatherData.class));
                } else {
                    mSrlDeviceList.setRefreshing(false);
                }
            }

            @Override
            public void onNext(WeatherData weatherData) {
//                if (locationStep == LOCATION_STEP_SYSTEM) {
//                    locationStep = LOCATION_STEP_IP;
//                    handleWeatherData(weatherData, true);
//                } else {
                    Log.e("TabHomeFragment", "weatherData onNext:" + JsonUtils.objectToJson(weatherData));

                    SharePreferenceUtils.setWeatherData(JsonUtils.objectToJson(weatherData));
                    handleWeatherData(weatherData);
//                }

            }
        };
            Log.e("TabHomeFragment", "param:" + param);
            FirstPageMethods.getInstance().getPMData(subscriber, param);

    }

    //只解决位置
    private void handleLocation(WeatherData weatherData) {

        String tempAreaId = weatherData.getCity().getAreaId();
        Log.d("TabHomeFragment", "tempAreaId:" + tempAreaId);
        if (tempAreaId == null) {
            Log.e("TabHomeFragment", "get area_id is null.");
            locationStep = TabHomeFragment.LOCATION_STEP_DEFAULT;
            return;
        }

            List<String> tempCurrentAreaIdList = new ArrayList<String>();
            tempCurrentAreaIdList.add(tempAreaId);
//			Log.d("TabHomeFragment", "定位完成存储tempAreaId:" + tempAreaId);

            SharePreferenceUtils.setAreaIdList(LocationManagerActivity.SP_CURRENT_AREA_ID_LIST, tempCurrentAreaIdList);

            if (areaID == null || "".equals(areaID)) {
                areaID = tempAreaId;
                mLocation.setText(displayLocationFromAreaId(tempAreaId));
                SharePreferenceUtils.setString(SP_SELECTED_AREA_ID, areaID);
                SharePreferenceUtils.setAreaIdList(LocationManagerActivity.SP_RECENT_AREA_ID_LIST, tempCurrentAreaIdList);
                handleWeatherData(weatherData);
                return;
            }

            Log.e("TabHomeFragment", "isAlert:"+SmartHomeApplication.isAlert + "\nareaID:" + areaID + "\ntempAreaId" + tempAreaId);
            // TODO 判断两个areaID是否相等， 大区域定位到小区域，不提示改变
            if (!SmartHomeApplication.isAlert && !areaID.equals(tempAreaId)) {
                SmartHomeApplication.isAlert = true;
                createAlert(tempAreaId, areaID);
            }

    }

    //只解决天气
    private void handleWeatherData(WeatherData weatherData) {

        mUsername.setText("当前温度"+weatherData.getWeather().getTemperature() + "℃  "+weatherData.getWeather().getText());
        mPm25.setText(weatherData.getAir().getPm25());
        mAirQuality.setText(weatherData.getAir().getQuality());
        mOutdoor.setText(weatherData.getSports().getBrief());
        areaID = weatherData.getCity().getAreaId();
        mLocation.setText(displayLocationFromAreaId(areaID));

    }


    private void isShowCheckbox(){
        MobclickAgent.onEvent(getActivity(),"TabHomeFragment_isShowCheckbox");
        if(editMode){
            editMode = false;
        }else {
            editMode = true;
        }
        refreshView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(getActivity());
    }
    @Override
    public void onResume()
    {
        super.onResume();
        MobclickAgent.onResume(getActivity());
        Log.e("====","onResume");


        String selectedID = SharePreferenceUtils.getString(SP_SELECTED_AREA_ID);
        if (areaID != null && !"".equals(areaID)) {
//            mLocation.setText(displayLocationFromAreaId(areaID));
            getPMData(selectedID);
        } else {
            mLocation.setText("--");
        }

        if (locationStep == LOCATION_STEP_DEFAULT) {
            locationStep = LOCATION_STEP_ING;
            getLocationAndCheckPermission();
        }



        editMode = false;
        num = 0;
        //获取设备列表
        needSynDevice = true;


        if (SharePreferenceUtils.getLoginStatus())
        {
            getLeDeviceList();
//			deviceList();

            String mobile = SharePreferenceUtils.getMobileNum();
            String enmobile = mobile.substring(0, 3) + "****" + mobile.substring(7);
//			mUsername.setText(getAMorPM()+","+enmobile);
//			mIconUser.setImageResource(R.drawable.ic_headpic_login);
        } else {
            //分
            getLeDeviceList();
//			mUsername.setText(getAMorPM()+","+"欢迎使用");
//			mIconUser.setImageResource(R.drawable.ic_headpic_unlogin);
        }
    }

    @Override
    public void onDestroyView()
    {
        Log.e("====","onDestroyView");
        super.onDestroyView();
        timer.cancel();
        getActivity().unregisterReceiver(mBluetoothReceiver);
    }

    private void init()
    {
        Log.e("====","init");

        areaID = SharePreferenceUtils.getString(SP_SELECTED_AREA_ID);
        if (areaID != null && !"".equals(areaID)) {
            mLocation.setText(displayLocationFromAreaId(areaID));
            getPMData(areaID);
        } else {
            mLocation.setText("--");
        }

        String weatherDataStr = SharePreferenceUtils.getWeatherData();
        if (!"".equals(weatherDataStr)) {
            WeatherData weatherData = (WeatherData) JsonUtils.jsonToBean(weatherDataStr,WeatherData.class);
            mUsername.setText("当前温度"+weatherData.getWeather().getTemperature() + "℃  "+weatherData.getWeather().getText());
            mPm25.setText(weatherData.getAir().getPm25());
            mAirQuality.setText(weatherData.getAir().getQuality());
            mOutdoor.setText(weatherData.getSports().getBrief());
        }

        mLeDeviceAdapter = new LeDeviceListAdapter(getActivity());
        mLeDeviceAdapter.setmListener(this::getLeDeviceList);
        mLeDeviceAdapter.setDialogConfirmListener(mDialogConfirmListener);

        IntentFilter bluetoothFilter = new IntentFilter();
        bluetoothFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        bluetoothFilter.addAction(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);

        getActivity().registerReceiver(mBluetoothReceiver, bluetoothFilter);

        mLvDevices.setAdapter(mLeDeviceAdapter);
        //获取数据
        myDeviceManager.refreshWiFiDevices();
    }

    /**
     * 初始化蓝牙扫描并自动连接
     */
    private void initScan()
    {
        Log.d("====","initScan");

        if (SmartHomeApplication.mBluetoothService == null)
        {
            new Thread()
            {
                @Override
                public void run()
                {
                    while (SmartHomeApplication.mBluetoothService == null)
                    {
                        try
                        {
                            Thread.sleep(100);
                        } catch (InterruptedException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    SmartHomeApplication.mBluetoothService.setConnectTimeOutListener(mConnectTimeOutListener);
                    BluetoothScanner.scan(1);
                    try
                    {
                        Thread.sleep(1000);
                    } catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                    BluetoothScanner.scanAndAutoConnect();
                }
            }.start();
        }
        else
        {
            SmartHomeApplication.mBluetoothService.setConnectTimeOutListener(mConnectTimeOutListener);
            BluetoothScanner.scan(1);
            new Handler(Looper.getMainLooper()).postDelayed(BluetoothScanner::scanAndAutoConnect, 1000);
        }
    }

    private void initRxBus()
    {
        Log.d("===","initRxBus");

        RxBus.with(this)
                .setEvent(Events.DEVICE_UPDATE_COMPLETE)
                .onNext(events -> {
//                    Log.e("===","DEVICE_UPDATE_COMPLETE");

                    mDeviceAddress = (String) events.content;

                    if (SharePreferenceUtils.getLoginStatus()) {
                        if (mDeviceAddress != null) {
                            mDevice = myDeviceManager.mData.get(mDeviceAddress);
                            if (mDevice != null) {
                                mDevice.connect();
                            }
                        }
                    } else {
                        if (mDeviceAddress != null) {
                            mDevice = myDeviceManager.mDataOff.get(mDeviceAddress);
                            if (mDevice != null) {
                                mDevice.connect();
                            }
                        }
                    }
                    getLeDeviceList();
                })
                .create();
        RxBus.with(this)
                .setEvent(Events.GATT_MANUAL_DISCONNECT)
                .onNext(events -> {
                    Log.e("**^===1","GATT_MANUAL_DISCONNECT");
                    mDeleteDevice = (BaseDevice) events.content;
                    if(editMode && !LoginInterceptor.getLogin()){
                        Log.e("**^===","GATT_MANUAL_DISCONNECT"+mDeleteDevice.getAddress()+mLeDeviceAdapter);
                        mLeDeviceAdapter.deleteDevice(mDeleteDevice.getAddress());
                    }
                })
                .create();
        RxBus.with(this)
                .setEvent(Events.GATT_CONNECTED)
                .onNext(events -> {
                    Log.e("===","GATT_CONNECTED");
                    //在用户登陆的状态下，添加设备

                    mDeviceAddress = (String) events.content;

                    if (SharePreferenceUtils.getLoginStatus()){
                        Log.d("===","GATT_CONNECTE---login");

                        if (mDeviceAddress != null)
                        {
                            mDevice = myDeviceManager.mData.get(mDeviceAddress);
                        }
                        if (mDevice == null)
                        {
                            return;
                        }
                        mDevice.setIsconnectd(BLEService.STATE_CONNECTED);
                        if (mDevice instanceof BaseBluetoothDevice)
                        {
                            ((BaseBluetoothDevice) mDevice).setAutoConnect(true);
                            if (mDevice.getTypeId() == 3){
                                SharePreferenceUtils.setLatestAddr(mDeviceAddress);
                            }
                        }

                        myDeviceManager.updateData(mDeviceAddress, mDevice);
                        getLeDeviceList();
                    } else {
                        Log.d("===","GATT_CONNECTE---logout");

                        if (mDeviceAddress != null)
                        {
                            mDevice = myDeviceManager.mDataOff.get(mDeviceAddress);
                        }
                        if (mDevice == null)
                        {
                            return;
                        }
//						mDevice.stopConnect();
                        mDevice.setIsconnectd(BLEService.STATE_CONNECTED);
                        if (mDevice instanceof BaseBluetoothDevice)
                        {
                            ((BaseBluetoothDevice) mDevice).setAutoConnect(true);
                            if (mDevice.getTypeId() == 3){
                                SharePreferenceUtils.setLatestAddr(mDeviceAddress);
                            }
                        }
                        Log.d("put---11","GATT_DISCONNECTED ---logout");
                        myDeviceManager.updateData(mDeviceAddress, mDevice);
                        getLeDeviceList();

                    }

                })
                .create();

        RxBus.with(this)
                .setEvent(Events.GATT_DISCONNECTED)
                .onNext(events -> {

                    Log.d("===","GATT_DISCONNECTED" + myDeviceManager.mDataOff.size());

                    mDeviceAddress = (String) events.content;
                    //账号登录
                    if (SharePreferenceUtils.getLoginStatus()){
                        Log.d("===","GATT_DISCONNECTED ---login");

                        if (mDeviceAddress != null)
                        {
                            mDevice = myDeviceManager.mData.get(mDeviceAddress);
                            if (mDevice != null)
                            {
                                mDevice.setIsconnectd(BLEService.STATE_DISCONNECTED);
                                myDeviceManager.updateData(mDeviceAddress, mDevice);
                                hasSingle();

                            }
                        }
                        getLeDeviceList();
                    } else {
                        Log.e("put---we","GATT_DISCONNECTED ---logout");

                        if (mDeviceAddress != null)
                        {
                            mDevice = myDeviceManager.mDataOff.get(mDeviceAddress);
                            if (mDevice != null)
                            {
                                Log.e("put---w","GATT_DISCONNECTED ---logout");
                                mDevice.setIsconnectd(BLEService.STATE_DISCONNECTED);
                                myDeviceManager.updateData(mDeviceAddress, mDevice);
                                hasSingleoff();
                            }
                        }
                        getLeDeviceList();
/*                        myDeviceManager.clearData(0);
                        SmartHomeApplication.mBluetoothService.clearData(0);*/
                    }
                })
                .create();

        RxBus.with(this)
                .setEvent(Events.DEVICE_ADDED)
                .onNext(events -> {
                    Log.e("===","DEVICE_ADDED");

                    if (LoginInterceptor.getLogin()){
                        mDeviceAddress = (String) events.content;
                        myDeviceManager.mData.get(mDeviceAddress).connect(false);
                        getLeDeviceList();
                    } else {
                        mDeviceAddress = (String) events.content;
                        myDeviceManager.mDataOff.get(mDeviceAddress).connect(false);
                        getLeDeviceList();
                    }

                })
                .create();

        RxBus.with(this)
                .setEvent(Events.MQTT_DEVICE_CHANGE)
                .onNext(events -> {
                    Log.e("===","MQTT_DEVICE_CHANGE");
                    if (needSynDevice)
                    {
                        Log.e("===","MQTT_DEVICE_CHANGE--devicelist");
                        //连接MQTT服务器，开始同步设备，
                        if(!LoginInterceptor.getLogin()){
                            return;
                        }
                    }
                })
                .create();

        RxBus.with(this)
                .setEvent(Events.MQTT_DEVICE_ONLINE)
                .onNext(events -> {
//                    Log.d("===","MQTT_DEVICE_ONLINE");

                    Bundle bundle = (Bundle) events.content;
                    String deviceID = bundle.getString(EventKey.MQTT_DEVICE_ID);
                    boolean onlienstatus = bundle.getBoolean(EventKey.MQTT_ONLINE_STATUS, false);
                    updateWifiOnlineStatus(deviceID, onlienstatus);
                })
                .create();
        RxBus.with(this)
                .setEvent(Events.PM25)
                .onNext(events -> {
                    Bundle bundle = (Bundle) events.content;
                    String addr = bundle.getString(EventKey.PM);
                    String maddr = SharePreferenceUtils.getLatestAddr();
                    if (LoginInterceptor.getLogin()) {
                        if (maddr != null) {
                            mDevice = myDeviceManager.mData.get(maddr);
                            if (mDevice != null) {
                                String pm = mDevice.getPm25() + "";
                                if (!single_pm25.getText().equals(pm)) {
                                    single_pm25.setText(pm);
                                }
                            }

                        }
                    } else {
                        if (maddr != null) {
                            mDevice = myDeviceManager.mDataOff.get(maddr);
                            if (mDevice != null) {
                                String pm = mDevice.getPm25() + "";
                                if (!single_pm25.getText().equals(pm)) {
                                    single_pm25.setText(pm);
                                }
                            }
                        }
                    }
                })
                .create();


    }
    //添加数据
    public void getLeDeviceList()
    {
//		Log.e("===","getLeDeviceList");

        hasSingle = false;
        if (LoginInterceptor.getLogin()) {
//			Log.e("===","1==getLeDeviceList");

            if (myDeviceManager.mData != null) {
                mLeDeviceAdapter.clear();
                Iterator iterator1 = myDeviceManager.mData.entrySet().iterator();
                Iterator iterator2 = myDeviceManager.mData.entrySet().iterator();
                while (iterator1.hasNext()) {
                    Map.Entry<String, BaseDevice> entry = (Map.Entry<String, BaseDevice>) iterator1.next();
                    if (entry.getValue().getIs_demo() == 1) {
                        mLeDeviceAdapter.addDevice(entry.getValue());
                    }
                    if ((entry.getValue().getTypeId() == 3) && (entry.getValue().getIsconnectd() == BLEService.STATE_CONNECTED)) {
                        hasSingle = true;
                    }
                }
                while (iterator2.hasNext()) {
                    Map.Entry<String, BaseDevice> entry = (Map.Entry<String, BaseDevice>) iterator2.next();
                    if (entry.getValue().getIs_demo() == 0) {
                        mLeDeviceAdapter.addDevice(entry.getValue());
                    }
                    if ((entry.getValue().getTypeId() == 3) && (entry.getValue().getIsconnectd() == BLEService.STATE_CONNECTED)) {
                        hasSingle = true;
                    }
                }
                mLeDeviceAdapter.notifyDataSetChanged();
            }
        } else {
//			Log.e("===","2==getLeDeviceList");
            if (myDeviceManager.mDataOff != null) {
                Log.e("===","3=="+myDeviceManager.mDataOff.size());

                mLeDeviceAdapter.clear();
                Iterator iterator1 = myDeviceManager.mDataOff.entrySet().iterator();
                while (iterator1.hasNext()) {
                    Map.Entry<String, BaseDevice> entry = (Map.Entry<String, BaseDevice>) iterator1.next();
                    mLeDeviceAdapter.addDevice(entry.getValue());
                    if ((entry.getValue().getTypeId() == 3) && (entry.getValue().getIsconnectd() == BLEService.STATE_CONNECTED)) {
                        hasSingle = true;
                    }
                    mLeDeviceAdapter.notifyDataSetChanged();
                }
            }
        }
        refreshView();
    }

    //deleteDevice
    public void deleteDevice(){
        hasSingle = false;
        mLeDeviceAdapter.clear();
        mLeDeviceAdapter.notifyDataSetChanged();
        refreshView();
    }


    private void updateDataSet(BaseDevice device)
    {
//        Log.e("===","updateDataSet");
        myDeviceManager.updateData(device.getAddress(), device);
        getLeDeviceList();
    }

    private void updateConnectStatus(final String addr)
    {
//        Log.e("===","updateConnectStatus");
        if (LoginInterceptor.getLogin()) {
            BaseDevice device = myDeviceManager.mData.get(addr);
            int status = BLEService.STATE_DISCONNECTED;
            if (device != null) {
                status = device.getIsconnectd();
                if (status != BLEService.STATE_CONNECTED) {
                    device.setIsconnectd(BLEService.STATE_DISCONNECTED);
                    updateDataSet(device);
                }
            }
        } else {
            BaseDevice device = myDeviceManager.mDataOff.get(addr);
            int status = BLEService.STATE_DISCONNECTED;
            if (device != null) {
                status = device.getIsconnectd();
                if (status != BLEService.STATE_CONNECTED) {
                    device.setIsconnectd(BLEService.STATE_DISCONNECTED);
                    updateDataSet(device);
                }
            }

        }
    }

    private void gotoLocationManagerActivity() {
        MobclickAgent.onEvent(getActivity(),"TabHomeFragment_gotoLocationManagerActivity");
        ControlManager.gotoLocationManagerActivity(getActivity());
    }

    private void gotoWeatherDetailActivity() {
        MobclickAgent.onEvent(getActivity(),"TabHomeFragment_gotoWeatherDetailActivity");
        ControlManager.gotoWeatherDetailActivity(getActivity(),displayLocationFromAreaId(areaID),areaID);
    }
    private void goDeviceScanActivity() {
        MobclickAgent.onEvent(getActivity(),"TabHomeFragment_gotoLoginActivity");
        ControlManager.goDeviceScanActivity(getActivity());
    }

    private void gotoDeviceControlActivity(BaseDevice device)
    {
//        Log.d("===","gotoDeviceControlActivity");
        MobclickAgent.onEvent(getActivity(),"TabHomeFragment_gotoDeviceControlActivity");
        needSynDevice = false;
        if (device.getTypeId() != 3){
            ControlManager.gotoControlActivity(getActivity(), device);
        } else {
            for (int i = 0; i < 50; i++) {
                if (!OfflinePackageManage.getPackagePath(device.getTypeId()).equals("")) {
                    ControlManager.gotoControlActivity(getActivity(), device);
                    break;
                } else {
                    if (i == 49) {
                        Toast.makeText(getActivity(), "网络不畅，请稍后重试", Toast.LENGTH_SHORT).show();
                        OfflinePackageManage.checkVersion(device.getTypeId());
                    } else {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
//		if (!OfflinePackageManage.getPackagePath(mDevice.getTypeId()).equals("")) {
//			ControlManager.gotoControlActivity(getActivity(), device);
//		}
    }
    private void gotoDeviceScanActivity()
    {
        MobclickAgent.onEvent(getActivity(),"TabHomeFragment_gotoDeviceScanActivity");
        if(LoginInterceptor.getLogin()){
            LoginInterceptor.interceptor(getContext(), "com.smarthome.lilos.lilossmarthome.activity.device.DeviceScanActivity", null);
        } else {
            //分
            goDeviceScanActivity();
        }
    }

    private void refreshView()
    {
        if (mLeDeviceAdapter.getCount() == 0)
        {
            hasSingle = false;
            mLvDevices.setVisibility(View.GONE);
            mTvPrompt.setVisibility(View.VISIBLE);
            mIconDelete.setVisibility(View.GONE);
        }
        else
        {
            mIconDelete.setVisibility(View.VISIBLE);
            mLvDevices.setVisibility(View.VISIBLE);
            mTvPrompt.setVisibility(View.GONE);
            mLeDeviceAdapter.notifyDataSetChanged();
        }
        Log.d("===","refreshView---" + hasSingle);

        if(hasSingle){
            single_delection.setVisibility(View.VISIBLE);
        }else {
            single_delection.setVisibility(View.GONE);
        }
    }

    private void connect(BaseDevice device)
    {
        switch (BluetoothAdapter.getDefaultAdapter().getState())
        {
            case BluetoothAdapter.STATE_ON:
                Logger.v("进入连接准备，尝试连接设备" + device.getAddress() + "(" + device.getName() + ")");
                Log.d("===","connect");
                device.setIsconnectd(BLEService.STATE_CONNECTING);
                updateDataSet(device);
                BluetoothScanner.scan(1);
                try
                {
                    Thread.sleep(1000);
                } catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                device.connect();
                break;
            case BluetoothAdapter.STATE_OFF:
                //请求蓝牙
                MainActivity.mContext.requestBluetooth(BLEService.REQUEST_CONNECT, device.getAddress());

                break;
            case BluetoothAdapter.STATE_TURNING_ON:
                AppToast.showToast(R.string.bluetooth_is_turning_on);
                closeConnect(device);
                break;
            case BluetoothAdapter.STATE_TURNING_OFF:
                AppToast.showToast(R.string.bluetooth_is_turning_off);
                closeConnect(device);
                break;
            default:
                AppToast.showToast(R.string.bluetooth_status_error);
                closeConnect(device);
                break;
        }
    }

    boolean isWifiDevices(BaseDevice device)
    {
        if (device.getTypeId() == 10)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    boolean isBtDevices(BaseDevice device)
    {
        if(device instanceof BaseBluetoothDevice)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void showLongPressDialog(int postion, final View view,int is_demo)
    {
        mLongPressDialog = new LongPressDialog(this, R.style.LongPressDialogStyle,is_demo);
        int[] location = new int[2];
        view.setBackgroundColor(getResources().getColor(
                android.R.color.darker_gray));
        view.getLocationOnScreen(location);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        Display display = this.getActivity().getWindowManager().getDefaultDisplay();
        display.getMetrics(displayMetrics);
        WindowManager.LayoutParams params = mLongPressDialog.getWindow().getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.y = display.getHeight() - location[1] - (int) (view.getHeight() * 1.5);
        mLongPressDialog.getWindow().setAttributes(params);
        mLongPressDialog.setCanceledOnTouchOutside(true);
        mLongPressDialog.setOnCancelListener(dialog -> view.setBackgroundColor(getResources().getColor(
                android.R.color.white)));
        mLongPressDialog.show();
        if ((isBtDevices(mDevice) == true) && mDevice.getIsconnectd() == BLEService.STATE_CONNECTED)
        {
            mLongPressDialog.setText(R.string.disconnect);
            Log.e("showLongPressDialog","  断开连接");
        }
        else
        {
            mLongPressDialog.setText(R.string.delete);
            Log.e("showLongPressDialog","  删除");
        }
        mDevice = mLeDeviceAdapter.getItem(postion);
    }

    /**
     * 蓝牙开关的监听
     * @param btState
     */
    private void printBTState(int btState)
    {
        switch (btState)
        {
            case BluetoothAdapter.STATE_OFF:
                SmartHomeApplication.mBluetoothService.close();
                if (LoginInterceptor.getLogin()) {
                    for (String key : myDeviceManager.mData.keySet())
                    {
                        myDeviceManager.mData.get(key).setIsconnectd(BLEService.STATE_DISCONNECTED);
                    }
                } else {
                    for (String key : myDeviceManager.mDataOff.keySet())
                    {
                        myDeviceManager.mDataOff.get(key).setIsconnectd(BLEService.STATE_DISCONNECTED);
                    }
                }
                Log.e("8888===","getLeDeviceList10");
                getLeDeviceList();
                break;
            case BluetoothAdapter.STATE_TURNING_OFF:
                Logger.d("蓝牙关闭中");
                break;
            case BluetoothAdapter.STATE_TURNING_ON:
                try
                {
                    Thread.sleep(300);
                } catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                SmartHomeApplication.mBluetoothService.initialize();
                break;
            case BluetoothAdapter.STATE_ON:
                break;
            default:
                break;
        }
    }

    @Override
    public void rename()
    {
        if (reName != null && reName.getName() != null)
        {
            mLeDeviceAdapter.postDialog(reName.getName());
            Logger.i("当前正在尝试修改设备" + reName.getAddress() + "(" + reName.getName() + ")的名称");
        }
    }

    @Override
    public void operation()
    {
        mLeDeviceAdapter.operation(reName);
    }

    /**
     * 同步设备
     * @param newData
     */
    public void comparisonDeviceList(List<UserDeviceEntity> newData)
    {
        Log.d("===TabHomeFragment", "进行设备同步");
        newMap.clear();
        for (UserDeviceEntity d : newData)
        {
            newMap.put(d.getDevice_id(), d);
        }

        if (LoginInterceptor.getLogin()) {
            for (String address : myDeviceManager.mData.keySet()) {
                //更改设备名字
                if (newMap.containsKey(address)) {
                    myDeviceManager.mData.get(address).setName(newMap.get(address).getName());
                    myDeviceManager.mData.get(address).setIs_demo(newMap.get(address).getIs_demo());
                    newMap.remove(address);
                } else {
                    //云端不包含设备，
                    if (myDeviceManager.mData.get(address) instanceof BaseBluetoothDevice) {
                        ((BaseBluetoothDevice) myDeviceManager.mData.get(address)).setAutoConnect(false);
                        SmartHomeApplication.mBluetoothService.disconnect(address);
                        try {
                            Thread.sleep(300);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    myDeviceManager.mData.remove(address);
                    myDeviceManager.saveData();
                    mLeDeviceAdapter.removeDevice(address);
                }
            }
        } else {
            for (String address : myDeviceManager.mDataOff.keySet()) {
                //更改设备名字
                if (newMap.containsKey(address)) {
                    myDeviceManager.mDataOff.get(address).setName(newMap.get(address).getName());
                    myDeviceManager.mDataOff.get(address).setIs_demo(newMap.get(address).getIs_demo());
                    newMap.remove(address);
                } else {
                    //云端不包含设备，
                    if (myDeviceManager.mDataOff.get(address) instanceof BaseBluetoothDevice) {
                        ((BaseBluetoothDevice) myDeviceManager.mDataOff.get(address)).setAutoConnect(false);
                        SmartHomeApplication.mBluetoothService.disconnect(address);
                        try {
                            Thread.sleep(300);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    myDeviceManager.mDataOff.remove(address);
                    myDeviceManager.saveData();
                    mLeDeviceAdapter.removeDevice(address);
                }
            }
        }

        //同步云端设备列表
        if (newMap.size() > 0)
        {
            for (String address : newMap.keySet())
            {
                myDeviceManager.addData(address, DeviceFactory.createDevice(newMap.get(address)));
            }
        }
        Log.d("===TabHomeFragment", "更新离线包");
        OfflinePackageManage.updateOfflinePackage();
        if(LoginInterceptor.getLogin()){
            //判断是否有单品
            hasSingle();
        }else {
            hasSingleoff();
        }


        mLeDeviceAdapter.notifyDataSetChanged();
        refreshView();
    }
    /**
     * 是否有单品
     */
    public void hasSingleoff() {

        hasSingle = false;

        if (myDeviceManager.mDataOff != null)
        {
            mLeDeviceAdapter.clear();
            Iterator iterator1 = myDeviceManager.mDataOff.entrySet().iterator();
            Iterator iterator2 = myDeviceManager.mDataOff.entrySet().iterator();
            while (iterator1.hasNext())
            {
                Map.Entry<String, BaseDevice> entry = (Map.Entry<String, BaseDevice>) iterator1.next();
                if(entry.getValue().getIs_demo() == 1){
                    mLeDeviceAdapter.addDevice(entry.getValue());
                }
                if((entry.getValue().getTypeId() == 3) && (entry.getValue().getIsconnectd() == BLEService.STATE_CONNECTED)){
                    hasSingle = true;
                }
            }
            while (iterator2.hasNext())
            {
                Map.Entry<String, BaseDevice> entry = (Map.Entry<String, BaseDevice>) iterator2.next();
                if(entry.getValue().getIs_demo() == 0){
                    mLeDeviceAdapter.addDevice(entry.getValue());
                }
                if((entry.getValue().getTypeId() == 3) && (entry.getValue().getIsconnectd() == BLEService.STATE_CONNECTED)){
                    hasSingle = true;
                }
            }
//            mLeDeviceAdapter.notifyDataSetChanged();
        }
    }
    /**
     * 是否有单品
     */
    public void hasSingle() {

        hasSingle = false;

        if (myDeviceManager.mData != null)
        {
            mLeDeviceAdapter.clear();
            Iterator iterator1 = myDeviceManager.mData.entrySet().iterator();
            Iterator iterator2 = myDeviceManager.mData.entrySet().iterator();
            while (iterator1.hasNext())
            {
                Map.Entry<String, BaseDevice> entry = (Map.Entry<String, BaseDevice>) iterator1.next();
                if(entry.getValue().getIs_demo() == 1){
                    mLeDeviceAdapter.addDevice(entry.getValue());
                }
                if((entry.getValue().getTypeId() == 3) && (entry.getValue().getIsconnectd() == BLEService.STATE_CONNECTED)){
                    hasSingle = true;
                }
            }
            while (iterator2.hasNext())
            {
                Map.Entry<String, BaseDevice> entry = (Map.Entry<String, BaseDevice>) iterator2.next();
                if(entry.getValue().getIs_demo() == 0){
                    mLeDeviceAdapter.addDevice(entry.getValue());
                }
                if((entry.getValue().getTypeId() == 3) && (entry.getValue().getIsconnectd() == BLEService.STATE_CONNECTED)){
                    hasSingle = true;
                }
            }
//            mLeDeviceAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 断开连接
     * @param device
     */
    private void closeConnect(BaseDevice device)
    {
        device.setIsconnectd(BLEService.STATE_DISCONNECTED);
        SmartHomeApplication.mBluetoothService.close(device.getAddress());
        SmartHomeApplication.mBluetoothService.mListener.connectTimeOut(device.getAddress());
        if (LoginInterceptor.getLogin()){
            myDeviceManager.mData.get(device.getAddress()).setIsconnectd(BLEService.STATE_DISCONNECTED);
        } else {
            myDeviceManager.mDataOff.get(device.getAddress()).setIsconnectd(BLEService.STATE_DISCONNECTED);
        }
    }

    /**
     * 重命名
     * @param devId
     * @param name
     */
    private void renameDevice(String devId, String name)
    {
        if(!LoginInterceptor.getLogin()){
            onRenameDeviceResult();
        }else {
            Subscriber<UpdateDeviceEntity> subscriber = new WaitingSubscriber<UpdateDeviceEntity>(getActivity())
            {
                @Override
                public void onNext(UpdateDeviceEntity updateDeviceEntity)
                {
                    onRenameDeviceResult();
                }

                @Override
                public void onError(Throwable e)
                {
                    super.onError(e);
                    if(e instanceof HttpException)
                    {
                        if(((HttpException) e).code() == 404)
                        {
                            mDevice.disconnect();
                            myDeviceManager.removeData(mDevice.getAddress());
                            mLeDeviceAdapter.notifyDataSetChanged();
                        }
                    }
                }
            };
            MopsV2Methods.getInstance().updateDevice(subscriber,devId,name);
        }

    }

    /**
     * 从服务器获取设备列表
     */
    private void deviceList()
    {
        Log.d("===","deviceList");

        Subscriber<List<UserDeviceEntity>> subscriber = new BaseSubscriber<List<UserDeviceEntity>>()
        {
            @Override
            public void onNext(List<UserDeviceEntity> entities)
            {
                Log.d("===", JsonUtils.objectToJson(entities));
                Log.e("数据列表-- refreshData--","  "+JsonUtils.objectToJson(entities));
                comparisonDeviceList(entities);
            }

            @Override
            public void onError(Throwable e)
            {
                Log.e("-- refreshData--","  "+e);
                Log.d("TabHomeFragment", "请检查网络连接deviceList");
                mSrlDeviceList.setRefreshing(false);
                Toast.makeText(getActivity(), "请检查网络连接", Toast.LENGTH_SHORT).show();
            }
        };
        MopsV2Methods.getInstance().devices(subscriber);
    }

    private void deviceListOff() {
        if (myDeviceManager.mDataOff != null) {
            mLeDeviceAdapter.clear();
            Iterator iterator1 = myDeviceManager.mDataOff.entrySet().iterator();
            while (iterator1.hasNext()) {
                Map.Entry<String, BaseDevice> entry = (Map.Entry<String, BaseDevice>) iterator1.next();
                mLeDeviceAdapter.addDevice(entry.getValue());
                if ((entry.getValue().getTypeId() == 3) && (entry.getValue().getIsconnectd() == BLEService.STATE_CONNECTED)) {
                    hasSingle = true;
                }
            }
            mLeDeviceAdapter.notifyDataSetChanged();

        }

    }


    private void onRenameDeviceResult()
    {
        reName.setName(mDeviceName);
        Log.e("put----onRename","  :");
        myDeviceManager.updateData(reName.getAddress(), reName);
        mLeDeviceAdapter.notifyDataSetChanged();
    }
//	private String getAMorPM(){
//		final Calendar mCalendar = Calendar.getInstance();
//		int apm = mCalendar.get(Calendar.AM_PM);
//		if(apm == 0){
//			return "上午好";
//		}else {
//			return "下午好";
//		}
//
//	}

    //获取设备列表
    public void refreshData()
    {
        MobclickAgent.onEvent(getActivity(),"TabHomeFragment_refreshData");
        Log.e("===","refreshData---"+mLeDeviceAdapter.getCount());
        //获取地理位置管理器
        mSrlDeviceList.setRefreshing(false);

        //TODO 刷新天气接口
        getPMData(areaID);

        if (locationStep == LOCATION_STEP_DEFAULT) {
            locationStep = LOCATION_STEP_ING;
            getLocationAndCheckPermission();
        }

        if (!TextUtils.equals(SharePreferenceUtils.getAccessToken(), ""))
        {
            deviceList();
        }
        else
        {
            mSrlDeviceList.setRefreshing(false);
        }
        refreshView();
    }

    public interface RefreshViewListener
    {
        void refresh();
    }

    public interface ConnectTimeOutListener
    {
        void connectTimeOut(String addr);
    }

    public void getLocationAndCheckPermission()
    {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            // TODO 获取权限后回调，重新定位
            locationStep = TabHomeFragment.LOCATION_STEP_DEFAULT;
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 10001);
        }
        else
        {
            //Looper.prepare();
            //Looper.loop();// Can't create handler inside thread that has not called Looper.prepare()
            new Thread() {
                public void run() {
                    getLocation();
                }
            }.start();
        }
    }


    private void  getLocation()
    {
//        locationStep = LOCATION_STEP_SYSTEM;
        String locationStr = LocationHolder.getLocation(getActivity());
//        if (locationStr == null || locationStr.equals(""))
//        {
//            return;
//        }
        getLocation(locationStr);

    }


    private void createAlert(String tempAreaId,String areaId) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        AlertDialog dialog = builder.create();
        builder.setTitle("位置信息已变更，是否切换？")
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        areaID = tempAreaId;
                        mLocation.setText(displayLocationFromAreaId(areaID));
                        SharePreferenceUtils.setString(SP_SELECTED_AREA_ID, areaID);
                        getPMData(areaID);

                        dialog.dismiss();
                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }

    private List<String> displayLocationList(List<String> areadIdList) {
        List<String> locationList = new ArrayList<>();
        if (areadIdList == null) {
            return locationList;
        }

        for (String item: areadIdList) {
            locationList.add(displayLocationFromAreaId(item));
        }
        return locationList;
    }

    private String displayLocationFromAreaId (String areadId) {
        if (mLocationAreaDao == null && GreenDaoUtils.isInitDataBase(GreenDaoHelper.DB_VERSION)) {
            mLocationAreaDao = GreenDaoHelper.getLocationDaoSession().getAreaDao();
        }
        if (mLocationAreaDao == null) {
            return null;
        }

        String displayLocation = null;
        List<Area> areaList = mLocationAreaDao.queryBuilder().where(AreaDao.Properties.AreaId.eq(areadId)).build().list();
        for (Area area : areaList) {
            displayLocation = area.getAreaNameCN();

        }

        return displayLocation;
    }

}