package com.smarthome.lilos.lilossmarthome.wigdet;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.smarthome.lilos.lilossmarthome.R;

/**
 * Created by Joker on 2016/8/2.
 */
public class ProgressBarDialog
{
    Activity context;
    String title = "";
    String content = "";
    String leftStr = "取消";
    String rightStr = "确认";
    View.OnClickListener leftClickListener;
    View.OnClickListener rightClickListener;
    Dialog dialog;

    TextView dialogTitle;
    TextView dialogContentText;
    Button dialogLeft;
    Button dialogRight;
    RoundProgressBar dialogProgress;
    TextView dialogProgressText;

    public ProgressBarDialog(Activity context)
    {
        this.context = context;
        initViews();
    }

    public ProgressBarDialog(Activity context, String title, String content, String leftStr,
                             String rightStr)
    {
        this.context = context;
        this.title = title;
        this.content = content;
        this.leftStr = leftStr;
        this.rightStr = rightStr;
        initViews();
    }

    public void setLeftClickListener(View.OnClickListener leftClickListener)
    {
        this.leftClickListener = leftClickListener;
        dialogLeft.setOnClickListener(leftClickListener);
    }

    public void setRightClickListener(View.OnClickListener rightClickListener)
    {
        this.rightClickListener = rightClickListener;
        dialogRight.setOnClickListener(rightClickListener);
    }

    public void setTitle(String title)
    {
        this.title = title;
        dialogTitle.setText(title);
    }

    public void setContent(String content)
    {
        this.content = content;
        dialogContentText.setText(content);
    }

    public void setLeftStr(String leftStr)
    {
        this.leftStr = leftStr;
        dialogLeft.setText(leftStr);
    }

    public void setRightStr(String rightStr)
    {
        this.rightStr = rightStr;
        dialogRight.setText(rightStr);
    }

    public void setProgress(float progress)
    {
        dialogProgress.setVisibility(View.VISIBLE);
        dialogProgressText.setVisibility(View.VISIBLE);
        dialogProgress.setProgress(progress);
        dialogProgressText.setText((int) progress + "%");
    }

    public void setLeftEnable(boolean enable)
    {
        dialogLeft.setEnabled(enable);
        if (enable)
        {
            dialogLeft.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        }
        else
        {
            dialogLeft.setTextColor(context.getResources().getColor(R.color.disable));
        }
    }

    public void setRightEnable(boolean enable)
    {
        dialogRight.setEnabled(enable);
        if (enable)
        {
            dialogRight.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        }
        else
        {
            dialogRight.setTextColor(context.getResources().getColor(R.color.disable));
        }
    }

    private void initViews()
    {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_progress_bar);

        dialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
        dialogContentText = (TextView) dialog.findViewById(R.id.dialog_content);
        dialogLeft = (Button) dialog.findViewById(R.id.dialog_cancel);
        dialogRight = (Button) dialog.findViewById(R.id.dialog_confirm);
        dialogProgress = (RoundProgressBar) dialog.findViewById(R.id.dialog_progress_bar);
        dialogProgressText = (TextView) dialog.findViewById(R.id.dialog_progress_text);

        dialogProgress.setVisibility(View.GONE);
        dialogProgressText.setVisibility(View.GONE);
    }

    public void show()
    {
        if (leftClickListener != null)
        {
            dialogLeft.setOnClickListener(leftClickListener);
        }
        if (rightClickListener != null)
        {
            dialogRight.setOnClickListener(rightClickListener);
        }

        dialogTitle.setText(title);
        dialogContentText.setText(content);
        dialogLeft.setText(leftStr);
        dialogRight.setText(rightStr);
        dialogProgress.setProgress(0.0f);

        dialog.show();
    }
    public boolean isShowing() {
        return dialog.isShowing();
    }

    /**
     * 设置是否可以取消
     *
     * @param flag 是否可以取消
     */
    public void setCancelable(boolean flag)
    {
        dialog.setCancelable(flag);
    }

    /**
     * 设置点击其他区域是否可以取消
     *
     * @param flag 是否可以取消
     */
    public void setCanceledOnTouchOutside(boolean flag)
    {
        dialog.setCanceledOnTouchOutside(false);
    }

    /**
     * 释放Dialog
     */
    public void dismiss()
    {
        dialog.dismiss();
    }
}
