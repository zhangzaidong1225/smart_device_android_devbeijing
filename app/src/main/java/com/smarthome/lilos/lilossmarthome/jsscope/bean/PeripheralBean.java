package com.smarthome.lilos.lilossmarthome.jsscope.bean;

/**
 * Created by Joker on 2016/10/24.
 */

public class PeripheralBean
{
    private String name;
    private String companyDataStr;
    private String deviceId;

    public PeripheralBean()
    {
    }

    public PeripheralBean(String name, String companyDataStr, String deviceId)
    {

        this.name = name;
        this.companyDataStr = companyDataStr;
        this.deviceId = deviceId;
    }

    public String getName()
    {

        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCompanyDataStr()
    {
        return companyDataStr;
    }

    public void setCompanyDataStr(String companyDataStr)
    {
        this.companyDataStr = companyDataStr;
    }

    public String getDeviceId()
    {
        return deviceId;
    }

    public void setDeviceId(String deviceId)
    {
        this.deviceId = deviceId;
    }

    @Override
    public String toString()
    {
        return "PeripheralBean{" +
                "name='" + name + '\'' +
                ", companyDataStr='" + companyDataStr + '\'' +
                ", deviceId='" + deviceId + '\'' +
                '}';
    }
}
