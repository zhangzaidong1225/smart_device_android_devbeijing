package com.smarthome.lilos.lilossmarthome.network.retrofit;

/**
 * 等待进度取消监听
 * Created by Joker on 2016/12/21.
 */

public interface ProgressCancelListener
{
    void onCancelProgress();
}
