package com.smarthome.lilos.lilossmarthome.greendao.utils;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by louis on 2017/4/13.
 */

public class Utils {

    /**
     * 复制数据库到手机指定文件夹下
     *
     * @throws IOException
     */
    public static void copyDataBase() throws IOException {

        String packageName = SmartHomeApplication.mContext.getPackageName();
        String DATABASE_PATH = "/data/data/" + packageName + "/databases/";
        String databaseFilenames = DATABASE_PATH + "location-db";
        File dir = new File(DATABASE_PATH);
        if (!dir.exists())// 判断文件夹是否存在，不存在就新建一个
            dir.mkdir();
        FileOutputStream os = new FileOutputStream(databaseFilenames);// 得到数据库文件的写入流
        InputStream is = SmartHomeApplication.mContext.getResources().openRawResource(R.raw.location);
        byte[] buffer = new byte[8192];
        int count = 0;
        while ((count = is.read(buffer)) > 0) {
            os.write(buffer, 0, count);
            os.flush();
        }
        is.close();
        os.close();
    }

}
