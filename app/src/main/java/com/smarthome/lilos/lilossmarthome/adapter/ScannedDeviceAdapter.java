package com.smarthome.lilos.lilossmarthome.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.device.BaseDevice;
import com.smarthome.lilos.lilossmarthome.network.bean.DeviceTypeListEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceTypeEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 扫描界面展示设备的适配器
 * 用于展示被扫描到的设备
 * Created by Kevin on 2016/7/28.
 */
public class ScannedDeviceAdapter extends BaseAdapter
{
    private List<DeviceTypeEntity> typeListData;
    private ArrayList<BaseDevice> mScannedDevices;
    private LayoutInflater mInflator;
    private Context mContext;
    public ScannedDeviceAdapter(Context context)
    {
        super();
        mContext = context;
        mScannedDevices = new ArrayList<>();
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setTypeListData(
            List<DeviceTypeEntity> typeListData)
    {
        this.typeListData = typeListData;
    }

    public void addDevice(BaseDevice device)
    {
        if (!mScannedDevices.contains(device))
        {
            mScannedDevices.add(device);
        }
    }

    public void removeDevice(BaseDevice device)
    {
        if (mScannedDevices.contains(device))
        {
            mScannedDevices.remove(device);
        }
    }

    public ArrayList<BaseDevice> getDevices()
    {
        return mScannedDevices;
    }

    public BaseDevice getDevice(int position)
    {
        return mScannedDevices.get(position);
    }

    public void clear()
    {
        mScannedDevices.clear();
    }

    public boolean isContained(BaseDevice device)
    {
        for (BaseDevice baseDevice : mScannedDevices)
        {
            if (baseDevice.getAddress().equals(device.getAddress()))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getCount()
    {
        return mScannedDevices.size();
    }

    @Override
    public BaseDevice getItem(int position)
    {
        return mScannedDevices.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewHolder;
        if (convertView == null)
        {
            convertView = mInflator.inflate(R.layout.listitem_device_type, null);
            viewHolder = new ViewHolder();
            viewHolder.mIvDevice = (ImageView) convertView
                    .findViewById(R.id.iv_device);
            viewHolder.deviceName = (TextView) convertView
                    .findViewById(R.id.device_name);
            viewHolder.indicator = (ImageView) convertView.findViewById(R.id.iv_indicator);
            viewHolder.vLine = convertView.findViewById(R.id.v_line);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        BaseDevice device = mScannedDevices.get(position);

        for (DeviceTypeEntity data : typeListData)
        {
            if (device.getTypeId() == data.getId())
            {
                DisplayImageOptions options = new DisplayImageOptions.Builder()
                        .showImageOnLoading(R.drawable.load_error)
                        .showImageOnFail(R.drawable.load_error)
                        .cacheInMemory(true)
                        .cacheOnDisk(true)
                        .build();
                ImageLoader.getInstance().displayImage(data.getIcon(), viewHolder.mIvDevice, options);
            }
        }

        String deviceName = device.getName();
        if (deviceName != null && deviceName.length() > 0)
        {
            viewHolder.deviceName.setText(deviceName);
        }
        else
        {
            viewHolder.deviceName.setText(R.string.unknown_device);
        }
        if (position == getCount() - 1)
        {
            viewHolder.vLine.setVisibility(View.GONE);
        }
        else
        {
            viewHolder.vLine.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    public class ViewHolder
    {
        ImageView mIvDevice;
        TextView deviceName;
        ImageView indicator;
        View vLine;
    }
}
