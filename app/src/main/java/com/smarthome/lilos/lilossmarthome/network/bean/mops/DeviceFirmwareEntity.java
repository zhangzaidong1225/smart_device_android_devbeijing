package com.smarthome.lilos.lilossmarthome.network.bean.mops;

/**
 * Created by Joker on 2016/12/30.
 */

public class DeviceFirmwareEntity
{
    /**
     * version : 34
     * device_type_id : 1
     * is_forced : 1
     * url : https://mops-dev.lianluo.com:883/uploads/20161230/58661dc9735ef.zip
     * md5 : 3314c12f0e9ff01ac8f6b4ff4a83f46a
     * description : 测试固件版本升级34版本
     */

    private String version;
    private int device_type_id;
    private int is_forced;
    private String url;
    private String md5;
    private String description;

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public int getDevice_type_id()
    {
        return device_type_id;
    }

    public void setDevice_type_id(int device_type_id)
    {
        this.device_type_id = device_type_id;
    }

    public int getIs_forced()
    {
        return is_forced;
    }

    public void setIs_forced(int is_forced)
    {
        this.is_forced = is_forced;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getMd5()
    {
        return md5;
    }

    public void setMd5(String md5)
    {
        this.md5 = md5;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
