package com.smarthome.lilos.lilossmarthome.bluetooth.analysis;

import android.bluetooth.BluetoothGattCharacteristic;

import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.bean.WS_Feature;
import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.bean.WS_Measurement;

import java.util.Calendar;

/**
 * 体重秤服务数据解析类
 * Created by Joker on 2016/11/16.
 */

public class WeightScaleUtil
{
    private static WeightScaleListener listener;

    public static void parseWeightScaleFeature(String address,
                                               BluetoothGattCharacteristic characteristic)
    {
        WS_Feature wsf = new WS_Feature();
        int offset = 0;

        int flags = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, offset);
        offset += 4;

        wsf.timestamp = (flags & 0x01) > 0;
        wsf.mulitple = (flags & 0x02) > 0;
        wsf.bmi = (flags & 0x04) > 0;
        wsf.weightMR = (flags & 0x78) >> 3;
        wsf.heightMR = (flags & 0x380) >> 7;

        if (listener != null)
        {
            listener.onWeightScaleFeatureChanged(wsf);
        }
    }

    public static void setWeightMeasurement(String address,
                                            BluetoothGattCharacteristic characteristic)
    {
        WS_Measurement wm = new WS_Measurement();
        int offset = 0;
        int flags = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset++);
        wm.units = flags & 0x01;
        boolean timestampPresent = (flags & 0x02) > 0;
        boolean userIdPresent = (flags & 0x04) > 0;
        boolean bmiHPresent = (flags & 0x08) > 0;

        Calendar calendar = Calendar.getInstance();
        float weightF = -1;
        float heightF = -1;
        int userId = -1;
        int bmi = -1;
        int height = -1;

        int weight = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);

        if (wm.units == 0)
        {
            wm.weight = weight * 0.005f;
        }
        else
        {
            wm.weight = weight * 0.01f;
        }

        offset += 2;

        if (timestampPresent)
        {
            calendar.set(Calendar.YEAR, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset));
            calendar.set(Calendar.MONTH, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 2) - 1); // months are 1-based
            calendar.set(Calendar.DAY_OF_MONTH, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 3));
            calendar.set(Calendar.HOUR_OF_DAY, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 4));
            calendar.set(Calendar.MINUTE, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 5));
            calendar.set(Calendar.SECOND, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 6));
            offset += 7;
        }
        wm.calendar = calendar;

        if (userIdPresent)
        {
            userId = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);
            offset += 1;
        }
        wm.userId = userId;

        if (bmiHPresent)
        {
            bmi = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
            offset += 2;

            height = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);

            if (wm.units == 0)
            {
                heightF = height * 0.001f;
            }
            else
            {
                heightF = height * 0.1f;
            }
        }
        wm.bmi = bmi;
        wm.height = height;

        if (listener != null)
        {
            listener.onWeightMeasurementChanged(wm);
        }
    }

    public static void setListener(WeightScaleListener weightScaleListener)
    {
        listener = null;
        listener = weightScaleListener;
    }

    public static void unbindListener()
    {
        listener = null;
    }

    public interface WeightScaleListener
    {
        void onWeightScaleFeatureChanged(WS_Feature wsf);

        void onWeightMeasurementChanged(WS_Measurement wm);
    }
}
