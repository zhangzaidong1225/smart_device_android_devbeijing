package com.smarthome.lilos.lilossmarthome.bluetooth.analysis;

import android.bluetooth.BluetoothGattCharacteristic;

import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.bean.BP_IntermediateCuffPressure;
import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.bean.BP_Measurement;

import java.util.Calendar;

/**
 * 血压服务上报数据解析
 * Created by Joker on 2016/11/21.
 */

public class BloodPressureUtil
{
    private static BloodPressureListener listener;

    public static void parseBloodPressureMeasurement(String address,
                                                     BluetoothGattCharacteristic characteristic)
    {
        BP_Measurement bpm = new BP_Measurement();

        int offset = 0;
        int flags = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset++);
        bpm.unit = flags & 0x01;
        bpm.timestampPresent = (flags & 0x02) > 0;
        bpm.pulseRatePresent = (flags & 0x04) > 0;

        bpm.systolic = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, offset);
        bpm.diastolic = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, offset + 2);
        bpm.meanArterialPressure = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, offset + 4);
        offset += 6;

        Calendar calendar = Calendar.getInstance();
        if (bpm.timestampPresent)
        {
            calendar.set(Calendar.YEAR, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset));
            calendar.set(Calendar.MONTH, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 2) - 1); // months are 1-based
            calendar.set(Calendar.DAY_OF_MONTH, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 3));
            calendar.set(Calendar.HOUR_OF_DAY, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 4));
            calendar.set(Calendar.MINUTE, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 5));
            calendar.set(Calendar.SECOND, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 6));
            offset += 7;
        }
        bpm.calendar = calendar;

        float pulseRate = -1f;
        if (bpm.pulseRatePresent)
        {
            pulseRate = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, offset);
        }
        bpm.pulseRate = pulseRate;

        if (listener != null)
        {
            listener.onBloodPressureMeasurementChanged(bpm);
        }
    }

    public static void parseIntermediateCuffPressure(String address,
                                                     BluetoothGattCharacteristic characteristic)
    {
        BP_IntermediateCuffPressure icp = new BP_IntermediateCuffPressure();

        int offset = 0;
        int flags = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset++);
        icp.unit = flags & 0x01;
        icp.timestampPresent = (flags & 0x02) > 0;
        icp.pulseRatePresent = (flags & 0x04) > 0;

        icp.cuffPressure = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, offset);
        offset += 6;

        Calendar calendar = Calendar.getInstance();
        if (icp.timestampPresent)
        {
            calendar.set(Calendar.YEAR, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset));
            calendar.set(Calendar.MONTH, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 2) - 1); // months are 1-based
            calendar.set(Calendar.DAY_OF_MONTH, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 3));
            calendar.set(Calendar.HOUR_OF_DAY, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 4));
            calendar.set(Calendar.MINUTE, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 5));
            calendar.set(Calendar.SECOND, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 6));
            offset += 7;
        }
        icp.calendar = calendar;

        float pulseRate = -1f;
        if (icp.pulseRatePresent)
        {
            pulseRate = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, offset);
        }
        icp.pulseRate = pulseRate;

        if (listener != null)
        {
            listener.onIntermediateCuffPressureChanged(icp);
        }
    }

    public static void setListener(BloodPressureListener bpListener)
    {
        listener = null;
        listener = bpListener;
    }

    public static void unbind()
    {
        listener = null;
    }

    public interface BloodPressureListener
    {
        void onBloodPressureMeasurementChanged(BP_Measurement bpm);

        void onIntermediateCuffPressureChanged(BP_IntermediateCuffPressure icp);
    }
}
