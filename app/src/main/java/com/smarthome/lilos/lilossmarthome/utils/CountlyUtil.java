package com.smarthome.lilos.lilossmarthome.utils;

import com.smarthome.lilos.lilossmarthome.network.bean.DeviceTypeListEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceTypeEntity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import ly.count.android.sdk.Countly;

/**
 * Countly工具类
 * 便于Countly的接入
 * Created by Joker on 2017/1/5.
 */

public class CountlyUtil
{
    private static Map<String,String> param = new HashMap<>();
    private static SimpleDateFormat hourFormat = new SimpleDateFormat("HH:00");

    public enum EVENT
    {
        EVENT_HEALTHY_LIFE("HealthyLife"),
        EVENT_FORGOT_PWD("ForgotPwd"),
        EVENT_RESET_PWD("reset_pwd"),
        EVENT_FEEDBACK("Feedback"),
        EVENT_ABOUT("About"),
        EVENT_ADD_DEVICE("AddDevice"),

        EVENT_LOGIN("Login"),
        EVENT_LOGOUT("Logout");

        private String eventName;

        EVENT(String eventName)
        {
            this.eventName = eventName;
        }

        public String getEventName()
        {
            return eventName;
        }
    }

    public enum KEY
    {
        ACCESS_TIME("Access_Time");

        private String keyName;
        KEY(String keyName)
        {
            this.keyName = keyName;
        }

        public String getKeyName()
        {
            return keyName;
        }
    }

    public static void recordEvent(EVENT event,KEY key)
    {
        param.clear();
        param.put(key.getKeyName(),hourFormat.format(new Date(System.currentTimeMillis())));
        Countly.sharedInstance().recordEvent(event.getEventName(),param,1);
    }

    public static void recordEvent(String event,double dur)
    {
        Countly.sharedInstance().recordEvent(event,null,1,0,dur);
    }

    public static void recordControlDurationTime(int deviceTypeId,long dur)
    {
        for (DeviceTypeEntity entity : SharePreferenceUtils.getTypeList())
        {
            if(entity.getId() == deviceTypeId)
            {
                Countly.sharedInstance().recordEvent(entity.getName(),null,1,0,dur);
                return;
            }
        }
    }
}
