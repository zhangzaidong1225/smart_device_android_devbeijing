package com.smarthome.lilos.lilossmarthome.network.bean;

/**
 * Created by Kevin on 2016/7/28.
 */
public class DeviceTypeInfoEntity extends ResponseEntity
{
    private DevTypeInfoData data;

    public DevTypeInfoData getData()
    {
        return data;
    }

    public void setData(DevTypeInfoData data)
    {
        this.data = data;
    }

    public class DevTypeInfoData
    {
        private int typeId;
        private String typeName;
        private String icon;
        private String guiImg;
        private String guiDesc;
        private String desc;
        private String conn;
        private String control_model;
        private String control_url;
        private String control_version;
        private String[] wifi_config_model;
        private String hotspot_ssid_prefix;

        public String getConn()
        {
            return conn;
        }

        public void setConn(String conn)
        {
            this.conn = conn;
        }

        public int getTypeId()
        {
            return typeId;
        }

        public void setTypeId(int typeId)
        {
            this.typeId = typeId;
        }

        public String getTypeName()
        {
            return typeName;
        }

        public void setTypeName(String typeName)
        {
            this.typeName = typeName;
        }

        public String getIcon()
        {
            return icon;
        }

        public void setIcon(String icon)
        {
            this.icon = icon;
        }

        public String getGuiImg()
        {
            return guiImg;
        }

        public void setGuiImg(String guiImg)
        {
            this.guiImg = guiImg;
        }

        public String getGuiDesc()
        {
            return guiDesc;
        }

        public void setGuiDesc(String guiDesc)
        {
            this.guiDesc = guiDesc;
        }

        public String getDesc()
        {
            return desc;
        }

        public void setDesc(String desc)
        {
            this.desc = desc;
        }

        public String getControl_model()
        {
            return control_model;
        }

        public void setControl_model(String control_model)
        {
            this.control_model = control_model;
        }

        public String getControl_url()
        {
            return control_url;
        }

        public void setControl_url(String control_url)
        {
            this.control_url = control_url;
        }

        public String[] getWifi_config_model()
        {
            return wifi_config_model;
        }

        public void setWifi_config_model(String[] wifi_config_model)
        {
            this.wifi_config_model = wifi_config_model;
        }

        public String getHotspot_ssid_prefix()
        {
            return hotspot_ssid_prefix;
        }

        public void setHotspot_ssid_prefix(String hotspot_ssid_prefix)
        {
            this.hotspot_ssid_prefix = hotspot_ssid_prefix;
        }

        public String getControl_version()
        {
            return control_version;
        }

        public void setControl_version(String control_version)
        {
            this.control_version = control_version;
        }
    }
}
