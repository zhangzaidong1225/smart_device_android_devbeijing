package com.smarthome.lilos.lilossmarthome.network.bean.usercenter.entity;

/**
 * Created by Joker on 2016/12/16.
 */

public class RegisterEntity
{
    private String phone;
    private String username;
    private String email;
    private String union_id;

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getUnion_id()
    {
        return union_id;
    }

    public void setUnion_id(String union_id)
    {
        this.union_id = union_id;
    }

    @Override
    public String toString()
    {
        return "RegisterEntity{" +
                "phone='" + phone + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", union_id='" + union_id + '\'' +
                '}';
    }
}
