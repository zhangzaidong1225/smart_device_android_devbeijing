package com.smarthome.lilos.lilossmarthome.utils.control;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.smarthome.lilos.lilossmarthome.device.BaseDevice;
import com.smarthome.lilos.lilossmarthome.device.DeviceManager;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.DownloadMethods;
import com.smarthome.lilos.lilossmarthome.utils.ConvertUtils;

import java.io.File;
import java.util.Map;

import rx.Subscriber;

/**
 * 离线包管理器
 * 用于管理不同设备类型的离线H5页面
 * Created by Joker on 2017/1/20.
 */

public class OfflinePackageManage
{
    private static Context mContext;

    public static void init(Context context)
    {
        mContext = context;
    }

    private static SharedPreferences get()
    {
        return mContext.getSharedPreferences("OFFLINE_PACKAGE", Activity.MODE_PRIVATE);
    }

    public static void setPackageInfo(int deviceTypeId, String version, String path)
    {
        get().edit().putString(deviceTypeId + "ver", version).apply();
        get().edit().putString(deviceTypeId + "path", path).apply();
    }

    public static String getPackageVersion(int deviceTypeId)
    {
        return get().getString(deviceTypeId + "ver", "");
    }

    public static String getPackagePath(int deviceTypeId)
    {
        return get().getString(deviceTypeId + "path", "");
    }

    public static void checkVersion(int deviceTypeId)
    {
        String offlineVer = getPackageVersion(deviceTypeId);
        String newestVer = ControlManager.getHtmlOfflineVersion(deviceTypeId);

        Log.d("Offine","离线版本为"+offlineVer+",线上版本为"+newestVer);

        String path = mContext.getFilesDir().getAbsolutePath() + "/offline/" + deviceTypeId + "_" + newestVer + ".zip";
        String pathD = mContext.getFilesDir().getAbsolutePath() + "/offline/" + deviceTypeId + "_" + newestVer + "";

        Log.d("Offine",",线上版本为"+ newestVer +"存储路径"+pathD);


        File folder = new File(pathD);
        folder.mkdirs();

        if (!offlineVer.equals(newestVer))
        {
            DownloadMethods.getInstance().download(new Subscriber<Boolean>()
            {
                @Override
                public void onCompleted()
                {

                }

                @Override
                public void onError(Throwable e)
                {
                    e.printStackTrace();
                    Log.d("OfflinePackageManage", "发生错误");
                }

                @Override
                public void onNext(Boolean aBoolean)
                {
                    if (aBoolean)
                    {
                        Log.d("OfflinePackageManage", path);
                        Log.d("OfflinePackageManage", "下载完成");
                        try
                        {
                            setPackageInfo(deviceTypeId, newestVer, pathD);
                            ZIP.UnZipFolder(path, pathD);

                            Log.e("OfflinePackageManage  ","deviceTypeId :"+deviceTypeId);
                        } catch (Exception e)
                        {
                            Log.e("OfflinePackageManage", "Er ror");

                            e.printStackTrace();
                        }
                    }
                }
            }, ControlManager.getHtml5Control(deviceTypeId), path);
        }
    }

    public static void updateVersion()
    {
        for (String key : get().getAll().keySet())
        {
            if (key.endsWith("ver"))
            {
                int typeId = ConvertUtils.convertToInt(key.substring(0, key.length() - 3), -1);
                if (typeId == -1)
                {
                    return;
                }
                else
                {
                    checkVersion(typeId);
                }
            }
        }
    }

    public static void updateOfflinePackage()
    {
        for(BaseDevice device : DeviceManager.getInstance().mData.values())
        {
            if(ControlManager.isHtml5OfflineControl(device.getTypeId()))
            {
                Log.d("Offline","当前进行设备号为"+device.getTypeId()+"的设备离线包判断");
                OfflinePackageManage.checkVersion(device.getTypeId());
            }
        }
    }
}