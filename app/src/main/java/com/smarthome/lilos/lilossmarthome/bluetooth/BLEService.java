package com.smarthome.lilos.lilossmarthome.bluetooth;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.orhanobut.logger.Logger;
import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.BatteryUtil;
import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.BloodPressureUtil;
import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.CyclingSpeedCadenceUtil;
import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.GlucoseUtil;
import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.HealthThermometerUtil;
import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.HeartRateUtil;
import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.RunningSpeedCadenceUtil;
import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.UartUtil;
import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.WeightScaleUtil;
import com.smarthome.lilos.lilossmarthome.device.BaseBluetoothDevice;
import com.smarthome.lilos.lilossmarthome.device.DeviceFactory;
import com.smarthome.lilos.lilossmarthome.device.DeviceManager;
import com.smarthome.lilos.lilossmarthome.rxbus.EventKey;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.ConvertUtils;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.login.LoginInterceptor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;

import static com.smarthome.lilos.lilossmarthome.bluetooth.ServiceManager.BPM_CHARACTERISTIC_UUID;
import static com.smarthome.lilos.lilossmarthome.bluetooth.ServiceManager.CSC_MEASUREMENT_CHARACTERISTIC_UUID;
import static com.smarthome.lilos.lilossmarthome.bluetooth.ServiceManager.GM_CHARACTERISTIC;
import static com.smarthome.lilos.lilossmarthome.bluetooth.ServiceManager.HR_CHARACTERISTIC_UUID;
import static com.smarthome.lilos.lilossmarthome.bluetooth.ServiceManager.HR_SENSOR_LOCATION_CHARACTERISTIC_UUID;
import static com.smarthome.lilos.lilossmarthome.bluetooth.ServiceManager.HT_MEASUREMENT_CHARACTERISTIC_UUID;
import static com.smarthome.lilos.lilossmarthome.bluetooth.ServiceManager.ICP_CHARACTERISTIC_UUID;
import static com.smarthome.lilos.lilossmarthome.bluetooth.ServiceManager.RSC_MEASUREMENT_CHARACTERISTIC_UUID;
import static com.smarthome.lilos.lilossmarthome.bluetooth.ServiceManager.TX_CHAR_UUID;
import static com.smarthome.lilos.lilossmarthome.bluetooth.ServiceManager.WS_FEATURE_CHAR_UUID;
import static com.smarthome.lilos.lilossmarthome.bluetooth.ServiceManager.WS_MEASURE_CHAR_UUID;

/**
 * BLE服务
 * 用于蓝牙设备的连接与断开等操作/读取蓝牙设备上报信息/分发处理蓝牙设备上报的连接状态
 */
public class BLEService extends Service
{
    public final static int REQUEST_SCAN = 110;
    public final static int REQUEST_CONNECT = 111;

    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;
    public static final int STATE_AUTO_CONNECT = 3;
    public static final int STATE_MANUAL_CONNECT = 4;

    public final static String EXTRA_DEVICE_NAME = "device_name";
    public final static String EXTRA_DEVICE_ADDR = "device_addr";
    public final static String EXTRA_DEVICE_TYPE = "device_type";
    public final static String EXTRA_CONNECT_STATE = "connect_status";

    public static Map<String, BluetoothGatt> mBluetoothGattMap;
    public static Map<String, Object> mConnectionStateMap;
    public static Map<String, Integer> mBluetoothGattTimeMap;
    public final IBinder mBluetoothLocalBinder = new LocalBinder();
    public List<String> mAddressList;
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private Handler mHandler;
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback()
    {
        private Queue<Request> initQueue;
        private List<BluetoothGattCharacteristic> characteristics = new ArrayList<>();
        private boolean initInProgress = false;

        @Override
        public void onConnectionStateChange(final BluetoothGatt gatt, int status, int newState)
        {
            @Events.EventCode int eventCode;
            Logger.v("连接到GATT服务器.连接状态码：" + status + " 新增状态码：" + newState);

            if (status == BluetoothGatt.GATT_SUCCESS && newState == BluetoothProfile.STATE_CONNECTED)
            {
                eventCode = Events.GATT_CONNECTED;
                mConnectionStateMap.put(gatt.getDevice().getAddress(), STATE_CONNECTED);
//                try
//                {
//                    Thread.sleep(300);
//                } catch (InterruptedException e)
//                {
//                    e.printStackTrace();
//                }
                broadcastUpdate(eventCode, gatt.getDevice().getAddress());

                mHandler.postDelayed(() -> {
                    if (gatt.getDevice().getBondState() != BluetoothDevice.BOND_BONDING)
                    {
                        gatt.discoverServices();
                    }
                }, 600);
            }
            else if (status == 133)
            {
                int times = mBluetoothGattTimeMap.get(gatt.getDevice().getAddress());
                if (times > 0)
                {
                    Logger.i("连接状态码为133，尝试重新连接，当前为第" + (3 - times) + "次");
                    mBluetoothGattTimeMap.put(gatt.getDevice().getAddress(), --times);
                    gatt.connect();
                }
                else
                {
                    gatt.disconnect();
                    Logger.i("连接次数过多，连接超时，设置为断开连接状态");
                    eventCode = Events.GATT_DISCONNECTED;
                    broadcastUpdate(eventCode, gatt.getDevice().getAddress());
                    mConnectionStateMap.put(gatt.getDevice().getAddress(), STATE_DISCONNECTED);
                    mBluetoothGattTimeMap.put(gatt.getDevice().getAddress(), 3);

                    close(gatt.getDevice().getAddress());
                }
            }
            else if (newState == BluetoothProfile.STATE_DISCONNECTED)
            {
                eventCode = Events.GATT_DISCONNECTED;
                broadcastUpdate(eventCode, gatt.getDevice().getAddress());
                mConnectionStateMap.put(gatt.getDevice().getAddress(), STATE_DISCONNECTED);
                Logger.v("与GATT服务器断开连接.");
                return;
            }
            else
            {
                mAddressList.remove(gatt.getDevice().getAddress());
                gatt.disconnect();

                onDeviceDisconnect();
                return;
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status)
        {
            if (status == BluetoothGatt.GATT_SUCCESS)
            {
                if (gatt.getDevice() == null)
                {
                    gatt.disconnect();
                    return;
                }

                if (checkServices(gatt))
                {
                    if (ensureServiceChangedEnabled(gatt))
                    {
                        return;
                    }

                    if (!readBatteryLevel(gatt.getDevice().getAddress()))
                    {
                        nextRequest(gatt.getDevice().getAddress());
                    }
                }
                else
                {
                    disconnect(gatt.getDevice().getAddress());
                    Log.d("bleservice","清理蓝牙缓存");
                    refreshDeviceCache(gatt);
                }

                //发送广播给Web端，通知有哪些服务
                List<String> serviceUuids = new ArrayList<>();
                List<String> characteristics = new ArrayList<>();
                List<BluetoothGattService> services = gatt.getServices();
                for (BluetoothGattService service : services)
                {
                    serviceUuids.add(service.getUuid().toString());
                    List<BluetoothGattCharacteristic> charas = service.getCharacteristics();
                    for (BluetoothGattCharacteristic chara : charas)
                    {
                        characteristics.add(chara.getUuid().toString());
                    }
                }

                Bundle bundle = new Bundle();
                bundle.putString(EventKey.EXTRA_DEVICE_ADDR, gatt.getDevice().getAddress());
                bundle.putString(EventKey.EXTRA_SERVICES, JsonUtils.objectToJson(serviceUuids));
                bundle.putString(EventKey.EXTRA_CHARACTERISTICS, JsonUtils.objectToJson(characteristics));
                RxBus.getInstance().send(Events.DEVICE_BLUETOOTH_SERVICES,bundle);
            }
            else
            {
                Logger.e("onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status)
        {
            if (status == BluetoothGatt.GATT_SUCCESS)
            {
                if (isBatteryLevelCharacteristic(characteristic))
                {
                    BatteryUtil.parseBattery(gatt.getDevice().getAddress(), characteristic);

                    if (setBatteryNotifications(true, gatt.getDevice().getAddress()))
                    {
                        nextRequest(gatt.getDevice().getAddress());
                    }
                }
                else
                {
                    analysis(gatt.getDevice().getAddress(), characteristic);
                    nextRequest(gatt.getDevice().getAddress());
                }
            }
            else if (status == BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION)
            {
                if (gatt.getDevice().getBondState() != BluetoothDevice.BOND_NONE)
                {
                    Log.d("BLECustomCallback", "状态错误");
                    // TODO: 2016/12/7 出错
                }
                else
                {
                    Log.d("BLECustomCallback", "状态错误");
                }
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt,
                                          BluetoothGattCharacteristic characteristic, int status)
        {
            if (status == BluetoothGatt.GATT_SUCCESS)
            {
                // TODO: 2016/12/7 写入完成
                nextRequest(gatt.getDevice().getAddress());
            }
            else if (status == BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION)
            {
                if (gatt.getDevice().getBondState() != BluetoothDevice.BOND_NONE)
                {
                    Log.d("BLECustomCallback", "状态错误");
                    // TODO: 2016/12/7 出错
                }
                else
                {
                    Log.d("BLECustomCallback", "状态错误");
                }
            }
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor,
                                      int status)
        {
            if (status == BluetoothGatt.GATT_SUCCESS)
            {
                if (isServiceChangedCCCD(descriptor))
                {
                    if (!readBatteryLevel(gatt.getDevice().getAddress()))
                    {
                        nextRequest(gatt.getDevice().getAddress());
                    }
                    else if (isBatteryLevelCCCD(descriptor))
                    {
                        byte[] value = descriptor.getValue();
                        if (value != null && value.length > 0 && value[0] == 0x01)
                        {
                            Log.d("BLECustomCallback", "电池等级通知已使能");
                            nextRequest(gatt.getDevice().getAddress());
                        }
                        else
                        {
                            Log.d("BLECustomCallback", "电池等级通知未使能");
                        }
                    }
                    else
                    {
                        nextRequest(gatt.getDevice().getAddress());
                    }
                }
            }
            else if (status == BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION)
            {
                if (gatt.getDevice().getBondState() != BluetoothDevice.BOND_NONE)
                {
                    Log.d("BLECustomCallback", "状态错误");
                    // TODO: 2016/12/7 出错
                }
                else
                {
                    Log.d("BLECustomCallback", "状态错误");
                }
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic)
        {
            if (isBatteryLevelCharacteristic(characteristic))
            {
                BatteryUtil.parseBattery(gatt.getDevice().getAddress(), characteristic);
            }
            else
            {
                BluetoothGattDescriptor cccd = characteristic.getDescriptor(ServiceManager.CCCD);
                boolean notifications = cccd == null || cccd.getValue() == null || cccd.getValue().length != 2 || cccd.getValue()[0] == 0x01;

                analysis(gatt.getDevice().getAddress(), characteristic);
            }
        }

        private void nextRequest(String address)
        {
            Queue<Request> requests = initQueue;
            Request request = requests != null ? requests.poll() : null;

            if (request == null)
            {
                if (initInProgress)
                {
                    initInProgress = false;
                    onDeviceReady(address);
                }
                return;
            }

            switch (request.type)
            {
                case READ:
                    readCharacteristic(request.characteristic, address);
                    break;
                case WRITE:
                    BluetoothGattCharacteristic characteristic = request.characteristic;
                    characteristic.setValue(request.value);
                    writeCharacteristic(characteristic, address);
                    break;
                case ENABLE_NOTIFICATIONS:
                    enableNotifications(request.characteristic, address);
                    break;
                case ENABLE_INDICATIONS:
                    enableIndications(request.characteristic, address);
                    break;
            }
        }

        public void onDeviceDisconnect()
        {
            for (BluetoothGattCharacteristic characteristic : characteristics)
            {
                characteristic = null;
            }
            characteristics.clear();
        }

        private boolean checkServices(BluetoothGatt gatt)
        {
            LinkedList<Request> requests = new LinkedList<>();

            BluetoothGattService service = null;
            service = gatt.getService(ServiceManager.BP_SERVICE_UUID);
            if (service != null)
            {
                BluetoothGattCharacteristic bpmChar = service.getCharacteristic(ICP_CHARACTERISTIC_UUID);
                BluetoothGattCharacteristic icpChar = service.getCharacteristic(BPM_CHARACTERISTIC_UUID);

                if (bpmChar != null)
                {
                    if (icpChar != null)
                    {
                        requests.push(Request.newEnableNotificationsRequest(icpChar));
                        characteristics.add(icpChar);
                    }
                    requests.push(Request.newEnableIndicationsRequest(bpmChar));
                    characteristics.add(bpmChar);
                }
            }

            service = gatt.getService(ServiceManager.CYCLING_SPEED_AND_CADENCE_SERVICE_UUID);
            if (service != null)
            {
                BluetoothGattCharacteristic cscChar = service.getCharacteristic(CSC_MEASUREMENT_CHARACTERISTIC_UUID);
                if (cscChar != null)
                {
                    requests.push(Request.newEnableNotificationsRequest(cscChar));
                    characteristics.add(cscChar);
                }
            }

            service = gatt.getService(ServiceManager.GLS_SERVICE_UUID);
            if (service != null)
            {
                BluetoothGattCharacteristic gmChar = service.getCharacteristic(GM_CHARACTERISTIC);
                BluetoothGattCharacteristic gmcChar = service.getCharacteristic(ServiceManager.GM_CONTEXT_CHARACTERISTIC);
                BluetoothGattCharacteristic racpChar = service.getCharacteristic(ServiceManager.RACP_CHARACTERISTIC);
                if (gmChar != null && racpChar != null)
                {
                    requests.push(Request.newEnableNotificationsRequest(gmChar));
                    characteristics.add(gmcChar);
                    if (gmcChar != null)
                    {
                        requests.push(Request.newEnableNotificationsRequest(gmcChar));
                        characteristics.add(gmcChar);
                    }
                    requests.push(Request.newEnableIndicationsRequest(racpChar));
                    characteristics.add(racpChar);
                }
            }

            service = gatt.getService(ServiceManager.HR_SERVICE_UUID);
            if (service != null)
            {
                BluetoothGattCharacteristic hrChar = service.getCharacteristic(HR_CHARACTERISTIC_UUID);
                BluetoothGattCharacteristic hrlChar = service.getCharacteristic(HR_SENSOR_LOCATION_CHARACTERISTIC_UUID);
                if (hrlChar != null)
                {
                    requests.push(Request.newReadRequest(hrlChar));
                    characteristics.add(hrlChar);
                }
                if (hrChar != null)
                {
                    requests.push(Request.newEnableNotificationsRequest(hrChar));
                    characteristics.add(hrChar);
                }
            }

            service = gatt.getService(ServiceManager.HT_SERVICE_UUID);
            if (service != null)
            {
                BluetoothGattCharacteristic htmChar = service.getCharacteristic(HT_MEASUREMENT_CHARACTERISTIC_UUID);
                if (htmChar != null)
                {
                    requests.push(Request.newEnableIndicationsRequest(htmChar));
                    characteristics.add(htmChar);
                }
            }

            service = gatt.getService(ServiceManager.RUNNING_SPEED_AND_CADENCE_SERVICE_UUID);
            if (service != null)
            {
                BluetoothGattCharacteristic rscChar = service.getCharacteristic(RSC_MEASUREMENT_CHARACTERISTIC_UUID);
                if (rscChar != null)
                {
                    requests.push(Request.newEnableNotificationsRequest(rscChar));
                    characteristics.add(rscChar);
                }
            }

            service = gatt.getService(ServiceManager.WS_SERVICE_UUID);
            if (service != null)
            {
                BluetoothGattCharacteristic wsmChar = service.getCharacteristic(WS_MEASURE_CHAR_UUID);
                BluetoothGattCharacteristic wsfChar = service.getCharacteristic(WS_FEATURE_CHAR_UUID);
                if (wsmChar != null)
                {
                    if (wsfChar != null)
                    {
                        requests.push(Request.newReadRequest(wsfChar));
                        characteristics.add(wsfChar);
                    }
                    requests.push(Request.newEnableIndicationsRequest(wsmChar));
                    characteristics.add(wsmChar);
                }
            }

            service = gatt.getService(ServiceManager.RX_SERVICE_UUID);
            if (service != null)
            {
                BluetoothGattCharacteristic txChar = service.getCharacteristic(TX_CHAR_UUID);
                if (txChar != null)
                {
                    requests.push(Request.newEnableNotificationsRequest(txChar));
                    characteristics.add(txChar);
                }
            }

            if (requests.size() != 0)
            {
                initInProgress = true;
                initQueue = requests;
                return true;
            }
            else
            {
                initInProgress = false;
                initQueue.clear();
                return false;
            }
        }
    };

    /**
     * 判断是否为服务变化
     *
     * @param descriptor
     * @return
     */
    private boolean isServiceChangedCCCD(BluetoothGattDescriptor descriptor)
    {
        if (descriptor == null)
        {
            return false;
        }
        return ServiceManager.SERVICE_CHANGED_CHARACTERISTIC.equals(descriptor.getCharacteristic().getUuid());
    }

    /**
     * 判断是否为电量等级特征上报
     *
     * @param characteristic
     * @return
     */
    private boolean isBatteryLevelCharacteristic(BluetoothGattCharacteristic characteristic)
    {
        if (characteristic == null)
        {
            return false;
        }
        return ServiceManager.BATTERY_LEVEL_CHARACTERISTIC.equals(characteristic.getUuid());
    }

    /**
     * 判断是否为电量等级变化的CCCD
     *
     * @param descriptor
     * @return
     */
    private boolean isBatteryLevelCCCD(BluetoothGattDescriptor descriptor)
    {
        if (descriptor == null)
        {
            return false;
        }
        return ServiceManager.BATTERY_LEVEL_CHARACTERISTIC.equals(descriptor.getCharacteristic().getUuid());
    }

    /**
     * 当设备为绑定状态并且具有通用属性服务以及服务更改特性时，启动这个特性指示。
     *
     * @param gatt
     * @return
     */
    private boolean ensureServiceChangedEnabled(BluetoothGatt gatt)
    {
        if (gatt == null)
        {
            return false;
        }

        BluetoothDevice device = gatt.getDevice();
        if (device.getBondState() != BluetoothDevice.BOND_BONDED)
        {
            return false;
        }

        BluetoothGattService service = gatt.getService(ServiceManager.GENERIC_ATTRIBUTE_SERVICE);
        if (service == null)
        {
            return false;
        }

        BluetoothGattCharacteristic characteristic = service.getCharacteristic(ServiceManager.SERVICE_CHANGED_CHARACTERISTIC);
        if (characteristic == null)
        {
            return false;
        }

        return enableIndications(characteristic, device.getAddress());
    }

    protected boolean enableNotifications(BluetoothGattCharacteristic characteristic,
                                          String address)
    {
        final BluetoothGatt gatt = mBluetoothGattMap.get(address);
        if (gatt == null || characteristic == null)
        {
            return false;
        }

        int properties = characteristic.getProperties();
        if ((properties & BluetoothGattCharacteristic.PROPERTY_NOTIFY) == 0)
        {
            return false;
        }

        gatt.setCharacteristicNotification(characteristic, true);
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(ServiceManager.CCCD);
        if (descriptor != null)
        {
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            return gatt.writeDescriptor(descriptor);
        }
        return false;
    }

    protected boolean enableIndications(BluetoothGattCharacteristic characteristic, String address)
    {
        final BluetoothGatt gatt = mBluetoothGattMap.get(address);
        if (gatt == null || characteristic == null)
        {
            return false;
        }

        int properties = characteristic.getProperties();
        if ((properties & BluetoothGattCharacteristic.PROPERTY_INDICATE) == 0)
        {
            return false;
        }

        gatt.setCharacteristicNotification(characteristic, true);
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(ServiceManager.CCCD);
        if (descriptor != null)
        {
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
            return gatt.writeDescriptor(descriptor);
        }
        return false;
    }

    protected boolean readCharacteristic(BluetoothGattCharacteristic characteristic, String address)
    {
        final BluetoothGatt gatt = mBluetoothGattMap.get(address);
        if (gatt == null || characteristic == null)
        {
            return false;
        }

        int properties = characteristic.getProperties();
        if ((properties & BluetoothGattCharacteristic.PROPERTY_READ) == 0)
        {
            return false;
        }

        return gatt.readCharacteristic(characteristic);
    }

    protected boolean writeCharacteristic(BluetoothGattCharacteristic characteristic,
                                          String address)
    {
        final BluetoothGatt gatt = mBluetoothGattMap.get(address);
        if (gatt == null || characteristic == null)
        {
            return false;
        }

        int properties = characteristic.getProperties();
        if ((properties & (BluetoothGattCharacteristic.PROPERTY_WRITE | BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE)) == 0)
        {
            return false;
        }

        return gatt.writeCharacteristic(characteristic);
    }

    public boolean readBatteryLevel(String address)
    {
        final BluetoothGatt gatt = mBluetoothGattMap.get(address);
        if (gatt == null)
        {
            return false;
        }

        BluetoothGattService batteryService = gatt.getService(ServiceManager.BATTERY_SERVICE);
        if (batteryService == null)
        {
            return false;
        }

        BluetoothGattCharacteristic batteryChar = batteryService.getCharacteristic(ServiceManager.BATTERY_LEVEL_CHARACTERISTIC);
        if (batteryChar == null)
        {
            return false;
        }

        int properties = batteryChar.getProperties();
        if ((properties & BluetoothGattCharacteristic.PROPERTY_READ) == 0)
        {
            return setBatteryNotifications(true, address);
        }

        return readCharacteristic(batteryChar, address);
    }

    public boolean setBatteryNotifications(boolean enable, String address)
    {
        final BluetoothGatt gatt = mBluetoothGattMap.get(address);
        if (gatt == null)
        {
            return false;
        }

        BluetoothGattService service = gatt.getService(ServiceManager.BATTERY_SERVICE);
        if (service == null)
        {
            return false;
        }

        BluetoothGattCharacteristic characteristic = service.getCharacteristic(ServiceManager.BATTERY_LEVEL_CHARACTERISTIC);
        if (characteristic == null)
        {
            return false;
        }

        int properties = characteristic.getProperties();
        if ((properties & BluetoothGattCharacteristic.PROPERTY_NOTIFY) == 0)
        {
            return false;
        }

        gatt.setCharacteristicNotification(characteristic, enable);
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(ServiceManager.CCCD);
        if (descriptor != null)
        {
            if (enable)
            {
                descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            }
            else
            {
                descriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
            }
            return gatt.writeDescriptor(descriptor);
        }
        return false;
    }

    private void onDeviceReady(String address)
    {
        // TODO: 2016/12/8 设备初始化成功
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        mHandler = new Handler();
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return mBluetoothLocalBinder;
    }

    @Override
    public boolean onUnbind(Intent intent)
    {
        close();
        return super.onUnbind(intent);
    }

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize()
    {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null)
        {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null)
            {
                Logger.e("无法初始化蓝牙管理器");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null)
        {
            Logger.e("无法缺的蓝牙适配器");
            return false;
        }

        mAddressList = new ArrayList<>();
        mBluetoothGattMap = new HashMap<>();
        mConnectionStateMap = new HashMap<>();
        mBluetoothGattTimeMap = new HashMap<>();
        return true;
    }

    protected void broadcastUpdate(@Events.EventCode int code,String address)
    {

        RxBus.getInstance().send(code,address);
    }

    public boolean connect(String address)
    {
        Logger.i("开始连接地址为：" + address + "的设备");
        if (mBluetoothAdapter == null || address == null)
        {
            Logger.e("蓝牙适配器没有初始化或者地址非法");
            return false;
        }

        if (mAddressList.indexOf(address) > -1 && mBluetoothGattMap.get(address) != null)
        {
            if (mBluetoothGattMap.get(address).connect())
            {
                mConnectionStateMap.put(address, STATE_CONNECTING);
                Logger.i("GATT列表中存在当前需要连接的设备，申请重新连接成功，正在尝试连接");
                return true;
            }
            else
            {
                Logger.i("GATT列表中存在当前需要连接的设备，申请重新连接失败");
                return false;
            }
        }

        BluetoothDevice device;
        try
        {
            address = address.toUpperCase();
            device = mBluetoothAdapter.getRemoteDevice(address);
        } catch (IllegalArgumentException e)
        {
            Logger.e("MAC地址格式错误");
            return false;
        }

        if (device == null)
        {
            Logger.e("设备未被发现，无法连接设备");
            return false;
        }

        BluetoothGatt gatt = device.connectGatt(this, false, mGattCallback);
        mBluetoothGattMap.put(address, gatt);
        Logger.i("正在尝试重新创建一个连接，用于连接设备" + address);
        if (mAddressList.indexOf(address) == -1)
        {
            mAddressList.add(address);
        }
        if (mBluetoothGattTimeMap.containsKey(address))
        {
            int time = mBluetoothGattTimeMap.get(address);
            mBluetoothGattTimeMap.put(address, time - 1);
        }
        else
        {
            mBluetoothGattTimeMap.put(address, 3);
        }

        mConnectionStateMap.put(address, STATE_CONNECTING);

        return true;
    }

    public void disconnect(String address)
    {
        Log.d("===","disconnect---Bleservice");

        Logger.i("开始与地址为：" + address + "的设备断开连接");
        if (mBluetoothAdapter == null)
        {
            Logger.e("蓝牙适配器未初始化或者未使能");
            return;
        }

        if (!mBluetoothGattMap.containsKey(address))
        {
            Logger.e("当前Gatt列表中不存在地址为" + address + "的设备");
            Logger.json(JsonUtils.objectToJson(mBluetoothGattMap.keySet()));
            return;
        }
        else
        {
            mBluetoothGattMap.get(address).disconnect();
        }
    }

    public void close()
    {
        Logger.v("关闭应用蓝牙服务功能，关闭全部Gatt");
        Iterator iter = mBluetoothGattMap.entrySet().iterator();

        while (iter.hasNext())
        {
            Map.Entry entry = (Map.Entry) iter.next();
            Object val = entry.getValue();
            BluetoothGatt gatt = (BluetoothGatt) val;
            if (gatt != null)
            {
                gatt.close();
            }
        }

        mAddressList.clear();
        mBluetoothGattMap.clear();
        mConnectionStateMap.clear();

        mBluetoothManager = null;
    }

    public void close(String address)
    {
        Logger.v("关闭地址为" + address + "的Gatt");
        Iterator iter = mBluetoothGattMap.entrySet().iterator();

        while (iter.hasNext())
        {
            Map.Entry entry = (Map.Entry) iter.next();
            Object val = entry.getValue();
            BluetoothGatt gatt = (BluetoothGatt) val;
            if (gatt != null && gatt.getDevice().getAddress().equals(address))
            {
                gatt.close();

                iter.remove();
                mAddressList.remove(gatt.getDevice().getAddress());
                mConnectionStateMap.remove(gatt.getDevice().getAddress());
            }
        }
    }

    /**
     * 清楚蓝牙缓存
     * @param gatt
     * @return
     */
    private boolean refreshDeviceCache (BluetoothGatt gatt){

        try {
            BluetoothGatt localBluetoothGatt = gatt;
            Method localMethod = localBluetoothGatt.getClass().getMethod("refresh",new Class[0]);
            localMethod.setAccessible(true);

            if (localMethod != null){
                boolean bool = ((Boolean) localMethod.invoke(localBluetoothGatt,new Object[0])).booleanValue();
                Log.d("Bleservice", "refreshDeviceCache----"+bool);
                return bool;
            }

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return false;

    }
    public void sendManualDelete(@Events.EventCode int code, BaseBluetoothDevice device)
    {
        RxBus.getInstance().send(code,device);
    }
    public void sendAddress(@Events.EventCode int code, String address)
    {
        RxBus.getInstance().send(code,address);
    }

    public void sendData(String address, String value)
    {
        Bundle bundle = new Bundle();
        bundle.putString(EventKey.DEVICE_ADDRESS, address);
        bundle.putString(EventKey.EXTRA_DEV_DATA, value);
        RxBus.getInstance().send(Events.GATT_DATA_AVAILABLE,bundle);
    }

    public void sendData(String address,BaseBluetoothDevice device){
        Bundle bundle = new Bundle();
        bundle.putString(EventKey.PM, address);
        RxBus.getInstance().send(Events.PM25,bundle);

    }

    public void analysis(String address, BluetoothGattCharacteristic characteristic)
    {
        BaseBluetoothDevice device = (BaseBluetoothDevice) DeviceManager.getInstance().getDevice(address);
//        Log.d("test","--"+device.getTypeId()+"");
        if (device != null){
        if (device.getTypeId() == 3){
//                Log.d("test","--1"+device.getTypeId()+"");

            DeviceFactory.parseData(characteristic.getValue(),device);
            sendData(address,device);
//                Log.d("test","--2"+device.getTypeId()+"");

        }
        }

        if (SharePreferenceUtils.getLoginStatus()) {
            if (device.isSubscribe()) {
            sendData(address, ConvertUtils.bytesToHexString(characteristic.getValue()));
        }
        } else {
            sendData(address, ConvertUtils.bytesToHexString(characteristic.getValue()));
        }

        UUID uuid = characteristic.getUuid();
        if (uuid.equals(ICP_CHARACTERISTIC_UUID))
        {
            BloodPressureUtil.parseIntermediateCuffPressure(address, characteristic);
        }
        else if (uuid.equals(BPM_CHARACTERISTIC_UUID))
        {
            BloodPressureUtil.parseBloodPressureMeasurement(address, characteristic);
        }
        else if (uuid.equals(CSC_MEASUREMENT_CHARACTERISTIC_UUID))
        {
            CyclingSpeedCadenceUtil.parseCyclingSpeedCadenceMeasurement(address, characteristic);
        }
        else if (uuid.equals(GM_CHARACTERISTIC))
        {
            GlucoseUtil.parseGlucoseMeasurement(address, characteristic);
        }
        else if (uuid.equals(GM_CHARACTERISTIC))
        {
            GlucoseUtil.parseGlucoseMeasurementContext(address, characteristic);
        }
        else if (uuid.equals(HR_SENSOR_LOCATION_CHARACTERISTIC_UUID))
        {
            HeartRateUtil.parseBodySensorPosition(address, characteristic);
        }
        else if (uuid.equals(HR_CHARACTERISTIC_UUID))
        {
            HeartRateUtil.parseHeartRateMeasurement(address, characteristic);
        }
        else if (uuid.equals(HT_MEASUREMENT_CHARACTERISTIC_UUID))
        {
            HealthThermometerUtil.parseHealthThermometerMeasurement(address, characteristic);
        }
        else if (uuid.equals(RSC_MEASUREMENT_CHARACTERISTIC_UUID))
        {
            RunningSpeedCadenceUtil.parseRunningSpeedCadenceMeasurement(address, characteristic);
        }
        else if (uuid.equals(WS_FEATURE_CHAR_UUID))
        {
            WeightScaleUtil.parseWeightScaleFeature(address, characteristic);
        }
        else if (uuid.equals(WS_MEASURE_CHAR_UUID))
        {
            WeightScaleUtil.setWeightMeasurement(address, characteristic);
        }
        else if (uuid.equals(TX_CHAR_UUID))
        {
            UartUtil.parseUart(address, characteristic);
        }
        else
        {
            Log.d("BluetoothService", "无法解析");
        }
    }

    public void writeUartDeviceCharacteristic(String address, byte[] value)
    {
        if (mBluetoothGattMap.containsKey(address))
        {
            BluetoothGattService rxService = mBluetoothGattMap.get(address).getService(ServiceManager.RX_SERVICE_UUID);

            if (rxService == null)
            {
                broadcastUpdate(Events.DEVICE_NOT_SUPPORT_UART, address);
                return;
            }
            BluetoothGattCharacteristic characteristic = rxService.getCharacteristic(ServiceManager.RX_CHAR_UUID);
            if (characteristic == null)
            {
                broadcastUpdate(Events.DEVICE_NOT_SUPPORT_UART, address);
                return;
            }
            characteristic.setValue(value);
            boolean status = mBluetoothGattMap.get(address).writeCharacteristic(characteristic);
        }
        else
        {
            broadcastUpdate(Events.GATT_DISCONNECTED, address);
        }
    }

    public class LocalBinder extends Binder
    {
        BLEService getService()
        {
            return BLEService.this;
        }
    }
}
