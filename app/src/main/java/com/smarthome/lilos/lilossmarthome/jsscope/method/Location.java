package com.smarthome.lilos.lilossmarthome.jsscope.method;

import android.content.Context;
import android.util.Log;
//import android.webkit.WebView;

import com.smarthome.lilos.lilossmarthome.activity.device.WebControlActivity;
import com.smarthome.lilos.lilossmarthome.jsscope.bean.CallbackBean;
import com.smarthome.lilos.lilossmarthome.network.bean.amap.AMAPGeo;
import com.smarthome.lilos.lilossmarthome.network.retrofit.BaseSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.AMAPMethods;
import com.smarthome.lilos.lilossmarthome.utils.ConvertUtils;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.tencent.smtt.sdk.WebView;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import rx.Subscriber;

/**
 * 对应JSSDK中Location集指令
 */
public class Location
{
    private static CallbackBean bean = new CallbackBean();
    private static Map<String, Object> resutlMap = new HashMap<>();

    public static boolean getLocation(final WebView webView, Map<String, String> params,
                                      final String callBackFuncName)
    {
        Context context = webView.getContext();
        WebControlActivity activity = (WebControlActivity) context;
        activity.getLocationAndCheckPermission(callBackFuncName);
        return true;
    }
    public static boolean searchLocation(final WebView webView, Map<String, String> params,
                                         String callBackFuncName)
    {
        resutlMap.clear();
        bean.callBackName = callBackFuncName;
        String addressStr = params.get("addrs");
        resutlMap.put("address", addressStr);

        Subscriber<AMAPGeo> subscriber = new BaseSubscriber<AMAPGeo>()
        {
            @Override
            public void onNext(AMAPGeo amapGeo)
            {
                if (amapGeo.getGeocodes().size() == 0)
                {
                    resutlMap.put("status", "fail");
                }
                else
                {
                    String locations = amapGeo.getGeocodes().get(0).getLocation();
                    String lngStr = locations.split(",")[0];
                    String latStr = locations.split(",")[1];
                    Double lng = ConvertUtils.convertToDouble(lngStr, 0);
                    Double lat = ConvertUtils.convertToDouble(latStr, 0);
                    DecimalFormat df = new DecimalFormat("0.00000");
                    lngStr = df.format(lng);
                    latStr = df.format(lat);
                    lng = ConvertUtils.convertToDouble(lngStr, 0);
                    lat = ConvertUtils.convertToDouble(latStr, 0);
                    resutlMap.put("status", "success");
                    resutlMap.put("latitude", lat);
                    resutlMap.put("longitude", lng);
                    Log.e("定位====","lng:  "+lng+"lat:  "+lat);
                }
                bean.callBackParams = JsonUtils.objectToJson(resutlMap);
                webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
            }
        };
        AMAPMethods.getInstance().search(subscriber, addressStr);

        return true;
    }
}
