package com.smarthome.lilos.lilossmarthome.activity.splash;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.anthonycr.grant.PermissionsManager;
import com.anthonycr.grant.PermissionsResultAction;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.activity.MainActivity;
import com.smarthome.lilos.lilossmarthome.device.DeviceManager;
import com.smarthome.lilos.lilossmarthome.greendao.GreenDaoHelper;
import com.smarthome.lilos.lilossmarthome.greendao.entity.Area;
import com.smarthome.lilos.lilossmarthome.greendao.entity.SyncManager;
import com.smarthome.lilos.lilossmarthome.greendao.gen.AreaDao;
import com.smarthome.lilos.lilossmarthome.greendao.gen.SyncManagerDao;
import com.smarthome.lilos.lilossmarthome.greendao.utils.GreenDaoUtils;
import com.smarthome.lilos.lilossmarthome.greendao.utils.Utils;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceTypeEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.NewestActivityEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.TokenEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.UserDeviceEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.body.AccessTokenBody;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.entity.AccessTokenEntity;
import com.smarthome.lilos.lilossmarthome.network.retrofit.BaseSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.MopsV2Methods;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.UserCenterMethods;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.SystemUtils;
import com.smarthome.lilos.lilossmarthome.utils.control.ControlManager;
import com.smarthome.lilos.lilossmarthome.utils.control.OfflinePackageManage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;

/**
 * 加载主页面
 * 用于展示APP名称与Logo等信息,并且在后台进行网络数据的预加载
 * 预处理包括:token刷新,设备类型列表刷新,设备列表刷新,最新广告获取
 * 决定进入引导页还是广告页
 */
public class SplashActivity extends BaseActivity
{
    private static final int GET_PERMISSION_SUCCESS = 100;
    private static final int GET_PERMISSION_FAIL = 200;
    private final int GOTO_GUIDANCE_ACTIVITY = 0;
    private final int GOTO_AD_ACTIVITY = 1;
    private final int GOTO_MAIN_ACTIVITY = 2;
    private int targetActivity = -1;

    private AreaDao mLocationAreaDao;
    private SyncManagerDao mSyncManagerDao;;

    private List<String> areaNameCN = new ArrayList<String>();
    private List<String> areaNameEN = new ArrayList<String>();
    private List<String> countryCode = new ArrayList<String>();
    private List<String> attributionCNOne = new ArrayList<String>();
    private List<String> attributionENOne = new ArrayList<String>();
    private List<String> attributionCNTwo = new ArrayList<String>();
    private List<String> attributionENTwo = new ArrayList<String>();

    private MyHandler myHandler = new MyHandler();

    private Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case GOTO_GUIDANCE_ACTIVITY:
                    gotoGuidanceActivity();
                    break;
                case GOTO_AD_ACTIVITY:
                    gotoAdActivity();
                    break;
                case GOTO_MAIN_ACTIVITY:
                    gotoMainActivity();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        Log.d("SplashActivity", "1.Begin init Database.");
        forceUpdateDB();

        tryGetToken();

        if (SystemUtils.getVersionCode(getBaseContext()) != SharePreferenceUtils.getOldVersion())
        {
            requestAllPermission();
            return;
        }
        else if (!SharePreferenceUtils.getADImage().equals(""))
        {
            targetActivity = GOTO_AD_ACTIVITY;
        }
        else
        {
            targetActivity = GOTO_MAIN_ACTIVITY;
        }
        handler.sendEmptyMessageDelayed(targetActivity, 2000);
        tryLoadingDevTypeList();

		new Thread(new Runnable() {
			@Override
			public void run() {
				DeviceManager.getInstance().readData();
//				deviceListOff();
//				refreshData();
			}
		}).start();

    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    private void gotoGuidanceActivity()
    {
        Intent intent = new Intent(getBaseContext(), GuidanceActivity.class);
        startActivity(intent);
        finish();
    }

    private void gotoAdActivity()
    {
        Intent intent = new Intent(getBaseContext(), ADActivity.class);
        startActivity(intent);
        finish();
    }

    private void gotoMainActivity()
    {
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void tryLoadingAD()
    {
        Subscriber<NewestActivityEntity> subscriber = new BaseSubscriber<NewestActivityEntity>()
        {
            @Override
            public void onNext(NewestActivityEntity newestActivityEntity)
            {
                onActivitiesResult(newestActivityEntity);
            }
        };
        MopsV2Methods.getInstance().activities(subscriber);
    }

    private void tryLoadingDevTypeList()
    {
        Subscriber<List<DeviceTypeEntity>> subscriber = new BaseSubscriber<List<DeviceTypeEntity>>()
        {
            @Override
            public void onNext(List<DeviceTypeEntity> deviceTypeEntities)
            {
                ControlManager.setTypeList(deviceTypeEntities);
                OfflinePackageManage.updateVersion();
                tryLoadingAD();
            }

            @Override
            public void onError(Throwable e)
            {
                super.onError(e);
                if (SharePreferenceUtils.getTypeList() == null)
                {
                    Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    System.exit(0);
                }
                else
                {
                    ControlManager.setTypeList(null);
                }
                OfflinePackageManage.updateVersion();
                tryLoadingAD();
            }
        };
        MopsV2Methods.getInstance().types(subscriber);
    }

    private void tryGetDeviceList()
    {
        if (SharePreferenceUtils.getLoginStatus())
        {
            Subscriber<List<UserDeviceEntity>> subscriber = new BaseSubscriber<List<UserDeviceEntity>>()
            {
                @Override
                public void onNext(List<UserDeviceEntity> entities)
                {
                    DeviceManager.getInstance().getDeviceList(entities);
                }

                @Override
                public void onError(Throwable e)
                {
                    DeviceManager.getInstance().readData();
                }
            };
            MopsV2Methods.getInstance().devices(subscriber);
        }
    }

    private void tryGetToken()
    {
        Subscriber<AccessTokenEntity> subscriber = new BaseSubscriber<AccessTokenEntity>()
        {
            @Override
            public void onNext(AccessTokenEntity accessTokenEntity)
            {
                Log.d("SplashActivity", "get token subscriber:" + accessTokenEntity.getAccess_token());
                UserCenterMethods.getInstance().setToken(accessTokenEntity.getAccess_token());
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
            }
        };
        AccessTokenBody body = new AccessTokenBody("5c72f5fc-82cc-31a7-bb9e-42de8586e29f", "_rXe29BIwLs-4nfvGCXFpCNQuvBYMdJp", "client_credentials");
        UserCenterMethods.getInstance().getToken(subscriber, body);
    }

    private void requestAllPermission()
    {
        PermissionsManager.getInstance().requestAllManifestPermissionsIfNecessary(SplashActivity.this, new PermissionsResultAction()
        {
            @Override
            public void onGranted()
            {
                myHandler.sendEmptyMessage(GET_PERMISSION_SUCCESS);
            }

            @Override
            public void onDenied(String permission)
            {
                myHandler.sendEmptyMessage(GET_PERMISSION_FAIL);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults)
    {
        PermissionsManager.getInstance().notifyPermissionsChange(permissions, grantResults);
    }

    private void onActivitiesResult(NewestActivityEntity entity)
    {
        if(entity != null && entity.getName() != null && !entity.getName().equals(""))
        {
            ImageLoader imageLoader = ImageLoader.getInstance();
            DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder()
                    .cacheOnDisk(true)
                    .build();
            imageLoader.loadImage(entity.getUrl(), displayImageOptions, new SimpleImageLoadingListener()
            {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage)
                {
                    super.onLoadingComplete(imageUri, view, loadedImage);
                    SharePreferenceUtils.setADImage(imageUri);
                    SharePreferenceUtils.setADUrl(entity.getUrl());
                }
            });
        }
        else
        {
            SharePreferenceUtils.setADImage("");
            SharePreferenceUtils.setADUrl("");
        }

        if (SharePreferenceUtils.getLoginStatus())
        {
            tryRefreshToken();
        }
    }

    private void tryRefreshToken()
    {
        Subscriber<TokenEntity> subscriber = new BaseSubscriber<TokenEntity>()
        {
            @Override
            public void onNext(TokenEntity tokenEntity)
            {
                UserCenterMethods.getInstance().setToken(tokenEntity.getAccess_token());
                MopsV2Methods.getInstance().setToken(tokenEntity.getAccess_token());
                SharePreferenceUtils.setAccessToken(tokenEntity.getAccess_token());
                SharePreferenceUtils.setRefreshToken(tokenEntity.getRefresh_token());
                tryGetDeviceList();
            }

            @Override
            public void onError(Throwable e)
            {
                SharePreferenceUtils.setLoginStatus(false);
                SmartHomeApplication.mBluetoothService.clearData(0);
                super.onError(e);
            }
        };
        UserCenterMethods.getInstance().refreshToken(subscriber,SharePreferenceUtils.getRefreshToken());
    }

//    private void initDataBase() {
//        mLocationAreaDao = GreenDaoHelper.getLocationDaoSession().getAreaDao();
//        mLocationSyncManagerDao = GreenDaoHelper.getLocationDaoSession().getSyncManagerDao();
//
//        if (GreenDaoUtils.isInitDataBase()) {
//            queryTest();
//            return;
//        }
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                Log.d("SplashActivity", "begin init Database.");
//
//                try {
//                    Utils.copyDataBase();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                Log.d("SplashActivity", String.valueOf(mLocationAreaDao.count()));
//
//                SyncManager syncManager = new SyncManager("InitDataBase", "1.1");
//                mLocationSyncManagerDao.insert(syncManager);
//
//                queryTest();
//            }
//        }).start();
//
//    }

    private void queryTest() {

        List<Area> areaList = (List<Area>) mLocationAreaDao.queryBuilder().where(AreaDao.Properties.AreaNameCN.like("朝阳%")).build().list();
        for (Area area:areaList) {
            Log.d("SplashActivity", area.getAreaNameCN());
            Log.d("SplashActivity", "" + area.getAttributionCNOne());
        }

    }

    private void forceUpdateDB() {
//        GreenDaoHelper.initDatabase();
//        mSyncManagerDao = GreenDaoHelper.getDaoSession().getSyncManagerDao();

        if (GreenDaoUtils.isInitDataBase(GreenDaoHelper.DB_VERSION)) {
            GreenDaoHelper.initLocationDatabase();
            mLocationAreaDao = GreenDaoHelper.getLocationDaoSession().getAreaDao();

            queryTest();
        } else {

            forceSubstitute();

        }
    }

    private void forceSubstitute() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Utils.copyDataBase();
                    GreenDaoUtils.setInitDataBase(GreenDaoHelper.DB_VERSION);

                    GreenDaoHelper.initLocationDatabase();
                    mLocationAreaDao = GreenDaoHelper.getLocationDaoSession().getAreaDao();
                    queryTest();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    private class MyHandler extends Handler
    {
        @Override
        public void handleMessage(Message msg)
        {
            super.handleMessage(msg);
            switch (msg.what)
            {
                case GET_PERMISSION_SUCCESS:
                    break;
                case GET_PERMISSION_FAIL:
                    break;
            }
            if (SystemUtils.getVersionCode(getBaseContext()) != SharePreferenceUtils.getOldVersion())
            {
                targetActivity = GOTO_GUIDANCE_ACTIVITY;
            }
            else if (!SharePreferenceUtils.getADImage().equals(""))
            {
                targetActivity = GOTO_AD_ACTIVITY;
            }
            else
            {
                targetActivity = GOTO_MAIN_ACTIVITY;
            }
            handler.sendEmptyMessageDelayed(targetActivity, 2000);

            tryLoadingDevTypeList();
        }
    }
}
