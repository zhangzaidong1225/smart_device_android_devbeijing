package com.smarthome.lilos.lilossmarthome.wigdet;

import com.smarthome.lilos.lilossmarthome.device.BaseDevice;

/**
 * Created by Kevin on 2016/7/15.
 */
public interface DialogConfirmListener
{
    void postMessage(String message);

    void postDeviceInfo(BaseDevice device);
}
