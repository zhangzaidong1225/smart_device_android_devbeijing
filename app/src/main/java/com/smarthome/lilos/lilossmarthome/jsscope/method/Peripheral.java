package com.smarthome.lilos.lilossmarthome.jsscope.method;

import android.util.Log;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.device.WebControlActivity;
import com.smarthome.lilos.lilossmarthome.jsscope.bean.CallbackBean;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceFirmwareEntity;
import com.smarthome.lilos.lilossmarthome.network.retrofit.BaseSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.MopsV2Methods;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.tencent.smtt.sdk.WebView;

import java.util.HashMap;
import java.util.Map;

import rx.Subscriber;

/**
 * Created by Joker on 2017/2/10.
 */

public class Peripheral {
    private static CallbackBean bean = new CallbackBean();
    private static Map<String, Object> resutlMap = new HashMap<>();

    public static boolean getDeviceInfo(WebView webView, Map<String,String> params, String callBackFuncName)
    {
        resutlMap.clear();

        String deviceId = (String) webView.getTag(R.id.device_mac_address);
        String name = (String) webView.getTag(R.id.device_name);

        resutlMap.put("deviceId",deviceId);
        resutlMap.put("name",name);
        bean.callBackName = callBackFuncName;
        bean.callBackParams = JsonUtils.objectToJson(resutlMap);
        webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
        return true;
    }

    public static boolean getFirmwareInfo(WebView webView, Map<String,String> params, String callBackFuncName)
    {
        Log.d("test","getFirmwareInfo");

        resutlMap.clear();
        int typeId = (int) webView.getTag(R.id.device_type_id);
        Subscriber<DeviceFirmwareEntity> subscriber = new BaseSubscriber<DeviceFirmwareEntity>()
        {
            @Override
            public void onNext(DeviceFirmwareEntity deviceFirmwareEntity)
            {
                bean.callBackName = callBackFuncName;
                Map<String,Object> temp = new HashMap<>();
                temp.put("version",deviceFirmwareEntity.getVersion());
                resutlMap.put("status","success");
                resutlMap.put("firmware",temp);
                bean.callBackParams = JsonUtils.objectToJson(resutlMap);
                webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
            }

            @Override
            public void onError(Throwable e)
            {
                super.onError(e);
                // TODO: 2017/2/17
            }
        };
        MopsV2Methods.getInstance().firmware(subscriber,typeId);

        return true;
    }

    public static boolean notifyDeviceShutDown(WebView webView, Map<String,String> params, String callBackFuncName) {
        String deviceId = (String) webView.getTag(R.id.device_mac_address);
        RxBus.getInstance().send(Events.GATT_DISCONNECTED,deviceId);
        return true;
    }

    public static boolean startUpgradeFirmware(WebView webView, Map<String,String> params, String callBackFuncName)
    {
        Log.d("test","startUpgradeFirmware");

        resutlMap.clear();
        bean.callBackName = callBackFuncName;

        if(webView.getContext() instanceof WebControlActivity)
        {
            String devFirmwareVersion = params.get("currentVersion");
            String devBattery = params.get("battery");
            String minAllowVersion = params.get("minAllowVersion");
            String autoCheck = params.get("autoCheck");

            DeviceFirmwareEntity entity = ((WebControlActivity) webView.getContext()).gotoUpload(devFirmwareVersion,devBattery,minAllowVersion,autoCheck);
            new Thread()
            {
                @Override
                public void run()
                {
                    while(true)
                    {
                        try
                        {
                            Thread.sleep(300);
                        } catch (InterruptedException e)
                        {
                            e.printStackTrace();
                        }
                        if (((WebControlActivity) webView.getContext()).cancelUpload) {
                            Log.d("test","cancelUpload");
                            resutlMap.put("status","cancel");
                            resutlMap.put("msg","");
                            resutlMap.put("isForced","");
                            resutlMap.put("version","");
                            bean.callBackParams = JsonUtils.objectToJson(resutlMap);
                            webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
                            return;
                        }
                        if(!((WebControlActivity) webView.getContext()).startUpload)
                        {
                            if(!((WebControlActivity) webView.getContext()).isUploading){
                                if(((WebControlActivity) webView.getContext()).uploadSuccess)
                                {
                                    // TODO: 2017/2/17 上报成功
                                    resutlMap.put("status","success");
                                    resutlMap.put("msg","");
                                    resutlMap.put("isForced",entity.getIs_forced());
                                    resutlMap.put("version",entity.getVersion());

                                    break;
                                }
                                else
                                {
                                    // TODO: 2017/2/17 上报失败
                                    resutlMap.put("status","failure");
                                    resutlMap.put("msg","Firmware upgrade failed!");// TODO: 2017/2/17 读取失败原因
                                    if (entity != null) {
                                        resutlMap.put("isForced",entity.getIs_forced());
                                        resutlMap.put("version",entity.getVersion());
                                    }

                                    break;
                                }
                            }
                        }
                    }
                    bean.callBackParams = JsonUtils.objectToJson(resutlMap);
                    webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
                }
            }.start();
        }
        else
        {
            resutlMap.put("status","failure");
            resutlMap.put("msg","不支持非UART");
            bean.callBackParams = JsonUtils.objectToJson(resutlMap);
            webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
        }
        return true;
    }
}