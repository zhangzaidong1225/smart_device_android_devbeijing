package com.smarthome.lilos.lilossmarthome.network.bean;

import java.util.ArrayList;

/**
 * Created by Kevin on 2016/7/28.
 */
public class DeviceTypeListEntity extends ResponseEntity
{
    private ArrayList<DevTypeListData> data;

    public ArrayList<DevTypeListData> getData()
    {
        return data;
    }

    public void setData(ArrayList<DevTypeListData> data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "DeviceTypeListEntity{" +
                "data=" + data +
                '}';
    }

    public class DevTypeListData
    {
        private int id;
        private String name;
        private String icon;
        private int status;
        private String control_model;
        private String control_url;
        private String control_version;
        private String bluetooth_server_id;

        public int getTypeId()
        {
            return id;
        }

        public void setTypeId(int typeId)
        {
            this.id = typeId;
        }

        public String getTypeName()
        {
            return name;
        }

        public void setTypeName(String typeName)
        {
            this.name = typeName;
        }

        public String getIcon()
        {
            return icon;
        }

        public void setIcon(String icon)
        {
            this.icon = icon;
        }

        public int getStatus()
        {
            return status;
        }

        public void setStatus(int status)
        {
            this.status = status;
        }

        public String getControl_model()
        {
            return control_model;
        }

        public void setControl_model(String control_model)
        {
            this.control_model = control_model;
        }

        public String getControl_url()
        {
            return control_url;
        }

        public void setControl_url(String control_url)
        {
            this.control_url = control_url;
        }

        public String getBluetooth_server_id()
        {
            return bluetooth_server_id;
        }

        public void setBluetooth_server_id(String bluetooth_server_id)
        {
            this.bluetooth_server_id = bluetooth_server_id;
        }

        public String getControl_version()
        {
            return control_version;
        }

        public void setControl_version(String control_version)
        {
            this.control_version = control_version;
        }

        @Override
        public String toString()
        {
            return "DevTypeListData{" +
                    "typeId=" + id +
                    ", typeName='" + name + '\'' +
                    ", icon='" + icon + '\'' +
                    ", status=" + status +
                    ", control_model='" + control_model + '\'' +
                    ", control_url='" + control_url + '\'' +
                    ", control_version='" + control_version + '\'' +
                    ", bluetooth_server_id='" + bluetooth_server_id + '\'' +
                    '}';
        }
    }
}
