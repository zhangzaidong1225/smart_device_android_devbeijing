package com.smarthome.lilos.lilossmarthome.APConfig;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * Created by Tristan on 2016/8/9.
 */
public class UdpSendUtils extends Thread
{
    public static final int DEFAULT_PORT = 43708;
    private static final String TAG = "UdpSender";
    private static final int MAX_DATA_PACKET_LENGTH = 100;
    private static final int SENDMESSAGE = 1;
    private static String LOG_TAG = "UdpSendUtils";
    boolean flag = false;
    private String dataString;
    private DatagramSocket udpSocket = null;
    private byte[] buffer = new byte[MAX_DATA_PACKET_LENGTH];

    public UdpSendUtils(String dataString)
    {
        this.dataString = dataString;
    }

    public void startUdp()
    {
        flag = true;
        this.start();

    }

    public void stopUdp()
    {
        Log.d(TAG, "stopUdp: udpSocket = " + udpSocket);
        flag = false;
        if (udpSocket != null)
        {
            udpSocket.close();
            udpSocket = null;
        }
    }

    public void sendData(String dataString)
    {
        if (udpSocket != null)
        {
            try
            {
                DatagramPacket dataPacket = null;
                dataPacket = new DatagramPacket(buffer, MAX_DATA_PACKET_LENGTH);
                byte[] data = dataString.getBytes();
                dataPacket.setData(data);
                dataPacket.setLength(data.length);
                dataPacket.setPort(DEFAULT_PORT);

                InetAddress broadcastAddr;
                broadcastAddr = InetAddress.getByName("255.255.255.255");
                dataPacket.setAddress(broadcastAddr);
                udpSocket.send(dataPacket);

                Log.d(TAG, "sendData: ---------------->>UDP发送成功 ");
            } catch (IOException e)
            {
                Log.d(TAG, "sendData: ---------------->>>UDP发送失败  ！！！！！");
                e.printStackTrace();
            }
        }
    }

    @Override
    public void run()
    {
//        Log.d(TAG, "run: UDP 服务开始 》》》》 ");
        try
        {
//            Log.d(TAG, "run: UDP 服务开始 》》》》１ ");
            udpSocket = new DatagramSocket(DEFAULT_PORT);
            udpSocket.setReuseAddress(true);
            while (flag)
            {
                sendData(dataString);
                Thread.sleep(5000);
            }
//            mHandler.sendEmptyMessage(SENDMESSAGE);
//            Log.d(LOG_TAG,"1 udpSocket = "+udpSocket);

        } catch (Exception e)
        {

            Log.e(LOG_TAG, e.toString());
        }

//        Log.d(LOG_TAG,"2 udpSocket = "+udpSocket);
    }
}
