package com.smarthome.lilos.lilossmarthome.network.bean.usercenter.body;

/**
 * Created by Joker on 2016/12/21.
 */

public class ModPwdBody
{
    /**
     * phone : 17083300514
     * password : pa55w0rd
     * old_password : pa66w0rd
     */

    private String phone;
    private String password;
    private String old_password;

    public ModPwdBody(String phone, String password, String old_password)
    {
        this.phone = phone;
        this.password = password;
        this.old_password = old_password;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getOld_password()
    {
        return old_password;
    }

    public void setOld_password(String old_password)
    {
        this.old_password = old_password;
    }
}
