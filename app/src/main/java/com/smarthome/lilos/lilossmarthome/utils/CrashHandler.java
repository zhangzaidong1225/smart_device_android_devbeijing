package com.smarthome.lilos.lilossmarthome.utils;

import android.content.Context;
import android.os.Process;

import com.orhanobut.logger.Logger;
import com.smarthome.lilos.lilossmarthome.R;

import ly.count.android.sdk.Countly;

/**
 * 崩溃信息采集类
 * 用于采集系统的意外崩溃信息,用于Debug
 * Created by Joker on 2016/11/2.
 */

public class CrashHandler implements Thread.UncaughtExceptionHandler
{
    private static CrashHandler INSTANCE;
    private Thread.UncaughtExceptionHandler mDefaultHandler;
    private Context mContext;

    private CrashHandler()
    {
    }

    public static CrashHandler getInstance()
    {
        if (INSTANCE == null)
        {
            INSTANCE = new CrashHandler();
        }
        return INSTANCE;
    }

    public void init(Context context)
    {
        mContext = context;
        mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    @Override
    public void uncaughtException(Thread t, Throwable e)
    {
        if (!handleException(e) && mDefaultHandler != null)
        {
            mDefaultHandler.uncaughtException(t, e);
        }
        else
        {
            AppToast.showToast(R.string.app_error);
            try
            {
                Thread.sleep(2000);
            } catch (InterruptedException e1)
            {
                Logger.e(e1, "异常");
            }
            Process.killProcess(Process.myPid());
            System.exit(1);
        }
    }

    private boolean handleException(Throwable ex)
    {
        if (ex instanceof Exception)
        {
            Countly.sharedInstance().logException((Exception) ex);
            ex.printStackTrace();
            return true;
        }
        else
        {
            return false;
        }
    }
}