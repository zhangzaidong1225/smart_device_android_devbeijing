package com.smarthome.lilos.lilossmarthome.network.bean.usercenter.entity;

/**
 * Created by Joker on 2016/12/21.
 */

public class SMSEntity
{
    /**
     * code : 验证码
     * phone : 手机号码
     */

    private String code;
    private String phone;

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }
}
