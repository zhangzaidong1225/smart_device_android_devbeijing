package com.smarthome.lilos.lilossmarthome.bluetooth.analysis.bean;

import java.util.Calendar;

/**
 * Created by Joker on 2016/11/21.
 */

public class BP_Measurement
{
    public int unit;
    public boolean timestampPresent;
    public boolean pulseRatePresent;
    public float systolic;
    public float diastolic;
    public float meanArterialPressure;
    public Calendar calendar;
    public float pulseRate;
}
