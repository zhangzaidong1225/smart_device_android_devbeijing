package com.smarthome.lilos.lilossmarthome.utils;

/**
 * 正则判断工具类
 * Created by Joker on 2016/8/3.
 */
public class RegexpUtils
{
    public static boolean isPhoneNum(String phone)
    {
        return phone.matches("^((13[0-9])|14[5,7]|15[0-9]|18[0-9]|17[6-8]|170)\\d{8}$");
    }

    public static boolean isMixPassword(String password)
    {
        return password.matches(".*[a-zA-Z].*[0-9]|.*[0-9].*[a-zA-Z]");
    }
}
