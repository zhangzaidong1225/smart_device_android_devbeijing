package com.smarthome.lilos.lilossmarthome.adapter;

import android.content.Context;
import android.view.View;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.adapter.abslistview.CommonAdapter;
import com.smarthome.lilos.lilossmarthome.adapter.abslistview.ViewHolder;
import com.smarthome.lilos.lilossmarthome.network.bean.DeviceTypeListEntity;

import java.util.List;

/**
 * 设备类型适配器
 * 用于设备类型选择界面中展示设备类型
 * Created by Kevin on 2016/7/5.
 */
public class DeviceTypeAdapter extends CommonAdapter<DeviceTypeListEntity.DevTypeListData>
{
    public DeviceTypeAdapter(Context context,
                             List<DeviceTypeListEntity.DevTypeListData> mDatas,
                             int itemLayoutId,
                             DisplayImageOptions displayImageOptions)
    {
        super(context, mDatas, itemLayoutId, displayImageOptions);
    }

    public DeviceTypeAdapter(Context context,
                             List<DeviceTypeListEntity.DevTypeListData> mDatas,
                             int itemLayoutId)
    {
        super(context, mDatas, itemLayoutId);
    }

    @Override
    public void convert(ViewHolder holder, DeviceTypeListEntity.DevTypeListData item,
                        int position)
    {
        holder.setImageUrl(R.id.iv_device, item.getIcon());
        holder.setText(R.id.device_name, item.getTypeName());
        if (position == getCount() - 1)
        {
            holder.setVisibility(R.id.v_line, View.GONE);
        }
        else
        {
            holder.setVisibility(R.id.v_line, View.VISIBLE);
        }
    }
}
