package com.smarthome.lilos.lilossmarthome.greendao.upgrade;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.github.yuweiguocn.library.greendao.MigrationHelper;
import com.smarthome.lilos.lilossmarthome.greendao.gen.AreaDao;
import com.smarthome.lilos.lilossmarthome.greendao.gen.DaoMaster;
import com.smarthome.lilos.lilossmarthome.greendao.gen.SyncManagerDao;

/**
 * Created by louis on 2017/4/13.
 */

public class GreenDaoOpenHelper extends DaoMaster.OpenHelper {
    public GreenDaoOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory) {
        super(context, name, factory);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        MigrationHelper.migrate(db, AreaDao.class, SyncManagerDao.class);
    }
}
