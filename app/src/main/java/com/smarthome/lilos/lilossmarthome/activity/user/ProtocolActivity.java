package com.smarthome.lilos.lilossmarthome.activity.user;

import android.os.Bundle;
import android.view.View;
//import android.webkit.WebResourceError;
//import android.webkit.WebResourceRequest;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.network.URLManager;
import com.tencent.smtt.export.external.interfaces.WebResourceError;
import com.tencent.smtt.export.external.interfaces.WebResourceRequest;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;
import com.umeng.analytics.MobclickAgent;

/**
 * 协议展示页面
 * 用于展示给用户<用户协议>
 */
public class ProtocolActivity extends BaseActivity implements View.OnClickListener
{
    private ImageView mIvBack;
    private WebView mWvProtocol;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_protocol);

        init();
    }

    private void init()
    {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mWvProtocol = (WebView) findViewById(R.id.wv_protocol);

        mWvProtocol.getSettings().setJavaScriptEnabled(true);
        mWvProtocol.loadUrl(new URLManager().getProtocolUrl());
        mWvProtocol.setWebViewClient(new WebViewClient()
        {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request,
                                        WebResourceError error)
            {
                super.onReceivedError(view, request, error);
                view.loadUrl("file:///android_asset/nowifi.html");
            }

            @Override
            public void onPageFinished(WebView view, String url)
            {
                view.loadUrl("javascript:setRelaodUrl('" + new URLManager().getProtocolUrl() + "')");
                super.onPageFinished(view, url);
            }
        });

        mIvBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.iv_back:
                MobclickAgent.onEvent(this, "ProtocolActivity_back");
                finish();
                break;
            default:
                break;
        }
    }
}
