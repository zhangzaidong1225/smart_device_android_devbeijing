package com.smarthome.lilos.lilossmarthome.bluetooth.analysis;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

/**
 * Uart设备读写服务上报数据解析类
 * Created by Joker on 2016/12/8.
 */

public class UartUtil
{
    public static UartListener listener;

    public static void parseUart(String address, BluetoothGattCharacteristic characteristic)
    {
        if (listener != null)
        {
            listener.onDataChanged(address,characteristic.getValue());
        }
    }

    public static void setListener(UartListener xListener)
    {
        listener = xListener;
    }

    public static void unbind()
    {
        listener = null;
    }

    public interface UartListener
    {
        void onDataChanged(String address,byte[] values);
    }
}
