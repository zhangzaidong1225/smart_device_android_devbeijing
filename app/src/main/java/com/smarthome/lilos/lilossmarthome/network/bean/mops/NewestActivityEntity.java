package com.smarthome.lilos.lilossmarthome.network.bean.mops;

/**
 * Created by Joker on 2016/12/30.
 */

public class NewestActivityEntity
{
    /**
     * name : 问卷调查
     * url : http://mops-questionnaire.lianluo.com/Weixin/Interview/index.html?questionnaireID=20
     * img : http://139.198.9.171/uploads/activity/08f3ca086e80b1c5172b5745b2077f77.png
     */

    private String name;
    private String url;
    private String img;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getImg()
    {
        return img;
    }

    public void setImg(String img)
    {
        this.img = img;
    }
}
