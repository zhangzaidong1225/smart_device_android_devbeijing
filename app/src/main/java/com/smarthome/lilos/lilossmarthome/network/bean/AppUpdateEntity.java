package com.smarthome.lilos.lilossmarthome.network.bean;

/**
 * Created by Kevin on 2016/7/26.
 */
public class AppUpdateEntity extends ResponseEntity
{

    private AppUpdateData data;

    public AppUpdateData getData()
    {
        return data;
    }

    public void setData(AppUpdateData data)
    {
        this.data = data;
    }

    public class AppUpdateData
    {
        private String url;
        private String info;
        private String ver;
        private String md5;
        private int force;

        public String getUrl()
        {
            return url;
        }

        public void setUrl(String url)
        {
            this.url = url;
        }

        public String getInfo()
        {
            return info;
        }

        public void setInfo(String info)
        {
            this.info = info;
        }

        public String getVer()
        {
            return ver;
        }

        public void setVer(String ver)
        {
            this.ver = ver;
        }

        public String getMd5()
        {
            return md5;
        }

        public void setMd5(String md5)
        {
            this.md5 = md5;
        }

        public int getForce()
        {
            return force;
        }

        public void setForce(int force)
        {
            this.force = force;
        }

    }
}
