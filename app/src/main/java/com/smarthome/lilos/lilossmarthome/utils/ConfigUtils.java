package com.smarthome.lilos.lilossmarthome.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.smarthome.lilos.lilossmarthome.network.retrofit.request.MopsV2Methods;

/**
 * 配置工具类
 * 可以对APP进行不同的配置
 * 当前仅支持服务器切换
 * Created by Joker on 2016/9/12.
 */
public class ConfigUtils
{
    public final static int ONLINE_SERVER = 1001;
    public final static int TEST_SERVER = 1002;
    private final static String SERVER_ADDRESS = "server_address";
    private static Context mContext;

    public static void init(Context context)
    {
        mContext = context;
    }

    private static SharedPreferences get()
    {
        return mContext.getSharedPreferences("SmartHomeConfig", Activity.MODE_PRIVATE);
    }

    public static int getServer()
    {
        return get().getInt(SERVER_ADDRESS, TEST_SERVER);
    }

    public static void setServer(int server)
    {
        get().edit().putInt(SERVER_ADDRESS, server).commit();
        // TODO: 2017/2/16 切换服务器
    }
}
