package com.smarthome.lilos.lilossmarthome.network;

/**
 * 第三方URL管理
 * Created by Joker on 2016/8/28.
 */
public class ThirdPartyURLManager
{
    public static final String AMAP_LOCATIONS_TO_AMAP = "http://restapi.amap.com/v3/assistant/coordinate/convert?";
    public static final String AMAP_ADDRESS = "http://restapi.amap.com/v3/geocode/regeo?";
    public static final String AMAP_SEARCH_ADDRESS = "http://restapi.amap.com/v3/geocode/geo?";
    private static final String KEY = "db50eba84b76f8ccccb87a0c2c0b034a";

    public static String getLocationsToAmap(String locations)
    {
        StringBuilder sb = new StringBuilder(AMAP_LOCATIONS_TO_AMAP);
        sb.append("key=");
        sb.append(KEY);
        sb.append("&locations=");
        sb.append(locations);
        sb.append("&coordsys=gps");
        return sb.toString().trim();
    }

    public static String getAddress(String locations)
    {
        StringBuilder sb = new StringBuilder(AMAP_ADDRESS);
        sb.append("key=");
        sb.append(KEY);
        sb.append("&location=");
        sb.append(locations);
        sb.append("&radius=0&extensions=base&batch=false&roadlevel=1");
        return sb.toString().trim();
    }

    public static String searchAddress(String address)
    {
        StringBuilder sb = new StringBuilder(AMAP_SEARCH_ADDRESS);
        sb.append("key=");
        sb.append(KEY);
        sb.append("&address=");
        sb.append(address);
        return sb.toString().trim();
    }
}
