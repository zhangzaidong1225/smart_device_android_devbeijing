package com.smarthome.lilos.lilossmarthome.network;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.TrafficStats;
import android.os.Handler;
import android.os.Message;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 检测当前网速
 * Android提供有获取当前总流量的方法
 * 上一秒 减去 下一秒的流量差便是网速
 * Created by songyu on 2017/3/27.
 */

public class NetworkSpeed {
    private final static String TAG = "NetworkSpeed";
    private long preRxBytes = 0;
    private Timer mTimer = null;
    private Context mContext;
    private static NetworkSpeed mNetworkSpeed;
    private Handler mHandler;

    private NetworkSpeed(Context mContext, Handler mHandler) {
        this.mContext = mContext;
        this.mHandler = mHandler;
    }

    public static NetworkSpeed getInstant(Context mContext, Handler mHandler) {
        if (mNetworkSpeed == null) {
            mNetworkSpeed = new NetworkSpeed(mContext, mHandler);
        }
        return mNetworkSpeed;
    }

    /**
     * 当前流量
     * @return
     */
    private long getNetworkRxBytes() {
        int currentUid = getUid();
        if (currentUid < 0) {
            return 0;
        }
        long rxBytes = TrafficStats.getUidRxBytes(currentUid);//获取某个网络UID的接受字节数
        if (rxBytes == TrafficStats.UNSUPPORTED) {
            rxBytes = TrafficStats.getTotalRxBytes();
        }
        return rxBytes;
    }

    /**
     * 获取当前网速
     * @return
     */
    public int getNetworkSpeed() {

        long curRxBytes = getNetworkRxBytes();
        long bytes = curRxBytes - preRxBytes;
        preRxBytes = curRxBytes;
        int kb = (int) Math.floor(bytes / 1024 + 0.5);
        return kb;
    }

    /**
     * 开始检测
     */
    public void startCalculateNetworkSpeed() {
        preRxBytes = getNetworkRxBytes();
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
        if (mTimer == null) {
            mTimer = new Timer();
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    Message msg = new Message();
                    msg.what = 1;
                    msg.arg1 = getNetworkSpeed();
                    mHandler.sendMessage(msg);
                }
            }, 1000, 1000);//1s后启动任务，每2s执行一次
        }
    }

    /**
     * 停止检测
     */
    public void stopCalculateNetworkSpeed() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    private int getUid() {
        try {
            PackageManager pm = mContext.getPackageManager();
            ApplicationInfo ai = pm.getApplicationInfo(
                    mContext.getPackageName(), PackageManager.GET_CONFIGURATIONS);
            return ai.uid;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
