package com.smarthome.lilos.lilossmarthome.wigdet;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smarthome.lilos.lilossmarthome.R;

/**
 * Created by Joker on 2016/8/9.
 */
public class LoadingLayout extends RelativeLayout
{
    private String loadingContent;
    private String loadingContentFail;
    private String reloadingButton;

    private TextView mTvContent;
    private Button mBtnReloading;

    private OnReloadingClickListener listener;

    public LoadingLayout(Context context)
    {
        this(context, null);
    }

    public LoadingLayout(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public LoadingLayout(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);

        TypedArray mTypedArray = context.obtainStyledAttributes(attrs, R.styleable.LoadingLayout);
        loadingContent = mTypedArray.getString(R.styleable.LoadingLayout_loadingContent);
        loadingContentFail = mTypedArray.getString(R.styleable.LoadingLayout_loadingContentFail);
        reloadingButton = mTypedArray.getString(R.styleable.LoadingLayout_reloadingButton);
        mTypedArray.recycle();
        LayoutInflater.from(context).inflate(R.layout.view_reloading, this, true);
        mTvContent = (TextView) findViewById(R.id.content);
        mBtnReloading = (Button) findViewById(R.id.reloading);

        mTvContent.setText(loadingContent);
        mBtnReloading.setText(reloadingButton);
        mBtnReloading.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                show();
                listener.reloading();
            }
        });

        setVisibility(GONE);
    }

    public void setOnReloadingClickListener(
            OnReloadingClickListener listener)
    {
        this.listener = listener;
    }

    public void setLoadingContent(String loadingContent)
    {
        this.loadingContent = loadingContent;
        mTvContent.setText(loadingContent);
    }

    public void setLoadingContentFail(String loadingContentFail)
    {
        this.loadingContentFail = loadingContentFail;
    }

    public void setReloadingButton(String reloadingButton)
    {
        this.reloadingButton = reloadingButton;
        mBtnReloading.setText(reloadingButton);
    }

    public void show()
    {
        if (getVisibility() == GONE)
        {
            setVisibility(VISIBLE);
            mTvContent.setText(loadingContent);
            mBtnReloading.setVisibility(GONE);
        }
    }

    public void fail()
    {
        if (getVisibility() == VISIBLE)
        {
            mTvContent.setText(loadingContentFail);
            mBtnReloading.setVisibility(VISIBLE);
        }
    }

    public void success()
    {
        setVisibility(GONE);
    }

    public interface OnReloadingClickListener
    {
        void reloading();
    }
}
