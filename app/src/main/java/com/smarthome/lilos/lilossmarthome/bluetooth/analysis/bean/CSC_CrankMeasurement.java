package com.smarthome.lilos.lilossmarthome.bluetooth.analysis.bean;

/**
 * Created by Joker on 2016/11/22.
 */

public class CSC_CrankMeasurement
{
    public float gearRatio;
    public int crankCadence;
}
