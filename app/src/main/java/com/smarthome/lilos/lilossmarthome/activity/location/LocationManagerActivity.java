package com.smarthome.lilos.lilossmarthome.activity.location;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.adapter.LocationAdapter;
import com.smarthome.lilos.lilossmarthome.adapter.SearchLocationAdapter;
import com.smarthome.lilos.lilossmarthome.greendao.GreenDaoHelper;
import com.smarthome.lilos.lilossmarthome.greendao.entity.Area;
import com.smarthome.lilos.lilossmarthome.greendao.gen.AreaDao;
import com.smarthome.lilos.lilossmarthome.greendao.utils.Cn2Spell;
import com.smarthome.lilos.lilossmarthome.jsscope.holder.LocationHolder;
import com.smarthome.lilos.lilossmarthome.network.bean.amap.AMAPAddress;
import com.smarthome.lilos.lilossmarthome.network.bean.location.CityListEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.pm25.WeatherData;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.FirstPageMethods;
import com.smarthome.lilos.lilossmarthome.rxbus.EventKey;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.umeng.analytics.MobclickAgent;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import rx.Subscriber;

/**
 * Created by louis on 2017/4/11.
 */

public class LocationManagerActivity extends BaseActivity
        implements AdapterView.OnItemClickListener{

    public static String SP_CURRENT_AREA_ID_LIST = "sp_current_area_id_list";
    public static String SP_RECENT_AREA_ID_LIST = "sp_recent_area_id_list";
    public static String SP_SEARCH_AREA_ID_LIST = "sp_search_area_id_list";
    public static String SP_SELECTED_AREA_ID = "sp_selected_area_id";

    private List<String> mCurrentLocationList = new ArrayList<>();
    private List<String> mCurrentAreaIdList = new ArrayList<>();
    private GridView mCurrentLocationGridView;
    private LocationAdapter mCurrentLocationAdapter;


    private List<String> mRecentLocationList = new ArrayList<>();
    private List<String> mRecentAreaIdList = new ArrayList<>();
    private GridView mRecentLocationGridView;
    private LocationAdapter mRecentLocationAdapter;


    private List<String> mHotLocationList = new ArrayList<>();
    // 北京 上海 广州 深圳
    // 成都 杭州 沈阳 哈尔滨
    // 重庆 天津 武汉 西安
    private List<String> mHotAreaIdList = new ArrayList<String>(){{
        add("WX4FBXXFKE4F"); add("WTW3SJ5ZBJUY"); add("WS0E9D8WN298"); add("WS10730EM8EV");
        add("WM6N2PM3WY2K"); add("WTMKQ069CCJ7"); add("WXRVB9QYXKY8"); add("YB1UX38K6DY1");
        add("WM7B0X53DZW2"); add("WWGQDCW6TBW1"); add("WT3Q0FW9ZJ3Q"); add("WQJ6YY8MHZP0");
    }};
    private GridView mHotLocationGridView;
    private LocationAdapter mHotLocationAdapter;

    private List<String> mSearchLocationList = new ArrayList<>();
    private List<String> mSearchLocationListAreadId = new ArrayList<>();
    private SearchLocationAdapter mSearchLocationAdapter;
    private ListView mSearchLocationListView;

    private AreaDao mLocationAreaDao;

    private EditText mSearchLocationEditText;

    private ImageView iv_back;

//    private static final int LOCATION_MSG = 0xF1;

//    private LocationHandler mHandler = new LocationHandler(this);
//
//     class LocationHandler extends Handler {
//        private WeakReference<Context> reference;
//         LocationHandler (Context context) {
//            reference = new WeakReference<Context>(context);
//        }
//
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            LocationManagerActivity activity = (LocationManagerActivity) reference.get();
//            if (activity != null){
//                switch (msg.what){
//                    case LOCATION_MSG:
//                        mCurrentLocationList.clear();
//                        String locate = msg.obj.toString();
//                        Log.d("===Locate",locate);
//                        if (locate != null) {
//                            if (locate.contains("区")) {
//                                int area = locate.indexOf("区");
//                                mCurrentLocationList.add(locate.substring(0, area));
//                            } else {
//                                mCurrentLocationList.add(locate);
//                            }
//                        } else {
//                            mCurrentLocationList.add("北京");
//                        }
//                        mCurrentLocationAdapter.notifyDataSetChanged();
//
//                        break;
//                }
//            }
//        }
//    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_manager);
//        location = getIntent().getStringExtra("location");

        FirstPageMethods.getInstance().init();

        initView();
        initData();

        getLocationAndCheckPermission();
    }

    private void initView() {
        //存储数据类型，当前定位城市 当前选中城市 最近访问城市
        //判别当前定位城市，是否改变的条件是：若大范围定到小范围，不提示改变；若小范围定到大范围，提示改变

        mCurrentLocationGridView = (GridView) findViewById(R.id.current_location_gridView);
        mCurrentLocationGridView.setOnItemClickListener(this);

        mRecentLocationGridView = (GridView) findViewById(R.id.recent_location_gridView);
        mRecentLocationGridView.setOnItemClickListener(this);

        mHotLocationGridView = (GridView) findViewById(R.id.hot_location_gridView);
        mHotLocationGridView.setOnItemClickListener(this);

        mSearchLocationListView = (ListView) findViewById(R.id.lv_search_locations);
        mSearchLocationListView.setOnItemClickListener(this);
        mSearchLocationEditText = (EditText) findViewById(R.id.et_search_location);
        mSearchLocationEditText.setOnEditorActionListener(((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE)
            {

            }
            return false;
        }));

        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener( v -> {
            MobclickAgent.onEvent(this, "LocationManagerActivity_back");
            finish();
        });
    }

    private void initData() {
        mLocationAreaDao = GreenDaoHelper.getLocationDaoSession().getAreaDao();

        //当前定位地理位置 有两个数据来源，一个是定位接口，另一个是存放在本地的当前地理位置
        mCurrentAreaIdList = SharePreferenceUtils.getAreaIdList(SP_CURRENT_AREA_ID_LIST);

        if (mCurrentAreaIdList == null) {
            mCurrentAreaIdList = new ArrayList<>();
            mCurrentLocationList.add("--");
        } else {
//            Log.d("LocationManagerActivity", "mCurrentAreaIdList:" + mCurrentAreaIdList.get(0));
            mCurrentLocationList.addAll(displayLocationList(mCurrentAreaIdList));
        }
        mCurrentLocationAdapter = new LocationAdapter(getBaseContext(), mCurrentLocationList, R.layout.item_location);
        mCurrentLocationGridView.setAdapter(mCurrentLocationAdapter);
        mCurrentLocationGridView.setOnItemClickListener(this);
        mCurrentLocationAdapter.notifyDataSetChanged();


        //最近访问城市，最多显示5个，就是选择的城市，也就是
        mRecentAreaIdList = SharePreferenceUtils.getAreaIdList(SP_RECENT_AREA_ID_LIST);
        if (mRecentAreaIdList == null) {
            //TODO 隐藏最近选择城市列表
            mRecentAreaIdList = new ArrayList<>();
            mRecentLocationList.add("--");
        } else {
            mRecentLocationList.addAll(displayLocationList(mRecentAreaIdList));
        }
        mRecentLocationAdapter = new LocationAdapter(getBaseContext(), mRecentLocationList, R.layout.item_location);
        mRecentLocationGridView.setAdapter(mRecentLocationAdapter);
        mRecentLocationGridView.setOnItemClickListener(this);
        mRecentLocationAdapter.notifyDataSetChanged();

        mHotLocationList.addAll(displayLocationList(mHotAreaIdList));
        mHotLocationAdapter = new LocationAdapter(getBaseContext(), mHotLocationList, R.layout.item_location);
        mHotLocationGridView.setAdapter(mHotLocationAdapter);
        mHotLocationGridView.setOnItemClickListener(this);
        mHotLocationAdapter.notifyDataSetChanged();


        mSearchLocationAdapter = new SearchLocationAdapter(getBaseContext(), mSearchLocationList, R.layout.item_location_search);
        mSearchLocationListView.setAdapter(mSearchLocationAdapter);
        mSearchLocationAdapter.notifyDataSetChanged();


        mSearchLocationEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchCity(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        mHandler.removeCallbacksAndMessages(null);
    }

    private void searchCity(CharSequence s) {
        mSearchLocationList.clear();
        mSearchLocationListAreadId.clear();
        mSearchLocationAdapter.notifyDataSetChanged();
        mSearchLocationListView.setVisibility(View.VISIBLE);

        if (s == null || s.length() <= 0) {
            mSearchLocationListView.setVisibility(View.GONE);
            return;
        }

        String firstLetter = Cn2Spell.getPinYinFirstLetter(s.toString());
        Log.d("LocationManagerActivity",firstLetter);


        String headLetters = Cn2Spell.getPinYinHeadChar(s.toString()).toUpperCase();
        String pinYing = Cn2Spell.getPinYin(s.toString());
        mSearchLocationAdapter.setSelectedStr(s.toString());
        //汉字查找 从第一个开始
        List<Area> areaListFirstInChina = (List<Area>) mLocationAreaDao.queryBuilder().where(AreaDao.Properties.AreaNameShorter.like(headLetters + "%"),AreaDao.Properties.AreaNameCN.like(s.toString() + "%")).build().list();
        for (Area area:areaListFirstInChina) {
            mSearchLocationListAreadId.add(area.getAreaId());
        }
        //汉字查找 包含
        List<Area> areaListContainInChina = (List<Area>) mLocationAreaDao.queryBuilder().where(AreaDao.Properties.AreaNameShorter.like("_%" + headLetters + "%"),AreaDao.Properties.AreaNameCN.like("_%" + s.toString() + "%")).build().list();
        for (Area area:areaListContainInChina) {
            mSearchLocationListAreadId.add(area.getAreaId());
        }

        mSearchLocationList.addAll(displayLocationList(mSearchLocationListAreadId));
        mSearchLocationAdapter.notifyDataSetChanged();
        if (mSearchLocationList.size() > 0) {
            return;
        }
        mSearchLocationAdapter.setSelectedStr(null);
        //英文／拼音全称查找
        List<Area> areaListContainInPinYin = (List<Area>) mLocationAreaDao.queryBuilder().where(AreaDao.Properties.AreaNameEN.like(pinYing + "%")).build().list();
        for (Area area:areaListContainInPinYin) {
            mSearchLocationListAreadId.add(area.getAreaId());
        }
        mSearchLocationList.addAll(displayLocationList(mSearchLocationListAreadId));
        mSearchLocationAdapter.notifyDataSetChanged();
        if (mSearchLocationList.size() > 0) {
            return;
        }
        // 英文简称
        List<Area> areaListContainInShorter = (List<Area>) mLocationAreaDao.queryBuilder().where(AreaDao.Properties.AreaNameShorter.like(pinYing.toUpperCase() + "%")).build().list();
        for (Area area:areaListContainInShorter) {
            mSearchLocationListAreadId.add(area.getAreaId());
        }
        mSearchLocationList.addAll(displayLocationList(mSearchLocationListAreadId));
        mSearchLocationAdapter.notifyDataSetChanged();

    }

    private List<String> displayLocationList(List<String> areadIdList) {
        List<String> locationList = new ArrayList<>();
        if (areadIdList == null) {
            return locationList;
        }

        for (String item: areadIdList) {
            locationList.add(displayLocationFromAreaId(item));
        }
        return locationList;
    }


    private String displayLocationFromAreaId (String areadId) {
        String displayLocation = null;
        List<Area> areaList = mLocationAreaDao.queryBuilder().where(AreaDao.Properties.AreaId.eq(areadId)).build().list();
        for (Area area : areaList) {
            if (area.getAttributionCNOne() == null || "".equals(area.getAttributionCNOne())) {
                displayLocation = area.getAreaNameCN();
            } else {
                displayLocation = area.getAreaNameCN() + "-" + area.getAttributionCNOne();
            }
        }

        return displayLocation;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()){
            case R.id.current_location_gridView:

                MobclickAgent.onEvent(this, "LocationManagerActivity_current_location_gridView");
                if (mCurrentAreaIdList.size() > position) {
                    saveAreaIdList(mCurrentAreaIdList.get(position), true);
                }
//                sendLocation(Events.LOCATION,mCurrentLocationAdapter.getItem(position),currentAreaID);

                finish();
                break;
            case R.id.recent_location_gridView:
                MobclickAgent.onEvent(this, "LocationManagerActivity_recent_location_gridView");
                if (mRecentAreaIdList.size() > position) {
                    saveAreaIdList(mRecentAreaIdList.get(position), true);
                }

//                sendLocation(Events.LOCATION,mRecentLocationAdapter.getItem(position),"");

                finish();
                break;
            case R.id.hot_location_gridView:
                MobclickAgent.onEvent(this, "LocationManagerActivity_hot_location_gridView");
                saveAreaIdList(mHotAreaIdList.get(position), true);

//                sendLocation(Events.LOCATION,mHotLocationAdapter.getItem(position),"");
                finish();
                break;
            case R.id.lv_search_locations:
                MobclickAgent.onEvent(this, "LocationManagerActivity_lv_search_locations");

//                String location = mSearchLocationAdapter.getItem(position);
//                String areaID = mSearchLocationListAreadId.get(position);
//
//                Log.d("test","===" + location + "--" + areaID);
//                String loca = location;
//                if(location.contains("朝阳")) {
//                    loca = location;
//                } else {
//                    int handPosition = location.indexOf("-");
//                    loca = location.substring(0,handPosition);
//                }
//
//                sendLocation(Events.LOCATION,loca,areaID);
                saveAreaIdList(mSearchLocationListAreadId.get(position), true);

                finish();

                break;
        }
    }

    // 标记是否应该添加到RecentAreaIdList中
    public void saveAreaIdList (String AreaId, boolean isAddRecentAreaIdList) {

        if (isAddRecentAreaIdList) {
            //须添加，存储
            setRecentAreaIDList(AreaId);
        }

        SharePreferenceUtils.setString(SP_SELECTED_AREA_ID, AreaId);
        SharePreferenceUtils.setAreaIdList(SP_RECENT_AREA_ID_LIST, mRecentAreaIdList);

    }

    public void setRecentAreaIDList (String AreaId) {
        //存储个数
        final int num = 5;
        if (mRecentAreaIdList.contains(AreaId)) {
            int index = mRecentAreaIdList.indexOf(AreaId);
            mRecentAreaIdList.remove(index);
        }
        if (mRecentAreaIdList.size() > (num - 1)) {
            mRecentAreaIdList.remove(num - 1);
        }
        mRecentAreaIdList.add(0, AreaId);
    }

//    public void sendLocation (@Events.EventCode int code, String location,String areaID)
//    {
//
//        Bundle bundle = new Bundle();
//        bundle.putString(EventKey.LOCATION, location);
//        bundle.putString(EventKey.AREAID, areaID);
//        RxBus.getInstance().send(code, bundle);
//
//        List<String> tmpList = new ArrayList<>();
//
//        if (!mRecentLocationList.contains(location)){
//            mRecentLocationList.add(0,location);//头部插入
//            if (mRecentLocationList.size()  > 5){
//                Iterator<String> listIterator = mRecentLocationList.iterator();
//                while (listIterator.hasNext()){
//                    String city = listIterator.next();
//                    if (mRecentLocationList.get(mRecentLocationList.size() - 1).equals(city)){
//                        listIterator.remove();
//                        break;
//                    }
//                }
//            }
//            tmpList = mRecentLocationList;
//        } else {
//            tmpList.add(0,location);
//            int widgth = mRecentLocationList.indexOf(location);
//            for (int i = 0;i < mRecentLocationList.size();i++){
//                if (i != widgth){
//                    tmpList.add(mRecentLocationList.get(i));
//                }
//                continue;
//            }
//        }
//
//        SharePreferenceUtils.setLocationList(tmpList);
//    }

    public void getLocationAndCheckPermission()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            //TODO 只检测，不请求
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 10001);
        }
        else
        {
            //Looper.prepare();
            //Looper.loop();// Can't create handler inside thread that has not called Looper.prepare()
            new Thread() {
                public void run() {
                    getLocation();
                }
            }.start();
        }
    }
    private void  getLocation()
    {
        String locationStr = LocationHolder.getLocation(this);
        //String locationStr = LocationHolder.getLocationGPSAndNet(getActivity());
        if (locationStr == null || locationStr.equals(""))
        {
            return;
        }
//
//        int commaPos = locationStr.indexOf(",");
//        String lat = locationStr.substring(0,commaPos);
//        String lng = locationStr.substring(commaPos+1,locationStr.length());
//        Log.d("test",""+lng +"--"+ lat);
//        currentAreaID = lng + ":" + lat;

        getPMData(locationStr);

/*        AMAPMethods.getInstance().locationToAmap(locationStr)
                .flatMap(amapLocation -> {
                    //onLocationResult(amapLocation);
                    return AMAPMethods.getInstance().address(amapLocation.locations);
                })
                .subscribe(new BaseSubscriber<AMAPAddress>()
                {
                    @Override
                    public void onNext(AMAPAddress amapAddress)
                    {
                        onAddressResult(amapAddress);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        Log.d("test",e.getMessage());
                        AppToast.showToast(""+e);
                    }
                });*/
    }

//    private void onAddressResult(AMAPAddress address)
//    {
//        if(!address.getRegeocode().getAddresscomponent().getDistrict().equals("")){
//            locationString = address.getRegeocode().getAddresscomponent().getDistrict();
//        }else {
//            locationString = address.getRegeocode().getAddresscomponent().getProvince();
//        }
//        Log.d("===Locate--result",locationString);
//
//        Message msg = new Message();
//        msg.what = LOCATION_MSG;
//        msg.obj = locationString;
//        mHandler.sendMessage(msg);
//    }

    private void getPMData(String locate){

        Subscriber<WeatherData> subscriber = new Subscriber<WeatherData>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(WeatherData weatherData) {
                Log.d("test---",""+ JsonUtils.objectToJson(weatherData));
                Log.d("test---",""+ weatherData.toString());

                mCurrentLocationList.clear();
                mCurrentAreaIdList.clear();
                String areaId = weatherData.getCity().getAreaId();
                if (areaId == null) {
                    return;
                }
                Log.d("===Locate",areaId);
                mCurrentAreaIdList.add(areaId);
                mCurrentLocationList.addAll(displayLocationList(mCurrentAreaIdList));
                mCurrentLocationAdapter.notifyDataSetChanged();
                SharePreferenceUtils.setAreaIdList(SP_CURRENT_AREA_ID_LIST, mCurrentAreaIdList);
            }
        };
        FirstPageMethods.getInstance().getPMData(subscriber,locate);
    }



}
