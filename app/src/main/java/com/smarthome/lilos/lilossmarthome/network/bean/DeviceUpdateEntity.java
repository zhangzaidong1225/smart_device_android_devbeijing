package com.smarthome.lilos.lilossmarthome.network.bean;

/**
 * Created by Kevin on 2016/7/11.
 */
public class DeviceUpdateEntity extends ResponseEntity
{

    private DevUpdateData data;

    public DevUpdateData getData()
    {
        return data;
    }

    public void setData(DevUpdateData data)
    {
        this.data = data;
    }

    public class DevUpdateData
    {
        private String url;
        private String info;
        private int ver;
        private String md5;

        public String getUrl()
        {
            return url;
        }

        public void setUrl(String url)
        {
            this.url = url;
        }

        public String getInfo()
        {
            return info;
        }

        public void setInfo(String info)
        {
            this.info = info;
        }

        public int getVer()
        {
            return ver;
        }

        public void setVer(int ver)
        {
            this.ver = ver;
        }

        public String getMd5()
        {
            return md5;
        }

        public void setMd5(String md5)
        {
            this.md5 = md5;
        }
    }

}
