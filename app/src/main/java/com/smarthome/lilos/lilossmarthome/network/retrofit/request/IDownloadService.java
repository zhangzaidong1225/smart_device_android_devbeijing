package com.smarthome.lilos.lilossmarthome.network.retrofit.request;

import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Streaming;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by Joker on 2017/1/20.
 */

public interface IDownloadService
{
    @Streaming
    @GET
    Observable<ResponseBody> downloadFile(@Url String fileUrl);
}
