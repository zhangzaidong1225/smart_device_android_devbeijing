package com.smarthome.lilos.lilossmarthome.notify;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.activity.MainActivity;
import com.smarthome.lilos.lilossmarthome.activity.weather.WeatherDetailActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;

/**
 * Created by songyu on 2017/4/27.
 */
public class Notify {
    /** Notification构造器 */
    public static NotificationCompat.Builder mBuilder;
    /** Notification管理 */
    public static NotificationManager mNotificationManager;
    public static Context mContext;
    private static RemoteViews contentView;
    public static NotifyData mNotifyData;
    public Notify(SmartHomeApplication smartHomeApplication) {
        this.mContext  = smartHomeApplication;
    }

    /**
     * 显示通知栏点击跳转到指定Activity
     */
    public static void createNotification() {
        contentView = new RemoteViews(mContext.getPackageName(),
                R.layout.notification);
        contentView.setImageViewResource(R.id.notificationImage, R.mipmap.logo);
        contentView.setTextViewText(R.id.notificationTitle, mNotifyData.getCustomContent_titile());
        mBuilder = new NotificationCompat.Builder(mContext);
        mBuilder.setWhen(System.currentTimeMillis())// 通知产生的时间，会在通知信息里显示
                .setContent(contentView).setAutoCancel(true)// 设置这个标志当用户单击面板就可以让通知自动取消
                .setSmallIcon(R.drawable.ic_logo);

        //control=控制型消息,open_page=打开某个app页面
        String type = mNotifyData.getCustomContent_type();
         if(type.equals("control")){

         }else if(type.equals("open_page")){
             //点击的意图ACTION是跳转到Intent
             Intent resultIntent = new Intent(mContext, switchOpenPage(mNotifyData.getCustomContent_action()));
             resultIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
             PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0,resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
             mBuilder.setContentIntent(pendingIntent);
         }

        Notification nitify = mBuilder.build();
        nitify.contentView = contentView;
        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(R.layout.notification, nitify);
    }

    /**
     * 数据解析
     * @param s
     */
    public static void parseMqttMessage(String s){
        String mqttMessage = s;
        JSONObject mqttObj = null;
        mNotifyData = new NotifyData();
        try {
            mqttObj = new JSONObject(mqttMessage);
            String titile = mqttObj.getString(MQTTJSONTAGS.MQTT_JSONTAG_TITLE);
            String description = mqttObj.getString(MQTTJSONTAGS.MQTT_JSONTAG_DESCRIPTION);
            int notification_builder_id = mqttObj.getInt(MQTTJSONTAGS.MQTT_JSONTAG_NOTIFYID);
            int notification_basic_style = mqttObj.getInt(MQTTJSONTAGS.MQTT_JSONTAG_NOTIFYSTYLE);
            int open_type = mqttObj.getInt(MQTTJSONTAGS.MQTT_JSONTAG_OPENTYPE);
            String url = mqttObj.getString(MQTTJSONTAGS.MQTT_JSONTAG_URL);
            String pkg_content = mqttObj.getString(MQTTJSONTAGS.MQTT_JSONTAG_PKGCONTENT);

            JSONObject customContent = mqttObj.getJSONObject(MQTTJSONTAGS.MQTT_JSONTAG_CUSTOMCONTENT);
            String customContent_titile = customContent.getString(MQTTJSONTAGS.MQTT_JSONTAG_CU_TITLE);
            int customContent_is_notify = customContent.getInt(MQTTJSONTAGS.MQTT_JSONTAG_CU_ISNOTIFY);
            String customContent_type = customContent.getString(MQTTJSONTAGS.MQTT_JSONTAG_CU_TYPE);
            String customContent_action= customContent.getString(MQTTJSONTAGS.MQTT_JSONTAG_CU_ACTION);
            //String customContent_content = customContent.getString(MQTTJSONTAGS.MQTT_JSONTAG_CU_CONTENT);
            //String customContent_params = customContent.getString(MQTTJSONTAGS.MQTT_JSONTAG_CU_PARAMS);


            mNotifyData.setTitile(titile);
            mNotifyData.setDescription(description);
            mNotifyData.setNotification_builder_id(notification_builder_id);
            mNotifyData.setNotification_basic_style(notification_basic_style);
            mNotifyData.setOpen_type(open_type);
            mNotifyData.setUrl(url);
            mNotifyData.setPkg_content(pkg_content);

            mNotifyData.setCustomContent_titile(customContent_titile);
            mNotifyData.setCustomContent_is_notify(customContent_is_notify);
            mNotifyData.setCustomContent_type(customContent_type);
            mNotifyData.setCustomContent_action(customContent_action);

            mNotifyData.setParseMqttMessageError(false);
        } catch (JSONException e) {
            mNotifyData.setParseMqttMessageError(true);
            Log.e("通知解析错误  ：","  "+e);
            e.printStackTrace();
        }

    }
    private int getStatusBarHeight() {
        Class<?> c = null;

        Object obj = null;

        Field field = null;

        int x = 0, sbar = 0;

        try {

            c = Class.forName("com.android.internal.R$dimen");

            obj = c.newInstance();

            field = c.getField("status_bar_height");

            x = Integer.parseInt(field.get(obj).toString());

            sbar = mContext.getResources().getDimensionPixelSize(x);

        } catch (Exception e1) {

            e1.printStackTrace();

        }

        return sbar;
    }
    /**
     * 清除所有通知栏
     * */
    public static void clearAllNotify() {
        if(mNotificationManager == null){
            return;
        }
        mNotificationManager.cancelAll();// 删除你发的所有通知
    }

    /**
     * action 转换
     * @param pageString
     * @return
     */
    private static Class<?> switchOpenPage(String pageString) {
        Class<?> page = MainActivity.class;
        if(pageString.equals("weather_page")){
             page = WeatherDetailActivity.class;
        }
        return page;
    }
}
