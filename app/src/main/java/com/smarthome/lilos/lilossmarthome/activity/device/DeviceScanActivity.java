package com.smarthome.lilos.lilossmarthome.activity.device;

import android.Manifest;
import android.app.AppOpsManager;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.activity.MainActivity;
import com.smarthome.lilos.lilossmarthome.adapter.ScannedDeviceAdapter;
import com.smarthome.lilos.lilossmarthome.bluetooth.BLEService;
import com.smarthome.lilos.lilossmarthome.bluetooth.BluetoothScanner;
import com.smarthome.lilos.lilossmarthome.device.BaseBluetoothDevice;
import com.smarthome.lilos.lilossmarthome.device.BaseDevice;
import com.smarthome.lilos.lilossmarthome.device.DeviceFactory;
import com.smarthome.lilos.lilossmarthome.device.DeviceManager;
import com.smarthome.lilos.lilossmarthome.fragment.TabHomeFragment;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.AddDeviceEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceTypeEntity;
import com.smarthome.lilos.lilossmarthome.network.retrofit.BaseSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.MopsV2Methods;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.utils.CountlyUtil;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.SystemUtils;
import com.smarthome.lilos.lilossmarthome.utils.control.ControlManager;
import com.smarthome.lilos.lilossmarthome.utils.control.OfflinePackageManage;
import com.smarthome.lilos.lilossmarthome.utils.login.LoginInterceptor;
import com.smarthome.lilos.lilossmarthome.wigdet.CustomDialog;
import com.smarthome.lilos.lilossmarthome.wigdet.DialogConfirmListener;
import com.smarthome.lilos.lilossmarthome.wigdet.LoadingLayout;
import com.smarthome.lilos.lilossmarthome.wigdet.WaitingDialog;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import rx.Subscriber;

/**
 * 设备扫描界面
 * 用于蓝牙设备的扫描
 */
public class DeviceScanActivity extends BaseActivity
{
    private final static int TIME_OUT = 1001;

    private LoadingLayout mVLoading;
    private LinearLayout mLlScanning;
    private LinearLayout mLlScannedNoDevice;
    private LinearLayout mLlScanned;
    private ListView mLvDevices;
    private Button mBtnBluetoothAdd;
    private Button mBtnManualAdd;
    private View mVBlock;
    private ImageView mIvScaning;
    private ImageView mIbBack;
    private TextView mTvTitle;
    private Animation mScanningAnim;

    private ScannedDeviceAdapter mScannedDeviceAdapter;
    private boolean mScanning;
    private BaseBluetoothDevice mDevice;
    private String mDeviceName;
    private WaitingDialog waitingDialog;

    private boolean isFromManually = false;
    private int targetTypeId = -1;
    private DeviceManager myManager = null;
    private ArrayList<DeviceTypeEntity> typeListData = new ArrayList<>();//服务器上设备类型

    private DialogConfirmListener mListener = new DialogConfirmListener()
    {
        @Override
        public void postMessage(String message)
        {
            mDeviceName = message;
            //TabHomeFragment.myDeviceManager.addData(mDevice.getAddress(),mDevice);
            addDevice(mDeviceName, mDevice.getAddress(), mDevice.getTypeId());

        }

        @Override
        public void postDeviceInfo(BaseDevice device)
        {
        }
    };

    private Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case TIME_OUT:
                    stopScan();
                    break;
            }
        }
    };
    private BroadcastReceiver bluetoothReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (intent.getAction().equals(BluetoothAdapter.ACTION_STATE_CHANGED))
            {
                int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch (state)
                {
                    case BluetoothAdapter.STATE_ON:
                        BluetoothScanner.scan(10);
                        break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_scan);

        BluetoothScanner.stopAutoConnect();

        isFromManually = getIntent().getBooleanExtra("is_from_manually", false);
        targetTypeId = getIntent().getIntExtra("target_type_id", -1);
        init();
        initRxBus();

        if(!isFromManually)
        {
            CountlyUtil.recordEvent(CountlyUtil.EVENT.EVENT_ADD_DEVICE, CountlyUtil.KEY.ACCESS_TIME);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 10001);
        }
        else
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                AppOpsManager appOpsManager = (AppOpsManager) getSystemService(APP_OPS_SERVICE);
                int checkOp = appOpsManager.checkOp(AppOpsManager.OPSTR_FINE_LOCATION, Process.myUid(), getPackageName());
                if (checkOp == AppOpsManager.MODE_IGNORED)
                {
                    gotoOpenPermission();
                }
            }

            getDevTypeList();
            mVLoading.show();
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(bluetoothReceiver, filter);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        unregisterReceiver(bluetoothReceiver);
        stopScan();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mScannedDeviceAdapter.clear();
        BluetoothScanner.scanAndAutoConnect();
    }

    private void init()
    {
        mScanningAnim = AnimationUtils.loadAnimation(this, R.anim.scanning);
        LinearInterpolator lin = new LinearInterpolator();
        mScanningAnim.setInterpolator(lin);

        mScanning = false;

        mVLoading = (LoadingLayout) findViewById(R.id.v_loading);
        mLvDevices = (ListView) findViewById(R.id.lv_devices);
        mLlScanning = (LinearLayout) findViewById(R.id.ll_scanning);
        mLlScannedNoDevice = (LinearLayout) findViewById(R.id.ll_scanned_no_device);
        mLlScanned = (LinearLayout) findViewById(R.id.ll_scanned);

        mIbBack = (ImageView) findViewById(R.id.ib_back);
        mTvTitle = (TextView) findViewById(R.id.tv_device_scan_title);
        mBtnBluetoothAdd = (Button) findViewById(R.id.btn_bluetooth_add);
        mBtnManualAdd = (Button) findViewById(R.id.btn_manual_add);
        mVBlock = findViewById(R.id.v_block);
        mIvScaning = (ImageView) findViewById(R.id.iv_scaning);
        mScannedDeviceAdapter = new ScannedDeviceAdapter(this);
        mLvDevices.setAdapter(mScannedDeviceAdapter);
        mLvDevices.setOnItemClickListener((parent, view, position, id) ->
        {
            mDevice = (BaseBluetoothDevice) mScannedDeviceAdapter.getDevice(position);
            if (mDevice == null)
            {
                return;
            }
            mDeviceName = mDevice.getName();
            postDialog();
        });
        mBtnBluetoothAdd.setOnClickListener(v -> controlScan());
        mBtnManualAdd.setOnClickListener(v -> gotoDeviceClassifyActivity());
        mIbBack.setOnClickListener(v -> {
            MobclickAgent.onEvent(this, "DeviceScanActivity_back");
            finish();
        });

        mVLoading.setOnReloadingClickListener(this::getDevTypeList);

        if (isFromManually)
        {
            mBtnBluetoothAdd.setVisibility(View.VISIBLE);
            mVBlock.setVisibility(View.GONE);
            mBtnManualAdd.setVisibility(View.GONE);
        }
        else
        {
            mBtnBluetoothAdd.setVisibility(View.VISIBLE);
            mVBlock.setVisibility(View.VISIBLE);
            mBtnManualAdd.setVisibility(View.VISIBLE);
        }

        waitingDialog = new WaitingDialog(this);

        myManager = DeviceManager.getInstance();
    }

    private void initRxBus()
    {
        RxBus.with(this)
                .setEvent(Events.DEVICE_SCANNED)
                .onNext(events -> {
                    BaseBluetoothDevice device = (BaseBluetoothDevice) events.content;
//                    Log.e("test",device.getTypeId()+"");

                    if (!isFromManually || device.getTypeId() == targetTypeId)
                    {
                        updateAdapter(device);
                    }
                    refreshView();
                })
                .createBindLC();
    }

    private void updateAdapter(BaseDevice device)
    {
//        Log.d("test",device.getTypeId()+"");

        if (!mScannedDeviceAdapter.isContained(device))
        {
            mScannedDeviceAdapter.addDevice(device);
            mScannedDeviceAdapter.notifyDataSetChanged();
        }
    }

    private void refreshView()
    {
        if (mScannedDeviceAdapter.getCount() == 0)
        {
            mTvTitle.setText(R.string.device_scan_title);
            mLlScanned.setVisibility(View.GONE);
            if (mScanning)
            {
                mLlScanning.setVisibility(View.VISIBLE);
                mLlScannedNoDevice.setVisibility(View.GONE);
            }
            else
            {
                mLlScanning.setVisibility(View.GONE);
                mLlScannedNoDevice.setVisibility(View.VISIBLE);
            }
        }
        else
        {
            mTvTitle.setText(R.string.found_device);
            mLlScanned.setVisibility(View.VISIBLE);
            mLlScanning.setVisibility(View.GONE);
            mLlScannedNoDevice.setVisibility(View.GONE);
        }
    }

    private void postDialog()
    {
        CustomDialog.Builder builder = new CustomDialog.Builder(this);
        builder.setTitle(R.string.please_named_your_device);
        builder.setMessage(mDeviceName);
        builder.setConfirmListener(mListener);
        builder.setPositiveButton(R.string.ok, (dialog, which) -> {
        });
        builder.setNegativeButton(R.string.cancel, (dialog, which) -> {
        });
        builder.create().show();
    }

    private void controlScan()
    {
        MobclickAgent.onEvent(this, "DeviceScanActivity_controlScan");
        if (mScanning)
        {
            stopScan();
            mBtnBluetoothAdd.setText(getResources().getString(R.string.bluetooth_add));
        }
        else
        {
            scanDevice();
            mBtnBluetoothAdd.setText(getResources().getString(R.string.scan_stop));
        }
    }

    private void scanDevice()
    {
        changeScanningStatus(true);
        mScannedDeviceAdapter.clear();
        mScannedDeviceAdapter.notifyDataSetChanged();
        refreshView();
        if (BluetoothAdapter.getDefaultAdapter().isEnabled())
        {
            BluetoothScanner.scan(10);
        }
        else
        {
            BluetoothAdapter.getDefaultAdapter().enable();
        }
        handler.removeMessages(TIME_OUT);
        handler.sendEmptyMessageDelayed(TIME_OUT, 1000 * 15);
    }

    private void stopScan()
    {
        changeScanningStatus(false);
        BluetoothScanner.stopScan();
    }

    private void gotoDeviceClassifyActivity()
    {
        MobclickAgent.onEvent(this, "DeviceScanActivity_gotoDeviceClassifyActivity");
        stopScan();
        Intent intent = new Intent(this, DeviceClassifyActivity.class);
        intent.putExtra("dev_type_list", new Gson().toJson(typeListData));
        startActivity(intent);
    }

    private void changeScanningStatus(boolean isScanning)
    {
        mScanning = isScanning;
        if (mScanning)
        {
            mBtnBluetoothAdd.setText(getResources().getText(R.string.scan_stop));
            if (mScanningAnim != null)
            {
                mIvScaning.startAnimation(mScanningAnim);
            }
        }
        else
        {
            mBtnBluetoothAdd.setText(getResources().getText(R.string.bluetooth_add));
            if (mScanningAnim != null)
            {
                mIvScaning.clearAnimation();
            }
        }
        refreshView();
    }

    private void gotoOpenPermission()
    {
        if (SystemUtils.isMIUI())
        {
            AppToast.showToast(R.string.msg_xiaomi_location);
        }
        else
        {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_SETTINGS);
            startActivity(intent);
            AppToast.showToast(R.string.msg_location);
        }
        finish();
    }
    //请求权限
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults)
    {
        if (requestCode == 10001)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                getDevTypeList();
                mVLoading.show();
            }
            else
            {
                gotoOpenPermission();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void getDevTypeList()
    {
        MobclickAgent.onEvent(this, "DeviceScanActivity_getDevTypeList");
        Subscriber<List<DeviceTypeEntity>> subscriber = new BaseSubscriber<List<DeviceTypeEntity>>()
        {
            @Override
            public void onNext(List<DeviceTypeEntity> entities)
            {
                onDeviceTypeListResult(entities);
            }

            @Override
            public void onError(Throwable e)
            {
                super.onError(e);
                mVLoading.fail();
            }
        };
        MopsV2Methods.getInstance().types(subscriber);
    }

    private void addDevice(String name, String devId, int typeId)
    {
        if(!SharePreferenceUtils.getLoginStatus()){
            BaseBluetoothDevice baseBluetoothDevice = new BaseBluetoothDevice();
            baseBluetoothDevice.setName(name);
            baseBluetoothDevice.setAddress(devId);
            baseBluetoothDevice.setTypeId(typeId);

            onAddDeviceResult(baseBluetoothDevice);
        } else {
            Subscriber<AddDeviceEntity> subscriber = new BaseSubscriber<AddDeviceEntity>()
            {
                @Override
                public void onNext(AddDeviceEntity addDeviceEntity)
                {
                    onAddDeviceResult(addDeviceEntity);
                }

                @Override
                public void onError(Throwable e) {
                    super.onError(e);
                }
            };
            MopsV2Methods.getInstance().addDevice(subscriber,name,devId,typeId);

        }
        waitingDialog.show();

    }



    private void onDeviceTypeListResult(List<DeviceTypeEntity> entities)
    {
        typeListData.clear();
        typeListData.addAll(entities);
        if (typeListData != null)
        {
            ControlManager.setTypeList(typeListData);
            mScannedDeviceAdapter.setTypeListData(typeListData);
        }
        mVLoading.success();
        scanDevice();
    }

    private void onAddDeviceResult(BaseBluetoothDevice device){
        mDevice = device;
        device.setAutoConnect(true);
        device.setIsmatchd(true);
        device.setUpdate_state(100);

        myManager.addData(mDevice.getAddress(), DeviceFactory.createDevice(mDevice));
        myManager.mDataOff.get(mDevice.getAddress()).connect();
        myManager.mDataOff.get(mDevice.getAddress()).setIsconnectd(BLEService.STATE_CONNECTING);

        mScannedDeviceAdapter.notifyDataSetChanged();

        AppToast.showToast(R.string.device_add_success);

        getTypeId(device);

    }

    private void getTypeId(BaseBluetoothDevice mDevice) {
        if (ControlManager.isHtml5OfflineControl(mDevice.getTypeId()))
        {
            OfflinePackageManage.checkVersion(mDevice.getTypeId());
        }

        new Thread()
        {
            @Override
            public void run()
            {
                while (true)
                {
                    if (myManager.mDataOff.get(mDevice.getAddress()).getIsconnectd() == BLEService.STATE_CONNECTED)
                    {

                        BaseBluetoothDevice device = (BaseBluetoothDevice) myManager.mDataOff.get(mDevice.getAddress());
//                        Log.e("分割  v2 ","===="+mDevice.getTypeId());
                        if (ControlManager.isHtml5OfflineControl(device.getTypeId()))
                        {
//                            Log.e("分割  w ","===="+mDevice.getTypeId());
                            for (int i = 0; i < 30; i++)
                            {
                                if (!OfflinePackageManage.getPackagePath(mDevice.getTypeId()).equals(""))
                                {
                                    ControlManager.gotoControlActivity(getBaseContext(), device);
                                    break;
                                } else
                                {
                                    if (i == 29){
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(DeviceScanActivity.this, "网络不畅，请稍后重试", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        waitingDialog.dismiss();
                                        finish();
                                    } else {
                                        try {
                                            Thread.sleep(100);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        } else
                        {
//                            Log.e("分割  v ","===="+device.getTypeId());
                            ControlManager.gotoControlActivity(getBaseContext(), device);
                        }
                        if (waitingDialog.isShowing()) {
                            waitingDialog.dismiss();
                        }
                        finish();
                        break;
                    } else if (myManager.mDataOff.get(mDevice.getAddress()).getIsconnectd() == BLEService.STATE_DISCONNECTED)
                    {
                        //连接失败，返回时白屏
//                        Intent intent = new Intent(getBaseContext(), MainActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
//                        Log.e("分割  v1 ","===="+mDevice.getTypeId());
                        waitingDialog.dismiss();
                        finish();
                        break;
                    } else
                    {
//                        Log.e("分割  v13 ","===="+mDevice.getTypeId()+"    "+mDevice.getAddress() + "   "+myManager.mDataOff.get(mDevice.getAddress()).getIsconnectd() );
                        try
                        {
                            Thread.sleep(100);
                        } catch (InterruptedException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }.start();
    }

    private void onAddDeviceResult(AddDeviceEntity entity)
    {
        mDevice.setName(mDeviceName);
        mDevice.setAutoConnect(true);
        mDevice.setIsmatchd(true);
        mDevice.setUpdate_state(100);
        myManager.addData(mDevice.getAddress(), DeviceFactory.createDevice(mDevice));
        myManager.mData.get(mDevice.getAddress()).connect();
        myManager.mData.get(mDevice.getAddress()).setIsconnectd(BLEService.STATE_CONNECTING);
        mScannedDeviceAdapter.notifyDataSetChanged();
        AppToast.showToast(R.string.device_add_success);
//        Log.e("分割","===="+mDevice.getTypeId());
        if (ControlManager.isHtml5OfflineControl(mDevice.getTypeId()))
        {
            OfflinePackageManage.checkVersion(mDevice.getTypeId());
        }

        new Thread()
        {
            @Override
            public void run()
            {
                while (true)
                {
                    if (myManager.mData.get(mDevice.getAddress()).getIsconnectd() == BLEService.STATE_CONNECTED)
                    {
                        BaseBluetoothDevice device = (BaseBluetoothDevice) myManager.mData.get(mDevice.getAddress());
//                        Log.e("分割  v2 ","===="+mDevice.getTypeId());
                        if (ControlManager.isHtml5OfflineControl(mDevice.getTypeId()))
                        {
//                            Log.e("分割  w ","===="+mDevice.getTypeId());
                            for (int i = 0; i < 20; i++)
                            {
                                if (!OfflinePackageManage.getPackagePath(mDevice.getTypeId()).equals(""))
                                {
                                    ControlManager.gotoControlActivity(getBaseContext(), device);
                                    break;
                                } else
                                {
                                    try
                                    {
                                        Thread.sleep(100);
                                    } catch (InterruptedException e)
                                    {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        } else
                        {
//                            Log.e("分割  v ","===="+mDevice.getTypeId());
                            ControlManager.gotoControlActivity(getBaseContext(), device);
                        }

                        waitingDialog.dismiss();
                        finish();
                        break;
                    } else if (myManager.mData.get(mDevice.getAddress()).getIsconnectd() == BLEService.STATE_DISCONNECTED)
                    {
                        //连接失败，返回时白屏
//                        Intent intent = new Intent(getBaseContext(), MainActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
//                        Log.e("分割  v1 ","===="+mDevice.getTypeId());
                        waitingDialog.dismiss();
                        finish();
                        break;
                    } else
                    {
//                        Log.e("分割  v13 ","===="+mDevice.getTypeId()+"    "+mDevice.getAddress() + "   "+myManager.mData.get(mDevice.getAddress()).getIsconnectd() );
                        try
                        {
                            Thread.sleep(100);
                        } catch (InterruptedException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }.start();
    }
}