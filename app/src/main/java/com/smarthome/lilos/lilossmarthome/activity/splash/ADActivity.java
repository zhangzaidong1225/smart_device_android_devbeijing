package com.smarthome.lilos.lilossmarthome.activity.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.activity.MainActivity;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.umeng.analytics.MobclickAgent;

/**
 * 广告页面
 * 用于启动APP时展示广告内容
 */
public class ADActivity extends BaseActivity
{
    private ImageView mIvAd;
    private TextView mTvSkip;

    private int count = 3;

    private Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case 0:
                    count--;
                    mTvSkip.setText(count + "" + getResources().getText(R.string.skip_ad));
                    if (count == 0)
                    {
                        gotoMainActivity();
                    }
                    else
                    {
                        handler.sendEmptyMessageDelayed(0, 1000);
                    }
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ad);

        init();
        handler.sendEmptyMessageDelayed(0, 1000);
    }

    private void init()
    {
        mIvAd = (ImageView) findViewById(R.id.iv_ad);
        mTvSkip = (TextView) findViewById(R.id.tv_skip);

        String imagePath = SharePreferenceUtils.getADImage();
        final String actionUrl = SharePreferenceUtils.getADUrl();

        DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .build();
        ImageLoader.getInstance().displayImage(imagePath, mIvAd, displayImageOptions);

        mIvAd.setOnClickListener(v -> openUrl(actionUrl));
        mTvSkip.setOnClickListener(v -> gotoMainActivity());
        mTvSkip.setText(count + "" + getResources().getText(R.string.skip_ad));
    }

    private void openUrl(String url)
    {
        MobclickAgent.onEvent(this, "adActivity_openUrl");
        handler.removeMessages(0);
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        intent.putExtra("url", url);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void gotoMainActivity()
    {
        MobclickAgent.onEvent(this, "adActivity_gotoMainActivity");
        handler.removeMessages(0);
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
