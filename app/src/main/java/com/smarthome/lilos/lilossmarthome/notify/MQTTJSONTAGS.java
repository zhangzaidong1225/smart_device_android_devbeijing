package com.smarthome.lilos.lilossmarthome.notify;

/**
 * Created by songyu on 2017/4/26.
 */
public class MQTTJSONTAGS {
    public static final String MQTT_JSONTAG_TITLE = "title";
    public static final String MQTT_JSONTAG_DESCRIPTION = "description";
    public static final String MQTT_JSONTAG_NOTIFYID = "notification_builder_id";
    public static final String MQTT_JSONTAG_NOTIFYSTYLE = "notification_basic_style";
    public static final String MQTT_JSONTAG_OPENTYPE = "open_type";
    public static final String MQTT_JSONTAG_URL = "url";
    public static final String MQTT_JSONTAG_PKGCONTENT = "pkg_content";
    public static final String MQTT_JSONTAG_CUSTOMCONTENT = "custom_content";

    public static final String MQTT_JSONTAG_CU_TITLE = "title";
    public static final String MQTT_JSONTAG_CU_ISNOTIFY = "is_notify";
    public static final String MQTT_JSONTAG_CU_TYPE = "type";
    public static final String MQTT_JSONTAG_CU_ACTION = "action";
    public static final String MQTT_JSONTAG_CU_CONTENT = "content";
    public static final String MQTT_JSONTAG_CU_PARAMS = "params";

}
