package com.smarthome.lilos.lilossmarthome.jsscope.holder;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * 网络操作Holder
 * 用于判断是否有网络
 * Created by Joker on 2016/8/29.
 */
public class NetworkHolder
{
    public static boolean isNetworkAvailable(Context context)
    {
        Context mContext = context.getApplicationContext();
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null)
        {
            return false;
        }
        else
        {
            NetworkInfo[] networkInfos = connectivityManager.getAllNetworkInfo();
            if (networkInfos != null && networkInfos.length > 0)
            {
                for (int i = 0; i < networkInfos.length; i++)
                {
                    if (networkInfos[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
