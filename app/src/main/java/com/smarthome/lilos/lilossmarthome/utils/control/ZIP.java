package com.smarthome.lilos.lilossmarthome.utils.control;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * 解压工具类
 * Created by Joker on 2017/1/20.
 */

public class ZIP
{
    public static void UnZipFolder(String zipFilePath,String outPathString) throws Exception
    {
        ZipInputStream inZip = new ZipInputStream(new FileInputStream(zipFilePath));
        ZipEntry zipEntry;
        String szName = "";
        while ((zipEntry = inZip.getNextEntry()) != null)
        {
            szName = zipEntry.getName();
            if(zipEntry.isDirectory())
            {
                szName = szName.substring(0,szName.length() - 1);
                File folder = new File(outPathString + File.separator + szName);
                Log.d("ZIP", "创建文件夹:" + folder.getAbsoluteFile());
                folder.mkdir();
            }
            else
            {
                File file = new File(outPathString + File.separator + szName);
                file.createNewFile();
                Log.d("ZIP", "创建文件:" + file.getAbsoluteFile());
                FileOutputStream out = new FileOutputStream(file);
                int len;
                byte[] buffer = new byte[1024];
                while ((len = inZip.read(buffer)) != -1)
                {
                    out.write(buffer,0,len);
                    out.flush();
                }
                out.close();
            }
        }
        inZip.close();
    }
}
