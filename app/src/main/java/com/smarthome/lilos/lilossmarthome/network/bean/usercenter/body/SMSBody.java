package com.smarthome.lilos.lilossmarthome.network.bean.usercenter.body;

/**
 * Created by Joker on 2016/12/21.
 */

public class SMSBody
{
    private String phone;

    public SMSBody(String phone)
    {
        this.phone = phone;
    }

    public String getPhone()
    {

        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }
}
