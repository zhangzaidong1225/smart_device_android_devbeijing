package com.smarthome.lilos.lilossmarthome.network.bean;

/**
 * Created by Kevin on 2016/7/26.
 */
public class AdEntity extends ResponseEntity
{
    private AdData data;

    public AdData getData()
    {
        return data;
    }

    public void setData(AdData data)
    {
        this.data = data;
    }

    public class AdData
    {
        private String url;
        private String action;

        public String getUrl()
        {
            return url;
        }

        public void setUrl(String url)
        {
            this.url = url;
        }

        public String getAction()
        {
            return action;
        }

        public void setAction(String action)
        {
            this.action = action;
        }
    }
}
