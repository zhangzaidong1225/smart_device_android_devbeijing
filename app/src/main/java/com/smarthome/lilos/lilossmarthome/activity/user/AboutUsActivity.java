package com.smarthome.lilos.lilossmarthome.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.activity.tool.BackdoorActivity;
import com.smarthome.lilos.lilossmarthome.utils.CountlyUtil;
import com.smarthome.lilos.lilossmarthome.utils.SystemUtils;
import com.umeng.analytics.MobclickAgent;

/**
 * 关于页面
 * 用于展示APP信息
 * 可以进入用户协议界面
 * 可以开启后门页面
 */
public class AboutUsActivity extends BaseActivity implements View.OnClickListener
{
    private ImageView mIvBack;
    private TextView mTvVersion;
    private FrameLayout mFlGotoComment;
    private FrameLayout mFlUserProtocol;
    private View mVBackdoor;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        CountlyUtil.recordEvent(CountlyUtil.EVENT.EVENT_ABOUT, CountlyUtil.KEY.ACCESS_TIME);
        init();
        getData();
    }

    @Override
    protected void onResume()
    {
        count = 0;
        super.onResume();
    }

    private void init()
    {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvVersion = (TextView) findViewById(R.id.tv_version);
        mFlGotoComment = (FrameLayout) findViewById(R.id.fl_goto_comment);
        mFlUserProtocol = (FrameLayout) findViewById(R.id.fl_user_protocol);
        mVBackdoor = findViewById(R.id.v_backdoor);

        mIvBack.setOnClickListener(this);
        mFlGotoComment.setOnClickListener(this);
        mFlUserProtocol.setOnClickListener(this);
        mVBackdoor.setOnClickListener(this);
    }

    private void getData()
    {
        mTvVersion.setText(getResources().getText(R.string.lb_version) + " " + SystemUtils.getVersionName(getBaseContext()));
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.iv_back:
                MobclickAgent.onEvent(this, "AboutUsActivity_back");
                finish();
                break;
            case R.id.fl_goto_comment:
                MobclickAgent.onEvent(this, "AboutUsActivity_gotoComment");
                gotoComment();
                break;
            case R.id.fl_user_protocol:
                MobclickAgent.onEvent(this, "AboutUsActivity_gotoUserProtocol");
                gotoUserProtocol();
                break;
            case R.id.v_backdoor:
                MobclickAgent.onEvent(this, "AboutUsActivity_gotoBackdoor");
                gotoBackdoor();
                break;
            default:
                break;
        }
    }

    private void gotoComment()
    {
        // TODO: 2016/7/20  暂时不需要该功能
    }

    private void gotoUserProtocol()
    {
        Intent intent = new Intent(getBaseContext(), ProtocolActivity.class);
        startActivity(intent);
    }

    private void gotoBackdoor()
    {
        count++;
        if (count == 2)
        {
            Intent intent = new Intent(getBaseContext(), BackdoorActivity.class);
            startActivity(intent);
        }
    }
}
