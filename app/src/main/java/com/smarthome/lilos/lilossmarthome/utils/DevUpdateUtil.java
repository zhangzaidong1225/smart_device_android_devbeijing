package com.smarthome.lilos.lilossmarthome.utils;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 设备更新类
 * Created by Kevin on 2016/7/19.
 */
public class DevUpdateUtil
{
    private static final String DOWNLOAD_DIR = "xinfeng";
    protected static char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    protected static MessageDigest messagedigest = null;
    public String mPath;
    private String mFileName;
    private Context mContext;

    public DevUpdateUtil(Context context)
    {
        this.mContext = context;
    }

    public long downloadFirmware(String url)
    {
        if (url != null)
        {
            Log.d("WebControlActivity","onUpdateResult--11");

            mFileName = url.split("/")[url.split("/").length - 1];
            mPath = String.format("%s%s%s%s%s", Environment.getExternalStorageDirectory().getPath().toString(), File.separator, DOWNLOAD_DIR, File.separator, mFileName);
            File file = new File(mPath);
            if (file.exists())
            {
                file.delete();
            }
            Log.d("WebControlActivity","onUpdateResult--12");

            DownloadManager downloadManager = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
            Uri uri = Uri.parse(url);
            DownloadManager.Request request = new DownloadManager.Request(uri);
            Log.d("WebControlActivity","onUpdateResult--13");

            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
            request.setDestinationInExternalPublicDir(DOWNLOAD_DIR, mFileName);
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI);
            request.setAllowedOverRoaming(true);
            Log.d("WebControlActivity","onUpdateResult--14");

            return downloadManager.enqueue(request);
        }
        else
        {
            return -1;
        }
    }

    public String getFileMD5String(File file) throws IOException
    {
        try
        {
            messagedigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        FileInputStream in = new FileInputStream(file);
        FileChannel ch = in.getChannel();
        MappedByteBuffer byteBuffer = ch.map(FileChannel.MapMode.READ_ONLY, 0, file.length());
        messagedigest.update(byteBuffer);
        return bufferToHex(messagedigest.digest());
    }

    private String bufferToHex(byte bytes[])
    {
        return bufferToHex(bytes, 0, bytes.length);
    }

    private String bufferToHex(byte bytes[], int m, int n)
    {
        StringBuffer stringbuffer = new StringBuffer(2 * n);
        int k = m + n;
        for (int l = m; l < k; l++)
        {
            appendHexPair(bytes[l], stringbuffer);
        }
        return stringbuffer.toString();
    }

    private void appendHexPair(byte bt, StringBuffer stringbuffer)
    {
        char c0 = hexDigits[(bt & 0xf0) >> 4];
        char c1 = hexDigits[bt & 0xf];
        stringbuffer.append(c0);
        stringbuffer.append(c1);
    }

    public boolean checkFileMd5(String sourceMd5)
    {
        try
        {
            String localMd5 = getFileMD5String(new File(mPath));
            return localMd5.equals(sourceMd5);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return false;
    }
}
