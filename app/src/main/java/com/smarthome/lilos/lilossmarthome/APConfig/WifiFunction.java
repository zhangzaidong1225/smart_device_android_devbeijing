package com.smarthome.lilos.lilossmarthome.APConfig;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.util.Log;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class WifiFunction
{
    private static final String TAG = "WifiFunction";
    // 定义一个WifiManager对象
    private static WifiManager meWifiManager;
    WifiLock meWifiLock;
    // 定义一个WifiInfo对象
    private WifiInfo meWifiInfo;
    // 扫描出的网络连接列表
    private List<ScanResult> meWifiList;
    // 网络连接列表
    private List<WifiConfiguration> meWifiConfigurations;

    public WifiFunction(Context context)
    {
        // 取得WifiManager对象
        meWifiManager = (WifiManager) context
                .getSystemService(Context.WIFI_SERVICE);
        // 取得WifiInfo对象
        meWifiInfo = meWifiManager.getConnectionInfo();
    }

    public static WifiConfiguration IsExsits(String SSID)
    {
        List<WifiConfiguration> existingConfigs = meWifiManager
                .getConfiguredNetworks();
        if (existingConfigs != null)
        {
            for (WifiConfiguration existingConfig : existingConfigs)
            {
                if (existingConfig.SSID.equals("\"" + SSID + "\""))
                {
                    return existingConfig;
                }
            }
        }
        return null;
    }

    public static void setIpAssignment(String assign, WifiConfiguration wifiConf)
            throws SecurityException, IllegalArgumentException,
            NoSuchFieldException, IllegalAccessException
    {
        setEnumField(wifiConf, assign, "ipAssignment");
    }

    /**
     * 手动设置Wifi IP
     *
     * @param addr
     * @param prefixLength
     * @param wifiConf
     * @throws SecurityException
     * @throws IllegalArgumentException
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws InvocationTargetException
     */
    public static void setIpAddress(InetAddress addr, int prefixLength,
                                    WifiConfiguration wifiConf) throws SecurityException,
            IllegalArgumentException, NoSuchFieldException,
            IllegalAccessException, NoSuchMethodException,
            ClassNotFoundException, InstantiationException,
            InvocationTargetException
    {
        Object linkProperties = getField(wifiConf, "linkProperties");
        if (linkProperties == null)
        {
            return;
        }
        Class laClass = Class.forName("android.net.LinkAddress");
        Constructor laConstructor = laClass.getConstructor(new Class[]{
                InetAddress.class, int.class});
        Object linkAddress = laConstructor.newInstance(addr, prefixLength);

        ArrayList mLinkAddresses = (ArrayList) getDeclaredField(linkProperties,
                "mLinkAddresses");
        mLinkAddresses.clear();
        mLinkAddresses.add(linkAddress);
    }

    /**
     * 设置网关
     *
     * @param gateway
     * @param wifiConf
     * @throws SecurityException
     * @throws IllegalArgumentException
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws InstantiationException
     * @throws InvocationTargetException
     */

    public static void setGateway(InetAddress gateway,
                                  WifiConfiguration wifiConf) throws SecurityException,
            IllegalArgumentException, NoSuchFieldException,
            IllegalAccessException, ClassNotFoundException,
            NoSuchMethodException, InstantiationException,
            InvocationTargetException
    {
        Object linkProperties = getField(wifiConf, "linkProperties");
        if (linkProperties == null)
        {
            return;
        }
        Class routeInfoClass = Class.forName("android.net.RouteInfo");
        Constructor routeInfoConstructor = routeInfoClass
                .getConstructor(new Class[]{InetAddress.class});
        Object routeInfo = routeInfoConstructor.newInstance(gateway);

        ArrayList mRoutes = (ArrayList) getDeclaredField(linkProperties,
                "mRoutes");
        mRoutes.clear();
        mRoutes.add(routeInfo);
    }

    /**
     * 设置DNS
     *
     * @param dns
     * @param wifiConf
     * @throws SecurityException
     * @throws IllegalArgumentException
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */

    public static void setDNS(InetAddress dns, WifiConfiguration wifiConf)
            throws SecurityException, IllegalArgumentException,
            NoSuchFieldException, IllegalAccessException
    {
        Object linkProperties = getField(wifiConf, "linkProperties");
        if (linkProperties == null)
        {
            return;
        }

        ArrayList<InetAddress> mDnses = (ArrayList<InetAddress>) getDeclaredField(
                linkProperties, "mDnses");
        mDnses.clear(); // or add a new dns address , here I just want to
        // replace DNS1
        mDnses.add(dns);
    }

    /**
     * 获取域名
     *
     * @param obj
     * @param name
     * @return
     * @throws SecurityException
     * @throws NoSuchFieldException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public static Object getField(Object obj, String name)
            throws SecurityException, NoSuchFieldException,
            IllegalArgumentException, IllegalAccessException
    {
        Field f = obj.getClass().getField(name);
        Object out = f.get(obj);
        return out;
    }

    /**
     * @param obj
     * @param name
     * @return
     * @throws SecurityException
     * @throws NoSuchFieldException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public static Object getDeclaredField(Object obj, String name)
            throws SecurityException, NoSuchFieldException,
            IllegalArgumentException, IllegalAccessException
    {
        Field f = obj.getClass().getDeclaredField(name);
        f.setAccessible(true);
        Object out = f.get(obj);
        return out;
    }

    public static void setEnumField(Object obj, String value, String name)
            throws SecurityException, NoSuchFieldException,
            IllegalArgumentException, IllegalAccessException
    {
        Field f = obj.getClass().getField(name);
        f.set(obj, Enum.valueOf((Class<Enum>) f.getType(), value));
    }

    /**
     * 手动设置IP
     *
     * @param SSID
     * @param static_ip
     * @param static_gateway
     * @param static_dns
     */
    public static void set_static(String SSID, String static_ip, String static_gateway,
                                  String static_dns)
    {

        WifiConfiguration tempConfig = WifiFunction.IsExsits(SSID);
        if (tempConfig != null)
        {
            try
            {
                setIpAssignment("STATIC", tempConfig);

                setIpAddress(InetAddress.getByName(static_ip), 24,
                        tempConfig);

                setGateway(InetAddress.getByName(static_gateway), tempConfig);

                setDNS(InetAddress.getByName(static_dns), tempConfig);

            } catch (Exception e)
            {

                e.printStackTrace();
            }
            meWifiManager.updateNetwork(tempConfig);
        }
    }

    public void reconnect()
    {
        meWifiManager.reconnect();
    }

    // 打开wifi
    public void openWifi()
    {
        if (!meWifiManager.isWifiEnabled())
        {
            meWifiManager.setWifiEnabled(true);
        }
    }

    public boolean isWifiEnabled()
    {
        return meWifiManager.isWifiEnabled();
    }

    // 关闭wifi
    public void closeWifi()
    {
        if (meWifiManager.isWifiEnabled())
        {
            meWifiManager.setWifiEnabled(false);
        }
    }

    // 检查当前wifi状态
    public int checkState()
    {
        return meWifiManager.getWifiState();
    }

    // 锁定wifiLock
    public void acquireWifiLock()
    {
        meWifiLock.acquire();
    }

    // 解锁wifiLock
    public void releaseWifiLock()
    {
        // 判断是否锁定
        if (meWifiLock.isHeld())
        {
            meWifiLock.acquire();
        }
    }

    // 创建一个wifiLock
    public void createWifiLock()
    {
        meWifiLock = meWifiManager.createWifiLock("test");
    }

    // 得到配置好的网络
    public List<WifiConfiguration> getConfiguration()
    {
        return meWifiConfigurations;
    }

    // 指定配置好的网络进行连接
    public void connetionConfiguration(int index)
    {
        if (index > meWifiConfigurations.size())
        {
            return;
        }
        // 连接配置好指定ID的网络
        meWifiManager.enableNetwork(meWifiConfigurations.get(index).networkId, true);
    }

    public void startScan()
    {

        meWifiManager.startScan();
        // 得到扫描结果
        meWifiList = meWifiManager.getScanResults();
        // 得到配置好的网络连接
        meWifiConfigurations = meWifiManager.getConfiguredNetworks();
        meWifiInfo = meWifiManager.getConnectionInfo();
//		Log.d("WIFILIST","meWifiInfo = "+meWifiInfo.toString()+"meWifiList = "+meWifiList.size());

    }

    // 得到网络列表
    public List<ScanResult> getWifiList()
    {
        meWifiList = meWifiManager.getScanResults();
        return meWifiList;
    }

    // 查看扫描结果
    public StringBuffer lookUpScan()
    {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < meWifiList.size(); i++)
        {
            sb.append("Index_" + new Integer(i + 1).toString() + ":");
            // 将ScanResult信息转换成一个字符串包
            // 其中把包括：BSSID、SSID、capabilities、frequency、level
            sb.append((meWifiList.get(i)).toString()).append("\n" + "~");
        }
        return sb;
    }

    public void regetConnection()
    {
        meWifiInfo = null;
        meWifiInfo = meWifiManager.getConnectionInfo();
    }

    public String getMacAddress()
    {
        return (meWifiInfo == null) ? "NULL" : meWifiInfo.getMacAddress();
    }

    public String getBSSID()
    {
        return (meWifiInfo == null) ? "NULL" : meWifiInfo.getBSSID();
    }

    public String getSSID()
    {
        return meWifiManager.getConnectionInfo().getSSID();

    }

    public void removeNetwork(String ssid)
    {
        removeWifi(getNetworkId(ssid));
    }

    private int getNetworkId(String wifiSSID)
    {

        List<WifiConfiguration> wifiConfigurationList = meWifiManager.getConfiguredNetworks();
        if (wifiConfigurationList != null && wifiConfigurationList.size() != 0)
        {
            for (int i = 0; i < wifiConfigurationList.size(); i++)
            {
                WifiConfiguration wifiConfiguration = wifiConfigurationList.get(i);

                // wifiSSID就是SSID 是否判断BSSID？
                if (wifiConfiguration.SSID != null && wifiConfiguration.SSID.equals(wifiSSID))
                {

                    return wifiConfiguration.networkId;

                }
            }
        }
        return -1;
    }

    public boolean createWifiInfoBySSIDandPass(String ssid, String pass)
    {
        // TODO: 2016/8/10   测试代码:   先在配置列表中删除SSID
        removeNetwork(ssid);

        return checkAndConnect(ssid, pass);


    }

    public boolean checkAndConnect(String ssid, String pass)
    {
        List<ScanResult> checkList = meWifiManager.getScanResults();
        ScanResult scanResult = null;
        WifiConfiguration config = null;
//        Log.d(TAG, "checkAndConnect:  checkList = "+checkList.size());

        if (checkList == null || checkList.size() == 0)
        {
            return false;
        }
        for (ScanResult result : checkList)
        {
//            Log.d(TAG, "checkAndConnect: result.SSID = "+result.SSID);
            if (result.SSID.equals(ssid))
            {
                scanResult = result;
                config = CreateWifiInfo2(scanResult, pass);
                if (config != null)
                {
                    boolean resultconfig = addNetWork(config);
//                    Log.d(TAG, "checkAndConnect: resultconfig = "+resultconfig);
                    if (resultconfig)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public int getIpAddress()
    {
        return (meWifiInfo == null) ? 0 : meWifiInfo.getIpAddress();
    }

    public int getLinkSpeed()
    {
        return (meWifiInfo == null) ? 0 : meWifiInfo.getLinkSpeed();
    }

    // 得到连接的ID
    public int getNetWordId()
    {
        return (meWifiInfo == null) ? 0 : meWifiInfo.getNetworkId();
    }

    // 得到wifiInfo的所有信息
    public String getWifiInfo()
    {
        return (meWifiInfo == null) ? "NULL" : meWifiInfo.toString();
    }

    // 添加一个网络并连接
    public boolean addNetWork(WifiConfiguration configuration)
    {
//        Log.d(TAG, "addNetWork: addNetWork configuration "+configuration.toString());
        int wcgId = meWifiManager.addNetwork(configuration);

//        Log.d(TAG, "addNetWork: wcgId " +wcgId);
        boolean b = meWifiManager.enableNetwork(wcgId, true);
        if (b)
        {
            //设置成功后，需要重新连接
            return meWifiManager.reconnect();
        }
        else
        {
            Log.e(TAG, " Setting WifiData Failed");
            return false;
        }

    }

    // 断开指定ID的网络
    public void disConnectionWifi(int netId)
    {
        meWifiManager.disableNetwork(netId);
        meWifiManager.disconnect();
        meWifiInfo = null; //FixMe: 本来想重新获取一下，但是获取的内容是和之前一样的，这里简单处理一下
    }

    public void removeWifi(int netId)
    {
        meWifiManager.disableNetwork(netId);
        meWifiManager.disconnect();
        meWifiManager.removeNetwork(netId);
        meWifiInfo = null;//FixMe: 本来想重新获取一下，但是获取的内容是和之前一样的，这里简单处理一下
    }

    public WifiConfiguration CreateWifiInfo2(ScanResult wifiinfo, String pwd)
    {
        WifiCipherType type;

        if (wifiinfo.capabilities.contains("WPA2-PSK"))
        {
            // WPA-PSK加密
            type = WifiCipherType.WIFI_CIPHER_WPA2_PSK;
        }
        else if (wifiinfo.capabilities.contains("WPA-PSK"))
        {
            // WPA-PSK加密
            type = WifiCipherType.WIFI_CIPHER_WPA_PSK;
        }
        else if (wifiinfo.capabilities.contains("WPA-EAP"))
        {
            // WPA-EAP加密
            type = WifiCipherType.WIFI_CIPHER_WPA_EAP;
        }
        else if (wifiinfo.capabilities.contains("WEP"))
        {
            // WEP加密
            type = WifiCipherType.WIFI_CIPHER_WEP;
        }
        else
        {
            // 无密码
            type = WifiCipherType.WIFI_CIPHER_NOPASS;
        }
        Log.d("WIFITEST", "CreateWifiInfo2: type = " + type + " pwd = " + pwd);
        WifiConfiguration config = CreateWifiInfo(wifiinfo.SSID,
                wifiinfo.BSSID, pwd, type);
        return config;
    }

    private void sortByPriority(List<WifiConfiguration> paramList)
    {
        Collections.sort(paramList, new SjrsWifiManagerCompare());
    }

    private int shiftPriorityAndSave()
    {
        List<WifiConfiguration> localList = meWifiManager
                .getConfiguredNetworks();
        sortByPriority(localList);
        int i = localList.size();
        for (int j = 0; ; ++j)
        {
            if (j >= i)
            {
                meWifiManager.saveConfiguration();
                return i;
            }
            WifiConfiguration localWifiConfiguration = (WifiConfiguration) localList.get(j);
            localWifiConfiguration.priority = j;
            meWifiManager.updateNetwork(localWifiConfiguration);
        }
    }

    public WifiConfiguration setMaxPriority(WifiConfiguration config)
    {
        int priority = getMaxPriority() + 1;
        if (priority > 99999)
        {
            priority = shiftPriorityAndSave();
        }

        config.priority = priority;
        meWifiManager.updateNetwork(config);

        // 本机之前配置过此wifi热点，直接返回
        return config;
    }

    private int getMaxPriority()
    {
        List<WifiConfiguration> localList = meWifiManager
                .getConfiguredNetworks();
        int i = 0;
        Iterator<WifiConfiguration> localIterator = localList.iterator();
        while (true)
        {
            if (!localIterator.hasNext())
            {
                return i;
            }
            WifiConfiguration localWifiConfiguration = (WifiConfiguration) localIterator
                    .next();
            if (localWifiConfiguration.priority <= i)
            {
                continue;
            }
            i = localWifiConfiguration.priority;
        }
    }

    /**
     * 配置一个连接
     */
    public WifiConfiguration CreateWifiInfo(String SSID, String BSSID,
                                            String password, WifiCipherType type)
    {

        int priority;

        WifiConfiguration config = this.IsExsits(SSID);
        if (config != null)
        {
            // 本机之前配置过此wifi热点，调整优先级后，直接返回
            return setMaxPriority(config);
        }

        config = new WifiConfiguration();
        /* 清除之前的连接信息 */
        config.allowedAuthAlgorithms.clear();
        config.allowedGroupCiphers.clear();
        config.allowedKeyManagement.clear();
        config.allowedPairwiseCiphers.clear();
        config.allowedProtocols.clear();
        config.SSID = "\"" + SSID + "\"";
        config.status = WifiConfiguration.Status.ENABLED;

        priority = getMaxPriority() + 1;
        if (priority > 99999)
        {
            priority = shiftPriorityAndSave();
        }

        config.priority = priority; // 2147483647;
		/* 各种加密方式判断 */
        if (type == WifiCipherType.WIFI_CIPHER_NOPASS)
        {

            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

        }
        else if (type == WifiCipherType.WIFI_CIPHER_WEP)
        {
            config.preSharedKey = "\"" + password + "\"";

            config.allowedAuthAlgorithms
                    .set(WifiConfiguration.AuthAlgorithm.SHARED);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
            config.allowedGroupCiphers
                    .set(WifiConfiguration.GroupCipher.WEP104);
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            config.wepTxKeyIndex = 0;
        }
        else if (type == WifiCipherType.WIFI_CIPHER_WPA_EAP)
        {

            config.preSharedKey = "\"" + password + "\"";
            config.hiddenSSID = true;
            config.status = WifiConfiguration.Status.ENABLED;
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP);

            config.allowedPairwiseCiphers
                    .set(WifiConfiguration.PairwiseCipher.TKIP);
            config.allowedPairwiseCiphers
                    .set(WifiConfiguration.PairwiseCipher.CCMP);
            config.allowedProtocols.set(WifiConfiguration.Protocol.RSN
                    | WifiConfiguration.Protocol.WPA);

        }
        else if (type == WifiCipherType.WIFI_CIPHER_WPA_PSK)
        {

            config.preSharedKey = "\"" + password + "\"";
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);

            config.allowedPairwiseCiphers
                    .set(WifiConfiguration.PairwiseCipher.TKIP);
            config.allowedPairwiseCiphers
                    .set(WifiConfiguration.PairwiseCipher.CCMP);
            config.allowedProtocols.set(WifiConfiguration.Protocol.RSN
                    | WifiConfiguration.Protocol.WPA);

        }
        else if (type == WifiCipherType.WIFI_CIPHER_WPA2_PSK)
        {

            config.preSharedKey = "\"" + password + "\"";
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);

            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);

            config.allowedPairwiseCiphers
                    .set(WifiConfiguration.PairwiseCipher.TKIP);
            config.allowedPairwiseCiphers
                    .set(WifiConfiguration.PairwiseCipher.CCMP);
            config.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
            config.allowedProtocols.set(WifiConfiguration.Protocol.WPA);

        }
        else
        {
            return null;
        }

        return config;
    }

    public boolean ConnectToNetID(int netID)
    {
        return meWifiManager.enableNetwork(netID, true);
    }

    /**
     * 定义几种加密方式，一种是WEP，一种是WPA/WPA2，还有没有密码的情况
     */
    public enum WifiCipherType
    {
        WIFI_CIPHER_WEP, WIFI_CIPHER_WPA_EAP, WIFI_CIPHER_WPA_PSK, WIFI_CIPHER_WPA2_PSK, WIFI_CIPHER_NOPASS
    }

    class SjrsWifiManagerCompare implements Comparator<WifiConfiguration>
    {
        public int compare(WifiConfiguration paramWifiConfiguration1,
                           WifiConfiguration paramWifiConfiguration2)
        {
            return paramWifiConfiguration1.priority
                    - paramWifiConfiguration2.priority;
        }
    }

}
