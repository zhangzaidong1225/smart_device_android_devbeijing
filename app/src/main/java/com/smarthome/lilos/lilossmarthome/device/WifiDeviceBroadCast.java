package com.smarthome.lilos.lilossmarthome.device;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.smarthome.lilos.lilossmarthome.rxbus.EventKey;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;

/**
 * Created by Tristan on 2016/11/18.
 * Wifi设备的广播操作
 */

public class WifiDeviceBroadCast
{

    private static final String TAG = WifiDeviceBroadCast.class.getSimpleName();
    Context mContext = null;

    public WifiDeviceBroadCast(Context context)
    {
        mContext = context;

    }

    public void broadCastDeviceChange()
    {
        RxBus.getInstance().send(Events.MQTT_DEVICE_CHANGE);
    }

    /**
     * 设备上下线消息：发送到控制页面和设备列表
     * 发送到控制页面反应提示用户无法操作，发送到设备列表页面更新列表状态
     *
     * @param context
     * @param onlinestatus: true--online ,false --offline
     */

    public void broadCastDeviceOnlinStatus(boolean onlinestatus, String deviceID)
    {
        Log.d(TAG, "broadCastDeviceOnlinStatus: ------------------>>>>onlinestatus = " + onlinestatus + "  deviceID = " + deviceID);

        Bundle bundle = new Bundle();
        bundle.putString(EventKey.MQTT_DEVICE_ID,deviceID);
        bundle.putBoolean(EventKey.MQTT_ONLINE_STATUS,onlinestatus);
        RxBus.getInstance().send(Events.MQTT_DEVICE_ONLINE,bundle);
    }

    public void broadBindDevice(String fromDeviceID)
    {
        RxBus.getInstance().send(Events.MQTT_ADD_DEVICE,fromDeviceID);
    }

    /**
     * 设备消息，仅仅发送到设备控制页面。
     *
     * @param context
     * @param message
     * @param deviceID
     */
    public void broadcastMessageArrivedContext(Context context, String message, String deviceID)
    {
        Intent broadCastIntent = new Intent();

        broadCastIntent.setAction(deviceID);
        broadCastIntent.putExtra("devid", deviceID);
        broadCastIntent.putExtra("message", message);
        context.sendBroadcast(broadCastIntent);
    }

    /**
     * @param message ACTION_LIANLUOIM_BROADCAST_MESSAGE
     */
    public void broadcastMessageArrivedContext(String message)
    {
        RxBus.getInstance().send(Events.MQTT_MESSAGE,message);
    }

}
