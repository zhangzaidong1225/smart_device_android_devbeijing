package com.smarthome.lilos.lilossmarthome;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

import com.lianluo.lianluoIM.LianluoIM;
import com.lianluo.lianluoIM.OnMessageListener;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.orhanobut.logger.Logger;
import com.smarthome.lilos.lilossmarthome.bluetooth.BluetoothManageService;
import com.smarthome.lilos.lilossmarthome.device.DeviceManager;
import com.smarthome.lilos.lilossmarthome.device.WifiDeviceBroadCast;
import com.smarthome.lilos.lilossmarthome.greendao.GreenDaoHelper;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.AMAPMethods;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.DownloadMethods;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.MopsV2Methods;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.UserCenterMethods;
import com.smarthome.lilos.lilossmarthome.notify.Notify;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.utils.ConfigUtils;
import com.smarthome.lilos.lilossmarthome.utils.CrashHandler;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.SystemUtils;
import com.smarthome.lilos.lilossmarthome.utils.control.ControlManager;
import com.smarthome.lilos.lilossmarthome.utils.control.OfflinePackageManage;
import com.tencent.smtt.sdk.QbSdk;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import ly.count.android.sdk.Countly;
import ly.count.android.sdk.DeviceId;


/**
 * Created by Kevin on 2016/6/27.
 */
public class SmartHomeApplication extends Application implements OnMessageListener
{
    /**
     * MQTT JSON TAGs
     */
    public static final String MQTT_JSONTAG_NOTIFYID = "notification_builder_id";
    public static final String MQTT_JSONTAG_CUSTOMCONTENT = "custom_content";
    public static final String MQTT_JSONTAG_DEVID = "devid";
    public static final String MQTT_JSONTAG_ONLINESTAT = "onlinestat";
    public static final String MQTT_JSONTAG_BINDSTAT = "bindstat";
    public static final String MQTT_JSONTAG_OPTION = "op";
    public static final String MQTT_JSONTAG_DEVCHANGE = "devchange";
    public static final String APPKEY = "6eb3c19e-5b16-3d8f-bd7f-e1107a001110";//6eb3c19e-5b16-3d8f-bd7f-e1107a001110
    public static final String APPSECRET = "T10hIRmzgx8sRyXSdXAGHxhkl-UhK4ox";//"GiX_oEFhKDYbUmydiOvZnITsOs3BG5Gg";
    private static final String TAG = "SMART HOME APP";
    public static Context mContext;
    public static BluetoothManageService mBluetoothService;
    public static DeviceManager myDeviceManager = null;
    //推送id
    public static String myClientID = null;
    private static SmartHomeApplication mInstance;
    public WifiDeviceBroadCast mybroadCast = null;

    public static boolean isAlert = false;




    private ServiceConnection mXinfengServiceConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service)
        {
            if (service instanceof BluetoothManageService.BluetoothLocalBinder)
            {
                mBluetoothService = ((BluetoothManageService.BluetoothLocalBinder) service).getService();
                mBluetoothService.initData();
                if (!mBluetoothService.initialize())
                {
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name)
        {
            mBluetoothService = null;
        }
    };

    public static SmartHomeApplication getInstance()
    {
        if (mInstance == null)
        {
            mInstance = new SmartHomeApplication();
        }
        return mInstance;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        QbSdk.initX5Environment(getApplicationContext(),null);
//        CrashReport.initCrashReport(getApplicationContext(), "126cf94498", false);

        SharePreferenceUtils.init(this);
        OfflinePackageManage.init(this);
        ConfigUtils.init(this);
        Logger.init("SMART HOME");
        AppToast.init(this);
        ControlManager.init();

        CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init(getApplicationContext());

        Countly.sharedInstance().setLoggingEnabled(true);
        Countly.sharedInstance().init(this, "https://mops-countly.lianluo.com", "c0a6eba99aea90a288b24f7bdfb199880f0a2913", null, DeviceId.Type.OPEN_UDID);
        Countly.sharedInstance().enableCrashReporting();

        String language = Locale.getDefault().getLanguage() + "-" + Locale.getDefault().getCountry();
        UserCenterMethods.getInstance().init(language);
        AMAPMethods.getInstance().init();
        MopsV2Methods.getInstance().init(language);
        DownloadMethods.getInstance().init();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .threadPoolSize(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .build();
        ImageLoader.getInstance().init(config);

        //home问题，被杀死
        if (mBluetoothService != null){
            mBluetoothService.stopSelf();
            mBluetoothService = null;
        }

        Intent xinfengServiceIntent = new Intent(this, BluetoothManageService.class);
        bindService(xinfengServiceIntent, mXinfengServiceConnection, Context.BIND_AUTO_CREATE);

        //设备列表初始化
        myDeviceManager = DeviceManager.getInstance();
        myDeviceManager.init(getApplicationContext());
        myDeviceManager.initData();


        mContext = this;
        mInstance = this;

        if (SystemUtils.isNetworkConn(getApplicationContext()))
        {
            startMQTTService();
        }
        mybroadCast = new WifiDeviceBroadCast(getApplicationContext());

        GreenDaoHelper.initDatabase();
        GreenDaoHelper.initLocationDatabase();
        Notify notify = new Notify(this);
    }

    public void startMQTTService()
    {
        LianluoIM.startIMWork(getApplicationContext(), APPKEY, APPSECRET, getPackageName(), this);
    }

    public String getDeviceID()
    {
        //        TelephonyManager tm  = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        String devid = null;
        devid = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("MQTTtest", "getDevid: ---------------------->>>>devid = " + devid + " length = " + devid.length());
        return devid;
    }

    @Override
    public void onTerminate()
    {
        unbindService(mXinfengServiceConnection);
        super.onTerminate();
    }

    @Override
    public void onConnected()
    {
        Log.d(TAG, "onConnected: -------------MQTT服务器已经连接");
        LianluoIM.getClientID();
    }

    @Override
    public void onDisconnected()
    {

    }
    @Override
    public void onMessageGot(String s)
    {
        Log.e("推送======", "onMessageGot: ---------------MQTT服务器传来的消息：： " + s);
        parseMqttMessage(s);

        Notify.parseMqttMessage(s);
        if(Notify.mNotifyData.getCustomContent_is_notify() == 1 && !Notify.mNotifyData.isParseMqttMessageError()){
            Notify.createNotification();
        }
    }

    @Override
    public void onTagsGot(String s)
    {

    }

    @Override
    public void onGetClientId(String s)
    {
        myClientID = s;
        Log.d(TAG, "myClientID:"+myClientID);
        SharePreferenceUtils.setMqttClient(s);
    }

    @Override
    public void onDevListGot(String s)
    {

    }
    private void parseMqttMessage(String s)
    {
        String mqttMessage = s;
        boolean isDeviceOpMessage = false;

        try
        {
            JSONObject mqttObj = new JSONObject(mqttMessage);
            if (mqttObj.has("from"))
            {
                String messageFrom = mqttObj.getString("from");
                Log.d(TAG, "parseMqttMessage: messageFrom = " + messageFrom);
                if ((!messageFrom.contains("SYSTEM")) && (!messageFrom.contains("SERVER")))
                {
                    if ((mqttObj.has(MQTT_JSONTAG_CUSTOMCONTENT)))
                    {
                        //有Custom_content
                        JSONObject customContent = mqttObj.getJSONObject(MQTT_JSONTAG_CUSTOMCONTENT);
                        if (customContent.has(MQTT_JSONTAG_OPTION))
                        {
                            //参数相关
                            isDeviceOpMessage = true;
                            String option = customContent.getString(MQTT_JSONTAG_OPTION);
                            if (option.equals("feedback"))
                            {
                                Log.d(TAG, "parseMqttMessage: has feedback");
                                //反馈信息
                                if (customContent.has(MQTT_JSONTAG_ONLINESTAT))
                                {
                                    String stat = customContent.getString(MQTT_JSONTAG_ONLINESTAT);
                                    String bindstat = customContent.getString(MQTT_JSONTAG_BINDSTAT);
                                    Log.d(TAG, "onGetMessage: Device OnlineStat = " + stat + "  bindstat = " + bindstat);
                                    // TODO: 2016/8/18 调用绑定接口。
                                    if (stat.equalsIgnoreCase("on"))
                                    {
                                        Log.d(TAG, "onMessageGot?>>>>>>parseMqttMessage: on------------");
                                        if (bindstat.equalsIgnoreCase("false"))
                                        {
//                                            startToBindDevices(messageFrom);
                                            mybroadCast.broadBindDevice(messageFrom);
                                            Log.d(TAG, "onMessageGot: -------------------->>设备上线，未绑定");

                                        }
                                        else
                                        {
                                            Log.d(TAG, "onMessageGot: -------------------->>设备上线，已绑定");
                                            mybroadCast.broadCastDeviceOnlinStatus(true, messageFrom);
                                        }

                                    }
                                    else
                                    {
                                        Log.d(TAG, "onMessageGot?>>>>>>parseMqttMessage: OFF------------");

                                        if (bindstat.equalsIgnoreCase("false"))
                                        {
                                            Log.d(TAG, "onMessageGot: -------------------->>设备下线，未绑定");
                                        }
                                        else
                                        {
                                            Log.d(TAG, "onMessageGot: -------------------->>设备下线，已绑定");
                                            mybroadCast.broadCastDeviceOnlinStatus(false, messageFrom);
                                        }
                                    }
                                }
                            }
                        }

                        if (customContent.has(MQTT_JSONTAG_DEVCHANGE))
                        {
                            isDeviceOpMessage = true;
                            String devchange = customContent.getString(MQTT_JSONTAG_DEVCHANGE);
                            Log.d(TAG, "parseMqttMessage: ----------------->>>>devchange = " + devchange);
                            mybroadCast.broadCastDeviceChange();
                        }
                        if (isDeviceOpMessage == false)
                        {
                            mybroadCast.broadcastMessageArrivedContext(getApplicationContext(), s, messageFrom);
                        }

                    }
                    else
                    {
                        //无CUstom——content
                        mybroadCast.broadcastMessageArrivedContext(getApplicationContext(), s, messageFrom);
                    }
                }
            }
            else
            {
                //系统消息
                if ((mqttObj.has(MQTT_JSONTAG_CUSTOMCONTENT)))
                {
                    //有Custom_content
                    JSONObject customContent = mqttObj.getJSONObject(MQTT_JSONTAG_CUSTOMCONTENT);

                    if (customContent.has(MQTT_JSONTAG_DEVCHANGE))
                    {
                        String devchange = customContent.getString(MQTT_JSONTAG_DEVCHANGE);
                        Log.d(TAG, "parseMqttMessage: ----------------->>>>devchange = " + devchange);
                        mybroadCast.broadCastDeviceChange();
                    }
                    mybroadCast.broadcastMessageArrivedContext(s);
                }
                else
                {
                    //无CUstom——content
                    mybroadCast.broadcastMessageArrivedContext(s);
                }
            }
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
    }
}