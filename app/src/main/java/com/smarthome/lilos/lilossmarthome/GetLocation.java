package com.smarthome.lilos.lilossmarthome;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.bluetooth.BLEService;
import com.smarthome.lilos.lilossmarthome.bluetooth.DfuService;
import com.smarthome.lilos.lilossmarthome.device.BaseBluetoothDevice;
import com.smarthome.lilos.lilossmarthome.device.BaseDevice;
import com.smarthome.lilos.lilossmarthome.device.DeviceManager;
import com.smarthome.lilos.lilossmarthome.jsscope.BleJsScope;
import com.smarthome.lilos.lilossmarthome.jsscope.bean.CallbackBean;
import com.smarthome.lilos.lilossmarthome.jsscope.bean.SubscribeBean;
import com.smarthome.lilos.lilossmarthome.jsscope.holder.LocationHolder;
import com.smarthome.lilos.lilossmarthome.jsscope.method.IM;
import com.smarthome.lilos.lilossmarthome.jsscope.util.InjectedChromeClient;
import com.smarthome.lilos.lilossmarthome.network.bean.amap.AMAPAddress;
import com.smarthome.lilos.lilossmarthome.network.bean.amap.AMAPLocation;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceFirmwareEntity;
import com.smarthome.lilos.lilossmarthome.network.retrofit.BaseSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.AMAPMethods;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.MopsV2Methods;
import com.smarthome.lilos.lilossmarthome.rxbus.EventKey;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.utils.ConvertUtils;
import com.smarthome.lilos.lilossmarthome.utils.CountlyUtil;
import com.smarthome.lilos.lilossmarthome.utils.DevUpdateUtil;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.StatusbarColorUtils;
import com.smarthome.lilos.lilossmarthome.utils.SystemUtils;
import com.smarthome.lilos.lilossmarthome.wigdet.ProgressBarDialog;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import no.nordicsemi.android.dfu.DfuProgressListener;
import no.nordicsemi.android.dfu.DfuProgressListenerAdapter;
import no.nordicsemi.android.dfu.DfuServiceInitiator;
import no.nordicsemi.android.dfu.DfuServiceListenerHelper;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

import static com.smarthome.lilos.lilossmarthome.SmartHomeApplication.myDeviceManager;

public class GetLocation
{

    private double lat;
    private double lng;

    private void getLocation(Context context)
    {
        Map<String, Object> resutlMap = new HashMap<>();
        resutlMap.clear();
        String locationStr = LocationHolder.getLocation(context);

        if (locationStr == null || locationStr.equals(""))
        {
            resutlMap.put("status", "fail");
            return;
        }

        AMAPMethods.getInstance().locationToAmap(locationStr)
                .flatMap(amapLocation -> {
                    onLocationResult(amapLocation);
                    return AMAPMethods.getInstance().address(amapLocation.locations);
                })
                .subscribe(new BaseSubscriber<AMAPAddress>()
                {
                    @Override
                    public void onNext(AMAPAddress amapAddress)
                    {
                        onAddressResult(amapAddress);
                    }
                });
    }

    private void onLocationResult(AMAPLocation location)
    {
        String lngStr = location.locations.split(",")[0];
        String latStr = location.locations.split(",")[1];
        DecimalFormat df = new DecimalFormat("0.00000");
        lngStr = df.format(ConvertUtils.convertToDouble(lngStr, 0));
        latStr = df.format(ConvertUtils.convertToDouble(latStr, 0));
        lng = ConvertUtils.convertToDouble(lngStr, 0);
        lat = ConvertUtils.convertToDouble(latStr, 0);
    }

    private void onAddressResult(AMAPAddress address)
    {
        Map<String, Object> resutlMap = new HashMap<>();
        resutlMap.put("status", "success");
        resutlMap.put("latitude", lat);
        resutlMap.put("longitude", lng);
        resutlMap.put("address", address.getRegeocode().getFormatted_address());;
    }

}
