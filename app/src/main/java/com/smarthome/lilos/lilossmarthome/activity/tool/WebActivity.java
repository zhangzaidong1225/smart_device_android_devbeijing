package com.smarthome.lilos.lilossmarthome.activity.tool;

import android.os.Bundle;
//import android.webkit.WebResourceError;
//import android.webkit.WebResourceRequest;
//import android.webkit.WebSettings;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.tencent.smtt.export.external.interfaces.WebResourceError;
import com.tencent.smtt.export.external.interfaces.WebResourceRequest;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;
import com.umeng.analytics.MobclickAgent;

/**
 * Web测试界面,无入口
 */
public class WebActivity extends BaseActivity
{
    private ImageView mIvBack;
    private TextView mTvTitle;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        webView = (WebView) findViewById(R.id.webview);
        mIvBack.setOnClickListener(v -> {
            MobclickAgent.onEvent(this, "WebActivity_back");
            finish();
        });

        final String urlStr = getIntent().getStringExtra("url");
        if (urlStr == null || urlStr.equals(""))
        {
            finish();
        }

        WebSettings ws = webView.getSettings();
        ws.setJavaScriptEnabled(true);
        ws.setDefaultTextEncodingName("UTF-8");
        webView.setWebViewClient(new WebViewClient()
        {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request,
                                        WebResourceError error)
            {
                super.onReceivedError(view, request, error);
                view.loadUrl("file:///android_asset/nowifi.html");
            }

            @Override
            public void onPageFinished(WebView view, String url)
            {
                view.loadUrl("javascript:setRelaodUrl('" + urlStr + "')");
                mTvTitle.setText(view.getTitle());
                super.onPageFinished(view, url);
            }
        });

        webView.loadUrl(urlStr);
    }
}
