package com.smarthome.lilos.lilossmarthome.jsscope.method;

import android.util.Log;
//import android.webkit.WebView;

import com.lianluo.lianluoIM.LianluoIM;
import com.lianluo.lianluoIM.OnSendMessageListener;
import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.jsscope.bean.CallbackBean;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.smarthome.lilos.lilossmarthome.utils.MessageUtls;
import com.tencent.smtt.sdk.WebView;

import java.util.HashMap;
import java.util.Map;

/**
* 对应JSSDK中IM集指令
 * Created by Tristan on 2016/11/8.
 */

public class IM
{

    private static final String TAG = "LianluoIM";
    private static CallbackBean bean = new CallbackBean();
    private static Map<String, Object> resultMap = new HashMap<>();

    public static boolean getDeviceId(final WebView webView, Map<String, String> params,
                                      final String callBackFuncName)
    {
        Log.d(TAG, "getDeviceId: 调用 getDeviceID  params = " + params);

        resultMap.clear();
        bean.callBackName = callBackFuncName;

        String curDevID = (String) webView.getTag(R.id.device_mac_address);
        resultMap.put("deviceId", curDevID);
        bean.callBackParams = JsonUtils.objectToJson(resultMap);
        Log.d(TAG, "getDeviceId: ------------>>>>> JsonUtils.objectToJson(bean) = " + JsonUtils.objectToJson(bean));
        webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");

        return true;
    }

    public static boolean sendMessage(final WebView webView, Map<String, String> params,
                                      final String callBackFuncName)
    {

        Log.d(TAG, "sendMessage: params = " + params.toString());
        resultMap.clear();
        bean.callBackName = callBackFuncName;
        String curDevID = (String) webView.getTag(R.id.device_mac_address);
        String curClientID = (String) webView.getTag(R.id.app_client_id);

//        Log.d(TAG, "sendMessage: params ---->> "+params);
        Log.d(TAG, "sendMessage: devid = " + params.get("deviceId") + " content = " + params.get("content"));
        String sendString = MessageUtls.convertMessage(curClientID, params.get("content"));

        LianluoIM.sendMessage(null, curDevID, sendString, new OnSendMessageListener()
        {
            @Override
            public void onMessageSendSuccess()
            {
                Log.d(TAG, "onSuccess: [OnSendMessageListener ] --------------------->>> ");
                resultMap.put("status", "success");
                bean.callBackParams = JsonUtils.objectToJson(resultMap);
                Log.d(TAG, "onSuccess: --------------->>>>参数 " + bean.callBackParams);
                webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
            }

            @Override
            public void onMessageSendFailed()
            {
                Log.d(TAG, "onSuccess: [OnSendMessageListener ] --------------------->>> ");

                resultMap.put("status", "failed");
                bean.callBackParams = JsonUtils.objectToJson(resultMap);
                webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
            }

            @Override
            public void onDeviceMessageGot(String s)
            {

            }


        });

        return true;
    }


    public static boolean deviceOnline(final WebView webView, final String deviceid)
    {
        resultMap.clear();
        resultMap.put("deviceId", deviceid);
        webView.loadUrl("javascript:LL.im.deviceOnline(" + JsonUtils.objectToJson(resultMap) + ")");
        return true;
    }

    public static boolean deviceOffline(final WebView webView, final String deviceid)
    {
        Log.d(TAG, "deviceOffline: [native] offline ");
        resultMap.clear();
        resultMap.put("deviceId", deviceid);
        webView.loadUrl("javascript:LL.im.deviceOffline(" + JsonUtils.objectToJson(resultMap) + ")");
        return true;
    }

    public static boolean receiveMessage(final WebView webView, final String deviceid,
                                         final String content)
    {
        resultMap.clear();
        resultMap.put("deviceId", deviceid);
        resultMap.put("content", content);
        Log.d(TAG, "receiveMessage: ___JS---收到普通消息 __   " + JsonUtils.objectToJson(resultMap));
        webView.loadUrl("javascript:LL.im.receiveMessage(" + JsonUtils.objectToJson(resultMap) + ")");
        return true;
    }
}
