package com.smarthome.lilos.lilossmarthome.device;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.lianluo.lianluoIM.LianluoIM;
import com.orhanobut.logger.Logger;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.UserDeviceEntity;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.login.LoginInterceptor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 设备管理类
 * 用于管理当前全部的设备,包括蓝牙与Wifi
 * Created by Tristan on 2016/11/16.
 */

public class DeviceManager
{
    public static final String SHAREPREFERENCE_NAME = "LIANLUO_SMARTDEV_SP";
//    public static final String SHAREPREFERENCE_NAME_OFF = "LIANLUO_SMARTDEV_SP_OFF";
    private static final String TAG = "DeviceManager";
    public static ConcurrentHashMap<String, BaseDevice> mData;
    public static ConcurrentHashMap<String, BaseDevice> mDataOff;
    static DeviceManager myDeviceManager = null;
    private static Context mContext = null;
    public DeviceMap deviceMap;

    public static DeviceManager getInstance()
    {
        if (myDeviceManager == null)
        {
            myDeviceManager = new DeviceManager();
        }
        return myDeviceManager;

    }

    public void init(Context context)
    {
        mContext = context;
    }

    public BaseDevice getDevice(String key)
    {
        if (!SharePreferenceUtils.getLoginStatus()) {
            return mDataOff.get(key);
        }
            return mData.get(key);

    }

    public void initData()
    {
        mData = new ConcurrentHashMap<>();
        deviceMap = new DeviceMap();
        deviceMap.map = new HashMap<>();
        deviceMap.map = mData;

        mDataOff = new ConcurrentHashMap<>();
        deviceMap.mapOff = new HashMap<>();
        deviceMap.mapOff = mDataOff;
    }


    /***
     * 清除数据
     *
     * @param mark 0-清除所有  1-只清除未配对
     */
    public void clearData(int mark)
    {
        Logger.v("清除蓝牙服务中的设备数据");
        if (!SharePreferenceUtils.getLoginStatus()) {
            if (mark == 0)
            {
                mDataOff.clear();
                SharedPreferences sp = mContext.getSharedPreferences(SHAREPREFERENCE_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("BLE_DATA_OFF", "");
//                editor.clear();
                editor.apply();
            }
            if (mark == 1)
            {
                if (mDataOff.size() > 0)
                {
                    for (Iterator iter = mDataOff.entrySet().iterator(); iter.hasNext(); )
                    {
                        Map.Entry element = (Map.Entry) iter.next();
                        Object strObj = element.getValue();

                        final BaseBluetoothDevice device = (BaseBluetoothDevice) strObj;
                        if (device.getIsconnectd() == 0 && !device.ismatchd())
                        {
                            iter.remove();
                        }
                    }
                }
            }
        } else {
            if (mark == 0)
            {
                mData.clear();
                SharedPreferences sp = mContext.getSharedPreferences(SHAREPREFERENCE_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("BLE_DATA", "");
//                editor.clear();
                editor.apply();
            }
            if (mark == 1)
            {
                if (mData.size() > 0)
                {
                    for (Iterator iter = mData.entrySet().iterator(); iter.hasNext(); )
                    {
                        Map.Entry element = (Map.Entry) iter.next();
                        Object strObj = element.getValue();

                        final BaseBluetoothDevice device = (BaseBluetoothDevice) strObj;
                        if (device.getIsconnectd() == 0 && !device.ismatchd())
                        {
                            iter.remove();
                        }
                    }
                }
            }
        }
    }

    /**
     * 离线清除数据
     */
    public void ClearOff(){
        SharedPreferences sp = mContext.getSharedPreferences(SHAREPREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("BLE_DATA_OFF", "");
//      editor.clear();
        editor.apply();
    }


    /***
     * 反序列化数据
     */
    public void readData()
    {
        Log.d("===", "从本地获取设备列表信息");
        if (SharePreferenceUtils.getLoginStatus()) {
            Log.d("===", "从本地获取设备列表信息--L");

            mData.clear();
            SharedPreferences sp = mContext.getSharedPreferences(SHAREPREFERENCE_NAME, Context.MODE_APPEND);
            String result = sp.getString("BLE_DATA", "");
            if (!result.isEmpty()) {
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject itemObject = jsonArray.getJSONObject(i);
                        BaseDevice device = DeviceFactory.createDevice(itemObject);
                        if (device != null) {
                            addData(device.getAddress(), device);
//                        broadcastUpdate(ADD_DEVICE, device.getAddress());
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {

            mDataOff.clear();
            SharedPreferences sp = mContext.getSharedPreferences(SHAREPREFERENCE_NAME, Context.MODE_APPEND);
            String result = sp.getString("BLE_DATA_OFF", "");
            Log.d("===", "从本地获取设备列表信息 --- LO--"+result);
            if (!result.isEmpty()) {
                try {

                    JSONArray jsonArray = new JSONArray(result);
                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject itemObject = jsonArray.getJSONObject(i);

                        BaseDevice device = DeviceFactory.createDevice(itemObject);

                        if (device != null) {
                            Log.e("put----addData-1","  :");
//                          addData(device.getAddress(), device);
                            mDataOff.put(device.getAddress(), device);
//                        broadcastUpdate(ADD_DEVICE, device.getAddress());
                        }
                    }
//                    Log.d("===", "从本地获取设备列表信息 --- LO-3-"+mDataOff.size());

                    saveData();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("===", "从本地获取设备列表信息 --- LO-e-");

                }
            }
        }
    }

    /**
     * 解析服务器中获取的设备列表
     *
     * @param data
     */
    public void getDeviceList(List<UserDeviceEntity> data)
    {
        Log.d(TAG, "从服务器获取设备列表，解析列表内容");

        mData.clear();
        if (!data.isEmpty())
        {
            for (UserDeviceEntity dev : data)
            {
                BaseDevice device = DeviceFactory.createDevice(dev);
                if (device != null)
                {
                    addData(device.getAddress(), device);
                }
            }
        }
    }


    /***
     * 清除一条数据
     * 取消配对时删除数据
     *
     * @param address 需要删除的设备MAC地址
     */
    public void removeData(String address)
    {
        Logger.v("===删除蓝牙服务内的设备信息，MAC地址为：" + address);
        if (SharePreferenceUtils.getLoginStatus()) {
            Iterator iter = mData.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                Object val = entry.getValue();
                BaseDevice device = (BaseDevice) val;
                if (device.getAddress().equals(address)) {
//                    device.disconnect();
                    iter.remove();
                    break;
                }
            }

            if (mData.isEmpty()) {
                clearData(0);
            } else {
                saveData();
            }
        } else {
            Log.d("===","删除蓝牙服务内的设备信息，MAC地址为：" + address);
            Iterator iter = mDataOff.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                Object val = entry.getValue();
                BaseDevice device = (BaseDevice) val;
                if (device.getAddress().equals(address)) {
//                    device.disconnect();
                    iter.remove();
                    Log.d("===shanchu","  --删除蓝牙服务内的设备信息，MAC地址为：" + address);
                    break;
                }
            }
            if (mDataOff.isEmpty()) {
                clearData(0);
            } else {
                saveData();
            }
            Log.d("===","删除蓝牙服务内的设备信息，MAC地址为：" + mDataOff.size());

        }
    }

    /***
     * 序列化数据
     * 将连接成功的数据 进行存储
     */

    public void saveData()
    {
        Log.d("===","savedata");

        JSONArray jsonArray = new JSONArray();
        if (SharePreferenceUtils.getLoginStatus()) {

            Collection values = mData.values();
            for (Iterator iterator = values.iterator(); iterator.hasNext(); ) {
                JSONObject jsonObject = new JSONObject();
                Object obj = iterator.next();
                BaseDevice device = (BaseDevice) obj;
                switch (device.getTypeId()) {
                    case DeviceFactory.XINFENG:
                        BaseBluetoothDevice mybtdevice = (BaseBluetoothDevice) obj;
                        try {
                            jsonObject.put("address", mybtdevice.getAddress());
                            jsonObject.put("name", mybtdevice.getName());
                            jsonObject.put("autoconnect", mybtdevice.isAutoConnect() || mybtdevice.isAutoConnect1());
                            jsonObject.put("matched", mybtdevice.ismatchd());
                            jsonObject.put("typeId", mybtdevice.getTypeId());
                            jsonObject.put("is_demo", mybtdevice.getIs_demo());
                            jsonArray.put(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;
                    case DeviceFactory.WIFI_TEST_DEVICE:
                        WifiDevice myWifiDevice = (WifiDevice) obj;
                        try {
                            jsonObject.put("address", myWifiDevice.getAddress());
                            jsonObject.put("name", myWifiDevice.getName());
                            jsonObject.put("typeId", myWifiDevice.getTypeId());
                            jsonArray.put(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                }
            }

            SharedPreferences sp = mContext.getSharedPreferences(SHAREPREFERENCE_NAME, Context.MODE_APPEND);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("BLE_DATA", jsonArray.toString());
            editor.apply();

        } else {

            Collection values = mDataOff.values();
            Log.d("===","savedata---loginOut" + mDataOff.size());

            for (Iterator iterator = values.iterator(); iterator.hasNext(); ) {
                JSONObject jsonObject = new JSONObject();
                Object obj = iterator.next();
                BaseDevice device = (BaseDevice) obj;
                switch (device.getTypeId()) {
                    case DeviceFactory.XINFENG:
                        BaseBluetoothDevice mybtdevice = (BaseBluetoothDevice) obj;
                        try {
                            jsonObject.put("address", mybtdevice.getAddress());
                            jsonObject.put("name", mybtdevice.getName());
                            jsonObject.put("autoconnect", mybtdevice.isAutoConnect() || mybtdevice.isAutoConnect1());
                            jsonObject.put("matched", mybtdevice.ismatchd());
                            jsonObject.put("typeId", mybtdevice.getTypeId());
                            jsonObject.put("is_demo", mybtdevice.getIs_demo());
                            jsonArray.put(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;
                    case DeviceFactory.WIFI_TEST_DEVICE:
                        WifiDevice myWifiDevice = (WifiDevice) obj;
                        try {
                            jsonObject.put("address", myWifiDevice.getAddress());
                            jsonObject.put("name", myWifiDevice.getName());
                            jsonObject.put("typeId", myWifiDevice.getTypeId());
                            jsonArray.put(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case DeviceFactory.PM_DEVICE:
                        BaseBluetoothDevice myPmdevice = (BaseBluetoothDevice) obj;
                        try {
                            jsonObject.put("address", myPmdevice.getAddress());
                            jsonObject.put("name", myPmdevice.getName());
                            jsonObject.put("autoconnect", myPmdevice.isAutoConnect() || myPmdevice.isAutoConnect1());
                            jsonObject.put("matched", myPmdevice.ismatchd());
                            jsonObject.put("typeId", myPmdevice.getTypeId());
                            jsonObject.put("is_demo", myPmdevice.getIs_demo());
                            jsonArray.put(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;

                }
            }
            SharedPreferences sp = mContext.getSharedPreferences(SHAREPREFERENCE_NAME, Context.MODE_APPEND);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("BLE_DATA_OFF", jsonArray.toString());
            editor.apply();
        }

    }

    /***
     * 添加一条数据
     * 每连接成功一个就添加进去
     *
     * @param address
     * @param item
     */
    public void addData(String address, BaseDevice item)
    {
        if (SharePreferenceUtils.getLoginStatus()){
            Log.d("===","L---adddata");

            if (wifiDevices(item) == true)
            {
                LianluoIM.isDeviceOnline(item.getDevId());
            }

            mData.put(address, item);
            SmartHomeApplication.mBluetoothService.sendAddress(Events.DEVICE_ADDED, address);
            saveData();
        } else  {
            Log.e("put----add","  :"+address);
            mDataOff.put(address, item);
            SmartHomeApplication.mBluetoothService.sendAddress(Events.DEVICE_ADDED, address);
            saveData();
        }

    }

    private boolean wifiDevices(BaseDevice item)
    {
        if (item.getTypeId() == 10)
        {
            return true;
        }
        return false;
    }

    /***
     * 更新数据
     * 状态改变时，更新数据
     *
     * @param address
     * @param item
     */
    public void updateData(String address, Object item)
    {
        Log.d("===","updateData");
        if (SharePreferenceUtils.getLoginStatus()) {
            mData.put(address, (BaseDevice) item);
            saveData();
            RxBus.getInstance().send(Events.GATT_DATA_CHANGED);
        } else {
            Log.e("put----","  :"+address);
            mDataOff.put(address, (BaseDevice) item);
            saveData();
            RxBus.getInstance().send(Events.GATT_DATA_CHANGED);
        }
    }


    public void refreshWiFiDevices()
    {
        Iterator iter = mData.entrySet().iterator();
        while (iter.hasNext())
        {
            Map.Entry entry = (Map.Entry) iter.next();
            Object val = entry.getValue();
            BaseDevice device = (BaseDevice) val;
            Log.d(TAG, "refreshWiFiDevices: typeid = " + device.getTypeId() + "  id= " + device.getDevId());
            if (device instanceof WifiDevice)
            {
                Log.d(TAG, "refreshWiFiDevices: typeid = " + device.getTypeId() + "  id= " + device.getDevId());
                LianluoIM.addDevice(device.getDevId());
                Log.d(TAG, "refreshWiFiDevices:dddfdf ");
                LianluoIM.isDeviceOnline(device.getDevId());
            }
        }

    }
}
