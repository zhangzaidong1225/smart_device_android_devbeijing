package com.smarthome.lilos.lilossmarthome.network.bean.location;

import java.util.List;

/**
 * Created by songyu on 2017/4/12.
 */

public class CityListEntity {
    private List<CityData> city;

    public List<CityData> getCity() {
        return city;
    }

    public void setCity(List<CityData> city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "CityListEntity{" +
                "city=" + city +
                '}';
    }
}
