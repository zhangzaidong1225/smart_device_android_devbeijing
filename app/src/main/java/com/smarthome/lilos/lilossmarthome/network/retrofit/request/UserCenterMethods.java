package com.smarthome.lilos.lilossmarthome.network.retrofit.request;

import android.support.annotation.NonNull;
import android.util.Log;

import com.smarthome.lilos.lilossmarthome.network.URLManager;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.TokenEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.body.AccessTokenBody;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.body.ModPwdBody;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.body.SMSBody;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.body.UserBody;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.entity.AccessTokenEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.entity.RegisterEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.entity.SMSEntity;
import com.smarthome.lilos.lilossmarthome.network.retrofit.SchedulersTransformer;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Subscriber;

/**
 * 用户中心网络操作
 * Created by Joker on 2016/12/21.
 */

public class UserCenterMethods
{
    private static final String BASE_URL = new URLManager().getUserCenterServerUrl();//"https://mops-staging.lianluo.com/account/v1/";

    private static final String CLIENT_ID = new URLManager().getUserCenterClientId(); //"5c72f5fc-82cc-31a7-bb9e-42de8586e29f";
    private static final String CLIENT_SECRET = new URLManager().getUserCenterClientSecret();//"_rXe29BIwLs-4nfvGCXFpCNQuvBYMdJp";

    private static final int DEFAULT_TIMEOUT = 10;

    private Retrofit retrofit;
    private IUserCenterService service;

    private  String token;

    private UserCenterMethods()
    {
    }

    public static UserCenterMethods getInstance()
    {
        return SingletonHolder.INSTANCE;
    }

    public void init(String language)
    {
        String lan = "en-US";
        if (language.equals("zh-CN"))
        {
            lan = language;
        }
        String finalLan = lan;

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);

        client.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().header("Accept-Language", finalLan).build();
            return chain.proceed(request);
        });

        retrofit = new Retrofit.Builder()
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .build();

        service = retrofit.create(IUserCenterService.class);
    }

    public void setToken(String token)
    {
        Log.d("```","tryGgetToken---2"+ token);
        this.token = token;
        Log.d("```","tryGgetToken---3"+ this.token);

    }

    public void refreshToken(Subscriber<TokenEntity> subscriber, String refresh_token)
    {
        Map<String,Object> params = new HashMap<>();
        params.clear();
        params.put("client_id",CLIENT_ID);
        params.put("client_secret",CLIENT_SECRET);
        params.put("grant_type","refresh_token");
        params.put("refresh_token",refresh_token);
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), JsonUtils.objectToJson(params));

        service.refreshToken(body)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    public void getToken(Subscriber<AccessTokenEntity> subscriber, AccessTokenBody body)
    {
        service.getToken(body)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    public void sendSMS(Subscriber<SMSEntity> subscriber, SMSBody body)
    {
        Log.d("===1",token +"---"+body.toString());

        service.sendSMS(token, body)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    public void register(Subscriber<RegisterEntity> subscriber, UserBody body)
    {
        service.register(token, body)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    public void resetPassword(Subscriber<UserBody> subscriber, UserBody body)
    {
        service.resetPassword(token, body)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    public void modPassword(Subscriber<ModPwdBody> subscriber, ModPwdBody body)
    {
        service.modPassword(token, body)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    private static class SingletonHolder
    {
        private static final UserCenterMethods INSTANCE = new UserCenterMethods();
    }
}
