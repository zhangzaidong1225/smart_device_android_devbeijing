package com.smarthome.lilos.lilossmarthome.network.bean.mops;

/**
 * Created by Joker on 2016/12/30.
 */

public class TokenEntity
{
    /**
     * access_token : d8a0766944e0f13ba54fd28bd695f2d6db8cba9e
     * expires_in : 86400
     * token_type : Bearer
     * scope : null
     * refresh_token : bdbc762d0e2ccc44bd82e8e156f3c0590de6aab0
     */

    private String access_token;
    private int expires_in;
    private String token_type;
    private Object scope;
    private String refresh_token;

    public String getAccess_token()
    {
        return access_token;
    }

    public void setAccess_token(String access_token)
    {
        this.access_token = access_token;
    }

    public int getExpires_in()
    {
        return expires_in;
    }

    public void setExpires_in(int expires_in)
    {
        this.expires_in = expires_in;
    }

    public String getToken_type()
    {
        return token_type;
    }

    public void setToken_type(String token_type)
    {
        this.token_type = token_type;
    }

    public Object getScope()
    {
        return scope;
    }

    public void setScope(Object scope)
    {
        this.scope = scope;
    }

    public String getRefresh_token()
    {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token)
    {
        this.refresh_token = refresh_token;
    }
}
