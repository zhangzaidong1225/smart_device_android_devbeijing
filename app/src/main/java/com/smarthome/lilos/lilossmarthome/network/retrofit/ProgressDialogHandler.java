package com.smarthome.lilos.lilossmarthome.network.retrofit;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.smarthome.lilos.lilossmarthome.wigdet.WaitingDialog;

/**
 * 网络操作进度条工具
 * 用于网络操作等待进度条的具体操作
 * Created by Joker on 2016/12/21.
 */

public class ProgressDialogHandler extends Handler
{
    public static final int SHOW_PROGRESS_DIALOG = 0;
    public static final int DISMISS_PROGRESS_DIALOG = 1;

    private WaitingDialog wd;

    private Context context;
    private boolean cancelable;
    private ProgressCancelListener listener;

    public ProgressDialogHandler(Context context, ProgressCancelListener progressCancelListener,
                                 boolean cancelable)
    {
        super();
        this.context = context;
        this.listener = progressCancelListener;
        this.cancelable = cancelable;
    }

    private void initProgressDialog()
    {
        if (wd == null)
        {
            wd = new WaitingDialog(context);
            wd.setCancelable(cancelable);

            if (cancelable)
            {
                wd.setOnCancelListener(dialog -> listener.onCancelProgress());
            }

            if (!wd.isShowing())
            {
                wd.show();
            }
        }
    }

    private void dismissProgressDialog()
    {
        if (wd != null)
        {
            wd.dismiss();
            wd = null;
        }
    }

    @Override
    public void handleMessage(Message msg)
    {
        switch (msg.what)
        {
            case SHOW_PROGRESS_DIALOG:
                initProgressDialog();
                break;
            case DISMISS_PROGRESS_DIALOG:
                dismissProgressDialog();
                break;
        }
    }
}
