package com.smarthome.lilos.lilossmarthome.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.util.Log;
import android.util.SparseArray;

import com.orhanobut.logger.Logger;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.activity.MainActivity;
import com.smarthome.lilos.lilossmarthome.device.BaseBluetoothDevice;
import com.smarthome.lilos.lilossmarthome.device.DeviceFactory;
import com.smarthome.lilos.lilossmarthome.jsscope.bean.PeripheralBean;
import com.smarthome.lilos.lilossmarthome.network.bean.DeviceTypeListEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceTypeEntity;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.ConvertUtils;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.login.LoginInterceptor;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanCallback;
import no.nordicsemi.android.support.v18.scanner.ScanFilter;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import no.nordicsemi.android.support.v18.scanner.ScanSettings;

/**
 * 蓝牙扫描类
 * 用于启动或停止蓝牙搜索扫描
 * 可以启动自动扫描后自动连接操作,提高连接效率(即可以被搜索到才连接)
 * Created by Joker on 2016/10/21.
 */

public class BluetoothScanner
{
    public final static int REQUEST_SCAN = 110;
    private final static String PARAM_UUID = "param_uuid";
    static BluetoothAdapter adapter;
    static BluetoothLeScannerCompat scanner, scannerAuto;
    static ScanCallback scanCallback, scanCallbackAuto;
    private static ParcelUuid mUuid;
    private static boolean scan_state = false;
    private static boolean scan_conn_state = false;
    private static Timer autoTimer = null;
    private static Timer timer = null;
    private static TimerTask task = null;
    private static PeripheralBean peripheralBean;

    public static boolean scan(int time)
    {
        return scan(time, false, null);
    }

    public static boolean scan(int time, Context context)
    {
        return scan(time, true, context);
    }

    private static boolean scan(int time, final boolean broadcast, final Context context)
    {
        if (!scan_state)
        {
            adapter = BluetoothAdapter.getDefaultAdapter();
            if (adapter.isEnabled())
            {
                scan_state = true;

                if (!SmartHomeApplication.myDeviceManager.mData.isEmpty())
                {
                    SmartHomeApplication.mBluetoothService.clearData(1);
                }

                scanner = BluetoothLeScannerCompat.getScanner();
                ScanSettings settings = new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                        .setReportDelay(1000)
                        .setUseHardwareBatchingIfSupported(false)
                        .build();

                List<ScanFilter> filterList = new ArrayList<>();
                Bundle args = new Bundle();
                mUuid = args.getParcelable(PARAM_UUID);
                scanCallback = new ScanCallback()
                {
                    @Override
                    public void onBatchScanResults(List<ScanResult> results)
                    {
                        for (ScanResult result : results)
                        {
                            if (broadcast)
                            {
                                peripheralBean = new PeripheralBean();
                                peripheralBean.setName(result.getDevice().getName());
                                SparseArray<byte[]> array = result.getScanRecord().getManufacturerSpecificData();

                                String comStr = Integer.toHexString(array.keyAt(0));
                                while (comStr.length() < 4)
                                {
                                    comStr = "0" + comStr;
                                }
                                comStr = comStr.substring(2) + comStr.substring(0, 2);

                                String valueStr = ConvertUtils.bytesToHexString(array.valueAt(0));
                                peripheralBean.setCompanyDataStr(comStr + valueStr);

                                peripheralBean.setDeviceId(result.getDevice().getAddress());
                                RxBus.getInstance().send(Events.SCAN_FEEDBACK_RESULT,JsonUtils.objectToJson(peripheralBean));
                            }

                            if (!isSupport(result))
                            {
                                continue;
                            }

                            byte[] key = result.getScanRecord().getManufacturerSpecificData().valueAt(0);
                            if (key[0] == 1)
                            {
                                Logger.d(ConvertUtils.bytesToHexStringWithSpace(key));
                                continue;
                            }

                            boolean is_contain = isDeviceMatch(result);
                            if (!is_contain)
                            {
                                BaseBluetoothDevice device = DeviceFactory.createDevice(result);
                                SmartHomeApplication.mBluetoothService.scannedDevice(device);
                            }
                        }
                        super.onBatchScanResults(results);
                    }
                };
                filterList.add(new ScanFilter.Builder().setServiceUuid(mUuid).build());
                scanner.startScan(filterList, settings, scanCallback);

                if (timer == null)
                {
                    timer = new Timer();
                }

                if (task == null)
                {
                    task = new TimerTask()
                    {
                        @Override
                        public void run()
                        {
                            if (scanner != null)
                            {
                                scanner.stopScan(scanCallback);
                                scanner = null;
                                scanCallback = null;
                                scan_state = false;
                            }
                            task = null;
                        }
                    };
                }
                timer.schedule(task, time * 1000);
            }
            else
            {
                MainActivity.mContext.requestBluetooth(REQUEST_SCAN, "");
            }
            return true;
        }
        return false;
    }

    public static void stopScan()
    {
        if (task != null)
        {
            task.cancel();
            task = null;
        }

        if (scanner != null)
        {
            scanner.stopScan(scanCallback);
            scanner = null;
            scanCallback = null;
            scan_state = false;
        }
//        SmartHomeApplication.mBluetoothService.broadcastUpdate(BluetoothService.STOP_SCAN);
    }

    public static void scanAndAutoConnect()
    {
        if (!scan_conn_state)
        {
            if (autoTimer == null)
            {
                autoTimer = new Timer();
            }

            scan_conn_state = true;

            TimerTask task = new TimerTask()
            {
                @Override
                public void run()
                {
                    adapter = BluetoothAdapter.getDefaultAdapter();
                    if (!adapter.isEnabled())
                    {
                        return;
                    }

                    scannerAuto = BluetoothLeScannerCompat.getScanner();
                    ScanSettings settings = new ScanSettings.Builder()
                            .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                            .setReportDelay(1000)
                            .setUseHardwareBatchingIfSupported(false)
                            .build();
                    List<ScanFilter> filterList = new ArrayList<>();
                    Bundle args = new Bundle();
                    mUuid = args.getParcelable(PARAM_UUID);
                    scanCallbackAuto = new ScanCallback()
                    {
                        @Override
                        public void onBatchScanResults(List<ScanResult> results)
                        {
                            for (ScanResult result : results)
                            {
                                if (!isSupport(result))
                                {
                                    continue;
                                }

                                boolean is_contain = isDeviceMatch(result);
                                if (is_contain)
                                {
                                    if (SharePreferenceUtils.getLoginStatus()) {
                                        if (((BaseBluetoothDevice) SmartHomeApplication.myDeviceManager.mData.get(result.getDevice().getAddress())).isAutoConnect()) {
                                            SmartHomeApplication.mBluetoothService.connect(result.getDevice().getAddress());
                                        }
                                    } else {
                                        if (((BaseBluetoothDevice) SmartHomeApplication.myDeviceManager.mDataOff.get(result.getDevice().getAddress())).isAutoConnect()) {
                                            Log.d("===","scanAndAutoConnect" + result.getDevice().getName());
                                            SmartHomeApplication.mBluetoothService.connect(result.getDevice().getAddress());
                                        }
                                    }
                                }
                            }
                            super.onBatchScanResults(results);
                        }
                    };
                    filterList.add(new ScanFilter.Builder().setServiceUuid(mUuid).build());
                    scannerAuto.startScan(filterList, settings, scanCallbackAuto);

                    Timer timer = new Timer();
                    TimerTask task = new TimerTask()
                    {
                        @Override
                        public void run()
                        {
                            if (scannerAuto != null)
                            {
                                scannerAuto.stopScan(scanCallbackAuto);
                                scannerAuto = null;
                                scanCallbackAuto = null;
                            }
                        }
                    };
                    timer.schedule(task, 3 * 1000);
                }
            };

            autoTimer.schedule(task, 100, 10000);
        }
    }

    public static void stopAutoConnect()
    {
        if (autoTimer != null)
        {
            autoTimer.cancel();
            autoTimer.purge();
            autoTimer = null;
        }
        scan_conn_state = false;
    }

    private static boolean isSupport(ScanResult result)
    {
        List<DeviceTypeEntity> typeList = SharePreferenceUtils.getTypeList();

        List<ParcelUuid> uuids = result.getScanRecord().getServiceUuids();
//        Log.e("BluetoothScanner",JsonUtils.objectToJson(typeList));

        if (uuids != null && uuids.size() > 0 && typeList != null)
        {
//            Log.d("BluetoothScanner",JsonUtils.objectToJson(typeList));

            for(ParcelUuid uuid : uuids)
            {
                for(DeviceTypeEntity type : typeList)
                {
                    String uuidStr = uuid.toString().toUpperCase();
                    String typeUuid = type.getBluetooth_server_id();
                    if(typeUuid == null || typeUuid.equals(""))
                    {
                        continue;
                    }

                    if(typeUuid.length() == 4)
                    {
                        typeUuid = "0000"+typeUuid+"-0000-1000-8000-00805f9b34fb";
                    }
                    typeUuid = typeUuid.toUpperCase();
                    if(uuidStr.equals(typeUuid))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * 判断设备是否已配对
     *
     * @param result 扫描信息
     * @return 是否已配对
     */
    private static boolean isDeviceMatch(ScanResult result)
    {
        if (SharePreferenceUtils.getLoginStatus()) {
            for (String key : SmartHomeApplication.myDeviceManager.mData.keySet()) {
                if (result.getDevice().getAddress().equals(key)) {
                    return true;
                }
            }
        } else  {
            for (String key : SmartHomeApplication.myDeviceManager.mDataOff.keySet()) {
                if (result.getDevice().getAddress().equals(key)) {
                    return true;
                }
            }
        }
        return false;
    }
}
