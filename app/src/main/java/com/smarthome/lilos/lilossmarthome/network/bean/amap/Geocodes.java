/**
 * Copyright 2016 aTool.org
 */
package com.smarthome.lilos.lilossmarthome.network.bean.amap;

import java.util.List;

/**
 * Auto-generated: 2016-08-29 10:35:13
 *
 * @author aTool.org (i@aTool.org)
 * @website http://www.atool.org/json2javabean.php
 */
public class Geocodes
{

    private String formatted_address;
    private String province;
    private String citycode;
    private String city;
    private String district;
    private List<String> township;
    private String adcode;
    private List<String> street;
    private List<String> number;
    private String location;
    private String level;

    public String getFormatted_address()
    {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address)
    {
        this.formatted_address = formatted_address;
    }

    public String getProvince()
    {
        return province;
    }

    public void setProvince(String province)
    {
        this.province = province;
    }

    public String getCitycode()
    {
        return citycode;
    }

    public void setCitycode(String citycode)
    {
        this.citycode = citycode;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getDistrict()
    {
        return district;
    }

    public void setDistrict(String district)
    {
        this.district = district;
    }

    public List<String> getTownship()
    {
        return township;
    }

    public void setTownship(List<String> township)
    {
        this.township = township;
    }

    public String getAdcode()
    {
        return adcode;
    }

    public void setAdcode(String adcode)
    {
        this.adcode = adcode;
    }

    public List<String> getStreet()
    {
        return street;
    }

    public void setStreet(List<String> street)
    {
        this.street = street;
    }

    public List<String> getNumber()
    {
        return number;
    }

    public void setNumber(List<String> number)
    {
        this.number = number;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getLevel()
    {
        return level;
    }

    public void setLevel(String level)
    {
        this.level = level;
    }

}