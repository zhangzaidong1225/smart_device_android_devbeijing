package com.smarthome.lilos.lilossmarthome.network.bean.mops;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Created by Joker on 2016/12/30.
 */

public class UserDeviceEntity
{

    /**
     * os : 0
     * device_type_id : 1
     * name : 我的忻风
     * device_id : AE:E3:B3:C8:B3
     * device_uuid : null
     */

    private int os;
    private int device_type_id;
    private String name;
    private String device_id;
    private Object device_uuid;
    private int is_demo;

    public int getIs_demo() {
        return is_demo;
    }

    public void setIs_demo(int is_demo) {
        this.is_demo = is_demo;
    }

    public int getOs()
    {
        return os;
    }

    public void setOs(int os)
    {
        this.os = os;
    }

    public int getDevice_type_id()
    {
        return device_type_id;
    }

    public void setDevice_type_id(int device_type_id)
    {
        this.device_type_id = device_type_id;
    }

    public String getName()
    {
        try {
            name = URLDecoder.decode(name, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDevice_id()
    {
        return device_id;
    }

    public void setDevice_id(String device_id)
    {
        this.device_id = device_id;
    }

    public Object getDevice_uuid()
    {
        return device_uuid;
    }

    public void setDevice_uuid(Object device_uuid)
    {
        this.device_uuid = device_uuid;
    }
}
