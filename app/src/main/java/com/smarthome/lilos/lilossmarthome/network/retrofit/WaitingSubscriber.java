package com.smarthome.lilos.lilossmarthome.network.retrofit;

import android.content.Context;

/**
 * 含有等待Dialog展示的Subscriber
 * 当网络操作执行时显示Dialog,当操作结束时关闭Dialog
 * Created by Joker on 2016/12/21.
 */

public abstract class WaitingSubscriber<T> extends BaseSubscriber<T>
        implements ProgressCancelListener
{
    private ProgressDialogHandler handler;

    private Context context;

    public WaitingSubscriber(Context context)
    {
        this.context = context;
        handler = new ProgressDialogHandler(context, this, true);
    }

    private void showDialog()
    {
        if (handler != null)
        {
            handler.obtainMessage(ProgressDialogHandler.SHOW_PROGRESS_DIALOG).sendToTarget();
        }
    }

    private void dismissDialog()
    {
        if (handler != null)
        {
            handler.obtainMessage(ProgressDialogHandler.DISMISS_PROGRESS_DIALOG).sendToTarget();
            handler = null;
        }
    }

    @Override
    public void onStart()
    {
        super.onStart();
        showDialog();
    }

    @Override
    public void onCompleted()
    {
        super.onCompleted();
        dismissDialog();
    }

    @Override
    public void onError(Throwable e)
    {
        super.onError(e);
        dismissDialog();
    }

    @Override
    public void onCancelProgress()
    {
        if (!this.isUnsubscribed())
        {
            this.unsubscribe();
        }
    }
}
