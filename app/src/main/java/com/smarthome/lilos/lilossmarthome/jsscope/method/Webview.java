package com.smarthome.lilos.lilossmarthome.jsscope.method;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
//import android.webkit.WebView;

import com.smarthome.lilos.lilossmarthome.activity.MainActivity;
import com.smarthome.lilos.lilossmarthome.jsscope.bean.CallbackBean;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.SystemUtils;
import com.tencent.smtt.sdk.WebView;

import java.util.HashMap;
import java.util.Map;

/**
 * 对应JSSDK中Webview集指令
 */

public class Webview
{
    private static CallbackBean bean = new CallbackBean();
    private static Map<String, Object> resutlMap = new HashMap<>();

    public static boolean pop(WebView webView, Map<String, String> params,
                              String callBackFuncName)
    {
        Log.d("Webview", "Webview pop method.");
        bean.callBackName = callBackFuncName;
        resutlMap.clear();
        resutlMap.put("status", "success");
        bean.callBackParams = JsonUtils.objectToJson(resutlMap);
        webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");

        if(SystemUtils.getVersionCode(webView.getContext()) != SharePreferenceUtils.getOldVersion()) {
            Intent intent = new Intent(webView.getContext(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            webView.getContext().startActivity(intent);
            SharePreferenceUtils.setOldVersion(SystemUtils.getVersionCode(webView.getContext()));
        } else {
            ((Activity)webView.getContext()).finish();
        }

        return true;
    }
}
