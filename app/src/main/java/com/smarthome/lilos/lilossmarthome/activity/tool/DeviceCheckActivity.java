package com.smarthome.lilos.lilossmarthome.activity.tool;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
//import android.webkit.WebSettings;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;

import com.orhanobut.logger.Logger;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.bluetooth.BluetoothScanner;
import com.smarthome.lilos.lilossmarthome.jsscope.BleJsScope;
import com.smarthome.lilos.lilossmarthome.jsscope.bean.PeripheralBean;
import com.smarthome.lilos.lilossmarthome.jsscope.util.InjectedChromeClient;
import com.smarthome.lilos.lilossmarthome.rxbus.EventKey;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import cn.pedant.SafeWebViewBridge.InjectedChromeClient;

/**
 * 设备检查页面
 * 一个特殊的Web界面,用于生产硬件厂商检查蓝牙设备的服务号是否正确,是否可以正确连接等功能
 */
public class DeviceCheckActivity extends BaseActivity
{
    private final static String STATE_CONNECTING = "connecting";
    private final static String STATE_CONNECTED = "connected";
    private final static String STATE_DISCONNECT = "disconnected";
    private String url;
    private WebView mWebView;
    private Map<String, String> mBluetoothConnectStatus;
    private Map<String, Object> mParamMap = new HashMap<>();
    private View.OnKeyListener listener = new View.OnKeyListener()
    {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event)
        {
            if (event.getAction() == KeyEvent.ACTION_DOWN)
            {
                if (keyCode == KeyEvent.KEYCODE_BACK)
                {
                    if (mWebView.canGoBack())
                    {
                        mWebView.goBack();
                    }
                    else
                    {
                        MobclickAgent.onEvent(DeviceCheckActivity.this, "DeviceCheckActivity_back");
                        finish();
                    }
                }
            }
            return false;
        }
    };

    private Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case 1:
                    mWebView.loadUrl(msg.obj.toString());
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mWebView = new WebView(this);
        setContentView(mWebView);

        initData();
        initViews();
        initRxBus();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mWebView.removeAllViews();
        mWebView.destroy();

        for (String macAddress : mBluetoothConnectStatus.keySet())
        {
            if (!mBluetoothConnectStatus.get(macAddress).equals(STATE_DISCONNECT))
            {
                SmartHomeApplication.mBluetoothService.close(macAddress);
            }
        }
    }

    private void initViews()
    {
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.setOnKeyListener(listener);
        mWebView.setWebChromeClient(new CustomChromeClient("android_obj", BleJsScope.class));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            mWebView.setWebContentsDebuggingEnabled(true);
        }

        mWebView.loadUrl(url);
        mWebView.setWebViewClient(new WebViewClient()
        {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }
        });
    }

    private void initData()
    {
        url = "http://139.198.9.41:8080/xinfeng/wwwroot/test/bleProtocol/app.html";
        mBluetoothConnectStatus = new HashMap<>();
    }

    private void initRxBus()
    {
        RxBus.with(this)
                .setEvent(Events.GATT_CONNECTED)
                .onNext(events -> {
                    String macAddress = (String) events.content;
                    Logger.d("成功接收到连接信息，连接成功的设备为：" + macAddress);
                    if (mBluetoothConnectStatus.containsKey(macAddress))
                    {
                        mBluetoothConnectStatus.remove(macAddress);
                        mBluetoothConnectStatus.put(macAddress, STATE_CONNECTED);
                    }
                })
                .createBindLC();

        RxBus.with(this)
                .setEvent(Events.GATT_DISCONNECTED)
                .onNext(events -> {
                    String macAddress = (String) events.content;
                    Logger.d("成功接收到连接信息，连接失败的设备为：" + macAddress);
                    if (mBluetoothConnectStatus.containsKey(macAddress))
                    {
                        mBluetoothConnectStatus.remove(macAddress);
                        mBluetoothConnectStatus.put(macAddress, STATE_DISCONNECT);
                    }
                })
                .createBindLC();

        RxBus.with(this)
                .setEvent(Events.DEVICE_BLUETOOTH_SERVICES)
                .onNext(events -> {
                    Bundle bundle = (Bundle) events.content;
                    String address = bundle.getString(EventKey.EXTRA_DEVICE_ADDR);
                    List<String> services = new JsonUtils<String>().jsonToList(bundle.getString(EventKey.EXTRA_SERVICES));
                    List<String> characteristics = new JsonUtils<String>().jsonToList(bundle.getString(EventKey.EXTRA_CHARACTERISTICS));

                    mParamMap.clear();
                    mParamMap.put("deviceId", address);
                    mParamMap.put("services", services);
                    mWebView.loadUrl("javascript:LL.ble.discoverServices(" + JsonUtils.objectToJson(mParamMap) + ")");

                    mParamMap.clear();
                    mParamMap.put("deviceId", address);
                    mParamMap.put("characteristics", characteristics);
                    mWebView.loadUrl("javascript:LL.ble.discoverCharacteristics(" + JsonUtils.objectToJson(mParamMap) + ")");
                })
                .createBindLC();

        RxBus.with(this)
                .setEvent(Events.SCAN_FEEDBACK_RESULT)
                .onNext(events -> {
                    PeripheralBean peripheral = (PeripheralBean) JsonUtils.jsonToBean((String) events.content, PeripheralBean.class);
                    mParamMap.clear();
                    mParamMap.put("peripheral", peripheral);
                    mWebView.loadUrl("javascript:LL.ble.discoverPeripherals(" + JsonUtils.objectToJson(mParamMap) + ")");
                })
                .createBindLC();
    }

    public void scan()
    {
        BluetoothScanner.scan(5, getBaseContext());
    }

    public void connectOneDevice(String macAddress)
    {
        if (mBluetoothConnectStatus.containsKey(macAddress))
        {
            if (!mBluetoothConnectStatus.get(macAddress).equals(STATE_CONNECTED))
            {
                mBluetoothConnectStatus.remove(macAddress);
                mBluetoothConnectStatus.put(macAddress, STATE_CONNECTING);
                SmartHomeApplication.mBluetoothService.connect(macAddress);
            }
        }
        else
        {
            mBluetoothConnectStatus.put(macAddress, STATE_CONNECTING);
            SmartHomeApplication.mBluetoothService.connect(macAddress);
        }
    }

    public boolean isConnected(String macAddress)
    {
        if (mBluetoothConnectStatus.containsKey(macAddress))
        {
            if (mBluetoothConnectStatus.get(macAddress).equals(STATE_CONNECTED))
            {
                return true;
            }
        }
        return false;
    }

    public boolean isDisconnected(String macAddress)
    {
        if (mBluetoothConnectStatus.containsKey(macAddress))
        {
            if (mBluetoothConnectStatus.get(macAddress).equals(STATE_DISCONNECT))
            {
                return true;
            }
        }
        else
        {
            return true;
        }
        return false;
    }

    public void loadUrl(String url)
    {
        Message message = handler.obtainMessage();
        message.what = 1;
        message.obj = url;
        handler.sendMessage(message);
    }

    private class CustomChromeClient extends InjectedChromeClient
    {
        public CustomChromeClient(String injectedName, Class injectedCls)
        {
            super(injectedName, injectedCls);
        }
    }
}
