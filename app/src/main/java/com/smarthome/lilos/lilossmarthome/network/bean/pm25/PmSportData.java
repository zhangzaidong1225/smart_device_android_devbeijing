package com.smarthome.lilos.lilossmarthome.network.bean.pm25;

/**
 * Created by Administrator on 2017/5/9.
 */

public class PmSportData {

    private String  brief;
    private String  details;

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return "PmSportData{" +
                "brief='" + brief + '\'' +
                ", details='" + details + '\'' +
                '}';
    }
}
