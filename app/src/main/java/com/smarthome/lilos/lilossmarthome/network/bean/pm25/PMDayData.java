package com.smarthome.lilos.lilossmarthome.network.bean.pm25;

import java.util.List;

/**
 * Created by songyu on 2017/4/7.
 */

public class PMDayData {

//    private String day;
//    private PmCityData city;
//    private String tips;
    private List<PMHoursData> hours;

//    public String getDay() {
//        return day;
//    }
//
//    public void setDay(String day) {
//        this.day = day;
//    }

//    public PmCityData getCity() {
//        return city;
//    }
//
//    public void setCity(PmCityData city) {
//        this.city = city;
//    }
//
//    public String getTips() {
//        return tips;
//    }
//
//    public void setTips(String tips) {
//        this.tips = tips;
//    }

    public List<PMHoursData> getHours() {
        return hours;
    }

    public void setHours(List<PMHoursData> hours) {
        this.hours = hours;
    }

    @Override
    public String toString() {
        return "PMDayData{" +
                ", hours=" + hours +
                '}';
    }
}
