package com.smarthome.lilos.lilossmarthome.bluetooth;

import android.bluetooth.BluetoothGattCharacteristic;

/**
 * 蓝牙Gatt创建请求
 * 用于存放蓝牙Gatt创建后需要进行操作的请求,比如读取数据,写入数据,使能通知,使能上报等
 * Created by Joker on 2016/12/5.
 */

public class Request
{
    public Type type;
    public BluetoothGattCharacteristic characteristic;
    public byte[] value;
    private Request(Type type, BluetoothGattCharacteristic characteristic)
    {
        this.type = type;
        this.characteristic = characteristic;
        this.value = null;
    }

    private Request(Type type, BluetoothGattCharacteristic characteristic, byte[] value)
    {
        this.type = type;
        this.characteristic = characteristic;
        this.value = value;
    }

    public static Request newReadRequest(BluetoothGattCharacteristic characteristic)
    {
        return new Request(Type.READ, characteristic);
    }

    public static Request newWriteRequest(BluetoothGattCharacteristic characteristic, byte[] value)
    {
        return new Request(Type.WRITE, characteristic, value);
    }

    public static Request newEnableNotificationsRequest(BluetoothGattCharacteristic characteristic)
    {
        return new Request(Type.ENABLE_NOTIFICATIONS, characteristic);
    }

    public static Request newEnableIndicationsRequest(BluetoothGattCharacteristic characteristic)
    {
        return new Request(Type.ENABLE_INDICATIONS, characteristic);
    }

    public enum Type
    {
        WRITE,
        READ,
        ENABLE_NOTIFICATIONS,
        ENABLE_INDICATIONS
    }
}
