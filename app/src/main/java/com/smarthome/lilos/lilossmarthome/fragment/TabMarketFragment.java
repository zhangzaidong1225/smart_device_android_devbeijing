package com.smarthome.lilos.lilossmarthome.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.activity.MainActivity;
import com.smarthome.lilos.lilossmarthome.activity.tool.WebArticleActivity;
import com.smarthome.lilos.lilossmarthome.jsscope.BleJsScope;
import com.smarthome.lilos.lilossmarthome.jsscope.util.InjectedChromeClient;
import com.smarthome.lilos.lilossmarthome.network.URLManager;
import com.smarthome.lilos.lilossmarthome.rxbus.EventKey;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.CountlyUtil;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.login.LoginInterceptor;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.export.external.interfaces.WebResourceError;
import com.tencent.smtt.export.external.interfaces.WebResourceRequest;
import com.tencent.smtt.sdk.CookieManager;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;
import com.umeng.analytics.MobclickAgent;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

//import android.webkit.WebResourceError;
//import android.webkit.WebResourceRequest;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;

/**
 * 商城Fragment
 */
public class TabMarketFragment extends Fragment
{

    private View mRootView;
    private WebView mMarketWebView;
    //private String realUrl = new URLManager().getMarketUrl();
    private String realUrl =  new URLManager().getFoundUrl();
    private boolean isSetToken = false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        mRootView = inflater.inflate(R.layout.fragment_tab_market, container, false);

//        Log.e("test", "=======3=======ACTION_DOWN");
        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {

        mMarketWebView = (WebView) mRootView.findViewById(R.id.wv_market);
        init();

        CountlyUtil.recordEvent(CountlyUtil.EVENT.EVENT_HEALTHY_LIFE, CountlyUtil.KEY.ACCESS_TIME);
        super.onActivityCreated(savedInstanceState);
//        Log.e("test", "=======4=======ACTION_DOWN");
    }

    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        Log.d("TabMarketFragment", "TabMarketFragment onKey");
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            if (mMarketWebView.canGoBack())
            {
//                mMarketWebView.goBack();
//                return true;
            }
//            else
//            {
//                getActivity().finish();
//                return true;
//            }
        }
        return false; //已处理
    }


    private void init()
    {
//        if (SystemUtils.isAppInstalled(getContext(), "com.jingdong.app.mall"))
//        {
//            realUrl = realUrl + "?isinstall=1";
//        }
//        else
//        {
//            realUrl = realUrl + "?isinstall=0";
//        }

//        Log.e("test", "=======5=======ACTION_DOWN");
//        String url = "http://139.198.9.141:903/community.html";

        mMarketWebView.getSettings().setTextZoom(100);//屏蔽系统字体
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mMarketWebView.getSettings().setDomStorageEnabled(true);
        } else {
            mMarketWebView.getSettings().setDomStorageEnabled(true);
            mMarketWebView.getSettings().setAppCacheMaxSize(1024*1024*100);
            String appCachePath = SmartHomeApplication.mContext.getCacheDir().getAbsolutePath();
            mMarketWebView.getSettings().setAppCachePath(appCachePath);
            mMarketWebView.getSettings().setAllowFileAccess(true);
            mMarketWebView.getSettings().setAppCacheEnabled(true);
        }
        mMarketWebView.getSettings().setJavaScriptEnabled(true);
        mMarketWebView.getSettings().setSupportMultipleWindows(true);

        mMarketWebView.loadUrl(realUrl);

//        Log.d("===","---" + SharePreferenceUtils.getAccessToken());//: ---b452fbf14772a2ba895a268d7b6b3a6b8fa91722
//
//        CookieManager cookieManager = CookieManager.getInstance();
////        cookieManager.setCookie(url, SharePreferenceUtils.getAccessToken());
//        cookieManager.setCookie(url, "Bearer cd0511e25c6e63ad2f227ec1f30f0776306ba45c");
//
//        mMarketWebView.loadUrl(url);

        mMarketWebView.setWebViewClient(new MarketWebViewClient());
        mMarketWebView.setWebChromeClient(new CustomChromeClient("android_obj", BleJsScope.class));
//        mMarketWebView.setOnKeyListener(listener);

        RxBus.with((MainActivity) getActivity())
                .setEvent(Events.LOGIN_SUCCESS)
                .onNext(events -> {
                    setTokenToLocalStorage(mMarketWebView);
//                    getTokenFromLocalStorage(mMarketWebView);

                })
                .create();

        RxBus.with((MainActivity) getActivity())
                .setEvent(Events.LOG_OUT)
                .onNext(events -> {
                    setTokenToLocalStorage(mMarketWebView);
//                    getTokenFromLocalStorage(mMarketWebView);

                })
                .create();
    }


    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(getActivity());
    }
    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(getActivity());
    }
    private class MarketWebViewClient extends WebViewClient
    {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            Log.d("fond url :",""+url);
            if(url.startsWith(realUrl))
            {
                view.loadUrl(url);
                return false;
            }
            else
            {
                if(url.startsWith("file:///android_asset/undefined")){
                    return false;
                }
//
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                return true;
            }
/*            if(url.startsWith("https://mops-dev.lianluo.com:881/"))
            {
                view.loadUrl(url);
                return false;
            }
            else
            {
                if(url.startsWith("file:///android_asset/undefined")){
                    return false;
                }
//
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                return true;
            }*/
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request,
                                    WebResourceError error)
        {
            super.onReceivedError(view, request, error);
            view.loadUrl("file:///android_asset/nowifi.html");
        }

        @Override
        public void onPageFinished(WebView view, String url)
        {
            ((MainActivity) getActivity()).refreshView();

            if (!isSetToken) {
                isSetToken = true;
                setTokenToLocalStorage(mMarketWebView);
            }
//            getTokenFromLocalStorage(mMarketWebView);

            view.loadUrl("javascript:setRelaodUrl('" + realUrl + "')");
            super.onPageFinished(view, url);
        }
    }

    private void setTokenToLocalStorage(WebView webview) {

        String token = SharePreferenceUtils.getAccessToken();
        if (token == null) {
            return;
        }
        Log.d("TabMarketFragment", "TabMarketFragment settokenToLocalStorage:" + token);
        //1.拼接 JavaScript 代码
        String js = "window.localStorage.setItem('authorization','Bearer " + token + "');";
        String jsUrl = "javascript:(function({ var localStorage = window.localStorage; localStorage.setItem('authorization','Bearer " + token + "')})()";

        if (webview == null) {
            return;
        }
        //2.根据不同版本，使用不同的 API 执行 Js
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webview.evaluateJavascript(js, null);

//        } else {
//            webview.loadUrl(jsUrl);
//            webview.reload();
//        }
    }

    private void getTokenFromLocalStorage(WebView webView) {
        if (webView != null ) {
            webView.loadUrl(
                    "javascript:(function(){ var localStorage = window.localStorage; var authorization =  localStorage.getItem('authorization'); console.log('authorization = ' + authorization);})()");
        }
    }

    public class CustomChromeClient extends InjectedChromeClient
    {
        public CustomChromeClient(String injectedName, Class injectedCls)
        {
            super(injectedName, injectedCls);
        }


        @Override
        public boolean onCreateWindow(WebView webView, boolean b, boolean b1, Message message) {
            Log.d("CustomChromeClient", "onCreateWindow" + webView.getUrl());

            WebView newWebView = new WebView(webView.getContext());
            newWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if(url.startsWith(realUrl))
                    {
                        Intent browserIntent = new Intent(webView.getContext(), WebArticleActivity.class);
                        browserIntent.putExtra("url",url);
                        startActivity(browserIntent);
                        return true;
                    }
                    else
                    {
                        if(url.startsWith("file:///android_asset/undefined")){
                            return false;
                        }
//
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(intent);
                        return true;
                    }

                }
            });
            WebView.WebViewTransport transport = (WebView.WebViewTransport) message.obj;
            transport.setWebView(newWebView);
            message.sendToTarget();
            return true;
        }
    }
}
