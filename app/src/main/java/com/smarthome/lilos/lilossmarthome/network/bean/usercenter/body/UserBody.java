package com.smarthome.lilos.lilossmarthome.network.bean.usercenter.body;

/**
 * Created by Joker on 2016/12/21.
 */

public class UserBody
{
    /**
     * phone : 17083300514
     * password : pa66w0rd
     * verify_code : 312338
     */

    private String phone;
    private String password;
    private String verify_code;

    public UserBody(String phone, String password, String verify_code)
    {
        this.phone = phone;
        this.password = password;
        this.verify_code = verify_code;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getVerify_code()
    {
        return verify_code;
    }

    public void setVerify_code(String verify_code)
    {
        this.verify_code = verify_code;
    }
}
