package com.smarthome.lilos.lilossmarthome.network.retrofit.request;

import com.smarthome.lilos.lilossmarthome.network.bean.amap.AMAPAddress;
import com.smarthome.lilos.lilossmarthome.network.bean.amap.AMAPGeo;
import com.smarthome.lilos.lilossmarthome.network.bean.amap.AMAPLocation;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Joker on 2016/12/23.
 */

public interface IAMAPService
{
    @GET("assistant/coordinate/convert")
    Observable<AMAPLocation> locationsToAmap(@Query("key") String key,
                                             @Query("locations") String locations,
                                             @Query("coordsys") String coordsys);

    @GET("geocode/regeo")
    Observable<AMAPAddress> address(@Query("key") String key, @Query("location") String location,
                                    @Query("radius") int radius,
                                    @Query("extensions") String extensions,
                                    @Query("batch") String batch,
                                    @Query("roadlevel") int roadlevel);

    @GET("geocode/geo")
    Observable<AMAPGeo> search(@Query("key") String key, @Query("address") String address);
}
