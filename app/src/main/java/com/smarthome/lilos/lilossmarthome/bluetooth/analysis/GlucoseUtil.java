package com.smarthome.lilos.lilossmarthome.bluetooth.analysis;

import android.bluetooth.BluetoothGattCharacteristic;

import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.bean.GlucoseRecord;

import java.util.Calendar;

/**
 * 血压服务数据解析
 * Created by Joker on 2016/11/22.
 */

public class GlucoseUtil
{
    private static GlucoseListener listener = null;

    public static void parseGlucoseMeasurement(String address,
                                               BluetoothGattCharacteristic characteristic)
    {
        int offset = 0;
        int flags = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);
        offset += 1;

        boolean timeOffsetPresent = (flags & 0x01) > 0;
        boolean typeAndLocationPresent = (flags & 0x02) > 0;
        int concentrationUnit = (flags & 0x04) > 0 ? GlucoseRecord.UNIT_molpl : GlucoseRecord.UNIT_kgpl;
        boolean sensorStatusAnnunciationPresent = (flags & 0x08) > 0;
        boolean contextInfoFollows = (flags & 0x10) > 0;

        GlucoseRecord record = new GlucoseRecord();
        record.sequenceNumber = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
        offset += 2;

        int year = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
        int month = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 2) - 1; // months are 1-based
        int day = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 3);
        int hours = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 4);
        int minutes = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 5);
        int seconds = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 6);
        offset += 7;

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day, hours, minutes, seconds);
        record.time = calendar;

        if (timeOffsetPresent)
        {
            record.timeOffset = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset);
            offset += 2;
        }

        if (typeAndLocationPresent)
        {
            record.glucoseConcentration = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, offset);
            record.unit = concentrationUnit;

            int typeAndLocation = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 2);
            record.type = (typeAndLocation & 0xF0) >> 4;
            record.sampleLocation = (typeAndLocation & 0x0F);
            offset += 3;
        }

        if (sensorStatusAnnunciationPresent)
        {
            record.status = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
        }

        listener.onGlucoseDataChanged(record.sequenceNumber, record);
    }

    public static void parseGlucoseMeasurementContext(String address,
                                                      BluetoothGattCharacteristic characteristic)
    {
        int offset = 0;

        int flags = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);
        offset += 1;

        boolean carbohydratePresent = (flags & 0x01) > 0;
        boolean mealPresent = (flags & 0x02) > 0;
        boolean testerHealthPresent = (flags & 0x04) > 0;
        boolean exercisePresent = (flags & 0x08) > 0;
        boolean medicationPresent = (flags & 0x10) > 0;
        int medicationUnit = (flags & 0x20) > 0 ? GlucoseRecord.MeasurementContext.UNIT_l : GlucoseRecord.MeasurementContext.UNIT_kg;
        boolean hbA1cPresent = (flags & 0x40) > 0;
        boolean moreFlagsPresent = (flags & 0x80) > 0;

        int sequenceNumber = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
        offset += 2;

        GlucoseRecord.MeasurementContext context = new GlucoseRecord.MeasurementContext();

        if (moreFlagsPresent)
        {
            offset += 1;
        }

        if (carbohydratePresent)
        {
            context.carbohydrateId = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);
            context.carbohydrateUnits = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, offset + 1);
            offset += 3;
        }

        if (mealPresent)
        {
            context.meal = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);
            offset += 1;
        }

        if (testerHealthPresent)
        {
            int testerHealth = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);
            context.tester = (testerHealth & 0xF0) >> 4;
            context.health = testerHealth & 0x0F;
            offset += 1;
        }

        if (exercisePresent)
        {
            context.exerciseDuration = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
            context.exerciseIntensity = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 2);
            offset += 3;
        }

        if (medicationPresent)
        {
            context.medicationId = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);
            context.medicationQuantity = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, offset + 1);
            context.medicationUnit = medicationUnit;
            offset += 3;
        }

        if (hbA1cPresent)
        {
            context.HbA1c = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, offset);
        }

        if (listener != null)
        {
            listener.onGlucoseDataContextChanged(sequenceNumber, context);
        }
    }

    public static void setListener(GlucoseListener gListener)
    {
        listener = null;
        listener = gListener;
    }

    public static void unbind()
    {
        listener = null;
    }

    public interface GlucoseListener
    {
        void onGlucoseDataChanged(int sequenceNumber, GlucoseRecord glucoseRecord);

        void onGlucoseDataContextChanged(int sequenceNumber,
                                         GlucoseRecord.MeasurementContext context);
    }
}
