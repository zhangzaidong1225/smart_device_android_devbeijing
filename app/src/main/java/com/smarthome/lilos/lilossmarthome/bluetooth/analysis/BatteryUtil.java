package com.smarthome.lilos.lilossmarthome.bluetooth.analysis;

import android.bluetooth.BluetoothGattCharacteristic;

import java.util.HashMap;
import java.util.Map;

/**
 * 电池服务数据解析类
 * 用于解析电池服务上报的数据
 * Created by Joker on 2016/12/7.
 */

public class BatteryUtil
{
    private static Map<String, BatteryListener> listenerMap = new HashMap<>();

    public static void parseBattery(String address, BluetoothGattCharacteristic characteristic)
    {
        int batteryValue = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);

        if (listenerMap.containsKey(address))
        {
            listenerMap.get(address).onBatteryChanged(batteryValue);
        }
    }

    public static void registered(BatteryListener listener, String address)
    {
        if (listenerMap.containsKey(address))
        {
            listenerMap.remove(address);
        }
        listenerMap.put(address, listener);
    }

    public static void unregistered(String address)
    {
        if (listenerMap.containsKey(address))
        {
            listenerMap.remove(address);
        }
    }

    public interface BatteryListener
    {
        void onBatteryChanged(int value);
    }
}
