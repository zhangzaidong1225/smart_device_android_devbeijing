package com.smarthome.lilos.lilossmarthome.network.retrofit.request;

import android.util.Log;

import com.smarthome.lilos.lilossmarthome.network.URLManager;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.AddDeviceEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.AppVersionEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceFirmwareEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceSettingsEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceTypeDetailEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceTypeEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.FeedbackEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.NewestActivityEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.TokenEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.UpdateDeviceEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.UserDeviceEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.synchro.SynchroData;
import com.smarthome.lilos.lilossmarthome.network.retrofit.SchedulersTransformer;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Subscriber;

/**
 * MOPS对应API网络操作
 * Created by Joker on 2016/12/30.
 */

public class MopsV2Methods
{
    private static final String BASE_URL = new URLManager().getMopsApiServerUrl();//"https://mops-dev.lianluo.com:883/api/v2/";
    private static final String CLIENT_ID = "5c72f5fc-82cc-31a7-bb9e-42de8586e29f";
    private static final String CLIENT_SECRET = "_rXe29BIwLs-4nfvGCXFpCNQuvBYMdJp";

    private static final int DEFAULT_TIMEOUT = 10;
    private String token = "";

    private Retrofit testRetrofit;

    private IMopsV2Service service;
    private Map<String,Object> params;
    private RequestBody body;

    private MopsV2Methods(){}

    private static class SingletonHolder
    {
        private static final MopsV2Methods INSTANCE = new MopsV2Methods();
    }

    public static MopsV2Methods getInstance()
    {
        return SingletonHolder.INSTANCE;
    }

    public void init(String language)
    {
        setToken(SharePreferenceUtils.getAccessToken());
        String lan = "en-US";
        if (language.equals("zh-CN"))
        {
            lan = language;
        }
        String finalLan = lan;

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
        client.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().header("Accept-Language", finalLan).build();
            return chain.proceed(request);
        });

        HttpLoggingInterceptor.Level level = HttpLoggingInterceptor.Level.BODY;
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(message ->
                Log.d("MopsV2", message));
        loggingInterceptor.setLevel(level);
        client.addInterceptor(loggingInterceptor);

        testRetrofit = new Retrofit.Builder()
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .build();
        service = testRetrofit.create(IMopsV2Service.class);

        params = new HashMap<>();
    }
    public void setToken(String token)
    {
        this.token = "Bearer "+token;
    }

    public void login(Subscriber<TokenEntity> subscriber, String username, String password, String mqtt_client_id)
    {
        params.clear();
        params.put("client_id",CLIENT_ID);
        params.put("client_secret",CLIENT_SECRET);
        params.put("username",username);
        params.put("password",password);
        params.put("mqtt_client_id",mqtt_client_id);
        body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), JsonUtils.objectToJson(params));

        service.login(body)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    public void logout(Subscriber<String> subscriber,String mqtt_client_id)
    {
        params.clear();
        params.put("mqtt_client_id",mqtt_client_id);
        body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), JsonUtils.objectToJson(params));
        service.logout(token,body)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    public void devices(Subscriber<List<UserDeviceEntity>> subscriber)
    {
        service.devices(token)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    public void addDevice(Subscriber<AddDeviceEntity> subscriber,String name,String device_id,int device_type_id)
    {
        params.clear();
        params.put("name",name);
        params.put("device_id",device_id);
        params.put("device_type_id",device_type_id);
        body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), JsonUtils.objectToJson(params));
        service.addDevice(token,body)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }
    public void synchroDevices(Subscriber<List<UserDeviceEntity>> subscriber,List<SynchroData> synchroList){
//        Log.d("```","login---"+ JsonUtils.objectToJson(synchroList));

        body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), JsonUtils.objectToJson(synchroList));
        service.synchroDevices(token,body)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }
    public void updateDevice(Subscriber<UpdateDeviceEntity> subscriber,String device_id,String name)
    {
        params.clear();
        params.put("name",name);
        body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), JsonUtils.objectToJson(params));
        service.updateDevice(token,device_id,body)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    public void deleteDevice(Subscriber<String> subscriber,String device_id)
    {
        Log.d("===","--"+token);

        service.deleteDevice(token,device_id)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    public void types(Subscriber<List<DeviceTypeEntity>> subscriber)
    {
        service.types()
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    public void typeDetail(Subscriber<DeviceTypeDetailEntity> subscriber,String id)
    {
        service.typeDetail(id)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    public void setSettings(Subscriber<DeviceSettingsEntity> subscriber,String device_id,String key,String value)
    {
        params.clear();
        params.put("device_id",device_id);
        params.put("key",key);
        params.put("value",value);
        body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), JsonUtils.objectToJson(params));
        service.setSettings(token,body)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    public void getSetting(Subscriber<DeviceSettingsEntity> subscriber,String device_id,String key)
    {
        service.getSetting(token,device_id,key)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    public void firmware(Subscriber<DeviceFirmwareEntity> subscriber,int device_type_id)
    {
        service.firmware(device_type_id+"")
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    public void appVersion(Subscriber<AppVersionEntity> subscriber)
    {
        service.appVersion()
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    public void feedback(Subscriber<FeedbackEntity> subscriber,String contact,String content)
    {
        params.clear();
        params.put("contact",contact);
        params.put("content",content);
        body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), JsonUtils.objectToJson(params));
        service.feedback(body)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    public void activities(Subscriber<NewestActivityEntity> subscriber)
    {
        service.activities()
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }
}
