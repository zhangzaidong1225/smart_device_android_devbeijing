package com.smarthome.lilos.lilossmarthome.adapter;

import android.content.Context;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.adapter.abslistview.CommonAdapter;
import com.smarthome.lilos.lilossmarthome.adapter.abslistview.ViewHolder;
import com.smarthome.lilos.lilossmarthome.network.bean.DeviceTypeListEntity;

import java.util.List;

/**
 * Created by louis on 2017/4/11.
 */

public class LocationAdapter extends CommonAdapter<String> {
    public LocationAdapter(Context context,
                             List<String> mDatas,
                             int itemLayoutId,
                             DisplayImageOptions displayImageOptions)
    {
        super(context, mDatas, itemLayoutId, displayImageOptions);
    }

    public LocationAdapter(Context context,
                             List<String> mDatas,
                             int itemLayoutId)
    {
        super(context, mDatas, itemLayoutId);
    }
    @Override
    public void convert(ViewHolder holder, String item, int position) {
        holder.setText(R.id.tv_location, item);
    }
}
