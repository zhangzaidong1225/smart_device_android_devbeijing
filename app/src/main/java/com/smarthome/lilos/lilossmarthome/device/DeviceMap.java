package com.smarthome.lilos.lilossmarthome.device;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/5/17.
 * 主要用来存储已连接的蓝牙设备，
 */
public class DeviceMap implements Parcelable
{

    public static final Creator<DeviceMap> CREATOR = new Creator<DeviceMap>()
    {
        @Override
        public DeviceMap createFromParcel(Parcel in)
        {
            return new DeviceMap(in);
        }

        @Override
        public DeviceMap[] newArray(int size)
        {
            return new DeviceMap[size];
        }
    };
    public Map<String, BaseDevice> map = new HashMap<>();
    public Map<String, BaseDevice> mapOff = new HashMap<>();

    public DeviceMap()
    {
    }

    protected DeviceMap(Parcel in)
    {
        map = in.readHashMap(HashMap.class.getClassLoader());
        mapOff = in.readHashMap(HashMap.class.getClassLoader());
    }


    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeMap(map);
        dest.writeMap(mapOff);
    }
}
