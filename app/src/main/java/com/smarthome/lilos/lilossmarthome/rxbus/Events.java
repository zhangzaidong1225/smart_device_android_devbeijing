package com.smarthome.lilos.lilossmarthome.rxbus;

import android.support.annotation.IntDef;

import org.fusesource.mqtt.codec.DISCONNECT;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * RxBus的事件类
 * 存放有具体事件的类型及信息
 * Created by Joker on 2017/1/11.
 */

public class Events<T>
{
    public static final int RETURN_MAIN_ACTIVITY = 1;

    public static final int DEVICE_UPDATE_COMPLETE = 101;
    public static final int DEVICE_NOT_SUPPORT_UART = 102;
    public static final int DEVICE_ADDED = 104;
    public static final int DEVICE_SCANNED = 105;
    public static final int DEVICE_BLUETOOTH_SERVICES = 107;


    public static final int GATT_CONNECTING = 201;
    public static final int GATT_CONNECTED = 202;
    public static final int GATT_DISCONNECTED = 203;
    public static final int GATT_SERVICES_DISCOVERED = 204;
    public static final int GATT_DATA_AVAILABLE = 205;
    public static final int GATT_DATA_CHANGED = 206;
    public static final int GATT_MANUAL_DISCONNECT = 207;

    public static final int SCAN_FEEDBACK_RESULT = 301;

    public static final int MQTT_DEVICE_CHANGE = 401;
    public static final int MQTT_DEVICE_ONLINE = 402;
    public static final int MQTT_MESSAGE = 403;
    public static final int MQTT_ADD_DEVICE = 404;

    public static final int PM25 = 501;
    public static final int LOCATION = 502;
    public static final int ADD_SHARE = 503;
    public static final int LOCATION_WEATHER = 504;

    public static final int LOGIN_SUCCESS = 601;
    public static final int LOG_OUT = 602;
    public static final int LOG_OUT_REPAS = 603;


    @IntDef({RETURN_MAIN_ACTIVITY,
            DEVICE_UPDATE_COMPLETE, DEVICE_NOT_SUPPORT_UART, DEVICE_ADDED, DEVICE_SCANNED, DEVICE_BLUETOOTH_SERVICES,
            GATT_CONNECTING, GATT_CONNECTED, GATT_DISCONNECTED, GATT_SERVICES_DISCOVERED, GATT_DATA_AVAILABLE, GATT_DATA_CHANGED,GATT_MANUAL_DISCONNECT,
            SCAN_FEEDBACK_RESULT, MQTT_DEVICE_CHANGE, MQTT_DEVICE_ONLINE, MQTT_MESSAGE, MQTT_ADD_DEVICE,PM25,LOCATION,ADD_SHARE,LOCATION_WEATHER, LOGIN_SUCCESS, LOG_OUT,LOG_OUT_REPAS})
    @Retention(RetentionPolicy.SOURCE)
    public @interface EventCode
    {
    }

    public
    @Events.EventCode
    int code;
    public T content;

    public static <O> Events<O> setContent(O t)
    {
        Events<O> events = new Events<>();
        events.content = t;
        return events;
    }

    public <T> T getContent()
    {
        return (T) content;
    }
}
