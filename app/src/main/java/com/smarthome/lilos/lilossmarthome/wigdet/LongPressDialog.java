package com.smarthome.lilos.lilossmarthome.wigdet;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.smarthome.lilos.lilossmarthome.R;

/**
 * Created by Kevin on 2016/8/2.
 */
public class LongPressDialog extends Dialog implements View.OnClickListener
{
    Context mContext;
    Fragment mFragment;
    IOnClickListener iOnclicklistener;
    private TextView mTvRename;
    private TextView mTvDelete;
    private int is_demo;

    public LongPressDialog(Fragment fragment, int theme,int is_demo)
    {
        super(fragment.getActivity(), theme);
        mFragment = fragment;
        this.mContext = mFragment.getActivity();
        this.is_demo = is_demo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_long_press);
        mTvRename = (TextView) findViewById(R.id.tv_rename);
        mTvRename.setOnClickListener(this);
        mTvDelete = (TextView) findViewById(R.id.tv_delete);
        mTvDelete.setOnClickListener(this);
        if(is_demo == 1){
            mTvRename.setVisibility(View.GONE);
        }
    }

    // 直接在这里面写的话就必须要将那些参数传进来，增加这个自定义控件的负担
    @Override
    public void onClick(View v)
    {
        iOnclicklistener = (IOnClickListener) mFragment;
        switch (v.getId())
        {
            case R.id.tv_rename:
                iOnclicklistener.rename();
                dismiss();
                break;
            case R.id.tv_delete:
                Log.d("DEVICEDELETE", "onClick: tv_delete");
                iOnclicklistener.operation();
                dismiss();
                break;
            default:
                break;
        }

    }

    public void setText(int resid)
    {
        mTvDelete.setText(resid);
    }

    public interface IOnClickListener
    {
        void rename();

        void operation();
    }
}
