package com.smarthome.lilos.lilossmarthome.utils;

import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;

/**
 * 文件写入工具类
 * 用于将下载内容写入本地
 * Created by Joker on 2017/1/20.
 */

public class WriteFileManager {
    public static boolean writeResponseBodyToDisk(ResponseBody body, String path) {
        File futureFile = new File(path);
        InputStream inputStream = null;
        OutputStream outputStream = null;
        long fileSize = body.contentLength();

        try {
            try {
                if (!futureFile.exists()) {
                    futureFile.createNewFile();
                }

                byte[] fileReader = new byte[1024 * 1024];
                long fileSizeDownload = 0;
                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }
                    outputStream.write(fileReader, 0, read);
                    fileSizeDownload += read;
                    Log.e("WriteFileManager", "已下载" + fileSizeDownload + " of " + fileSize);
                }

                outputStream.flush();
                return true;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.e("WriteFileManager", "已下载1" + e);
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("WriteFileManager", "已下载2" +  e);
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            Log.e("WriteFileManager", "已下载3" +  e);
            return false;
        }
    }
}
