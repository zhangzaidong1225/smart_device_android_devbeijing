package com.smarthome.lilos.lilossmarthome.jsscope.method;

import android.bluetooth.BluetoothAdapter;
import android.util.Log;
//import android.webkit.WebView;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.activity.device.WebControlActivity;
import com.smarthome.lilos.lilossmarthome.activity.tool.DeviceCheckActivity;
import com.smarthome.lilos.lilossmarthome.bluetooth.BLEService;
import com.smarthome.lilos.lilossmarthome.device.BaseBluetoothDevice;
import com.smarthome.lilos.lilossmarthome.device.BaseDevice;
import com.smarthome.lilos.lilossmarthome.jsscope.bean.CallbackBean;
import com.smarthome.lilos.lilossmarthome.utils.ConvertUtils;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.login.LoginInterceptor;
import com.tencent.smtt.sdk.WebView;

import java.util.HashMap;
import java.util.Map;

/**
 * 对应JSSDK中BlutTooth集指令
 * Created by Joker on 2016/8/25.
 */
public class BlueTooth
{
    private static CallbackBean bean = new CallbackBean();
    private static Map<String, Object> resutlMap = new HashMap<>();

    public static boolean scanPeripherals(WebView webView, Map<String, String> params,
                                          String callBackFuncName)
    {
        bean.callBackName = callBackFuncName;
        resutlMap.clear();

        if (webView.getContext() instanceof DeviceCheckActivity)
        {
            resutlMap.put("status", "success");
            DeviceCheckActivity dca = (DeviceCheckActivity) webView.getContext();
            dca.scan();
        }
        else
        {
            resutlMap.put("status", "fail");
        }
        bean.callBackParams = JsonUtils.objectToJson(resutlMap);

        webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");

        return true;
    }

    public static boolean getDeviceId(WebView webView, Map<String, String> params,
                                      String callBackFuncName)
    {
        String deviceId = (String) webView.getTag(R.id.device_mac_address);

        bean.callBackName = callBackFuncName;
        resutlMap.clear();
        resutlMap.put("status", "success");
        resutlMap.put("deviceId", deviceId);
        bean.callBackParams = JsonUtils.objectToJson(resutlMap);
        webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");

        return true;
    }

    public static boolean isEnabled(WebView webView, Map<String, String> params,
                                    String callBackFuncName)
    {
        boolean isenable;
        if (BluetoothAdapter.getDefaultAdapter() == null)
        {
            isenable = false;
        }
        else
        {
            isenable = BluetoothAdapter.getDefaultAdapter().isEnabled();
        }

        bean.callBackName = callBackFuncName;
        resutlMap.clear();
        resutlMap.put("isEnabled", isenable);
        bean.callBackParams = JsonUtils.objectToJson(resutlMap);
        webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");

        return true;
    }

    public static boolean isConnect(WebView webView, Map<String, String> params,
                                    String callBackFuncName)
    {
        resutlMap.clear();
        String deviceId = params.get("deviceId");
        if (SharePreferenceUtils.getLoginStatus()) {

            BaseDevice device = SmartHomeApplication.myDeviceManager.mData.get(deviceId);
            if (device == null) {
                resutlMap.put("isconnect", false);
            } else {
                resutlMap.put("isconnect", SmartHomeApplication.myDeviceManager.mData.get(deviceId).isConnected());
            }
        } else {
            BaseDevice device = SmartHomeApplication.myDeviceManager.mDataOff.get(deviceId);
            if (device == null) {
                resutlMap.put("isconnect", false);
            } else {
                resutlMap.put("isconnect", SmartHomeApplication.myDeviceManager.mDataOff.get(deviceId).isConnected());
            }
        }
        resutlMap.put("deviceId", deviceId);
        bean.callBackName = callBackFuncName;
        bean.callBackParams = JsonUtils.objectToJson(resutlMap);
        webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
        return true;
    }

    public static boolean connect(final WebView webView, Map<String, String> params,
                                  final String callBackFuncName)
    {
        resutlMap.clear();
        final String deviceId = params.get("deviceId");
        if (webView.getContext() instanceof WebControlActivity)
        {
            if (SharePreferenceUtils.getLoginStatus()) {

                SmartHomeApplication.myDeviceManager.mData.get(deviceId).setIsconnectd(BLEService.STATE_CONNECTING);
                SmartHomeApplication.myDeviceManager.mData.get(deviceId).connect();

                while (true) {
                    if (SmartHomeApplication.myDeviceManager.mData.get(deviceId).getIsconnectd() == BLEService.STATE_CONNECTED) {
                        resutlMap.put("status", "success");
                        resutlMap.put("deviceId", deviceId);
                        break;
                    } else if (SmartHomeApplication.myDeviceManager.mData.get(deviceId).getIsconnectd() == BLEService.STATE_DISCONNECTED) {
                        resutlMap.put("status", "fail");
                        resutlMap.put("deviceId", deviceId);
                        break;
                    } else {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else  {
                SmartHomeApplication.myDeviceManager.mDataOff.get(deviceId).setIsconnectd(BLEService.STATE_CONNECTING);
                SmartHomeApplication.myDeviceManager.mDataOff.get(deviceId).connect();

                while (true) {
                    if (SmartHomeApplication.myDeviceManager.mDataOff.get(deviceId).getIsconnectd() == BLEService.STATE_CONNECTED) {
                        resutlMap.put("status", "success");
                        resutlMap.put("deviceId", deviceId);
                        break;
                    } else if (SmartHomeApplication.myDeviceManager.mDataOff.get(deviceId).getIsconnectd() == BLEService.STATE_DISCONNECTED) {
                        resutlMap.put("status", "fail");
                        resutlMap.put("deviceId", deviceId);
                        break;
                    } else {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            bean.callBackName = callBackFuncName;
            bean.callBackParams = JsonUtils.objectToJson(resutlMap);
            webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
        }
        else if (webView.getContext() instanceof DeviceCheckActivity)
        {
            final DeviceCheckActivity dca = (DeviceCheckActivity) webView.getContext();
            dca.connectOneDevice(deviceId);

            new Thread()
            {
                @Override
                public void run()
                {
                    while (true)
                    {
                        if (dca.isConnected(deviceId))
                        {
                            resutlMap.put("status", "success");
                            resutlMap.put("deviceId", deviceId);
                            break;
                        }
                        else if (dca.isDisconnected(deviceId))
                        {
                            resutlMap.put("status", "fail");
                            resutlMap.put("deviceId", deviceId);
                            break;
                        }
                        else
                        {
                            try
                            {
                                Thread.sleep(100);
                            } catch (InterruptedException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                    bean.callBackName = callBackFuncName;
                    bean.callBackParams = JsonUtils.objectToJson(resutlMap);
                    dca.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
                }
            }.start();
            return true;
        }

        return true;
    }

    public static boolean disconnect(WebView webView, Map<String, String> params,
                                     String callBackFuncName)
    {
        resutlMap.clear();
        String deviceId = params.get("deviceId");
        if (SharePreferenceUtils.getLoginStatus()) {

            if (SmartHomeApplication.myDeviceManager.mData.containsKey(deviceId)) {
                ((BaseBluetoothDevice) SmartHomeApplication.myDeviceManager.mData.get(deviceId)).setAutoConnect(false);
                SmartHomeApplication.myDeviceManager.mData.get(deviceId).disconnect();
                resutlMap.put("status", "success");
                resutlMap.put("deviceId", deviceId);
            } else {
                SmartHomeApplication.mBluetoothService.disconnect(deviceId);
            }
        } else {
            if (SmartHomeApplication.myDeviceManager.mDataOff.containsKey(deviceId)) {
                ((BaseBluetoothDevice) SmartHomeApplication.myDeviceManager.mDataOff.get(deviceId)).setAutoConnect(false);
                SmartHomeApplication.myDeviceManager.mDataOff.get(deviceId).disconnect();
                resutlMap.put("status", "success");
                resutlMap.put("deviceId", deviceId);
            } else {
                SmartHomeApplication.mBluetoothService.disconnect(deviceId);
            }
        }
        bean.callBackName = callBackFuncName;
        bean.callBackParams = JsonUtils.objectToJson(resutlMap);
        webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
        return true;
    }

    public static boolean subscribe(WebView webView, Map<String, String> params,
                                    String callBackFuncName)
    {
        resutlMap.clear();
        String deviceId = params.get("deviceId");
        if (SharePreferenceUtils.getLoginStatus()) {

            if (SmartHomeApplication.myDeviceManager.mData.containsKey(deviceId)) {
                ((BaseBluetoothDevice) SmartHomeApplication.myDeviceManager.mData.get(deviceId)).setSubscribe(true);
                resutlMap.put("status", "success");
                resutlMap.put("deviceId", deviceId);
            } else {
                resutlMap.put("status", "fail");
            }
        } else {
            if (SmartHomeApplication.myDeviceManager.mDataOff.containsKey(deviceId)) {
                ((BaseBluetoothDevice) SmartHomeApplication.myDeviceManager.mDataOff.get(deviceId)).setSubscribe(true);
                resutlMap.put("status", "success");
                resutlMap.put("deviceId", deviceId);
            } else {
                resutlMap.put("status", "fail");
            }
        }
        bean.callBackName = callBackFuncName;
        bean.callBackParams = JsonUtils.objectToJson(resutlMap);
        webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
        return true;
    }

    public static boolean unsubscribe(WebView webView, Map<String, String> params,
                                      String callBackFuncName)
    {
        resutlMap.clear();
        String deviceId = params.get("deviceId");
        if (SharePreferenceUtils.getLoginStatus()) {

            if (SmartHomeApplication.myDeviceManager.mData.containsKey(deviceId)) {
                ((BaseBluetoothDevice) SmartHomeApplication.myDeviceManager.mData.get(deviceId)).setSubscribe(false);
                resutlMap.put("status", "success");
                resutlMap.put("deviceId", deviceId);
            } else {
                resutlMap.put("status", "fail");
            }
        } else {
            if (SmartHomeApplication.myDeviceManager.mDataOff.containsKey(deviceId)) {
                ((BaseBluetoothDevice) SmartHomeApplication.myDeviceManager.mDataOff.get(deviceId)).setSubscribe(false);
                resutlMap.put("status", "success");
                resutlMap.put("deviceId", deviceId);
            } else {
                resutlMap.put("status", "fail");
            }
        }
        bean.callBackName = callBackFuncName;
        bean.callBackParams = JsonUtils.objectToJson(resutlMap);
        webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
        return true;
    }

    public static boolean write(WebView webView, Map<String, String> params,
                                String callBackFuncName)
    {
        resutlMap.clear();
        String deviceId = params.get("deviceId");
        String hexValue = params.get("data");
        Log.d("BlueTooth", hexValue);
        byte[] dataBytes = ConvertUtils.hexStringToBytes(hexValue);

        if (SharePreferenceUtils.getLoginStatus()) {
            if (SmartHomeApplication.myDeviceManager.mData.containsKey(deviceId)) {
                SmartHomeApplication.mBluetoothService.writeUartDeviceCharacteristic(deviceId, dataBytes);
                resutlMap.put("status", "success");
                resutlMap.put("deviceId", deviceId);
            } else {
                resutlMap.put("status", "fail");
            }
        } else {
            if (SmartHomeApplication.myDeviceManager.mDataOff.containsKey(deviceId)) {
                SmartHomeApplication.mBluetoothService.writeUartDeviceCharacteristic(deviceId, dataBytes);
                resutlMap.put("status", "success");
                resutlMap.put("deviceId", deviceId);
            } else {
                resutlMap.put("status", "fail");
            }
        }
        bean.callBackName = callBackFuncName;
        bean.callBackParams = JsonUtils.objectToJson(resutlMap);
        webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
        return true;
    }

    public static boolean getDeviceInfo(WebView webView,Map<String,String> params,String callBackFuncName)
    {
        resutlMap.clear();

        String deviceId = (String) webView.getTag(R.id.device_mac_address);
        String name = (String) webView.getTag(R.id.device_name);

        resutlMap.put("deviceId",deviceId);
        resutlMap.put("name",name);
        bean.callBackName = callBackFuncName;
        bean.callBackParams = JsonUtils.objectToJson(resutlMap);
        webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
        return true;
    }
}
