package com.smarthome.lilos.lilossmarthome.activity.tool;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;

import com.google.zxing.Result;
import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.device.WebControlActivity;
import com.smarthome.lilos.lilossmarthome.bluetooth.BLEService;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.umeng.analytics.MobclickAgent;
import com.zxing.view.ViewfinderView;

import ly.count.android.sdk.Countly;

/**
 * 二维码扫描界面
 * 目前用于扫描二维码获取网址以进入Web控制界面,方便与H5开发人员调试界面
 */
public class QRScanActivity extends com.lianluo.tool.scanner.QRScanActivity
{
    private ImageView mIvPhotoDecode;
    private ImageView mIvFlashlight;
    private ImageView mIvFinish;
    private String mDeviceName;
    private String mDeviceAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_qrscan);
        PreViewViewId = R.id.sf_preview;
        initData();
        initView();

        Countly.onCreate(this);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Countly.sharedInstance().onStart(this);
    }
    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }
    @Override
    protected void onStop()
    {
        Countly.sharedInstance().onStop();
        super.onStop();
    }

    private void initData()
    {
        Bundle deviceInfo = getIntent().getExtras();
        if (deviceInfo == null)
        {
            deviceInfo = new Bundle();
        }
        mDeviceName = deviceInfo.getString(BLEService.EXTRA_DEVICE_NAME, "智能设备");
        mDeviceAddress = deviceInfo.getString(BLEService.EXTRA_DEVICE_ADDR, "E0:CD:51:0C:91:07");
    }

    private void initView()
    {
        viewfinderView = (ViewfinderView) findViewById(R.id.vfv_finder);
        mIvPhotoDecode = (ImageView) findViewById(R.id.iv_photo_decode);
        mIvFlashlight = (ImageView) findViewById(R.id.iv_flashlight);
        mIvFinish = (ImageView) findViewById(R.id.iv_finish);

        mIvPhotoDecode.setOnClickListener(v -> {
            MobclickAgent.onEvent(this, "QRScanActivity_selectImageToDecode");
            selectImageToDecode();
        });
        mIvFlashlight.setOnClickListener(v -> controlFlashlight());
        mIvFinish.setOnClickListener(v -> {
            MobclickAgent.onEvent(this, "QRScanActivity_back");
            finish();
        });
    }

    private void controlFlashlight()
    {
        MobclickAgent.onEvent(this, "QRScanActivity_controlFlashlight");
        setFlashlight(!getFlashlight());
    }

    @Override
    public void handleDecode(Result result)
    {
        if (result != null)
        {
            if ((result.getText().startsWith("http") && (result.getText().endsWith("htm") || result.getText().endsWith("html"))) || result.getText().startsWith("file"))
            {
                Intent intent = new Intent(getBaseContext(), WebControlActivity.class);
                Bundle deviceInfo = new Bundle();
                deviceInfo.putString(WebControlActivity.WEB_PATH, result.getText());
                deviceInfo.putString(BLEService.EXTRA_DEVICE_NAME, mDeviceName);
                deviceInfo.putString(BLEService.EXTRA_DEVICE_ADDR, mDeviceAddress);
                intent.putExtras(deviceInfo);
                startActivity(intent);
            }
            else
            {
                AppToast.showToast(R.string.msg_qr_error);
            }
        }
    }
}
