package com.smarthome.lilos.lilossmarthome.device;

/**
 * Created by Tristan on 2016/11/16.
 */

public class WifiDevice extends BaseDevice
{

    boolean isOnline = false;


    @Override
    public void connect()
    {
    }

    @Override
    public void connect(boolean isConnecting)
    {
    }

    @Override
    public boolean isConnected()
    {
        return false;
    }

    @Override
    public void stopConnect()
    {
    }

    @Override
    public void disconnect()
    {
    }

    public void setOnline(boolean online)
    {
        isOnline = online;
    }

    public boolean getisOnline()
    {
        return isOnline;
    }
}
