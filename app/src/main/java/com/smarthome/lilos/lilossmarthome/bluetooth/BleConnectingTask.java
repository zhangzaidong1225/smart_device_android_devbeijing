package com.smarthome.lilos.lilossmarthome.bluetooth;

import com.orhanobut.logger.Logger;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.device.BaseBluetoothDevice;
import com.smarthome.lilos.lilossmarthome.device.BaseDevice;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;

import java.util.Timer;
import java.util.TimerTask;

/**
 * BLE连接计时器
 * 用于在设备连接超过预设时间时,取消连接并且将设备状态由连接中改为未连接
 * Created by Joker on 2016/8/8.
 */
public class BleConnectingTask extends TimerTask
{
    String addr = null;
    Timer mTimer = null;

    public BleConnectingTask(String addr, Timer timer)
    {
        this.addr = addr;
        this.mTimer = timer;
    }

    @Override
    public void run()
    {
        Logger.init("连接超时计时器");
        Logger.d("连接地址为" + addr + "的设备中……");
        if (SharePreferenceUtils.getLoginStatus()) {
            if (!SmartHomeApplication.myDeviceManager.mData.isEmpty() && SmartHomeApplication.myDeviceManager.mData.containsKey(addr)) {
                BaseDevice baseDevice = SmartHomeApplication.myDeviceManager.mData.get(addr);
                if (baseDevice instanceof BaseBluetoothDevice) {
                    BaseBluetoothDevice device = (BaseBluetoothDevice) baseDevice;
                    if (device.getIsconnectd() != BLEService.STATE_CONNECTED) {
                        Logger.d("设备未连接成功，连接超时");
                        device.setIsconnectd(BLEService.STATE_DISCONNECTED);
                        SmartHomeApplication.mBluetoothService.close(device.getAddress());
                        SmartHomeApplication.mBluetoothService.mListener.connectTimeOut(device.getAddress());
                        SmartHomeApplication.myDeviceManager.mData.get(addr).setIsconnectd(BLEService.STATE_DISCONNECTED);
                    }
                }
            }
        } else {
            if (!SmartHomeApplication.myDeviceManager.mDataOff.isEmpty() && SmartHomeApplication.myDeviceManager.mDataOff.containsKey(addr)) {
                BaseDevice baseDevice = SmartHomeApplication.myDeviceManager.mDataOff.get(addr);
                if (baseDevice instanceof BaseBluetoothDevice) {
                    BaseBluetoothDevice device = (BaseBluetoothDevice) baseDevice;
                    if (device.getIsconnectd() != BLEService.STATE_CONNECTED) {
                        Logger.d("设备未连接成功，连接超时");
                        device.setIsconnectd(BLEService.STATE_DISCONNECTED);
                        SmartHomeApplication.mBluetoothService.close(device.getAddress());
                        SmartHomeApplication.mBluetoothService.mListener.connectTimeOut(device.getAddress());
                        SmartHomeApplication.myDeviceManager.mDataOff.get(addr).setIsconnectd(BLEService.STATE_DISCONNECTED);
                    }
                }
            }
        }
        Logger.init("SMART HOME");
        mTimer.cancel();
        mTimer.purge();
        mTimer = null;
    }
}
