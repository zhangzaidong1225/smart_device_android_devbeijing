package com.smarthome.lilos.lilossmarthome.device;

import android.util.Log;
import android.util.SparseArray;

import com.smarthome.lilos.lilossmarthome.network.bean.DeviceListEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.UserDeviceEntity;
import com.smarthome.lilos.lilossmarthome.utils.UMengDeviceUtil;

import org.json.JSONException;
import org.json.JSONObject;

import no.nordicsemi.android.support.v18.scanner.ScanResult;

/**
 * 设备创建工厂
 * 根据不同的传入对象以创建符合要求的设备实例(内部选择类型)
 * Created by Joker on 2016/8/5.
 */
public class DeviceFactory
{
    public static final int XINFENG = 1;
    public static final int CAR_TEMP = 2;
    public static final int WIFI_TEST_DEVICE = 10;
    public static final int PM_DEVICE = 3;

    /**
     * 根据蓝牙扫描结果返回设备对象
     *
     * @param result
     * @return
     */
    public static BaseBluetoothDevice createDevice(ScanResult result)
    {
        BaseBluetoothDevice device = new BaseBluetoothDevice();
        if (result.getDevice().getName() == null)
        {
            device.setName("未命名");
        }
        else
        {
            device.setName(result.getDevice().getName());
        }
        device.setAddress(result.getDevice().getAddress());
        device.setIsmatchd(false);

        SparseArray<byte[]> manufacturerSpecificData = result.getScanRecord().getManufacturerSpecificData();
        if (manufacturerSpecificData.size() != 0)
        {
            device.setCompanyId(manufacturerSpecificData.keyAt(0));
            device.setDef_version(((int) manufacturerSpecificData.valueAt(0)[0]));
            device.setTypeId(((manufacturerSpecificData.valueAt(0)[1 + 1] & 0xFF) << 8) +
                    (manufacturerSpecificData.valueAt(0)[1] & 0xFF));
            device.setIs_demo(0);
        }
        else
        {
            device.setCompanyId(30326);
            device.setDef_version(0);
            device.setTypeId(1);
            //device.setIs_demo(0);
        }
        Log.e("DeviceFactory", "device:" + device);
        return device;
    }

    public static BaseDevice createDevice(JSONObject itemObject)
    {
        try
        {
            int typeId = (int) itemObject.get("typeId");
            switch (typeId)
            {
                case XINFENG:
                    XinfengDevice device = new XinfengDevice();
                    device.setAddress((String) itemObject.get("address"));
                    device.setName((String) itemObject.get("name"));
                    device.setAutoConnect((boolean) itemObject.get("autoconnect"));
                    device.setIsmatchd((boolean) itemObject.get("matched"));
                    device.setTypeId((int) itemObject.get("typeId"));
                    device.setIs_demo((Integer) itemObject.get("is_demo"));
                    device.setIsconnectd(0);
                    return device;
                case WIFI_TEST_DEVICE:
                    WifiDevice wifidevice = new WifiDevice();
                    wifidevice.setAddress((String) itemObject.get("address"));
                    wifidevice.setDevId((String) itemObject.get("devid"));
                    wifidevice.setName((String) itemObject.get("name"));
                    wifidevice.setTypeId((int) itemObject.get("typeId"));
                    wifidevice.setIsconnectd(0);
                    return wifidevice;
                default:
                    BaseBluetoothDevice baseDevice = new BaseBluetoothDevice();
                    baseDevice.setAddress((String) itemObject.get("address"));
                    baseDevice.setName((String) itemObject.get("name"));
                    baseDevice.setAutoConnect((boolean) itemObject.get("autoconnect"));
                    baseDevice.setIsmatchd((boolean) itemObject.get("matched"));
                    baseDevice.setTypeId((int) itemObject.get("typeId"));
                    baseDevice.setIs_demo((Integer) itemObject.get("is_demo"));
                    baseDevice.setIsconnectd(0);
                    return baseDevice;
            }
        } catch (JSONException e)
        {
            return null;
        }
//        return null;
    }

    public static BaseDevice createDevice(UserDeviceEntity dev)
    {
        switch (dev.getDevice_type_id())
        {
            case XINFENG:
                XinfengDevice device = new XinfengDevice();
                device.setAddress(dev.getDevice_id());
                device.setDevId(dev.getDevice_id());
                device.setName(dev.getName());
                device.setAutoConnect(true);
                device.setIsmatchd(true);
                device.setIsconnectd(0);
                device.setTypeId(dev.getDevice_type_id());
                device.setIs_demo(dev.getIs_demo());
                return device;

            case WIFI_TEST_DEVICE:

                WifiDevice wifidevice = new WifiDevice();
                wifidevice.setAddress(dev.getDevice_id());
                wifidevice.setDevId(dev.getDevice_id());
                wifidevice.setName(dev.getName());
                wifidevice.setTypeId(dev.getDevice_type_id());
                wifidevice.setIsconnectd(0);
                wifidevice.setOnline(false);
                return wifidevice;
            default:
                BaseBluetoothDevice baseDevice = new BaseBluetoothDevice();
                baseDevice.setAddress(dev.getDevice_id());
                baseDevice.setDevId(dev.getDevice_id());
                baseDevice.setName(dev.getName());
                baseDevice.setAutoConnect(true);
                baseDevice.setIsmatchd(true);
                baseDevice.setIsconnectd(0);
                baseDevice.setTypeId(dev.getDevice_type_id());
                baseDevice.setIs_demo(dev.getIs_demo());
                return baseDevice;
        }
    }

    public static BaseDevice createDevice(BaseBluetoothDevice dev)
    {
        switch (dev.getTypeId())
        {
            case XINFENG:
                XinfengDevice device = new XinfengDevice();
                device.setAddress(dev.getAddress());
                device.setTypeId(dev.getTypeId());
                device.setDevId(dev.getDevId());
                device.setName(dev.getName());
                device.setIsmatchd(dev.ismatchd());
                device.setAutoConnect(dev.isAutoConnect());
                device.setAutoConnect1(dev.isAutoConnect1());
                device.setUpdate_state(100);
                return device;
            default:
                return dev;
        }
    }

    public static boolean parseData(byte[] Value,BaseBluetoothDevice bleDevice){
        boolean isChange = false;

        if (Value[1] == 0x50){
            if (Value[2] == 0x6){
                int firstByte = (0x000000FF & ((int) Value[3]));
                int secondByte = (0x000000FF & ((int) Value[4]));
                int unsignedInt = ((int) (secondByte | firstByte << 8));

                if (bleDevice.getPm25() != unsignedInt) {
                    Log.e("DeviceFactory","Pm2.5标准浓度````"+unsignedInt + "");
                    bleDevice.setPm25(unsignedInt);
                    isChange = true;
                }
            }
        }

        return isChange;
    }

    public static boolean parseData(byte[] value, XinfengDevice xinfengDevice)
    {
        UMengDeviceUtil.getInstance().parseData(value, xinfengDevice.getAddress());
        boolean isChange = false;

        if (value[1] == 0x01)
        {
            xinfengDevice.disconnect();

            isChange = true;
        }

        if (value[1] == 0x03)
        {
            isChange = true;
        }

        if (value[1] == 0x54)
        {
            //获取固件版本
            int firstByte = (0x000000FF & ((int) value[2]));
            int secondByte = (0x000000FF & ((int) value[3]));
            int unsignedInt = ((int) (secondByte | firstByte << 8)) & 0xFFFFFFFF;
            //Log.e("固件版本", unsignedInt + "");
            if (xinfengDevice.getVersion() != unsignedInt)
            {

                xinfengDevice.setVersion(unsignedInt);
                isChange = true;
            }
        }

        if (value[1] == 0x50)
        {
            if (value[2] == 0x4)
            {
                //防止电量重复发送
                if (xinfengDevice.getBattery_power() != value[3])
                {
                    xinfengDevice.setBattery_power(value[3]);
                    isChange = true;
                }

                if (xinfengDevice.getBattery_charge() != value[6])
                {
                    xinfengDevice.setBattery_charge(value[6]);
                    isChange = true;
                }
            }
            if (value[2] == 0x5)
            {
                int firstByte = (0x000000FF & ((int) value[11]));
                int secondByte = (0x000000FF & ((int) value[12]));
                int unsignedInt = ((int) (firstByte << 8 | secondByte)) & 0xFFFFFFFF;
                if ((unsignedInt - xinfengDevice.getWind_use_time_this()) > 10 || (xinfengDevice.getWind_use_time_this() - unsignedInt) > 0)
                {
                    xinfengDevice.setWind_use_time_this(unsignedInt);
                    isChange = true;
                }
            }

            if (value[2] == 0x2)
            {
                int firstByte = (0x000000FF & ((int) value[5]));
                int secondByte = (0x000000FF & ((int) value[6]));
                int unsignedInt = ((int) (secondByte | firstByte << 8)) & 0xFFFFFFFF;
                if (xinfengDevice.getPm25() != unsignedInt)
                {
                    xinfengDevice.setPm25(unsignedInt);
                    isChange = true;
                }
            }

            if (value[2] == 0x1)
            {
                if (xinfengDevice.getSpeed() != value[5])
                {
                    xinfengDevice.setSpeed(value[5]);
                    isChange = true;
                }

                int firstByte = (0x000000FF & ((int) value[14]));
                int secondByte = (0x000000FF & ((int) value[13]));
                int threeByte = (0x000000FF & ((int) value[12]));
                int fourByte = (0x000000FF & ((int) value[11]));

                int unsignedInt = ((int) (firstByte | secondByte << 8 | threeByte << 16 | fourByte << 24)) & 0xFFFFFFFF;

                if ((unsignedInt - xinfengDevice.getUse_time()) > 60 || xinfengDevice.getUse_time() - unsignedInt > 0)
                {
                    xinfengDevice.setUse_time(unsignedInt);
                    isChange = true;
                }

                int firstByteThis = (0x000000FF & ((int) value[8]));
                int secondByteThis = (0x000000FF & ((int) value[7]));
                int unsignedIntThis = ((int) (firstByteThis | secondByteThis << 8)) & 0xFFFFFFFF;

                if ((xinfengDevice.getUse_time_this() - unsignedIntThis > 0) || (unsignedIntThis - xinfengDevice.getUse_time_this()) > 10)
                {
                    xinfengDevice.setUse_time_this(unsignedIntThis);
                    isChange = true;
                }
                if (xinfengDevice.getFilter_in() != value[15])
                {
                    xinfengDevice.setFilter_in(value[15]);
                    isChange = true;
                }
            }
        }

        xinfengDevice.initialize();

        return isChange;
    }


}
