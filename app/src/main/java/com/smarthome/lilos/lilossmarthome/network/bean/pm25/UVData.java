package com.smarthome.lilos.lilossmarthome.network.bean.pm25;

/**
 * Created by louis on 2017/5/12.
 */

public class UVData {
    private String brief;
    private String details;

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }



    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }
}
