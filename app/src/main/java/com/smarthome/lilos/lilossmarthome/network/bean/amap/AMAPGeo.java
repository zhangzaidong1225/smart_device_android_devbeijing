/**
 * Copyright 2016 aTool.org
 */
package com.smarthome.lilos.lilossmarthome.network.bean.amap;

import java.util.List;

/**
 * Auto-generated: 2016-08-29 10:35:13
 *
 * @author aTool.org (i@aTool.org)
 * @website http://www.atool.org/json2javabean.php
 */
public class AMAPGeo
{

    private String status;
    private String info;
    private String infocode;
    private String count;
    private List<Geocodes> geocodes;

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getInfo()
    {
        return info;
    }

    public void setInfo(String info)
    {
        this.info = info;
    }

    public String getInfocode()
    {
        return infocode;
    }

    public void setInfocode(String infocode)
    {
        this.infocode = infocode;
    }

    public String getCount()
    {
        return count;
    }

    public void setCount(String count)
    {
        this.count = count;
    }

    public List<Geocodes> getGeocodes()
    {
        return geocodes;
    }

    public void setGeocodes(List<Geocodes> geocodes)
    {
        this.geocodes = geocodes;
    }

}