package com.smarthome.lilos.lilossmarthome.bluetooth.analysis.bean;

import java.util.Calendar;

/**
 * Created by Joker on 2016/11/21.
 */

public class BP_IntermediateCuffPressure
{
    public int unit;
    public boolean timestampPresent;
    public boolean pulseRatePresent;
    public float cuffPressure;
    public Calendar calendar;
    public float pulseRate;
}
