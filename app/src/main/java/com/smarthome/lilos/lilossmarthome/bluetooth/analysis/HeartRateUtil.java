package com.smarthome.lilos.lilossmarthome.bluetooth.analysis;

import android.bluetooth.BluetoothGattCharacteristic;

/**
 * 心率服务数据解析
 * Created by Joker on 2016/11/23.
 */

public class HeartRateUtil
{
    private static HeartRateListener listener;

    private static String[] locations = new String[]{"Other", "Chest", "Wrist", "Finger", "Hand", "Ear Lobe", "Foot"};

    public static void parseHeartRateMeasurement(String address,
                                                 BluetoothGattCharacteristic characteristic)
    {
        int hrValue;
        int flag = characteristic.getValue()[0];
        if ((flag & 0x01) != 0)
        {
            hrValue = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 1);
        }
        else
        {
            hrValue = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 1);
        }

        if (listener != null)
        {
            listener.onHeartRateMeasurementChanged(hrValue);
        }
    }

    public static void parseBodySensorPosition(String address,
                                               BluetoothGattCharacteristic characteristic)
    {
        String sensorPosition = "";
        byte bspByte = characteristic.getValue()[0];
        if (bspByte > locations.length)
        {
            sensorPosition = "Reserved";
        }
        else
        {
            sensorPosition = locations[bspByte];
        }

        if (listener != null)
        {
            listener.onHeartRateSensorPositionFountChanged(sensorPosition);
        }
    }

    public static void setListener(HeartRateListener hrListener)
    {
        listener = null;
        listener = hrListener;
    }

    public static void unbind()
    {
        listener = null;
    }

    public interface HeartRateListener
    {
        void onHeartRateMeasurementChanged(int value);

        void onHeartRateSensorPositionFountChanged(String sensorPosition);
    }
}

