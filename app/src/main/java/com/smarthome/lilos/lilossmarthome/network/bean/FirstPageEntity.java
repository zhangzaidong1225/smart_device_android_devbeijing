package com.smarthome.lilos.lilossmarthome.network.bean;

public class FirstPageEntity
{
        private String city;
        private String pm25;
        private String quality;
        private String tips;


        public String getPm25() {
            return pm25;
        }

        public void setPm25(String pm25) {
            this.pm25 = pm25;
        }

        public String getCity() { return city; }

        public void setCity(String city) {
            this.city = city;
        }
        public String getQuality() {
        return quality;
    }

        public void setQuality(String quality) {
            this.quality = quality;
        }

        public String getTips() {
            return tips;
        }

        public void setTips(String tips) {
            this.tips = tips;
        }
}
