package com.smarthome.lilos.lilossmarthome.bluetooth;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.activity.MainActivity;
import com.smarthome.lilos.lilossmarthome.device.BaseDevice;
import com.smarthome.lilos.lilossmarthome.device.XinfengDevice;

import no.nordicsemi.android.dfu.DfuProgressListenerAdapter;

/**
 * 设备固件升级进度监听
 * 用于监听当前设备固件升级的进度
 * Created by Joker on 2016/8/8.
 */
public class DfuProgressListener extends DfuProgressListenerAdapter
{
    @Override
    public void onDfuProcessStarting(String deviceAddress)
    {
        Log.d("DfuProgressListener", "DFU进度开始");
        BaseDevice baseDevice = SmartHomeApplication.myDeviceManager.mData.get(deviceAddress);
        if (baseDevice instanceof XinfengDevice)
        {
            XinfengDevice xinfengDevice = (XinfengDevice) baseDevice;
            xinfengDevice.setUpdate_state(104);
        }
        super.onDfuProcessStarting(deviceAddress);
    }

    @Override
    public void onDfuCompleted(String deviceAddress)
    {
        Log.d("DfuProgressListener", "DFU完成");

        BaseDevice baseDevice = SmartHomeApplication.myDeviceManager.mData.get(deviceAddress);
        if (baseDevice instanceof XinfengDevice)
        {
            XinfengDevice xinfengDevice = (XinfengDevice) baseDevice;
            xinfengDevice.setPercentage(0);
            xinfengDevice.setUpdate_state(101);
            xinfengDevice.setUpdate_state(100);

            new Handler().postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    NotificationManager manager = (NotificationManager) MainActivity.mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                    manager.cancel(DfuService.NOTIFICATION_ID);
                }
            }, 200);
        }

        super.onDfuCompleted(deviceAddress);
    }

    @Override
    public void onProgressChanged(String deviceAddress, int percent, float speed, float avgSpeed,
                                  int currentPart, int partsTotal)
    {
        Log.d("DfuProgressListener", "当前DFU进度为" + percent);

        BaseDevice baseDevice = SmartHomeApplication.myDeviceManager.mData.get(deviceAddress);
        if (baseDevice instanceof XinfengDevice)
        {
            XinfengDevice xinfengDevice = (XinfengDevice) baseDevice;
            xinfengDevice.setPercentage(percent);
            xinfengDevice.setUpdate_state(103);
        }

        super.onProgressChanged(deviceAddress, percent, speed, avgSpeed, currentPart, partsTotal);
    }

    @Override
    public void onError(String deviceAddress, int error, int errorType, String message)
    {
        Log.d("DfuProgressListener", "DFU失败");

        BaseDevice baseDevice = SmartHomeApplication.myDeviceManager.mData.get(deviceAddress);
        if (baseDevice instanceof XinfengDevice)
        {
            XinfengDevice xinfengDevice = (XinfengDevice) baseDevice;
            xinfengDevice.setUpdate_state(102);
            xinfengDevice.setUpdate_state(100);

            new Handler().postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    NotificationManager manager = (NotificationManager) MainActivity.mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                    manager.cancel(DfuService.NOTIFICATION_ID);
                }
            }, 200);
        }

        super.onError(deviceAddress, error, errorType, message);
    }
}
