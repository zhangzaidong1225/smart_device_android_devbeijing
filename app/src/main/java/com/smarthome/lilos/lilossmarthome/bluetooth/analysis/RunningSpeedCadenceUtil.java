package com.smarthome.lilos.lilossmarthome.bluetooth.analysis;

import android.bluetooth.BluetoothGattCharacteristic;

import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.bean.RSC_Measurement;

/**
 * 跑步机速度与里程服务数据解析类
 * Created by Joker on 2016/11/23.
 */

public class RunningSpeedCadenceUtil
{
    private static RunningSpeedCadenceListener listener;

    public static void parseRunningSpeedCadenceMeasurement(String address,
                                                           BluetoothGattCharacteristic characteristic)
    {
        int offset = 0;
        int flags = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);
        offset += 1;

        boolean isLmPreset = (flags & 0x01) > 0;
        boolean tdPreset = (flags & 0x02) > 0;
        boolean running = (flags & 0x04) > 0;

        float instantaneousSpeed = (float) characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset) / 256.0f;
        offset += 2;
        int instantaneousCadence = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);
        offset += 1;

        float instantaneousStrideLength = RSC_Measurement.NOT_AVAILABLE;
        if (isLmPreset)
        {
            instantaneousStrideLength = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset);
            offset += 2;
        }

        float totalDistance = RSC_Measurement.NOT_AVAILABLE;
        if (tdPreset)
        {
            totalDistance = (float) characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, offset) / 10.0f;
        }

        RSC_Measurement rsc = new RSC_Measurement();
        rsc.speed = instantaneousSpeed;
        rsc.cadence = instantaneousCadence;
        rsc.distance = totalDistance;
        rsc.stridenLen = instantaneousStrideLength;
        rsc.activity = running ? RSC_Measurement.ACTIVITY_RUNNING : RSC_Measurement.ACTIVITY_WALKING;

        if (listener != null)
        {
            listener.onRunningSpeedCadenceMeasurementChanged(rsc);
        }
    }

    public static void setListener(
            RunningSpeedCadenceListener rscListener)
    {
        listener = null;
        listener = rscListener;
    }

    public static void unbind()
    {
        listener = null;
    }

    public interface RunningSpeedCadenceListener
    {
        void onRunningSpeedCadenceMeasurementChanged(RSC_Measurement rsc);
    }
}
