package com.smarthome.lilos.lilossmarthome.utils;

/**
 * 忻风工具类
 * 用于处理当前过滤使用的比例
 * Created by Joker on 2016/7/25.
 */
public class XinfengUtlis
{
    public static float getFilterLevel(int usedTime)
    {
        if (usedTime <= 30)
        {
            return usedTime / 30.0f * 20;
        }
        else if (usedTime <= 300)
        {
            return (usedTime - 30) / (300.0f - 30.f) * 20 + 20;
        }
        else if (usedTime <= 3000)
        {
            return (usedTime - 300) / (3000.0f - 300.0f) * 20 + 40;
        }
        else if (usedTime <= 30000)
        {
            return (usedTime - 3000) / (30000.0f - 3000.0f) * 20 + 60;
        }
        else if (usedTime <= 300000)
        {
            return (usedTime - 30000) / (300000.0f - 30000.0f) * 20 + 80;
        }
        else
        {
            return 100.0f;
        }
    }
}
