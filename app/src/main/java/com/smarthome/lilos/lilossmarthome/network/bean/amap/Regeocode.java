/**
 * Copyright 2016 aTool.org
 */
package com.smarthome.lilos.lilossmarthome.network.bean.amap;

/**
 * Auto-generated: 2016-08-29 10:38:47
 *
 * @author aTool.org (i@aTool.org)
 * @website http://www.atool.org/json2javabean.php
 */
public class Regeocode
{

    private String formatted_address;
    private Addresscomponent addressComponent;



    public Addresscomponent getAddresscomponent() {
        return addressComponent;
    }
    public void setAddresscomponent(Addresscomponent addressComponent) {
        this.addressComponent = addressComponent;
    }




    public String getFormatted_address()
    {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address)
    {
        this.formatted_address = formatted_address;
    }

}