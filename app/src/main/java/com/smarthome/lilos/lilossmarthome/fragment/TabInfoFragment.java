package com.smarthome.lilos.lilossmarthome.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.activity.MainActivity;
import com.smarthome.lilos.lilossmarthome.activity.tool.WebArticleActivity;
import com.smarthome.lilos.lilossmarthome.activity.tool.WebNotifyActivity;
import com.smarthome.lilos.lilossmarthome.activity.user.AboutUsActivity;
import com.smarthome.lilos.lilossmarthome.activity.user.LoginActivity;
import com.smarthome.lilos.lilossmarthome.device.BaseBluetoothDevice;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.AddDeviceEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.AppVersionEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.UpdateDeviceEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.synchro.SynchroData;
import com.smarthome.lilos.lilossmarthome.network.retrofit.BaseSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.WaitingSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.MopsV2Methods;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.service.UpdateService;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.SystemUtils;
import com.smarthome.lilos.lilossmarthome.utils.login.LoginInterceptor;
import com.smarthome.lilos.lilossmarthome.wigdet.NormalDialog;
import com.umeng.analytics.MobclickAgent;

import java.util.List;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;

/**
 * 个人信息Fragment
 * 用于展示个人信息,以及部分个人相关功能
 * Edited by Joker on 2016/7/19. 删除在设计稿中没有的控件，添加控件，删除界面中可以取消掉的功能及对应代码。
 */
public class TabInfoFragment extends Fragment
{
    private static final String TAG = TabInfoFragment.class.getName();

    private ImageView mIvHead;
    private TextView mTvAccount;
    private FrameLayout mFlFeedback;
    private FrameLayout mFlResetPassword;
    private FrameLayout mFlDevicesManagerContent;
    private FrameLayout mFlSoftwareUpdate;
    private FrameLayout mFlAboutUs;
    private FrameLayout mFlNotify;
    private Button btnLogout;

    private AppVersionEntity data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_tab_info, container, false);

        mIvHead = (ImageView) v.findViewById(R.id.iv_head);
        mTvAccount = (TextView) v.findViewById(R.id.tv_account);
        mFlResetPassword = (FrameLayout) v.findViewById(R.id.fl_reset_password);
        mFlDevicesManagerContent = (FrameLayout) v.findViewById(R.id.fl_devices_manager_content);
        mFlSoftwareUpdate = (FrameLayout) v.findViewById(R.id.fl_software_update);
        mFlAboutUs = (FrameLayout) v.findViewById(R.id.fl_about_us);
        mFlFeedback = (FrameLayout) v.findViewById(R.id.fl_feedback);
        mFlNotify = (FrameLayout) v.findViewById(R.id.fl_notify);
        btnLogout = (Button) v.findViewById(R.id.btn_logout);

        mIvHead.setOnClickListener(v1 -> gotoLoginActivity());
        mFlResetPassword.setOnClickListener(v1 -> gotoResetPasswordActivity());
        mFlDevicesManagerContent.setOnClickListener(v1 -> gotoSecurityCheckActivity());
        mFlSoftwareUpdate.setOnClickListener(v1 -> gotoCheckSoftwareVersion());
        mFlFeedback.setOnClickListener(v1 -> gotoFeedbackActivity());
        mFlAboutUs.setOnClickListener(v1 -> gotoAboutUsActivity());
        btnLogout.setOnClickListener(v1 -> logout());
        mFlNotify.setOnClickListener(v1 -> gotoNotifyActivity());


        int bottomHeight = SystemUtils.getBottomStatusHeight(getContext());
        double topMin = bottomHeight * 53.3 / (53.3 + 38);
        double bottomMin = bottomHeight * 38 / (53.3 + 38);

        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mIvHead.getLayoutParams();
        lp.setMargins(0, (int) (SystemUtils.dip2px(getContext(), 53.3f) - topMin), 0, 0);
        mIvHead.setLayoutParams(lp);

        LinearLayout.LayoutParams lp2 = (LinearLayout.LayoutParams) mTvAccount.getLayoutParams();
        lp2.setMargins(0, SystemUtils.dip2px(getContext(), 14.7f), 0, (int) (SystemUtils.dip2px(getContext(), 38) - bottomMin));
        mTvAccount.setLayoutParams(lp2);
        init();
        return v;
    }
    public void init(){
        RxBus.with((MainActivity) getActivity())
                .setEvent(Events.LOG_OUT_REPAS)
                .onNext(events -> {
                    logout_rePass();
                })
                .create();
    }
    private void logout_rePass()
    {
        MobclickAgent.onEvent(getActivity(),"TabInfoFragment_verify_logout");

        MopsV2Methods.getInstance().logout(new BaseSubscriber<String>()
        {
            @Override
            public void onNext(String s)
            {
                Log.e("TabInfoFragment--",s);
            }

            @Override
            public void onCompleted() {
//                    super.onCompleted();
                Log.e("TabInfoFragment--","onCompleted");

            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                Log.e("TabInfoFragment--","Error--"+e.getMessage());

            }
        },SmartHomeApplication.myClientID);

        SharePreferenceUtils.setLoginStatus(false);
        SharePreferenceUtils.setAccessToken("");
        RxBus.getInstance().send(Events.LOG_OUT,null);

        SharePreferenceUtils.setMobileNum("");
        mTvAccount.setText(getResources().getText(R.string.not_login));
        mIvHead.setImageResource(R.drawable.ic_headpic_unlogin);
        btnLogout.setVisibility(View.GONE);
        ((MainActivity) getActivity()).refreshList();

        for (String address : SmartHomeApplication.myDeviceManager.mData.keySet())
        {
            SmartHomeApplication.myDeviceManager.mDataOff.put(address,
                    SmartHomeApplication.myDeviceManager.mData.get(address));
/*            if (SmartHomeApplication.myDeviceManager.mData.get(address) instanceof BaseBluetoothDevice)
            {
                SmartHomeApplication.mBluetoothService.disconnect(address);
            }*/
        }
        SmartHomeApplication.mBluetoothService.clearData(0);
    }
    private void logout()
    {
        MobclickAgent.onEvent(getActivity(),"TabInfoFragment_logout");
        final NormalDialog normalDialog = new NormalDialog(getActivity(), getResources().getString(R.string.logout_title), getResources().getString(R.string.logout_context), getResources().getString(R.string.msg_cancel), getResources().getString(R.string.msg_confirm));
        normalDialog.setLeftClickListener(v -> {
            MobclickAgent.onEvent(getActivity(),"TabInfoFragment_cancel_logout");
            normalDialog.dismiss();
        });
        normalDialog.setRightClickListener(v ->
        {
            normalDialog.dismiss();
            logout_rePass();
        });
        normalDialog.show();
    }

    private void gotoAboutUsActivity()
    {
        MobclickAgent.onEvent(getActivity(),"TabInfoFragment_gotoAboutUsActivity");
        Intent intent = new Intent(getContext(), AboutUsActivity.class);
        startActivity(intent);
    }

    private void gotoNotifyActivity() {
        MobclickAgent.onEvent(getActivity(),"TabInfoFragment_gotoNotifyActivity");
        if (LoginInterceptor.getLogin()) {
            Intent intent = new Intent(getContext(), WebNotifyActivity.class);
            startActivity(intent);
        } else {
            gotoLoginActivity();
        }
    }


    private void gotoCheckSoftwareVersion()
    {
        MobclickAgent.onEvent(getActivity(),"TabInfoFragment_gotoCheckSoftwareVersion");
        Subscriber<AppVersionEntity> subscriber = new WaitingSubscriber<AppVersionEntity>(TabInfoFragment.this.getActivity())
        {
            @Override
            public void onNext(AppVersionEntity appVersionEntity)
            {
                onAppUpdateResult(appVersionEntity);
            }
        };
        MopsV2Methods.getInstance().appVersion(subscriber);
    }

    private void gotoSecurityCheckActivity()
    {
        MobclickAgent.onEvent(getActivity(),"TabInfoFragment_gotoSecurityCheckActivity");
        LoginInterceptor.interceptor(getActivity(), "com.smarthome.lilos.lilossmarthome.activity.user.SecurityCheckActivity", null);
    }

    private void gotoResetPasswordActivity()
    {
        MobclickAgent.onEvent(getActivity(),"TabInfoFragment_gotoResetPasswordActivity");
        LoginInterceptor.interceptor(getActivity(), "com.smarthome.lilos.lilossmarthome.activity.user.ResetPasswordActivity", null);
    }

    private void gotoLoginActivity()
    {
        MobclickAgent.onEvent(getActivity(),"TabInfoFragment_gotoLoginActivity");
        if (!SharePreferenceUtils.getLoginStatus())
        {
            Intent intent = new Intent(getContext(), LoginActivity.class);
            startActivity(intent);
        }
    }

    private void gotoFeedbackActivity()
    {
        MobclickAgent.onEvent(getActivity(),"TabInfoFragment_gotoFeedbackActivity");
        LoginInterceptor.interceptor(getActivity(), "com.smarthome.lilos.lilossmarthome.activity.user.FeedbackActivity", null);
    }
    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(getActivity());
    }
    @Override
    public void onResume()
    {
        super.onResume();
        MobclickAgent.onResume(getActivity());
        if (SharePreferenceUtils.getLoginStatus())
        {
            String mobile = SharePreferenceUtils.getMobileNum();
            String enmobile = mobile.substring(0, 3) + "****" + mobile.substring(7);
            mTvAccount.setText(enmobile);
            mIvHead.setImageResource(R.drawable.ic_headpic_login);
            btnLogout.setVisibility(View.VISIBLE);
        }
        else
        {
            mTvAccount.setText(getResources().getText(R.string.not_login));
            mIvHead.setImageResource(R.drawable.ic_headpic_unlogin);
            btnLogout.setVisibility(View.GONE);
        }

        Log.d(TAG, "onResume");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults)
    {
        if (requestCode == 10001)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                Intent intent = new Intent(getContext(), UpdateService.class);
                intent.putExtra("url", data.getUrl());
                intent.putExtra("ver", data.getVersion());
                getContext().startService(intent);
            }
            else
            {
                AppToast.showToast(R.string.msg_update_write);
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void onAppUpdateResult(AppVersionEntity entity)
    {
        data = entity;
        String versionName = SystemUtils.getVersionName(TabInfoFragment.this.getActivity());
        if(versionName.equals(entity.getVersion()))
        {
            AppToast.showToast(R.string.version_is_newest);
        }
        else
        {
            NormalDialog normalDialog = new NormalDialog(getActivity(), data.getDescription(), data.getDescription(), getResources().getString(R.string.update_next_time), getResources().getString(R.string.update_now));
            normalDialog.setCancelable(data.getIs_forced() == 0);
            normalDialog.setCanceledOnTouchOutside(data.getIs_forced() == 0);
            normalDialog.setLeftEnable(data.getIs_forced() == 0);
            normalDialog.setLeftClickListener(v -> {
                MobclickAgent.onEvent(getActivity(),"TabInfoFragment_cancel_Softwareupdate");
                if (data.getIs_forced() == 0)
                {
                    normalDialog.dismiss();
                }
            });
            normalDialog.setRightClickListener(v -> {
                MobclickAgent.onEvent(getActivity(),"TabInfoFragment_Softwareupdate");
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 10001);
                }
                else
                {
                    Intent intent = new Intent(getContext(), UpdateService.class);
                    intent.putExtra("url", data.getUrl());
                    intent.putExtra("ver", data.getVersion());
                    getContext().startService(intent);
                }
                normalDialog.dismiss();
            });
            normalDialog.show();
        }
    }

}