package com.smarthome.lilos.lilossmarthome.activity.device;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Layout;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
//import android.webkit.JsResult;
//import android.webkit.WebSettings;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.bluetooth.BLEService;
import com.smarthome.lilos.lilossmarthome.bluetooth.DfuService;
import com.smarthome.lilos.lilossmarthome.device.BaseBluetoothDevice;
import com.smarthome.lilos.lilossmarthome.device.BaseDevice;
import com.smarthome.lilos.lilossmarthome.device.DeviceManager;
import com.smarthome.lilos.lilossmarthome.jsscope.BleJsScope;
import com.smarthome.lilos.lilossmarthome.jsscope.bean.CallbackBean;
import com.smarthome.lilos.lilossmarthome.jsscope.bean.SubscribeBean;
import com.smarthome.lilos.lilossmarthome.jsscope.holder.LocationHolder;
import com.smarthome.lilos.lilossmarthome.jsscope.method.IM;
import com.smarthome.lilos.lilossmarthome.jsscope.method.Webview;
import com.smarthome.lilos.lilossmarthome.jsscope.util.InjectedChromeClient;
import com.smarthome.lilos.lilossmarthome.network.bean.amap.AMAPAddress;
import com.smarthome.lilos.lilossmarthome.network.bean.amap.AMAPLocation;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceFirmwareEntity;
import com.smarthome.lilos.lilossmarthome.network.retrofit.BaseSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.WaitingSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.AMAPMethods;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.MopsV2Methods;
import com.smarthome.lilos.lilossmarthome.rxbus.EventKey;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.utils.ConvertUtils;
import com.smarthome.lilos.lilossmarthome.utils.CountlyUtil;
import com.smarthome.lilos.lilossmarthome.utils.DevUpdateUtil;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.StatusbarColorUtils;
import com.smarthome.lilos.lilossmarthome.utils.SystemUtils;
import com.smarthome.lilos.lilossmarthome.utils.login.LoginInterceptor;
import com.smarthome.lilos.lilossmarthome.wigdet.ProgressBarDialog;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.export.external.interfaces.WebResourceError;
import com.tencent.smtt.export.external.interfaces.WebResourceRequest;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

//import cn.pedant.SafeWebViewBridge.InjectedChromeClient;
import no.nordicsemi.android.dfu.DfuProgressListener;
import no.nordicsemi.android.dfu.DfuProgressListenerAdapter;
import no.nordicsemi.android.dfu.DfuServiceInitiator;
import no.nordicsemi.android.dfu.DfuServiceListenerHelper;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

import static android.os.Build.VERSION_CODES.LOLLIPOP;
import static com.smarthome.lilos.lilossmarthome.SmartHomeApplication.myDeviceManager;

/**
 * 通用Web控制界面
 * 用于加载不同的Html页面以达到控制不同设备的目的
 */
public class WebControlActivity extends BaseActivity
{
    private long startTime;
    private long endTime;

    public final static String WEB_PATH = "web_path";
    private final int SUBSCRIBE_DATA = 100;
    private final int BLUETOOTH_CHANGE = 101;
    private final int DEVICE_CONNECT_CHANGE = 102;
    private final int WIFI_DEVICE_OFFLINE = 103;

    private ProgressBar mProgressBar;
    private int mProgressStatus = 0;
    private Handler mHandler = new Handler();
    private WebView mWebView;

    private ProgressBarDialog progressBarDialog;


    private MyHandler handler;

    private String mDeviceAddress;
    private BaseDevice mDevice;

    private String mDevFirmwareVersion;
    private String mDevBattery;
    private String mMinAllowVersion;
    private String mAutoCheck;



    BroadcastReceiver myReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            Log.d(WEB_PATH, "onReceive: action = " + action);
            if (action.equals(mDeviceAddress))
            {
                String message = intent.getStringExtra("message");
                IM.receiveMessage(mWebView, mDeviceAddress, message);
            }
        }
    };
    private SubscribeBean bean;
    private boolean oldConnectStatus = true;
    private String mUrl;
    private String tempCallFuncName;
    private double lat;
    private double lng;

    public boolean startUpload = false; //开始进入固件升级流程 1.获取固件及服务器版本号；2.下载升级固件包;3.升级固件
    public boolean cancelUpload = false; //取消固件升级流程
    public boolean isUploading = false; //固件升级
    public boolean uploadSuccess = false; // 升级成功
    public boolean notUpload = false;
    public String uploadFailMsg = "";
    private DeviceFirmwareEntity mEntity;
    private long mReference;
    private DevUpdateUtil mDevUpdateUtil;
    private int mFileType = DfuService.TYPE_AUTO;
    private Uri mFileStreamUri;

    private  boolean isClick = false;


    private BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (intent.getAction().equals(BluetoothAdapter.ACTION_STATE_CHANGED))
            {
                int blueState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0);
                Message message = handler.obtainMessage();
                message.what = BLUETOOTH_CHANGE;
                switch (blueState)
                {
                    case BluetoothAdapter.STATE_TURNING_ON:
                        message.obj = 0;
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        message.obj = 0;
                        break;
                    case BluetoothAdapter.STATE_ON:
                        message.obj = 3;
                        break;
                    case BluetoothAdapter.STATE_OFF:
                        message.obj = 2;
                        break;
                    default:
                        message.obj = 0;
                        break;
                }
                handler.sendMessage(message);
            }
        }
    };

    private BroadcastReceiver mDownloadReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            if (action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
            {
                long reference = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                Log.d("WebControlActivity", "已下载" + reference + "//" + mReference);
                if (reference == mReference)
                {
                    Log.d("WebControlActivity", "下载成功");
                    AppToast.showToast(R.string.upload_download_complete);
                    progressBarDialog.setContent(getResources().getString(R.string.upload_preparing));
                }
                if (mDevUpdateUtil.checkFileMd5(mEntity.getMd5()))
                {
                    Log.d("WebControlActivity", "启动DFU服务");
                    startDfuService();
                }
            }
        }
    };

    private BroadcastReceiver mDfuUpdateReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            if (action.equals(DfuService.BROADCAST_PROGRESS))
            {
                int progress = intent.getIntExtra(DfuService.EXTRA_PROGRESS, 0);
            }
        }
    };

    private DfuProgressListener mDfuProgressListener = new DfuProgressListenerAdapter()
    {
        @Override
        public void onDeviceConnecting(final String deviceAddress)
        {
            Log.d("change","onDeviceConnecting");
            // 设备连接中
            progressBarDialog.setContent(getResources().getString(R.string.upload_connecting));
        }

        @Override
        public void onDfuProcessStarting(final String deviceAddress)
        {
            Log.d("change","onDfuProcessStarting");
            //DFU进程启动中
            progressBarDialog.setContent(getResources().getString(R.string.upload_init));
        }

        @Override
        public void onEnablingDfuMode(final String deviceAddress)
        {
            Log.d("change","onEnablingDfuMode");

        }

        @Override
        public void onFirmwareValidating(final String deviceAddress)
        {
            Log.d("change","onFirmwareValidating");

        }

        @Override
        public void onDeviceDisconnecting(final String deviceAddress)
        {
            Log.d("change","onDeviceDisconnecting");

        }

        @Override
        public void onDfuCompleted(final String deviceAddress)
        {
            Log.d("change","onDfuCompleted");

            progressBarDialog.setProgress(100);
            RxBus.getInstance().send(Events.DEVICE_UPDATE_COMPLETE,deviceAddress);

            if (LoginInterceptor.getLogin()) {
                ((BaseBluetoothDevice) myDeviceManager.mData.get(mDevice.getAddress())).setAutoConnect(true);
            } else {
                ((BaseBluetoothDevice) myDeviceManager.mDataOff.get(mDevice.getAddress())).setAutoConnect(true);
            }
            isUploading = false;
            startUpload = false;
            progressBarDialog.dismiss();
            AppToast.showToast(R.string.update_success);
            final NotificationManager manager = (NotificationManager) WebControlActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
//            manager.cancel(DfuService.NOTIFICATION_ID);
            manager.cancelAll();

            finish();
            uploadSuccess = true;
            // TODO: 2017/2/17 升级成功
        }

        @Override
        public void onDfuAborted(final String deviceAddress)
        {
            Log.d("change","onDfuAborted");

            isUploading = false;
            startUpload = false;
            progressBarDialog.dismiss();
            // TODO: 2017/2/17 升级取消 
        }

        @Override
        public void onProgressChanged(final String deviceAddress, final int percent,
                                      final float speed, final float avgSpeed,
                                      final int currentPart, final int partsTotal)
        {
            Log.d("change","onProgressChanged--" +  percent);

            // TODO: 2017/2/17 上报进度
            progressBarDialog.setContent(getResources().getString(R.string.upload_uploading));
            progressBarDialog.setProgress(percent);
        }

        @Override
        public void onError(final String deviceAddress, final int error, final int errorType,
                            final String message)
        {
            Log.d("change","onError--" + error + "--" +  message );

            if (LoginInterceptor.getLogin()) {
                ((BaseBluetoothDevice) myDeviceManager.mData.get(mDevice.getAddress())).setAutoConnect(true);
            } else {
                ((BaseBluetoothDevice) myDeviceManager.mDataOff.get(mDevice.getAddress())).setAutoConnect(true);

            }
            AppToast.showToast(message);
            isUploading = false;
            startUpload = false;
            progressBarDialog.dismiss();
            final NotificationManager manager = (NotificationManager) WebControlActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
//            manager.cancel(DfuService.NOTIFICATION_ID);
            manager.cancelAll();


            finish();
            // TODO: 2017/2/17 升级失败
        }
    };

    private View.OnKeyListener listener = (v, keyCode, event) ->
    {
        if (event.getAction() == KeyEvent.ACTION_DOWN)
        {
            if (keyCode == KeyEvent.KEYCODE_BACK)
            {
                Log.d("===","web---1");
                if (mWebView.canGoBack())
                {
                    mWebView.goBack();
                }
                else
                {
                    Log.d("===","web---2");
                    finish();
                }
            }
        }
        return false;
    };

    private IntentFilter makeDfuUpdateIntentFilter()
    {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DfuService.BROADCAST_PROGRESS);
        intentFilter.addAction(DfuService.BROADCAST_ERROR);
        intentFilter.addAction(DfuService.BROADCAST_LOG);
        return intentFilter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.d("WebControlActivity","onCreate");

        startTime = System.currentTimeMillis();
        listenClose = false;
        super.onCreate(savedInstanceState);
//        mWebView = new WebView(this);
//        setContentView(mWebView);
        setContentView(R.layout.activity_web_control);
        SmartHomeApplication.isAlert = true;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if(SystemUtils.isFlayme())
            {
                StatusbarColorUtils.setStatusBarDarkIcon(this,false);
            }
            if(SystemUtils.isMIUI())
            {
                SystemUtils.setMIUIStatusBarLightMode(getWindow(),false);
            }
            ViewGroup rootView = (ViewGroup)((ViewGroup)findViewById(android.R.id.content)).getChildAt(0);
            rootView.setFitsSystemWindows(false);
            rootView.setClipToPadding(true);
        }

        initData();
        initViews();
        initRxBus();

        RxBus.getInstance().send(Events.RETURN_MAIN_ACTIVITY);

        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(mDownloadReceiver, filter);
        registerReceiver(mBluetoothReceiver, bluetoothFilter());

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mDfuUpdateReceiver, makeDfuUpdateIntentFilter());
        DfuServiceListenerHelper.registerProgressListener(getApplicationContext(), mDfuProgressListener);

    }

    @Override
    protected void onResume()
    {

        super.onResume();
        Log.d("WebControlActivity","onResume--10");

//        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
//        registerReceiver(mDownloadReceiver, filter);
//        registerReceiver(mBluetoothReceiver, bluetoothFilter());
//
//        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mDfuUpdateReceiver, makeDfuUpdateIntentFilter());
//        DfuServiceListenerHelper.registerProgressListener(getApplicationContext(), mDfuProgressListener);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        Log.d("WebControlActivity","onPause--10");

//        unregisterReceiver(mDownloadReceiver);
//        unregisterReceiver(mBluetoothReceiver);
//
//        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mDfuUpdateReceiver);
//        DfuServiceListenerHelper.unregisterProgressListener(getApplicationContext(), mDfuProgressListener);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.d("WebControlActivity","onDestroy--10");

        unregisterReceiver(mDownloadReceiver);
        unregisterReceiver(mBluetoothReceiver);

        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mDfuUpdateReceiver);
        DfuServiceListenerHelper.unregisterProgressListener(getApplicationContext(), mDfuProgressListener);

        endTime = System.currentTimeMillis();
        long durationTime = (endTime - startTime)/1000;
        if(mDevice !=null)
        {
            CountlyUtil.recordControlDurationTime(mDevice.getTypeId(), durationTime);
        }

        mWebView.removeAllViews();
        mWebView.destroy();
        unRegistReceiver();
        progressBarDialog = null;
    }

    private void initViews()
    {
        mWebView = (WebView) this.findViewById(R.id.web_control);
//        mWebView.setVisibility(View.GONE);
        mProgressBar = (ProgressBar) this.findViewById(R.id.progressbar);
        mProgressBar.setVisibility(View.VISIBLE);


        new Thread(new Runnable() {
            @Override
            public void run() {
                mProgressStatus = 0;
                while(mProgressStatus <= 100){
                    mProgressStatus += 5;
                    mHandler.post(new Runnable(){
                        @Override
                        public void run() {
                            mProgressBar.setProgress(mProgressStatus);
                            if (mProgressStatus >= 100) {
                                mProgressBar.setVisibility(View.GONE);
//                                mWebView.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                    if (mProgressStatus >= 100) {
                        break;
                    }
                    try
                    {
                        Thread.sleep(100);
                    } catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        mWebView.setTag(R.id.device_mac_address, mDeviceAddress);
        mWebView.setTag(R.id.device_name,mDevice.getName());
        mWebView.setTag(R.id.app_client_id, SmartHomeApplication.myClientID);
        mWebView.setTag(R.id.device_type_id,mDevice.getTypeId());
        if(mDevice instanceof BaseBluetoothDevice)
        {
            mWebView.setTag(R.id.device_firmware_ver,((BaseBluetoothDevice) mDevice).getVersion());
        }
        else
        {
            mWebView.setTag(R.id.device_firmware_ver,-1);
        }
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.setOnKeyListener(listener);
        mWebView.setWebChromeClient(new CustomChromeClient("android_obj", BleJsScope.class));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            mWebView.setWebContentsDebuggingEnabled(true);
        }

        mWebView.loadUrl(mUrl);
        mWebView.setWebViewClient(new WebViewClient()
        {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView webView, String s) {
                super.onPageFinished(webView, s);
            }

            @Override
            public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
                super.onReceivedError(webView, webResourceRequest, webResourceError);
                Log.d("test","ERROR-1-" + webResourceError.getDescription() +"---"+ webResourceRequest.toString());
            }

            @Override
            public void onReceivedError(WebView webView, int i, String s, String s1) {
                super.onReceivedError(webView, i, s, s1);
                Log.d("test","ERROR-2-" + s +"---"+s1);
            }
        });

        handler = new MyHandler(this);

        registBroadCastReceiver();
        progressBarDialog = new ProgressBarDialog(this);
    }

    private void initRxBus()
    {
        RxBus.with(this)
                .setEvent(Events.GATT_CONNECTED)
                .onNext(events -> {
                    String address = (String) events.content;
                    if(TextUtils.equals(address,mDeviceAddress))
                    {
                        if (LoginInterceptor.getLogin()) {

                            if (myDeviceManager.mData.containsKey(mDeviceAddress)) {
                                myDeviceManager.mData.get(mDeviceAddress).setIsconnectd(BLEService.STATE_CONNECTED);
                            }
                        } else {
                            if (myDeviceManager.mDataOff.containsKey(mDeviceAddress)) {
                                myDeviceManager.mDataOff.get(mDeviceAddress).setIsconnectd(BLEService.STATE_CONNECTED);
                            }
                        }
                        if (oldConnectStatus == false)
                        {
                            Message message1 = handler.obtainMessage();
                            message1.what = DEVICE_CONNECT_CHANGE;
                            message1.obj = "connect";
                            handler.sendMessage(message1);
                            oldConnectStatus = true;
                        }
                    }
                })
                .create();

        RxBus.with(this)
                .setEvent(Events.GATT_DISCONNECTED)
                .onNext(events -> {
                    String disAddress = (String) events.content;
                    if (TextUtils.equals(disAddress, mDeviceAddress))
                    {
                        if(!isUploading)
                        {
                            if (progressBarDialog != null && progressBarDialog.isShowing()) {
                                cancelUpload = true;
                                startUpload = false;
                                progressBarDialog.dismiss();
                            }
                            if (LoginInterceptor.getLogin()) {
                                if (myDeviceManager.mData.containsKey(mDeviceAddress)) {
                                    myDeviceManager.mData.get(mDeviceAddress).setIsconnectd(BLEService.STATE_DISCONNECTED);
                                }
                            } else {
                                if (myDeviceManager.mDataOff.containsKey(mDeviceAddress)) {
                                    myDeviceManager.mDataOff.get(mDeviceAddress).setIsconnectd(BLEService.STATE_DISCONNECTED);
                                }
                            }
                            if (oldConnectStatus == true)
                            {
                                Message message2 = handler.obtainMessage();
                                message2.what = DEVICE_CONNECT_CHANGE;
                                message2.obj = "disconnect";
                                handler.sendMessage(message2);
                                oldConnectStatus = false;
                            }
                        }
                    }
                })
                .create();

        RxBus.with(this)
                .setEvent(Events.GATT_DATA_AVAILABLE)
                .onNext(events -> {
                    Bundle bundle = (Bundle) events.content;
                    String address = bundle.getString(EventKey.DEVICE_ADDRESS);
                    String data = bundle.getString(EventKey.EXTRA_DEV_DATA);
                    if(TextUtils.equals(address,mDeviceAddress))
                    {
                        Message message = handler.obtainMessage();
                        message.what = SUBSCRIBE_DATA;
                        message.obj = data;
                        handler.sendMessage(message);
                    }
                })
                .create();

        RxBus.with(this)
                .setEvent(Events.MQTT_DEVICE_ONLINE)
                .onNext(events -> {
                    Log.d(WEB_PATH, "@@@@WebControlActivity>> onReceive: >>>>>>>UartService.ACTION_LIANLUOIM_BROADCAST_DEVICEONLINE");
                    Bundle bundle = (Bundle) events.content;
                    boolean bIsonLine = bundle.getBoolean(EventKey.MQTT_ONLINE_STATUS, false);
                    String deviceID = bundle.getString(EventKey.MQTT_DEVICE_ID);
                    Log.d(WEB_PATH, "@@@@WebControlActivity>> bIsonLine = " + bIsonLine);

                    if (deviceID.equals(mDeviceAddress) && bIsonLine == false)
                    {
                        Message message2 = handler.obtainMessage();
                        message2.what = WIFI_DEVICE_OFFLINE;
                        message2.obj = "disconnect";
                        handler.sendMessage(message2);
                    }
                })
                .create();
    }

    private void registBroadCastReceiver()
    {
        Log.d(WEB_PATH, "registBroadCastReceiver: ");
        IntentFilter ifilter = new IntentFilter();
        ifilter.addAction(mDeviceAddress);
        registerReceiver(myReceiver, ifilter);
    }

    private void unRegistReceiver()
    {
        unregisterReceiver(myReceiver);
    }

    private void initData()
    {
        Bundle deviceInfo = getIntent().getExtras();
        mDeviceAddress = deviceInfo.getString(BLEService.EXTRA_DEVICE_ADDR, "E0:CD:51:0C:91:07");

        mUrl = deviceInfo.getString(WEB_PATH, "file:///android_asset/demo.html");

//        mUrl = "file:///android_asset/D1/app.html";
        Log.d("WebControlActivity", mUrl);

        mDevice = DeviceManager.getInstance().getDevice(mDeviceAddress);

        if (mDevice.getTypeId() == 3){
            SharePreferenceUtils.setLatestAddr(mDeviceAddress);
        }
        mDevUpdateUtil = new DevUpdateUtil(getBaseContext());
    }

    private IntentFilter bluetoothFilter()
    {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        return filter;
    }

    public void getLocationAndCheckPermission(String callBackFuncName)
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 10001);
            tempCallFuncName = callBackFuncName;
        }
        else
        {
            getLocation(callBackFuncName);
        }
    }

    private void getLocation(final String callBackFuncName)
    {
        CallbackBean bean = new CallbackBean();
        Map<String, Object> resutlMap = new HashMap<>();

        resutlMap.clear();
        bean.callBackName = callBackFuncName;

        String locationStr = LocationHolder.getLocation(this);

        if (locationStr == null || locationStr.equals(""))
        {
            bean.callBackName = callBackFuncName;
            resutlMap.put("status", "fail");
            bean.callBackParams = JsonUtils.objectToJson(resutlMap);
            mWebView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
            return;
        }

        AMAPMethods.getInstance().locationToAmap(locationStr)
                .flatMap(amapLocation -> {
                    onLocationResult(amapLocation);
                    return AMAPMethods.getInstance().address(amapLocation.locations);
                })
                .subscribe(new BaseSubscriber<AMAPAddress>()
                {
                    @Override
                    public void onNext(AMAPAddress amapAddress)
                    {
                        onAddressResult(amapAddress, callBackFuncName);
                    }
                });
    }

    private void onLocationResult(AMAPLocation location)
    {
        String lngStr = location.locations.split(",")[0];
        String latStr = location.locations.split(",")[1];
        DecimalFormat df = new DecimalFormat("0.00000");
        lngStr = df.format(ConvertUtils.convertToDouble(lngStr, 0));
        latStr = df.format(ConvertUtils.convertToDouble(latStr, 0));
        lng = ConvertUtils.convertToDouble(lngStr, 0);
        lat = ConvertUtils.convertToDouble(latStr, 0);
    }

    private void onAddressResult(AMAPAddress address, String callBackFuncName)
    {
        CallbackBean bean = new CallbackBean();
        Map<String, Object> resutlMap = new HashMap<>();

        bean.callBackName = callBackFuncName;
        resutlMap.put("status", "success");
        resutlMap.put("latitude", lat);
        resutlMap.put("longitude", lng);
        resutlMap.put("address", address.getRegeocode().getFormatted_address());
        bean.callBackParams = JsonUtils.objectToJson(resutlMap);
        Log.d("Location", JsonUtils.objectToJson(bean));
        mWebView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 10001)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                getLocation(tempCallFuncName);
            }
            else
            {
                AppToast.showToast(R.string.msg_get_location);
            }
        }
    }

    /**
     * 固件包下载
     * @param entity
     */
    private void onUpdateResult(DeviceFirmwareEntity entity)
    {

        mEntity = entity;

        if (mDevFirmwareVersion != null && mEntity != null)
        {

            if (Integer.valueOf(mAutoCheck) == 0) {
                //手动点击的
                if (Integer.parseInt(mDevFirmwareVersion) < Integer.parseInt(mMinAllowVersion) || (Integer.parseInt(mDevFirmwareVersion) < Integer.parseInt(mEntity.getVersion()))) {

                    progressBarDialog.show();
                    progressBarDialog.setCanceledOnTouchOutside(false);
                    progressBarDialog.setCancelable(false);
                    progressBarDialog.setTitle(getResources().getString(R.string.upload_dialog_title));
                    String content;
                    if (SystemUtils.isWifi(getBaseContext())) {
                        content = getResources().getString(R.string.upload_content);
                    } else {
                        content = getResources().getString(R.string.upload_content_no_wifi);
                    }
                    //TODO get device version from h5(js).
                    content = String.format(content, mDevFirmwareVersion, mEntity.getVersion());
                    progressBarDialog.setContent(content);
                    progressBarDialog.setLeftStr(getResources().getString(R.string.upload_cancel));
                    progressBarDialog.setRightStr(getResources().getString(R.string.upload_confirm));

                    progressBarDialog.setLeftClickListener(v ->
                    {
                        progressBarDialog.dismiss();
                        cancelUpload = true;
                        startUpload = false;

                    });
                    progressBarDialog.setRightClickListener(v ->
                    {
//                    if (isClick)  {
//                        progressBarDialog.setRightEnable(false);
//                        progressBarDialog.dismiss();
//                        isClick = false;
//                        return;
//                    }
                        if (Integer.valueOf(mDevBattery) >= 30) {

                            mReference = mDevUpdateUtil.downloadFirmware(mEntity.getUrl());

                            progressBarDialog.setContent(getResources().getString(R.string.upload_downloading));

                            progressBarDialog.setLeftEnable(false);
                            progressBarDialog.setRightEnable(false);
                            progressBarDialog.setCancelable(false);
                            progressBarDialog.setCanceledOnTouchOutside(false);
                        } else {
                            if (!notUpload) {
                                progressBarDialog.setContent(getResources().getString(R.string.upload_dialog_battery_title));
                                notUpload = true;
                                progressBarDialog.setLeftEnable(false);
                                progressBarDialog.setRightEnable(true);
                                progressBarDialog.setCancelable(false);
                                progressBarDialog.setCanceledOnTouchOutside(false);
                            }  else {

                                cancelUpload = true;
                                startUpload = false;
                                progressBarDialog.dismiss();
                                finish();
                            }
                        }

                    });
                } else {
                    Log.d("test","cancelUpload---1");
                    cancelUpload = true;
                    startUpload = false;
                    AppToast.showToast(R.string.upload_no_version);
                }
            } else {
                //自动检测的
                if (Integer.parseInt(mDevFirmwareVersion) < Integer.parseInt(mMinAllowVersion)) {

                    progressBarDialog.show();
                    progressBarDialog.setCanceledOnTouchOutside(false);
                    progressBarDialog.setCancelable(false);
                    progressBarDialog.setTitle(getResources().getString(R.string.upload_dialog_title));
                    String content;
                    if (SystemUtils.isWifi(getBaseContext())) {
                        content = getResources().getString(R.string.upload_content_force);
                    } else {
                        content = getResources().getString(R.string.upload_content_no_wifi);
                    }
                    //TODO get device version from h5(js).
                    content = String.format(content, mDevFirmwareVersion, mEntity.getVersion());
                    progressBarDialog.setContent(content);
                    progressBarDialog.setLeftEnable(false);
                    progressBarDialog.setLeftStr(getResources().getString(R.string.upload_cancel));
                    progressBarDialog.setRightStr(getResources().getString(R.string.upload_confirm));
                    progressBarDialog.setCancelable(false);

                    progressBarDialog.setLeftClickListener(v ->
                    {
                        progressBarDialog.dismiss();
                        cancelUpload = true;
                        startUpload = false;
                        finish();

                    });

                    progressBarDialog.setRightClickListener(v ->
                    {
                        if (Integer.valueOf(mDevBattery) >= 30) {
                            mReference = mDevUpdateUtil.downloadFirmware(mEntity.getUrl());

                            progressBarDialog.setContent(getResources().getString(R.string.upload_downloading));

                            progressBarDialog.setLeftEnable(false);
                            progressBarDialog.setRightEnable(false);
                            progressBarDialog.setCancelable(false);
                            progressBarDialog.setCanceledOnTouchOutside(false);

                        } else {

                            if (!notUpload) {
                                progressBarDialog.setContent(getResources().getString(R.string.upload_dialog_battery_title));
                                notUpload = true;
                                progressBarDialog.setLeftEnable(false);
                                progressBarDialog.setRightEnable(true);
                                progressBarDialog.setCancelable(false);
                                progressBarDialog.setCanceledOnTouchOutside(false);
                            }  else {

                                cancelUpload = true;
                                startUpload = false;
                                progressBarDialog.dismiss();
                                finish();
                            }

                        }

                    });
                } else {
                    cancelUpload = true;
                    startUpload = false;
//                    AppToast.showToast(R.string.upload_no_version);
                }
            }
        } else {
            // TODO Entity is null.
        }
    }

    public class CustomChromeClient extends InjectedChromeClient
    {
        public CustomChromeClient(String injectedName, Class injectedCls)
        {
            super(injectedName, injectedCls);
        }

        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {

            Observable.empty()
                    .compose(bindToLifecycle())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnCompleted(()->{
                        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                        builder.setMessage(message);
                        builder.setPositiveButton("OK", (dialog, which) -> dialog.dismiss() );
                        builder.setCancelable(false);
                        builder.create();
                        builder.show();
                    })
                    .subscribe();
            return super.onJsAlert(view, url, message, result);
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            Log.d("WebControlActivity", String.valueOf(newProgress));
            if(newProgress >= 100) {
                mProgressBar.setVisibility(View.GONE);
            }
            super.onProgressChanged(view, newProgress);
        }

        @Override
        public boolean onJsConfirm(WebView webView, String url, String message, JsResult result) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(webView.getContext());
            Button btn_cancel = null;
            Button btn_comfirm = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                View webview_confirm = View
                        .inflate(webView.getContext(), R.layout.webview_confirm, null);
                TextView msg = ((TextView)webview_confirm.findViewById(R.id.dialog_message));
                msg.setText(message);
                msg.setPadding(0, 20, 0, 20);

                btn_cancel=(Button)webview_confirm
                        .findViewById(R.id.btn_cancel);//取消按钮
                btn_comfirm=(Button)webview_confirm
                        .findViewById(R.id.btn_comfirm);//确定按钮


                builder.setView(webview_confirm);
            } else {
                builder.setTitle("友情提醒")
                        .setMessage(message)
                        .setPositiveButton("确定",new OnClickListener() {
                            public void onClick(DialogInterface dialog,int which) {
                                result.confirm();
                                dialog.dismiss();
                            }
                        })
                        .setNeutralButton("取消", new OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                result.cancel();
                                dialog.dismiss();
                            }
                        });
            }


            builder.setOnCancelListener(new OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    result.cancel();
                }
            });
            // 屏蔽keycode等于84之类的按键，避免按键后导致对话框消息而页面无法再弹出对话框的问题
            builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,KeyEvent event) {
                Log.v("onJsConfirm", "keyCode==" + keyCode + "event="+ event);
                return true;
            }});
            // 禁止响应按back键的事件
            // builder.setCancelable(false);
            AlertDialog dialog = builder.create();
            dialog.show();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btn_cancel.setOnClickListener(new Button.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        result.cancel();
                        dialog.dismiss();
                    }
                });
                btn_comfirm.setOnClickListener(new Button.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        result.confirm();
                        dialog.dismiss();
                    }
                });
            }
            return true;
            // return super.onJsConfirm(view, url, message, result);
        }
    }

    /**
     * 点击升级
     * @param devFirmwareVersion
     */
    public DeviceFirmwareEntity gotoUpload(String devFirmwareVersion,String battery,String minAllowVersion,String autoCheck)
    {

        this.mDevFirmwareVersion = devFirmwareVersion;
        this.mDevBattery = battery;
        this.mMinAllowVersion = minAllowVersion;
        this.mAutoCheck = autoCheck;

        Log.d("WebControlActivity", "开始更新");
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            Log.d("WebControlActivity", "获取权限");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 10001);
        } else
        {

            Log.d("WebControlActivity", "正式开始");
            isUploading = false;
            startUpload = true;
            uploadSuccess = false;
            cancelUpload = false;

            if (Integer.valueOf(autoCheck) == 1){
                //自动
                if (Integer.valueOf(mDevFirmwareVersion) < Integer.valueOf(mMinAllowVersion)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(WebControlActivity.this);
                    builder.setMessage("即将更新设备");
                    builder.setPositiveButton("OK", (dialog, which) -> {
                        dialog.dismiss();

                        Subscriber<DeviceFirmwareEntity> subscriber = new WaitingSubscriber<DeviceFirmwareEntity>(WebControlActivity.this)//new BaseSubscriber<DeviceFirmwareEntity>()
                        {
                            @Override
                            public void onNext(DeviceFirmwareEntity deviceFirmwareEntity)
                            {
                                Log.d("WebControlActivity", "版本获取成功");
//                    startDownloadFirmware(deviceFirmwareEntity);
                                onUpdateResult(deviceFirmwareEntity);
                                mEntity = deviceFirmwareEntity;
                            }

                            @Override
                            public void onError(Throwable e)
                            {
                                Log.d("WebControlActivity", "版本获取失败");
                                super.onError(e);
                                cancelUpload = true;
                                startUpload = false;
                            }
                        };
                        Log.d("WebControlActivity", "获取版本");
                        MopsV2Methods.getInstance().firmware(subscriber, mDevice.getTypeId());

                    });
                    builder.setCancelable(false);
                    builder.create();
                    builder.show();
                } else {
                    Log.d("test","cancelUpload---3");
                    cancelUpload = true;
                    startUpload = false;
                }
            } else {
                 //手动
//                if (Integer.valueOf(mDevFirmwareVersion) < Integer.valueOf(mMinAllowVersion)) {

                    Subscriber<DeviceFirmwareEntity> subscriber = new WaitingSubscriber<DeviceFirmwareEntity>(WebControlActivity.this)//new BaseSubscriber<DeviceFirmwareEntity>()
                    {
                        @Override
                        public void onNext(DeviceFirmwareEntity deviceFirmwareEntity)
                        {
                            Log.d("WebControlActivity", "版本获取成功");
//                    startDownloadFirmware(deviceFirmwareEntity);
                            onUpdateResult(deviceFirmwareEntity);
                            mEntity = deviceFirmwareEntity;
                        }

                        @Override
                        public void onError(Throwable e)
                        {
                            Log.d("WebControlActivity", "版本获取失败");
                            super.onError(e);
                            cancelUpload = true;
                            startUpload = false;
                        }
                    };
                    Log.d("WebControlActivity", "获取版本");
                    MopsV2Methods.getInstance().firmware(subscriber, mDevice.getTypeId());
//                } else {
//                    Log.d("test","cancelUpload---2");
//                    cancelUpload = true;
//                    startUpload = false;
//                }
            }
        }

        return mEntity;
    }

    private void startDownloadFirmware(DeviceFirmwareEntity entity)
    {
        Log.d("WebControlActivity", "进入下载步骤");
        if (LoginInterceptor.getLogin()) {
            mDevice = DeviceManager.getInstance().mData.get(mDeviceAddress);
        } else {
            mDevice = DeviceManager.getInstance().mDataOff.get(mDeviceAddress);
        }
        mEntity = entity;

        Log.d("WebControlActivity", "更新信息检查");
        if (mDevice != null && mEntity != null)
        {
            Log.d("WebControlActivity", "启动下载");
            mReference = mDevUpdateUtil.downloadFirmware(mEntity.getUrl());
        }
    }

    private void startDfuService()
    {
        mDevice.disconnect();
        if (LoginInterceptor.getLogin()) {
            ((BaseBluetoothDevice) myDeviceManager.mData.get(mDevice.getAddress())).setAutoConnect(false);
        } else {
            ((BaseBluetoothDevice) myDeviceManager.mDataOff.get(mDevice.getAddress())).setAutoConnect(false);
        }
        final DfuServiceInitiator starter = new DfuServiceInitiator(mDevice.getAddress())
                .setDeviceName(mDevice.getName())
                .setKeepBond(true);
        if (mFileType == DfuService.TYPE_AUTO)
        {
            starter.setZip(mFileStreamUri, mDevUpdateUtil.mPath);
        }
        else
        {
            Log.d("WebControlActivity", "不是自动模式");
            return;
        }
        isUploading = true;
        starter.start(this, DfuService.class);
    }

    private class MyHandler extends Handler
    {
        private WeakReference<WebControlActivity> ref;

        public MyHandler(WebControlActivity activity)
        {
            if (activity != null)
            {
                this.ref = new WeakReference<>(activity);
            }
        }

        @Override
        public void handleMessage(Message msg)
        {
            if (ref == null)
            {
                return;
            }

            WebControlActivity v = ref.get();
            if (v == null)
            {
                return;
            }

            switch (msg.what)
            {
                case 0:
                    finish();
                    break;
                case SUBSCRIBE_DATA:
                    String data = (String) msg.obj;

                    if (bean == null)
                    {
                        bean = new SubscribeBean();
                    }
                    bean.deviceId = v.mDeviceAddress;
                    bean.value = data;
                    v.mWebView.loadUrl("javascript:LL.ble.subScribeCallBack(" + JsonUtils.objectToJson(bean) + ")");
                    break;
                case BLUETOOTH_CHANGE:
                    int state = (int) msg.obj;
//                    Map<String,String> map = new HashMap<>();
//                    map.put("state",state + "");
//                    v.mWebView.loadUrl("javascript:LL.ble.stateDidChanged(" + JsonUtils.objectToJson(map) + ")");
                      v.mWebView.loadUrl("javascript:LL.ble.stateDidChanged(" + state + ")");

                    break;
                case DEVICE_CONNECT_CHANGE:
                    String status = (String) msg.obj;
//                    Map<String,String> map2 = new HashMap<>();
//                    map2.put("state",status +"");
//                    v.mWebView.loadUrl("javascript:LL.ble.deviceStateDidChanged('" + JsonUtils.objectToJson(map2) + "')");
                    v.mWebView.loadUrl("javascript:LL.ble.stateDidChanged(" + status + ")");

                    break;
                case WIFI_DEVICE_OFFLINE:
                    AppToast.showToast("当前设备下线，退出当前页面");
                    sendEmptyMessageDelayed(0, 1000);
                    break;
            }
        }
    }
}
