package com.smarthome.lilos.lilossmarthome.jsscope.method;

//import android.webkit.WebView;

import com.smarthome.lilos.lilossmarthome.jsscope.bean.CallbackBean;
import com.smarthome.lilos.lilossmarthome.jsscope.holder.StorageHolder;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceSettingsEntity;
import com.smarthome.lilos.lilossmarthome.network.retrofit.BaseSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.MopsV2Methods;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.tencent.smtt.sdk.WebView;

import java.util.HashMap;
import java.util.Map;

import rx.Subscriber;

/**
 * 对应JSSDK中Storage集指令
 */
public class Storage
{
    private static CallbackBean bean = new CallbackBean();
    private static Map<String, Object> resutlMap = new HashMap<>();

    public static boolean setItem(WebView webView, Map<String, String> params,
                                  String callBackFuncName)
    {
        resutlMap.clear();
        String deviceId = params.get("deviceId");
        String itemKey = params.get("itemKey");
        String itemValue = params.get("itemValue");

        StorageHolder.putInfo(webView.getContext().getApplicationContext(), deviceId, itemKey, itemValue);

        resutlMap.put("status", "success");
        bean.callBackName = callBackFuncName;
        bean.callBackParams = JsonUtils.objectToJson(resutlMap);
        webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
        return true;
    }

    public static boolean getItem(WebView webView, Map<String, String> params,
                                  String callBackFuncName)
    {
        resutlMap.clear();
        String deviceId = params.get("deviceId");
        String itemKey = params.get("itemKey");

        String result = StorageHolder.getInfo(webView.getContext().getApplicationContext(), deviceId, itemKey);

        resutlMap.put(itemKey, result);
        bean.callBackName = callBackFuncName;
        bean.callBackParams = JsonUtils.objectToJson(resutlMap);
        webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
        return true;
    }

    public static boolean removeItem(WebView webView, Map<String, String> params,
                                     String callBackFuncName)
    {
        resutlMap.clear();
        String deviceId = params.get("deviceId");
        String itemKey = params.get("itemKey");

        StorageHolder.removeInfo(webView.getContext().getApplicationContext(), deviceId, itemKey);

        resutlMap.put("status", "success");
        bean.callBackName = callBackFuncName;
        bean.callBackParams = JsonUtils.objectToJson(resutlMap);
        webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
        return true;
    }

    public static boolean setServerItem(final WebView webView, Map<String, String> params,
                                        final String callBackFuncName)
    {
        resutlMap.clear();
        String deviceId = params.get("deviceId");
        String itemKey = params.get("dataKey");
        String itemValue = params.get("dataValue");

        Subscriber<DeviceSettingsEntity> subscriber = new BaseSubscriber<DeviceSettingsEntity>()
        {
            @Override
            public void onNext(DeviceSettingsEntity deviceSettingsEntity)
            {
                resutlMap.put("status", "success");
                bean.callBackName = callBackFuncName;
                bean.callBackParams = JsonUtils.objectToJson(resutlMap);
                webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
            }

            @Override
            public void onError(Throwable e)
            {
                super.onError(e);
                resutlMap.put("status", "fail");
                bean.callBackName = callBackFuncName;
                bean.callBackParams = JsonUtils.objectToJson(resutlMap);
                webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
            }
        };
        MopsV2Methods.getInstance().setSettings(subscriber,deviceId,itemKey,itemValue);

        return true;
    }

    public static boolean getServerItem(final WebView webView, Map<String, String> params,
                                        final String callBackFuncName)
    {
        resutlMap.clear();
        String deviceId = params.get("deviceId");
        String itemKey = params.get("dataKey");

        Subscriber<DeviceSettingsEntity> subscriber = new BaseSubscriber<DeviceSettingsEntity>()
        {
            @Override
            public void onNext(DeviceSettingsEntity deviceSettingsEntity)
            {
                resutlMap.put("status", "success");
                resutlMap.put(deviceSettingsEntity.getKey(), deviceSettingsEntity.getValue());
                bean.callBackName = callBackFuncName;
                bean.callBackParams = JsonUtils.objectToJson(resutlMap);
                webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
            }

            @Override
            public void onError(Throwable e)
            {
                super.onError(e);
                resutlMap.put("status", "fail");
                bean.callBackName = callBackFuncName;
                bean.callBackParams = JsonUtils.objectToJson(resutlMap);
                webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
            }
        };
        MopsV2Methods.getInstance().getSetting(subscriber,deviceId,itemKey);

        return true;
    }
}
