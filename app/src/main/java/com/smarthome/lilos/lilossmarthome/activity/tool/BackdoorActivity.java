package com.smarthome.lilos.lilossmarthome.activity.tool;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.network.retrofit.BaseSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.MopsV2Methods;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.utils.ConfigUtils;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.umeng.analytics.MobclickAgent;

import rx.Subscriber;

/**
 * 后门页面
 * 面向测试人员的后门页
 * 用于切换线上服务器与测试服务器
 * 用于扫描二维码进入Web控制页面,便于H5开发人员调试
 * 后期可以接入各类测试功能
 */
public class BackdoorActivity extends BaseActivity
{
    ImageView mIvBack;
    RadioGroup mRgServerAddress;
    RadioButton mRbtnOnline;
    RadioButton mRbtnTest;
    Button mBtnQrScan;
    Button mBtnDevCheck;

    private boolean initComplete = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_backdoor);

        initView();
    }

    private void initView()
    {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mRgServerAddress = (RadioGroup) findViewById(R.id.rg_server_address);
        mRbtnOnline = (RadioButton) findViewById(R.id.rbtn_online);
        mRbtnTest = (RadioButton) findViewById(R.id.rbtn_test);
        mBtnQrScan = (Button) findViewById(R.id.btn_qr_scan);
        mBtnDevCheck = (Button) findViewById(R.id.btn_device_check);

        mRgServerAddress.setOnCheckedChangeListener((group, checkedId) -> {
            if (initComplete)
            {
                MobclickAgent.onEvent(this, "BackdoorActivity_logout");
                logout();
            }
            else
            {
                initComplete = true;
            }
            switch (checkedId)
            {
                case R.id.rbtn_online:
                    ConfigUtils.setServer(ConfigUtils.ONLINE_SERVER);
                    break;
                case R.id.rbtn_test:
                    ConfigUtils.setServer(ConfigUtils.TEST_SERVER);
                    break;
            }
        });


        switch (ConfigUtils.getServer())
        {
            case ConfigUtils.ONLINE_SERVER:
                mRbtnOnline.setChecked(true);
                break;
            case ConfigUtils.TEST_SERVER:
                mRbtnTest.setChecked(true);
                break;
        }

        mBtnQrScan.setOnClickListener(v -> gotoQrScanActivity());
        mBtnDevCheck.setOnClickListener(v -> gotoDeviceCheckActivity());
        mIvBack.setOnClickListener(v -> {
            MobclickAgent.onEvent(this, "BackdoorActivity_back");
            finish();
        });
    }

    private void gotoQrScanActivity()
    {
        if (ActivityCompat.checkSelfPermission(BackdoorActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(BackdoorActivity.this, new String[]{Manifest.permission.CAMERA}, 10001);
        }
        else
        {
            Intent intent = new Intent(getBaseContext(), QRScanActivity.class);
            startActivity(intent);
        }
    }

    private void gotoDeviceCheckActivity()
    {
        if (ActivityCompat.checkSelfPermission(BackdoorActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(BackdoorActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 10002);
        }
        else
        {
            Intent intent = new Intent(getBaseContext(), DeviceCheckActivity.class);
            startActivity(intent);
        }
    }

    private void logout()
    {
        MopsV2Methods.getInstance().logout(new BaseSubscriber<String>()
        {
            @Override
            public void onNext(String s)
            {

            }
        }, SmartHomeApplication.myClientID);

        SharePreferenceUtils.setLoginStatus(false);
        SharePreferenceUtils.setAccessToken("");
        SharePreferenceUtils.setMobileNum("");

        for (String address : SmartHomeApplication.myDeviceManager.mData.keySet())
        {
            SmartHomeApplication.mBluetoothService.disconnect(address);
        }
        SmartHomeApplication.mBluetoothService.clearData(0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults)
    {
        if (requestCode == 10001)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                Intent intent = new Intent(getBaseContext(), QRScanActivity.class);
                startActivity(intent);
            }
            else
            {
                AppToast.showToast(R.string.msg_qr_camera);
            }
        }
        else if (requestCode == 10002)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                Intent intent = new Intent(getBaseContext(), DeviceCheckActivity.class);
                startActivity(intent);
            }
            else
            {
                AppToast.showToast(R.string.msg_scan_location);
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
