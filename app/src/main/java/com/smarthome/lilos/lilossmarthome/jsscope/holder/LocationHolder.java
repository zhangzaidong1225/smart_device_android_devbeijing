package com.smarthome.lilos.lilossmarthome.jsscope.holder;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;

import java.util.List;

/**
 * 位置服务的Holder
 * 用于获取位置信息
 * Created by Joker on 2016/8/28.
 */
public class LocationHolder {
    public static String getLocationGPSAndNet(Context context) {
        LocationManager locationManager;
        Location location;
        String loctionString = "";
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (gps || network)
        {
            Log.e("===location==","   LocationHolder");
        }
        else
        {
            AppToast.showToast(R.string.msg_open_loaction);

            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            context.startActivity(intent);
        }
        // 获取location对象
        location = getBestLocation(locationManager);
        //获取所有可用的位置提供器
        List<String> providers = locationManager.getProviders(true);
        if(providers.contains(LocationManager.GPS_PROVIDER)){
        }else if(providers.contains(LocationManager.NETWORK_PROVIDER)){
        }else{
        }

        loctionString = updateWithNewLocation(location);
        Log.e("定位47","  --"+loctionString);
/*        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }*/
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                3000, 8, new LocationListener() {

                    @Override
                    public void onStatusChanged(String provider, int status,
                                                Bundle extras) {
                    }

                    @Override
                    public void onProviderEnabled(String provider) {
                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        updateWithNewLocation(locationManager
                                .getLastKnownLocation(provider));
                    }

                    @Override
                    public void onProviderDisabled(String provider) {
                        updateWithNewLocation(null);
                    }

                    @Override
                    public void onLocationChanged(Location location) {
                        location = getBestLocation(locationManager);// 每次都去获取GPS_PROVIDER优先的location对象
                        updateWithNewLocation(location);
                    }
                });
        return loctionString;
    }
    /**
     * 获取location对象，优先以GPS_PROVIDER获取location对象，当以GPS_PROVIDER获取到的locaiton为null时
     * ，则以NETWORK_PROVIDER获取location对象，这样可保证在室内开启网络连接的状态下获取到的location对象不为空
     *
     * @param locationManager
     * @return
     */
    private static Location getBestLocation(LocationManager locationManager) {
        Location result = null;
        if (locationManager != null) {
            if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                result = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                Log.e("定位2","  "+result);
            }
/*            result = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);*/
            if (result != null) {
                return result;
            } else {
                if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
                    result = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    Log.e("定位3","  "+result);
                }
/*                result = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);*/
                return result;
            }
        }
        return result;
    }
    public static String getLocalAddress(Context context) {
        LocationManager locationManager;
        locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        String provider = locationManager.getBestProvider(criteria, true);

        Location location = locationManager.getLastKnownLocation(provider);
        //updateWithNewLocation(location);
        locationManager.requestLocationUpdates(provider, 2000, 10,
                locationListener);
        return updateWithNewLocation(location);
    }


    private final static LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            updateWithNewLocation(location);
        }

        public void onProviderDisabled(String provider) {
            updateWithNewLocation(null);
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };


    private static String updateWithNewLocation(Location location) {
        String latLongString;
        if (location != null) {
            double lat = location.getLatitude();
            double lng = location.getLongitude();
            latLongString = lng + "," + lat;
        } else {
            latLongString = "";
        }
        Log.e("定位1","  "+latLongString);
        return latLongString;
    }
    public static String getLocation(Context context)
    {
        String locationStr = "";
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (gps || network)
        {
            Log.e("===location==","   LocationHolder");
        }
        else
        {
//            AppToast.showToast(R.string.msg_open_loaction);

            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            context.startActivity(intent);
            //context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }

        for (int i = 0; i < 5; i++)
        {
            Log.e("===location==","   LocationHolder5");
            android.location.Location gpsLocaiton = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            android.location.Location networkLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (gpsLocaiton != null && networkLocation != null)
            {
                if (networkLocation.getTime() > gpsLocaiton.getTime())
                {
                    locationStr = networkLocation.getLongitude() + ":" + networkLocation.getLatitude();
                }
                else
                {
                    locationStr = gpsLocaiton.getLongitude() + ":" + gpsLocaiton.getLatitude();
                }
            }
            else if (gpsLocaiton != null && networkLocation == null)
            {
                locationStr = gpsLocaiton.getLongitude() + ":" + gpsLocaiton.getLatitude();
            }
            else if (gpsLocaiton == null && networkLocation != null)
            {
                locationStr =  networkLocation.getLatitude() + ":" + networkLocation.getLongitude();
            }
            else
            {
                try
                {
                    Thread.sleep(1000);
                } catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        }
        Log.e("定位","  "+locationStr);

        return locationStr;
    }


}
