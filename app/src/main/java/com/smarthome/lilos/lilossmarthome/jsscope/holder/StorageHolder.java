package com.smarthome.lilos.lilossmarthome.jsscope.holder;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 本地存储Holder
 * 用于在本地存储内容
 * Created by Joker on 2016/8/19.
 */
public class StorageHolder
{
    private static SharedPreferences getSP(Context context)
    {
        return context.getSharedPreferences("SPHelper", Activity.MODE_PRIVATE);
    }

    public static void putInfo(Context context, String deviceId, String key, String value)
    {
        Map<String, Object> root;
        String rootMapStr = getSP(context).getString(deviceId, "");
        if (TextUtils.equals(rootMapStr, ""))
        {
            root = new HashMap<>();
        }
        else
        {
            root = (Map<String, Object>) JsonUtils.jsonToMap(rootMapStr);
        }

        String[] keys = key.split(":");
        root = put(root, keys, 0, value);
        String putInfo = JsonUtils.objectToJson(root);
        getSP(context).edit().putString(deviceId, putInfo).commit();
        Log.d("SPHelper", "ADD:" + rootMapStr + " >> " + getSP(context).getString(deviceId, ""));
    }

    public static String getInfo(Context context, String deviceId, String key)
    {
        Map<String, Object> root;
        String rootMapStr = getSP(context).getString(deviceId, "");
        if (TextUtils.equals(rootMapStr, ""))
        {
            return "";
        }
        else
        {
            root = (Map<String, Object>) JsonUtils.jsonToMap(rootMapStr);
        }

        String[] keys = key.split(":");
        String result = get(root, keys, 0);
        if (result == null || TextUtils.equals(result, "null"))
        {
            result = "";
        }
        Log.d("SPHelper", "GET:" + result);
        return result;
    }

    public static void removeInfo(Context context, String deviceId, String key)
    {
        Map<String, Object> root;
        String rootMapStr = getSP(context).getString(deviceId, "");
        if (TextUtils.equals(rootMapStr, ""))
        {
            return;
        }
        else
        {
            root = (Map<String, Object>) JsonUtils.jsonToMap(rootMapStr);
        }

        String[] keys = key.split(":");
        root = remove(root, keys, 0);
        String putInfo = JsonUtils.objectToJson(root);
        getSP(context).edit().putString(deviceId, putInfo).commit();
        Log.d("SPHelper", "REMOVE:" + rootMapStr + " >> " + getSP(context).getString(deviceId, ""));
    }

    private static Map<String, Object> put(Map<String, Object> map, String[] key, int index,
                                           String value)
    {
        if (index == key.length - 1)
        {
            map.put(key[index], value);
        }
        else
        {
            Map<String, Object> nextMap = new HashMap<>();
            if (map.containsKey(key[index]))
            {
                Object ob = map.get(key[index]);
                if (ob instanceof Map)
                {
                    nextMap = (Map<String, Object>) ob;
                }
            }

            map.put(key[index], put(nextMap, key, ++index, value));
        }
        return map;
    }

    private static String get(Map<String, Object> map, String[] key, int index)
    {
        if (index == key.length - 1)
        {
            if (map.containsKey(key[index]))
            {
                return JsonUtils.objectToJson(map.get(key[index]));
            }
            else
            {
                return "";
            }
        }
        else
        {
            Map<String, Object> nextMap;
            if (map.containsKey(key[index]))
            {
                Object ob = map.get(key[index]);
                if (ob instanceof Map)
                {
                    nextMap = (Map<String, Object>) ob;
                    return get(nextMap, key, ++index);
                }
            }
        }
        return "";
    }

    private static Map<String, Object> remove(Map<String, Object> map, String[] key, int index)
    {
        if (index == key.length - 1)
        {
            map.remove(key[index]);
        }
        else
        {
            Map<String, Object> nextMap = new HashMap<>();
            if (map.containsKey(key[index]))
            {
                Object ob = map.get(key[index]);
                if (ob instanceof Map)
                {
                    nextMap = (Map<String, Object>) ob;
                }
            }
            map.put(key[index], remove(nextMap, key, ++index));
        }
        return map;
    }
}
