package com.smarthome.lilos.lilossmarthome.wigdet;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.smarthome.lilos.lilossmarthome.R;

/**
 * Created by Joker on 2016/7/22.
 */
public class NormalDialog
{
    Activity context;
    String title = "";
    String content = "";
    String leftStr = "取消";
    String rightStr = "确认";
    View.OnClickListener leftClickListener;
    View.OnClickListener rightClickListener;
    Dialog dialog;

    TextView dialogTitle;
    TextView dialogContentText;
    Button dialogLeft;
    Button dialogRight;

    /**
     * 构造方法
     *
     * @param context
     */
    public NormalDialog(Activity context)
    {
        this.context = context;
        initViews();
    }

    /**
     * 构造方法
     *
     * @param context  上下文对象
     * @param title    标题
     * @param content  显示内容
     * @param leftStr  左边按键文字
     * @param rightStr 右边案件文字
     */
    public NormalDialog(Activity context, String title, String content, String leftStr,
                        String rightStr)
    {
        this.context = context;
        this.title = title;
        this.content = content;
        this.leftStr = leftStr;
        this.rightStr = rightStr;
        initViews();
    }

    /**
     * 设置左边按键监听事件
     *
     * @param leftClickListener 按键监听事件
     */
    public void setLeftClickListener(View.OnClickListener leftClickListener)
    {
        this.leftClickListener = leftClickListener;
    }

    /**
     * 设置右边按键监听事件
     *
     * @param rightClickListener 按键监听事件
     */
    public void setRightClickListener(View.OnClickListener rightClickListener)
    {
        this.rightClickListener = rightClickListener;
    }

    /**
     * 设置标题
     *
     * @param title 标题
     */
    public void setTitle(String title)
    {
        this.title = title;
        dialogTitle.setText(title);
    }

    /**
     * 设置内容
     *
     * @param content 内容
     */
    public void setContent(String content)
    {
        this.content = content;
        dialogContentText.setText(content);
    }

    /**
     * 设置左按键文本
     *
     * @param leftStr 左按键文本
     */
    public void setLeftStr(String leftStr)
    {
        this.leftStr = leftStr;
    }

    /**
     * 设置右按键文本
     *
     * @param rightStr 右按键文本
     */
    public void setRightStr(String rightStr)
    {
        this.rightStr = rightStr;
    }

    /**
     * 初始化控件
     */
    private void initViews()
    {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_normal);

        dialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
        dialogContentText = (TextView) dialog.findViewById(R.id.dialog_content);
        dialogLeft = (Button) dialog.findViewById(R.id.dialog_cancel);
        dialogRight = (Button) dialog.findViewById(R.id.dialog_confirm);
    }

    /**
     * 显示Dialog
     */
    public void show()
    {
        if (leftClickListener != null)
        {
            dialogLeft.setOnClickListener(leftClickListener);
        }
        if (rightClickListener != null)
        {
            dialogRight.setOnClickListener(rightClickListener);
        }

        dialogTitle.setText(title);
        dialogContentText.setText(content);
        dialogLeft.setText(leftStr);
        dialogRight.setText(rightStr);

        dialog.show();
    }

    /**
     * 设置是否可以取消
     *
     * @param flag 是否可以取消
     */
    public void setCancelable(boolean flag)
    {
        dialog.setCancelable(flag);
    }

    /**
     * 设置点击其他区域是否可以取消
     *
     * @param flag 是否可以取消
     */
    public void setCanceledOnTouchOutside(boolean flag)
    {
        dialog.setCanceledOnTouchOutside(false);
    }

    /**
     * 释放Dialog
     */
    public void dismiss()
    {
        dialog.dismiss();
    }

    public void setLeftEnable(boolean enable)
    {
        if (enable)
        {
            dialogLeft.setTextColor(context.getResources().getColor(R.color.blue_btn));
        }
        else
        {
            dialogLeft.setTextColor(context.getResources().getColor(R.color.dimgrey));
        }
    }
}
