package com.smarthome.lilos.lilossmarthome.network.retrofit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 切换RxJava的进程
 * Created by Joker on 2016/12/21.
 */

public class SchedulersTransformer<T> implements Observable.Transformer<T, T>
{
    @Override
    public Observable<T> call(Observable<T> tObservable)
    {
        return tObservable
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
