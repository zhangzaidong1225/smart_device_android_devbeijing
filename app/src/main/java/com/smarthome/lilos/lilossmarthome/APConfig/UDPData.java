package com.smarthome.lilos.lilossmarthome.APConfig;

/**
 * Created by Tristan on 2016/8/10.
 */
public class UDPData
{
    String hostIP;
    String message;

    public UDPData(String hostIP, String message)
    {
        this.hostIP = hostIP;
        this.message = message;
    }
}
