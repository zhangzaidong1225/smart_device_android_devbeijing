package com.smarthome.lilos.lilossmarthome.activity.device;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.bluetooth.BLEService;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceTypeDetailEntity;
import com.smarthome.lilos.lilossmarthome.network.retrofit.BaseSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.MopsV2Methods;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.wigdet.LoadingLayout;
import com.smarthome.lilos.lilossmarthome.wigdet.WaitingDialog;
import com.umeng.analytics.MobclickAgent;

import java.util.Arrays;

import rx.Subscriber;

/**
 * 设备操作指示页面
 * 用于引导用户确认当前打开设备并且正确启动了连接状态
 */
public class DevicePowerOnPromptActivity extends BaseActivity
{
    DeviceTypeDetailEntity bean = null;
    private LoadingLayout mVLoading;
    private ImageView mIvBack;
    private SimpleDraweeView mIvDevice;
    private TextView mTvPowerOnPrompr;
    private Button mBtnNext;
    private CheckBox mCbConfirm;
    private int typeId;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_device_power_on_prompt);
        initView();

        mVLoading.show();
        typeId = getIntent().getIntExtra(BLEService.EXTRA_DEVICE_TYPE, 0);

        getDeviceTypeInfo();
    }

    private void initView()
    {
        mVLoading = (LoadingLayout) findViewById(R.id.v_loading);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvDevice = (SimpleDraweeView) findViewById(R.id.iv_device);
        mTvPowerOnPrompr = (TextView) findViewById(R.id.tv_power_on_prompr);
        mBtnNext = (Button) findViewById(R.id.btn_next);
        mCbConfirm = (CheckBox) findViewById(R.id.cb_confirm);

        mBtnNext.setOnClickListener(v -> checkBoxConfirm());
        mIvBack.setOnClickListener(v -> {
            MobclickAgent.onEvent(this, "DevicePowerOnPromptActivity_back");
            finish();
        });
        mVLoading.setOnReloadingClickListener(this::getDeviceTypeInfo);

        mCbConfirm.setOnCheckedChangeListener((buttonView, isChecked) ->{
            MobclickAgent.onEvent(this, "DevicePowerOnPromptActivity_mCbConfirm_CheckedChange");
            mBtnNext.setEnabled(isChecked);
        });
        mCbConfirm.setChecked(false);
    }

    private void checkBoxConfirm()
    {
        if (bean.getConn_type() == 0) // TODO: 2017/2/14 根据协议改成wifi对应的值
        {
            boolean isContains = Arrays.asList(bean.getWifi_config_model()).contains("AP");
            if (isContains == true)
            {
                MobclickAgent.onEvent(this, "DevicePowerOnPromptActivity_startApConfigActivity");
                startApConfigActivity();
            }
            else
            {
                AppToast.showToast(R.string.ap_config_notsupported);
            }

        }
        else
        {
            MobclickAgent.onEvent(this, "DevicePowerOnPromptActivity_gotoDeviceScanActivity");
            gotoDeviceScanActivity();
        }
    }

    private void gotoDeviceScanActivity()
    {
        Intent intent = new Intent(this, DeviceScanActivity.class);
        intent.putExtra("is_from_manually", true);
        intent.putExtra("target_type_id", typeId);
        startActivity(intent);
    }

    public void startApConfigActivity()
    {
        Intent intent = new Intent();
        intent.putExtra("curclientid", SmartHomeApplication.myClientID);
        intent.putExtra("devicetype", bean.getId());
        intent.putExtra("devicename", bean.getName());
        intent.putExtra("hotspot_ssid_prefix", bean.getHotspot_ssid_prefix());
        intent.setClass(getApplicationContext(), APConfigActivity.class);
        startActivity(intent);
    }

    private void getDeviceTypeInfo()
    {
        MobclickAgent.onEvent(this, "DevicePowerOnPromptActivity_getDeviceTypeInfo");
        Subscriber<DeviceTypeDetailEntity> subscriber = new BaseSubscriber<DeviceTypeDetailEntity>()
        {
            @Override
            public void onNext(DeviceTypeDetailEntity deviceTypeDetailEntity)
            {
                onDeviceTypeInfoResult(deviceTypeDetailEntity);
            }

            @Override
            public void onError(Throwable e)
            {
                super.onError(e);
                mVLoading.fail();
            }
        };
        MopsV2Methods.getInstance().typeDetail(subscriber,typeId+"");
    }

    private void onDeviceTypeInfoResult(DeviceTypeDetailEntity entity)
    {
        bean = entity;

        mTvPowerOnPrompr.setText(bean.getWifi_config_desc());

        Uri uri = Uri.parse(bean.getWifi_config_gif());
        DraweeController draweeController = Fresco.newDraweeControllerBuilder()
                .setUri(uri)
                .setAutoPlayAnimations(true)
                .build();
        mIvDevice.setController(draweeController);
        mVLoading.success();

    }
}
