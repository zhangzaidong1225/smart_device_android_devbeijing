package com.smarthome.lilos.lilossmarthome.network.retrofit.request;

import com.smarthome.lilos.lilossmarthome.network.bean.mops.TokenEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.body.AccessTokenBody;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.body.ModPwdBody;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.body.SMSBody;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.body.UserBody;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.entity.AccessTokenEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.entity.RegisterEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.entity.SMSEntity;

import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Joker on 2016/12/21.
 */

public interface IUserCenterService
{
    @POST("tokens")
    @Headers({"Content-Type:application/json", "Accept:application/json"})
    Observable<TokenEntity> refreshToken(@Body RequestBody body);

    @POST("tokens")
    Observable<AccessTokenEntity> getToken(@Body AccessTokenBody body);

    @POST("sms-codes")
    Observable<SMSEntity> sendSMS(@Query("access_token") String token, @Body SMSBody body);

    @POST("users")
    Observable<RegisterEntity> register(@Query("access_token") String token, @Body UserBody body);

    @PUT("users/password")
    Observable<UserBody> resetPassword(@Query("access_token") String token,
                                       @Body UserBody userBody);

    @PUT("users/password")
    Observable<ModPwdBody> modPassword(@Query("access_token") String token,
                                       @Body ModPwdBody modPwdBody);
}
