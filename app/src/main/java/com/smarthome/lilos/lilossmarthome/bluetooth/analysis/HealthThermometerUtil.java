package com.smarthome.lilos.lilossmarthome.bluetooth.analysis;

import android.bluetooth.BluetoothGattCharacteristic;

/**
 * 体温计服务数据解析
 * Created by Joker on 2016/11/23.
 */

public class HealthThermometerUtil
{
    public final static int UNIT_CELSIUS = 0;
    public final static int UNIT_FAHRENHEIT = 1;

    private final static int HIDE_MSB_8BITS_OUT_OF_32BITS = 0x00FFFFFF;
    private final static int HIDE_MSB_8BITS_OUT_OF_16BITS = 0x00FF;
    private final static int SHIFT_LEFT_8BITS = 8;
    private final static int SHIFT_LEFT_16BITS = 16;
    private final static int GET_BIT24 = 0x00400000;
    private final static int FIRST_BIT_MASK = 0x01;

    private static HealthThermometerListener listener;

    public static void parseHealthThermometerMeasurement(String address,
                                                         BluetoothGattCharacteristic characteristic)
    {
        byte[] data = characteristic.getValue();

        double temperatureValue;
        byte flag = data[0];
        byte exponential = data[4];
        short firstOctet = convertNegativeByteToPositiveShort(data[1]);
        short secondOctet = convertNegativeByteToPositiveShort(data[2]);
        short thirdOctet = convertNegativeByteToPositiveShort(data[3]);
        int mantissa = ((thirdOctet << SHIFT_LEFT_16BITS) | (secondOctet << SHIFT_LEFT_8BITS) | (firstOctet)) & HIDE_MSB_8BITS_OUT_OF_32BITS;
        mantissa = getTwosComplimentOfNegativeMantissa(mantissa);
        temperatureValue = (mantissa * Math.pow(10, exponential));

        int unit = flag & FIRST_BIT_MASK;
        if (unit != 0)
        {
            temperatureValue = (float) ((temperatureValue - 32) * (5 / 9.0));
        }

        if (listener != null)
        {
            listener.onHealthThermometerValueChanged(temperatureValue, unit);
        }
    }

    private static short convertNegativeByteToPositiveShort(byte octet)
    {
        if (octet < 0)
        {
            return (short) (octet & HIDE_MSB_8BITS_OUT_OF_16BITS);
        }
        else
        {
            return octet;
        }
    }

    private static int getTwosComplimentOfNegativeMantissa(int mantissa)
    {
        if ((mantissa & GET_BIT24) != 0)
        {
            return ((((~mantissa) & HIDE_MSB_8BITS_OUT_OF_32BITS) + 1) * (-1));
        }
        else
        {
            return mantissa;
        }
    }

    public static void setListener(
            HealthThermometerListener htListener)
    {
        listener = null;
        listener = htListener;
    }

    public static void unbind()
    {
        listener = null;
    }

    public interface HealthThermometerListener
    {
        void onHealthThermometerValueChanged(double value, int unit);
    }
}
