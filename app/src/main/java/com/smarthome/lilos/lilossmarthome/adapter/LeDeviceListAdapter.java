package com.smarthome.lilos.lilossmarthome.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.lianluo.lianluoIM.LianluoIM;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.L;
import com.orhanobut.logger.Logger;
import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.bluetooth.BLEService;
import com.smarthome.lilos.lilossmarthome.device.BaseBluetoothDevice;
import com.smarthome.lilos.lilossmarthome.device.BaseDevice;
import com.smarthome.lilos.lilossmarthome.fragment.TabHomeFragment;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceTypeEntity;
import com.smarthome.lilos.lilossmarthome.network.retrofit.WaitingSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.MopsV2Methods;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.login.LoginInterceptor;
import com.smarthome.lilos.lilossmarthome.wigdet.CustomDialog;
import com.smarthome.lilos.lilossmarthome.wigdet.DialogConfirmListener;
import com.smarthome.lilos.lilossmarthome.wigdet.SlideView;

import java.util.ArrayList;
import java.util.Iterator;

import rx.Subscriber;

/**
 * 主界面设备列表适配器
 * 包含有一个监听器,用于刷新列表
 * 包含一个对话框确认监听器,用于修改名称对话框确认时进行具体操作
 * 包含有一个"operation"方法,用于针对不同状态的设备进行删除或者断开连接的操作
 * Created by Kevin on 2016/7/5.
 */
public class LeDeviceListAdapter extends BaseAdapter
{
    private ArrayList<BaseDevice> mLeDevices;
    private LayoutInflater mInflator;
    private Context mContext;
    private String mDeviceAddress;
    private TabHomeFragment.RefreshViewListener mListener;
    private DialogConfirmListener mDialogConfirmListener;
    private String mDeviceName;

    public LeDeviceListAdapter(Context context)
    {
        super();
        mContext = context;
        mLeDevices = new ArrayList<>();
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addDevice(BaseDevice device)
    {
        if (!mLeDevices.contains(device))
        {
            mLeDevices.add(device);
        }
        notifyDataSetChanged();
    }

    public void removeDevice(String address)
    {

        Iterator<BaseDevice> iterator = mLeDevices.iterator();
        while (iterator.hasNext())
        {
            BaseDevice device = iterator.next();
            if (device.getAddress().equals(address))
            {
                iterator.remove();
            }
        }
        notifyDataSetChanged();
    }

    public void removeDevice(BaseDevice device)
    {
        if (mLeDevices.contains(device))
        {
            mLeDevices.remove(device);
        }
    }

    public void setmListener(TabHomeFragment.RefreshViewListener mListener)
    {
        this.mListener = mListener;
    }

    public void setDialogConfirmListener(DialogConfirmListener listener)
    {
        this.mDialogConfirmListener = listener;
    }

    public void clear()
    {
        mLeDevices.clear();
        notifyDataSetChanged();
    }

    public boolean isContained(BaseDevice device)
    {
        for (BaseDevice baseDevice : mLeDevices)
        {
            if (baseDevice.getAddress().equals(device.getAddress()))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getCount()
    {
        return mLeDevices.size();
    }

    @Override
    public BaseDevice getItem(int i)
    {
        return mLeDevices.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return i;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup)
    {
        ViewHolder viewHolder = null;
        SlideView slideView = (SlideView) convertView;

        if (viewHolder == null || slideView == null)
        {
            View itemView = mInflator.inflate(R.layout.listitem_device, null);

            slideView = new SlideView(mContext);
            slideView.setContentView(itemView);

            viewHolder = new ViewHolder();
            ViewHolder.mIvDevice = (ImageView) slideView
                    .findViewById(R.id.iv_device);
            viewHolder.deviceName = (TextView) slideView
                    .findViewById(R.id.device_name);
            viewHolder.connectStatus = (TextView) slideView.findViewById(R.id.tv_connect_status);
            viewHolder.isDelete = (ImageView) slideView.findViewById(R.id.device_delete);
            //viewHolder.isDelete = (CheckBox) slideView.findViewById(R.id.delete_checkbox);
            viewHolder.ic_next = (ImageView) slideView.findViewById(R.id.ic_next);

            viewHolder.rename = (TextView) slideView.findViewById(R.id.tv_rename);
            viewHolder.operation = (TextView) slideView.findViewById(R.id.tv_operation);
            viewHolder.vLine = slideView.findViewById(R.id.v_line);
            slideView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) slideView.getTag();
        }

        final BaseDevice device = mLeDevices.get(i);
        for (DeviceTypeEntity data : SharePreferenceUtils.getTypeList())
        {

            if (device.getTypeId() == data.getId())
            {
                DisplayImageOptions options = new DisplayImageOptions.Builder()
                        .showImageOnLoading(R.drawable.load_error)
                        .showImageOnFail(R.drawable.load_error)
                        .cacheInMemory(true)
                        .cacheOnDisk(true)
                        .build();
                ImageLoader.getInstance().displayImage(data.getIcon(), viewHolder.mIvDevice, options);
//                Log.e("列表数据  ","  device "+device.getTypeId() +" date  "+data.getId()+" data.getIcon()"+data.getIcon());
            }
        }

        final String deviceName = device.getName();
        if (deviceName != null && deviceName.length() > 0)
        {
            viewHolder.deviceName.setText(deviceName);
        }
        else
        {
            viewHolder.deviceName.setText(R.string.unknown_device);
        }
        if(mLeDevices.get(i).getIs_demo() == 1){
            viewHolder.ic_next.setVisibility(View.VISIBLE);
            viewHolder.connectStatus.setVisibility(View.GONE);
        }else {
            switch (device.getIsconnectd())
            {
                case BLEService.STATE_DISCONNECTED:
                    viewHolder.connectStatus.setText(R.string.disconnected);
                    slideView.setOperationText(mContext.getString(R.string.delete));
                    break;
                case BLEService.STATE_CONNECTING:
                    viewHolder.connectStatus.setText(R.string.connecting);
                    break;
                case BLEService.STATE_CONNECTED:
                    viewHolder.connectStatus.setText(R.string.connected);
                    slideView.setOperationText(mContext.getString(R.string.disconnect));
                    break;
                default:
                    break;
            }
        }
        viewHolder.operation.setOnClickListener(v -> operation(mLeDevices.get(i)));
        viewHolder.rename.setOnClickListener(v -> {
            if (mLeDevices.get(i) == null)
            {
                return;
            }

            mDeviceName = mLeDevices.get(i).getName();
            mDialogConfirmListener.postDeviceInfo(mLeDevices.get(i));
            postDialog(mDeviceName);
        });
        if(TabHomeFragment.editMode) {
            viewHolder.isDelete.setVisibility(View.VISIBLE);
        }else{
            viewHolder.isDelete.setVisibility(View.GONE);
        }
        if (i == getCount() - 1)
        {
            viewHolder.vLine.setVisibility(View.GONE);
        }
        else
        {
            viewHolder.vLine.setVisibility(View.VISIBLE);
        }

        return slideView;
    }

    public void updateStatus(BaseDevice device){
        Log.d("===删除设备===1","updateStatus" );

        if ((device instanceof BaseBluetoothDevice) && device.getIsconnectd() == BLEService.STATE_CONNECTED)
        {
            Log.d("===删除设备====2","-deleteDevice-"+"删除设备" );

            ((BaseBluetoothDevice) device).setAutoConnect(false);
            device.stopConnect();
            Log.e("put----updateStatus","  :");
            SmartHomeApplication.myDeviceManager.updateData(device.getAddress(), device);
            mDeviceAddress = device.getAddress();
            if(SharePreferenceUtils.getLoginStatus()){
                deleteDevice(device.getAddress());
            }
            //deleteDevice(device.getAddress());
        } else {
            Log.d("===删除设备====3","-deleteDevice-"+"删除设备" );
            mDeviceAddress = device.getAddress();
            deleteDevice(device.getAddress());
        }
/*        else if((device instanceof BaseBluetoothDevice) && device.getIsconnectd() == BLEService.STATE_DISCONNECTED) {
            Log.e("删除设备====3","-deleteDevice-"+"删除设备" );
            mDeviceAddress = device.getAddress();
            deleteDevice(device.getAddress());
        }*/
    }

    public void operation(BaseDevice device)
    {

        if ((device instanceof BaseBluetoothDevice) && device.getIsconnectd() == BLEService.STATE_CONNECTED)
        {
            ((BaseBluetoothDevice) device).setAutoConnect(false);
            Logger.v("设置设备" + device.getAddress() + "(" + device.getName() + ")为非自动连接", device);
            device.stopConnect();
            Logger.v("尝试与设备" + device.getAddress() + "(" + device.getName() + ")断开连接");
            Log.e("put----operation","  :");
            SmartHomeApplication.myDeviceManager.updateData(device.getAddress(), device);
            Logger.v("更新蓝牙服务中的信息");
        }
        else
        {
            mDeviceAddress = device.getAddress();
            deleteDevice(device.getAddress());
        }
    }

    public void postDialog(String name)
    {
        CustomDialog.Builder builder = new CustomDialog.Builder(mContext);
        builder.setTitle(R.string.please_named_your_device);
        builder.setMessage(name);
        builder.setConfirmListener(mDialogConfirmListener);
        builder.setPositiveButton(R.string.ok, (dialog, which) -> {
        });
        builder.setNegativeButton(R.string.cancel, (dialog, which) -> {
        });
        builder.create().show();
    }

    public void deleteDevice(String devId)
    {
        Log.d("删除设备====","-deleteDevice-"+"删除设备" + devId);
        if(!SharePreferenceUtils.getLoginStatus()){
            onDeleteDeviceResult();
        }else {
            Subscriber<String> subscriber = new WaitingSubscriber<String>(mContext)
            {
                @Override
                public void onCompleted() {
                    super.onCompleted();
                }

                @Override
                public void onNext(String s)
                {
                    onDeleteDeviceResult();
                }

                @Override
                public void onError(Throwable e) {
                    super.onError(e);
                    Log.e("===",e.getMessage());
                }
            };
            MopsV2Methods.getInstance().deleteDevice(subscriber,devId);
        }

    }

    private void onDeleteDeviceResult()
    {
        Log.e("===","delete--"+mLeDevices.size());
        SmartHomeApplication.myDeviceManager.removeData(mDeviceAddress);
        LianluoIM.delDevice(mDeviceAddress);
        //更新状态
        mListener.refresh();

    }

    static class ViewHolder
    {
        static ImageView mIvDevice;
        TextView deviceName;
        TextView connectStatus;
        TextView rename;
        TextView operation;
        View vLine;

        //CheckBox isDelete;
        ImageView isDelete;
        ImageView ic_next;
    }
}
