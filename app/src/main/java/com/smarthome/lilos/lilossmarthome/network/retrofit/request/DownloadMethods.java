package com.smarthome.lilos.lilossmarthome.network.retrofit.request;

import com.smarthome.lilos.lilossmarthome.utils.WriteFileManager;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * 下载对应网络操作
 * Created by Joker on 2017/1/20.
 */

public class DownloadMethods
{
    private static final int DEFAULT_TIMEOUT = 10;

    private Retrofit retrofit;
    private IDownloadService service;

    private DownloadMethods(){}

    public static DownloadMethods getInstance()
    {
        return SingletonHolder.INSTANCE;
    }

    public void init()
    {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);

        retrofit = new Retrofit.Builder()
                .client(client.build())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl("http://www.baidu.com/")
                .build();
        service = retrofit.create(IDownloadService.class);
    }

    private static class SingletonHolder
    {
        private static final DownloadMethods INSTANCE = new DownloadMethods();
    }

    public void download(Subscriber<Boolean> subscriber,String url, String path)
    {
        service.downloadFile(url)
                .subscribeOn(Schedulers.io())
                .map(responseBody -> WriteFileManager.writeResponseBodyToDisk(responseBody,path))
                .subscribeOn(Schedulers.io())
                .subscribe(subscriber);
    }
}
