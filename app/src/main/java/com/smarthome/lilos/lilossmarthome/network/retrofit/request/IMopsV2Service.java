package com.smarthome.lilos.lilossmarthome.network.retrofit.request;

import com.smarthome.lilos.lilossmarthome.network.bean.mops.AddDeviceEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.AppVersionEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceFirmwareEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.FeedbackEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.NewestActivityEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.UpdateDeviceEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceSettingsEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceTypeDetailEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceTypeEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.TokenEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.UserDeviceEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.synchro.SynchroData;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by Joker on 2016/12/30.
 */

public interface IMopsV2Service
{
    @POST("users/login")
    @Headers({"Content-Type:application/json", "Accept:application/json"})
    Observable<TokenEntity> login(@Body RequestBody body);

    @POST("users/logout")
    @Headers({"Content-Type:application/json", "Accept:application/json"})
    Observable<String> logout(@Header("Authorization") String token, @Body RequestBody body);

    @GET("user-devices")
    Observable<List<UserDeviceEntity>> devices(@Header("Authorization") String token);

    @POST("user-devices")
    @Headers({"Content-Type:application/json", "Accept:application/json"})
    Observable<AddDeviceEntity> addDevice(@Header("Authorization") String token,
                                          @Body RequestBody body);

    @PUT("user-devices/{device_id}")
    @Headers({"Content-Type:application/json", "Accept:application/json"})
    Observable<UpdateDeviceEntity> updateDevice(@Header("Authorization") String token,
                                                @Path("device_id") String device_id,
                                                @Body RequestBody body);

    @DELETE("user-devices/{device_id}")
    Observable<String> deleteDevice(@Header("Authorization") String token,
                                    @Path("device_id") String device_id);

    @GET("device-types")
    Observable<List<DeviceTypeEntity>> types();

    @GET("device-types/{id}")
    Observable<DeviceTypeDetailEntity> typeDetail(@Path("id") String id);

    @POST("user-device-settings")
    @Headers({"Content-Type:application/json", "Accept:application/json"})
    Observable<DeviceSettingsEntity> setSettings(@Header("Authorization") String token,
                                                 @Body RequestBody Body);

    @GET("user-device-settings/{device_id}/{key}")
    Observable<DeviceSettingsEntity> getSetting(@Header("Authorization") String token,
                                                @Path("device_id") String device_id,
                                                @Path("key") String key);

    @GET("rom-releases")
    Observable<List<DeviceFirmwareEntity>> firmwares();

    @GET("rom-releases/{device_type_id}")
    Observable<DeviceFirmwareEntity> firmware(@Path("device_type_id") String device_type_id);

    @GET("app-releases/latest")
    Observable<AppVersionEntity> appVersion();

    @POST("feedbacks")
    @Headers({"Content-Type:application/json", "Accept:application/json"})
    Observable<FeedbackEntity> feedback(@Body RequestBody body);

    @GET("activities/latest")
    Observable<NewestActivityEntity> activities();

    @PUT("user-devices")
    @Headers({"Content-Type:application/json", "Accept:application/json"})
    Observable<List<UserDeviceEntity>> synchroDevices(@Header("Authorization") String token,
                                                 @Body RequestBody body);
}
