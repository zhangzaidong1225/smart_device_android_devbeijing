package com.smarthome.lilos.lilossmarthome.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.adapter.abslistview.CommonAdapter;
import com.smarthome.lilos.lilossmarthome.adapter.abslistview.ViewHolder;

import java.util.List;

/**
 * Created by louis on 2017/4/17.
 */

public class SearchLocationAdapter extends CommonAdapter<String> {

    private String mSelectedStr;
    public SearchLocationAdapter(Context context,
                           List<String> mDatas,
                           int itemLayoutId,
                           DisplayImageOptions displayImageOptions)
    {
        super(context, mDatas, itemLayoutId, displayImageOptions);
    }

    public SearchLocationAdapter(Context context,
                           List<String> mDatas,
                           int itemLayoutId)
    {
        super(context, mDatas, itemLayoutId);
    }
    @Override
    public void convert(ViewHolder holder, String item, int position) {

        SpannableStringBuilder builder = new SpannableStringBuilder(item);
        ForegroundColorSpan unSelectedSpanBefore = new ForegroundColorSpan(Color.BLACK);
        ForegroundColorSpan selectedSpan = new ForegroundColorSpan(Color.rgb(0x00, 0x7E, 0xFE));
        ForegroundColorSpan unSelectedSpanAfter = new ForegroundColorSpan(Color.BLACK);
        if (mSelectedStr == null) {
            builder.setSpan(unSelectedSpanBefore, 0, item.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            int selectedStrPos = item.indexOf(mSelectedStr);
            Log.d("SearchLocationAdapter", item + String.valueOf(selectedStrPos));
            builder.setSpan(unSelectedSpanBefore, 0, selectedStrPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.setSpan(selectedSpan, selectedStrPos, selectedStrPos + mSelectedStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.setSpan(unSelectedSpanAfter, selectedStrPos + mSelectedStr.length(), item.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        holder.setText(R.id.tv_location, builder);
    }

    public void setSelectedStr(String selectedStr) {
        this.mSelectedStr = selectedStr;
    }
}
