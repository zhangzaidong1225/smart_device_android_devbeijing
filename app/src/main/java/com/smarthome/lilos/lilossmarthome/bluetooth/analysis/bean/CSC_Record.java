package com.smarthome.lilos.lilossmarthome.bluetooth.analysis.bean;

/**
 * Created by Joker on 2016/11/22.
 */

public class CSC_Record
{
    private int firstWheelRevolutions = -1;
    private int lastWheelRevolutions = -1;
    private int lastWheelEventTime = -1;
    private float wheelCadence = -1;
    private int lastCrankRevolutions = -1;
    private int lastCrankEventTime = -1;

    public CSC_Record()
    {
        firstWheelRevolutions = -1;
        lastWheelRevolutions = -1;
        lastWheelEventTime = -1;
        wheelCadence = -1;
        lastCrankRevolutions = -1;
        lastCrankEventTime = -1;
    }

    public CSC_Record(int firstWheelRevolutions, int lastWheelRevolutions, int lastWheelEventTime,
                      float wheelCadence, int lastCrankRevolutions, int lastCrankEventTime)
    {

        this.firstWheelRevolutions = firstWheelRevolutions;
        this.lastWheelRevolutions = lastWheelRevolutions;
        this.lastWheelEventTime = lastWheelEventTime;
        this.wheelCadence = wheelCadence;
        this.lastCrankRevolutions = lastCrankRevolutions;
        this.lastCrankEventTime = lastCrankEventTime;
    }

    public int getFirstWheelRevolutions()
    {

        return firstWheelRevolutions;
    }

    public void setFirstWheelRevolutions(int firstWheelRevolutions)
    {
        this.firstWheelRevolutions = firstWheelRevolutions;
    }

    public int getLastWheelRevolutions()
    {
        return lastWheelRevolutions;
    }

    public void setLastWheelRevolutions(int lastWheelRevolutions)
    {
        this.lastWheelRevolutions = lastWheelRevolutions;
    }

    public int getLastWheelEventTime()
    {
        return lastWheelEventTime;
    }

    public void setLastWheelEventTime(int lastWheelEventTime)
    {
        this.lastWheelEventTime = lastWheelEventTime;
    }

    public float getWheelCadence()
    {
        return wheelCadence;
    }

    public void setWheelCadence(float wheelCadence)
    {
        this.wheelCadence = wheelCadence;
    }

    public int getLastCrankRevolutions()
    {
        return lastCrankRevolutions;
    }

    public void setLastCrankRevolutions(int lastCrankRevolutions)
    {
        this.lastCrankRevolutions = lastCrankRevolutions;
    }

    public int getLastCrankEventTime()
    {
        return lastCrankEventTime;
    }

    public void setLastCrankEventTime(int lastCrankEventTime)
    {
        this.lastCrankEventTime = lastCrankEventTime;
    }

    @Override
    public String toString()
    {
        return "CSC_Record{" +
                "firstWheelRevolutions=" + firstWheelRevolutions +
                ", lastWheelRevolutions=" + lastWheelRevolutions +
                ", lastWheelEventTime=" + lastWheelEventTime +
                ", wheelCadence=" + wheelCadence +
                ", lastCrankRevolutions=" + lastCrankRevolutions +
                ", lastCrankEventTime=" + lastCrankEventTime +
                '}';
    }
}
