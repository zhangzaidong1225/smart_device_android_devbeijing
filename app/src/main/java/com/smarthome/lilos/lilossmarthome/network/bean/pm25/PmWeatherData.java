package com.smarthome.lilos.lilossmarthome.network.bean.pm25;

/**
 * Created by Administrator on 2017/5/9.
 */

public class PmWeatherData {

    private String text;
    private String code;
    private String temperature;
    private String feels_like;
    private String humidity;
    private String wind_direction;
    private String wind_scale;
    private String temperature_high;
    private String temperature_low;

    public String getTemperature_low() {
        return temperature_low;
    }

    public void setTemperature_low(String temperature_low) {
        this.temperature_low = temperature_low;
    }

    public String getTemperature_high() {

        return temperature_high;
    }

    public void setTemperature_high(String temperature_high) {
        this.temperature_high = temperature_high;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getFeels_like() {
        return feels_like;
    }

    public void setFeels_like(String feels_like) {
        this.feels_like = feels_like;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getWind_direction() {
        return wind_direction;
    }

    public void setWind_direction(String wind_direction) {
        this.wind_direction = wind_direction;
    }

    public String getWind_scale() {
        return wind_scale;
    }

    public void setWind_scale(String wind_scale) {
        this.wind_scale = wind_scale;
    }

    @Override
    public String toString() {
        return "PmWeatherData{" +
                "text='" + text + '\'' +
                ", code='" + code + '\'' +
                ", temperature='" + temperature + '\'' +
                ", feels_like='" + feels_like + '\'' +
                ", humidity='" + humidity + '\'' +
                ", wind_direction='" + wind_direction + '\'' +
                ", wind_scale='" + wind_scale + '\'' +
                ", temperature_high='" + temperature_high + '\'' +
                ", temperature_low='" + temperature_low + '\'' +
                '}';
    }
}
