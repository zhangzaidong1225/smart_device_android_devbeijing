package com.smarthome.lilos.lilossmarthome.network.retrofit.request;

import com.smarthome.lilos.lilossmarthome.network.bean.FirstPageEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.location.CityData;
import com.smarthome.lilos.lilossmarthome.network.bean.location.CityListEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceSettingsEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.pm25.PMDayData;
import com.smarthome.lilos.lilossmarthome.network.bean.pm25.WeatherData;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface FirstPageService
{

    @GET("weather/aqi")
    Observable<FirstPageEntity> getFirst();
    @GET("weather/index")
    Observable<WeatherData> getPMData(@Query("location") String location);
    @GET("areas")
    Observable<CityListEntity> searchCity(@Query("search") String location);
}
