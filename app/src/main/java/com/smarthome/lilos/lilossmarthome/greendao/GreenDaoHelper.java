package com.smarthome.lilos.lilossmarthome.greendao;

import android.database.sqlite.SQLiteDatabase;

import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.greendao.gen.DaoMaster;
import com.smarthome.lilos.lilossmarthome.greendao.gen.DaoSession;
import com.smarthome.lilos.lilossmarthome.greendao.upgrade.GreenDaoOpenHelper;

/**
 * Created by louis on 2017/4/13.
 */

public class GreenDaoHelper {

    public static final String DB_VERSION = "1.3";

    private static GreenDaoOpenHelper mHelper;
    private static SQLiteDatabase db;
    private static DaoMaster mDaoMaster;
    private static DaoSession mDaoSession;

    private static GreenDaoOpenHelper mLocationHelper;
    private static SQLiteDatabase locationDb;
    private static DaoMaster mLocationDaoMaster;
    private static DaoSession mLocationDaoSession;

    /**
     * 初始化greenDao，这个操作建议在Application初始化的时候添加；
     */
    public static void initDatabase() {

        mHelper = new GreenDaoOpenHelper(SmartHomeApplication.mContext, "lianluo-db", null);
        db = mHelper.getWritableDatabase();
        mDaoMaster = new DaoMaster(db);
        mDaoSession = mDaoMaster.newSession();
    }

    public static void initLocationDatabase() {
        mLocationHelper = new GreenDaoOpenHelper(SmartHomeApplication.mContext, "location-db", null);
        locationDb = mLocationHelper.getWritableDatabase();
        mLocationDaoMaster = new DaoMaster(locationDb);
        mLocationDaoSession = mLocationDaoMaster.newSession();
    }
    public static DaoSession getDaoSession() {
        return mDaoSession;
    }
    public static DaoSession getLocationDaoSession() {
        return mLocationDaoSession;
    }
    public static SQLiteDatabase getDb() {
        return db;
    }
    public static SQLiteDatabase getLocationDb() {
        return locationDb;
    }
}
