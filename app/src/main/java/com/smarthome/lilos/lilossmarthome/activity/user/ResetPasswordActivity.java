package com.smarthome.lilos.lilossmarthome.activity.user;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.body.ModPwdBody;
import com.smarthome.lilos.lilossmarthome.network.retrofit.WaitingSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.UserCenterMethods;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.utils.RegexpUtils;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.wigdet.WaitingDialog;
import com.umeng.analytics.MobclickAgent;

import rx.Subscriber;

/**
 * 重置密码页
 * 用于在登录情况下修改密码
 */
public class ResetPasswordActivity extends BaseActivity
{
    private ImageView mIvBack;
    private TextView mTvAccount;
    private EditText[] mEtPsws = new EditText[3];
    private ImageView[] mIvClears = new ImageView[3];
    private Button mBtnCommit;

    private String mAccount;
    private String mPswOld;
    private String mPsw;
    private String mPswRe;

    private WaitingDialog waitingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        init();
        getData();
    }

    private void init()
    {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvAccount = (TextView) findViewById(R.id.tv_account);
        mEtPsws[0] = (EditText) findViewById(R.id.et_password_old);
        mEtPsws[1] = (EditText) findViewById(R.id.et_password);
        mEtPsws[2] = (EditText) findViewById(R.id.et_password_re);
        mIvClears[0] = (ImageView) findViewById(R.id.iv_clear_psw_old);
        mIvClears[1] = (ImageView) findViewById(R.id.iv_clear_psw);
        mIvClears[2] = (ImageView) findViewById(R.id.iv_clear_psw_re);
        mBtnCommit = (Button) findViewById(R.id.btn_commit);

        for (int i = 0; i < 3; i++)
        {
            mEtPsws[i].setTypeface(Typeface.DEFAULT);
            mEtPsws[i].setTransformationMethod(new PasswordTransformationMethod());
        }

        mIvBack.setOnClickListener(v -> {
            MobclickAgent.onEvent(this, "ResetPasswordActivity_back");
            finish();
        });
        for (int i = 0; i < 3; i++)
        {
            int finalI = i;
            mIvClears[i].setOnClickListener(v -> clearPsw(finalI));
        }
        mBtnCommit.setOnClickListener(v -> rePass());

        mEtPsws[2].setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE)
            {
                rePass();
                return true;
            }
            return false;
        });

        waitingDialog = new WaitingDialog(this);
    }

    private void getData()
    {
        mAccount = SharePreferenceUtils.getMobileNum();
        mAccount = SharePreferenceUtils.getMobileNum();
        String enmobile = mAccount.substring(0, 3) + "****" + mAccount.substring(7);
        mTvAccount.setText(enmobile);
    }

    private void clearPsw(int index)
    {
        MobclickAgent.onEvent(this, "ResetPasswordActivity_clearPsw");
        mEtPsws[index].setText("");
    }

    private void rePass()
    {
        MobclickAgent.onEvent(this, "ResetPasswordActivity_rePass");
        mPswOld = mEtPsws[0].getText().toString();
        mPsw = mEtPsws[1].getText().toString();
        mPswRe = mEtPsws[2].getText().toString();

        if (mPswOld == null || mPsw == null || mPswRe == null || mPswOld.equals("") || mPsw.equals("") || mPswRe.equals(""))
        {
            AppToast.showToast(R.string.info_unfilled);
            return;
        }

        if (!mPsw.equals(mPswRe))
        {
            AppToast.showToast(R.string.psw_not_equal);
            return;
        }

        if (mPsw.length() <= 5)
        {
            AppToast.showToast(R.string.psw_unfilled);
            return;
        }

        if (mPswOld.equals(mPsw))
        {
            AppToast.showToast(R.string.psw_equal);
            return;
        }

        if (!RegexpUtils.isMixPassword(mPsw))
        {
            AppToast.showToast(R.string.password_must_mix);
            return;
        }

        Subscriber<ModPwdBody> subscriber = new WaitingSubscriber<ModPwdBody>(ResetPasswordActivity.this)
        {
            @Override
            public void onNext(ModPwdBody modPwdBody)
            {
                AppToast.showToast(R.string.modify_password_success);
                Log.e("test","*****");
                RxBus.getInstance().send(Events.LOG_OUT_REPAS,null);
                finish();
            }
        };
        ModPwdBody body = new ModPwdBody(mAccount, mPsw, mPswOld);
        UserCenterMethods.getInstance().modPassword(subscriber, body);
    }
}
