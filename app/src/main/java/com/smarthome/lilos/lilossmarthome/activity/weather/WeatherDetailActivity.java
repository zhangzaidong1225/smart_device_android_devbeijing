package com.smarthome.lilos.lilossmarthome.activity.weather;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.format.Time;
import android.text.style.AbsoluteSizeSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.fragment.TabHomeFragment;
import com.smarthome.lilos.lilossmarthome.network.bean.pm25.PMDayData;
import com.smarthome.lilos.lilossmarthome.network.bean.pm25.PMHoursData;
import com.smarthome.lilos.lilossmarthome.network.bean.pm25.PmAirData;
import com.smarthome.lilos.lilossmarthome.network.bean.pm25.PmWeatherData;
import com.smarthome.lilos.lilossmarthome.network.bean.pm25.PmWeatherTimeData;
import com.smarthome.lilos.lilossmarthome.network.bean.pm25.WeatherData;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.FirstPageMethods;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.wigdet.WaitingDialog;
import com.umeng.analytics.MobclickAgent;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import louis.cyclewheelviewlib.CycleWheelView;
import rx.Subscriber;
import sunrain.linechartlib.LineChartView;

import static java.util.Arrays.asList;

/**
 * Created by louis on 2017/4/10.
 */

public class WeatherDetailActivity extends BaseActivity {

    private CycleWheelView mCycleWheelView;
    private LineChartView mLineChartView;

    private static final int todayMsg = 101;
    private static final int tomorrowMsg = 102;

    private TextView tv_today;
    private TextView tv_tomorrow;
    private TextView tv_lasted;
    private ImageView ic_selected;
    private ImageView ic_back;
    private TextView tv_weather_value;
    private TextView tv_weather_state;
    private TextView tv_location;


    private TextView tv_max_temp, tv_min_temp, tv_rain, tv_pm25, tv_humidity, tv_wind, tv_wind_direction, tv_sport;

    float start_position = 0.0f;
    private int screen_width, screen_hight;
    private float density;
    private Map<Integer, PMHoursData> todayListData, tomorrowListData, lastedListData;

    private PmAirData todayAirData;

    private int width;
    private int now;
    private String location, areaID;
    private SpannableStringBuilder spannableString;
    int widgth = 0;
    String hour = "";
    //x轴坐标对应的数据
    List<String> xValue = new LinkedList<>();
    List<String> xString = new LinkedList<>();
    //y轴坐标对应的数据
    List<Integer> yValue = new LinkedList<>();
    //折线对应的数据
    Map<String, Integer> value = new HashMap<>();
    //折现y值对应的文字描述
    Map<String, String> yDegreemap = new HashMap<>();

    List<Integer> yDegreeValue = new ArrayList<>(asList(500, 350, 100, 0));
    List<Integer> pointColor = new ArrayList<>(asList(Color.RED, Color.YELLOW, Color.GREEN));

    private WaitingDialog waitingDialog;
    List<Integer> hourList = new ArrayList<>();
    int yinitValue = 50;

    private WeatherHandler mHandler = new WeatherHandler(this);

    class WeatherHandler extends Handler {
        private WeakReference<Context> reference;

        public WeatherHandler(Context context) {
            reference = new WeakReference<Context>(context);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            WeatherDetailActivity activity = (WeatherDetailActivity) reference.get();

            if (activity != null) {
                switch (msg.what) {
                    case todayMsg:

                        xString.clear();
                        xValue.clear();
                        yDegreemap.clear();

                        setLineChartView(todayListData, true);
                        break;
                    case tomorrowMsg:
                        tv_location.setText(location);
                        break;
                }
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_detail);

        todayListData = new HashMap<>();
        tomorrowListData = new HashMap<>();
        todayListData = new HashMap<>();
        getScreenWH();
        initView();
//        initRxBus();
        location = getIntent().getStringExtra("location");
        areaID = getIntent().getStringExtra("areaId");
        waitingDialog = new WaitingDialog(this);
        initData(location, areaID);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
        mHandler = null;
    }

    private void initData(String location, String areaID) {
        mHandler.sendEmptyMessage(tomorrowMsg);

        if (areaID == null || areaID.equals("")) {
            getPMData(location);
        } else {
            getPMData(areaID);
        }
        waitingDialog.show();
    }

    private void setTvString(String mTv_weather_value, String unitString) {

        spannableString.append(mTv_weather_value);
        AbsoluteSizeSpan absoluteSizeSpan = new AbsoluteSizeSpan(64);
        int selectedStrPos = mTv_weather_value.indexOf(unitString);
        if (selectedStrPos == -1) {
            selectedStrPos = mTv_weather_value.length();
        }
        spannableString.setSpan(absoluteSizeSpan, 0, selectedStrPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv_weather_value.setText(spannableString);
        spannableString.clear();
    }

    private void initView() {

        tv_max_temp = (TextView) findViewById(R.id.tv_max_temperature);
        tv_min_temp = (TextView) findViewById(R.id.tv_min_temperature);
        tv_rain = (TextView) findViewById(R.id.tv_rain);
        tv_pm25 = (TextView) findViewById(R.id.tv_pm_num);
        tv_humidity = (TextView) findViewById(R.id.tv_air_humidity);
        tv_wind = (TextView) findViewById(R.id.tv_air_wind);
        tv_wind_direction = (TextView) findViewById(R.id.tv_air_wind_define);


        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        width = wm.getDefaultDisplay().getWidth();
        mCycleWheelView = (CycleWheelView) findViewById(R.id.cycle_wheel_view);
        initCycleWheelView();

        spannableString = new SpannableStringBuilder();


        mCycleWheelView.setOnWheelItemSelectedListener(new CycleWheelView.WheelItemSelectedListener() {
            @Override
            public void onItemSelected(int position, String label) {
                MobclickAgent.onEvent(WeatherDetailActivity.this, "WeatherDetailActivity_mCycleWheelView");
                switch (label) {
                    case "AQI":
                        if (todayAirData != null) {
                            setTvString(todayAirData.getAqi(), " μg/m³");
                            tv_weather_state.setText(R.string.AQI);
                        }
                        break;
                    case "CO":
                        if (todayAirData != null) {
                            setTvString(Float.parseFloat(todayAirData.getCo()) * 1000 + " μg/m³", " μg/m³");
                            tv_weather_state.setText(R.string.CO);
                        }
                        break;
                    case "NO²":
                        if (todayAirData != null) {
                            setTvString(todayAirData.getNo2() + " μg/m³", " μg/m³");
                            tv_weather_state.setText(R.string.NO2);
                        }
                        break;
                    case "PM2.5":
                        if (todayAirData != null) {
                            setTvString(todayAirData.getPm25() + " μg/m³", " μg/m³");
                            tv_weather_state.setText(R.string.pm2_5);
                        }

                        break;
                    case "PM10":
                        if (todayAirData != null) {
                            setTvString(todayAirData.getPm10() + " μg/m³", " μg/m³");
                            tv_weather_state.setText(R.string.pm10);
                        }

                        break;
                    case "SO²":
                        if (todayAirData != null) {
                            setTvString(todayAirData.getSo2() + " μg/m³", " μg/m³");
                            tv_weather_state.setText(R.string.SO2);
                        }

                        break;
                    case "O³":
                        if (todayAirData != null) {
                            setTvString(todayAirData.getO3() + " μg/m³", " μg/m³");
                            //tv_weather_value.setText(todayAirData.getO3()+"μg/m3");
                            tv_weather_state.setText(R.string.O3);
                        }

                        break;
                }
            }
        });


        mLineChartView = (LineChartView) findViewById(R.id.weather_line_chart_view);
        //必须初始化
        initLineChartView();

        tv_today = (TextView) findViewById(R.id.tv_date_today);
        tv_today.setTextColor(getResources().getColor(R.color.tv_date_color));

        tv_tomorrow = (TextView) findViewById(R.id.tv_date_tomorrow);
        tv_lasted = (TextView) findViewById(R.id.tv_date_after_tomorrow);
        ic_selected = (ImageView) findViewById(R.id.iv_selected);

        ic_back = (ImageView) findViewById(R.id.iv_back);
        tv_sport = (TextView) findViewById(R.id.tv_sport);
        ic_back.setOnClickListener(v -> {
            MobclickAgent.onEvent(this, "WeatherDetailActivity_back");
            finish();
        });

        tv_weather_value = (TextView) findViewById(R.id.tv_weather_value);
        tv_weather_state = (TextView) findViewById(R.id.tv_weather_state);
        tv_location = (TextView) findViewById(R.id.tv_title);
        if (location != null) {
            tv_location.setText(location);
        }

        tv_today.setOnClickListener(v -> {
            MobclickAgent.onEvent(this, "WeatherDetailActivity_tv_today");
            if (start_position != Dp2Px(0.0f)) {
                move(ic_selected, start_position, Dp2Px(0.0f), todayListData, true);
                start_position = Dp2Px(0.0f);
                tv_today.setTextColor(getResources().getColor(R.color.tv_date_color));
                tv_tomorrow.setTextColor(getResources().getColor(R.color.white));
                tv_lasted.setTextColor(getResources().getColor(R.color.white));


            }
        });

        tv_tomorrow.setOnClickListener(v -> {
            MobclickAgent.onEvent(this, "WeatherDetailActivity_tv_tomorrow");
            if (start_position != screen_width / 2 - Dp2Px(85.0f)) {

                move(ic_selected, start_position, screen_width / 2 - Dp2Px(85.0f), tomorrowListData, false);
                start_position = screen_width / 2 - Dp2Px(85.0f);
                tv_tomorrow.setTextColor(getResources().getColor(R.color.tv_date_color));
                tv_today.setTextColor(getResources().getColor(R.color.white));
                tv_lasted.setTextColor(getResources().getColor(R.color.white));

            }
        });

        tv_lasted.setOnClickListener(v -> {
            MobclickAgent.onEvent(this, "WeatherDetailActivity_tv_lasted");
            if (start_position != screen_width-Dp2Px(170.0f)) {
                move(ic_selected, start_position, screen_width-Dp2Px(170.0f), lastedListData, false);

                start_position = screen_width-Dp2Px(170.0f);
                tv_lasted.setTextColor(getResources().getColor(R.color.tv_date_color));
                tv_today.setTextColor(getResources().getColor(R.color.white));
                tv_tomorrow.setTextColor(getResources().getColor(R.color.white));


            }
        });

    }
    public int Dp2Px( float dp) {
        final float scale = this.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    public int Px2Dp( float px) {
        final float scale = this.getResources().getDisplayMetrics().density;
        return (int) (px / scale + 0.5f);
    }
    public void move(final ImageView view, float start, float end, Map<Integer, PMHoursData> pmDayData, boolean today) {
        ValueAnimator va = ValueAnimator.ofFloat(start, end);
        va.setDuration(300);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                view.setTranslationX((float) animation.getAnimatedValue());
                view.invalidate();
            }

        });
        va.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

                xString.clear();
                xValue.clear();
                yDegreemap.clear();


                setLineChartView(pmDayData, today);

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        va.setRepeatCount(0);
        va.start();
    }

    private void initCycleWheelView() {
        List<String> labels2 = new ArrayList<>();
        labels2.add("AQI");
        labels2.add("CO");
        labels2.add("NO²");
        labels2.add("PM2.5");
        labels2.add("PM10");
        labels2.add("SO²");
        labels2.add("O³");

        mCycleWheelView.setLabels(labels2);
        try {
            mCycleWheelView.setWheelSize(7);
        } catch (CycleWheelView.CycleWheelViewException e) {
            e.printStackTrace();
        }

        mCycleWheelView.setWheelItemLayout(R.layout.item_cycle_wheel_view, R.id.tv_label_item_wheel);

        mCycleWheelView.setCycleEnable(false);
        mCycleWheelView.setSelection(3);
        mCycleWheelView.setAlphaGradual(1.0f);
        mCycleWheelView.setDivider(Color.TRANSPARENT, 2);
        mCycleWheelView.setSolid(Color.TRANSPARENT, Color.TRANSPARENT);
        mCycleWheelView.setLabelColor(Color.argb(76, 255, 255, 255));
        mCycleWheelView.setLabelSelectColor(Color.WHITE);
    }

    private void setLineChartView(Map<Integer, PMHoursData> pmDayData, boolean today) {
        if (pmDayData == null) {
            return;
        }
        hourList.clear();
        for (int i = 0; i < 24; i++) {

            PMHoursData hoursData = pmDayData.get(i);
            if (today) {
                if (now == i) {
                    xString.add("现在");
                } else {
                    xString.add(i + "时");
                }
            } else {
                xString.add(i + "时");
            }
            if (hoursData != null) {
                xValue.add(i + "");
                yDegreemap.put(i + "", hoursData.getQuality() + "");
                Log.e("data:", "i:  " + i + "   yString:  " + hoursData.getQuality() + "");
                if (Integer.valueOf(hoursData.getAqi()) > 500) {
                    value.put(i + "", yinitValue + 300);
                } else if (Integer.valueOf(hoursData.getAqi()) >= 350) {
                    value.put(i + "", yinitValue + Integer.valueOf(hoursData.getAqi()) / 3 + 400 / 3);
                } else if (Integer.valueOf(hoursData.getAqi()) >= 100) {
                    value.put(i + "", yinitValue + Integer.valueOf(hoursData.getAqi()) * 4 / 5 - 30);
                } else {
                    value.put(i + "", yinitValue + Integer.valueOf(hoursData.getAqi()) / 2);
                }
            } else {
                xValue.add(i + "");
                yDegreemap.put(i + "", "");
                Log.e("data:", "i:  " + i + "   yString:  ");
                value.put(i + "", 0);
            }
        }
        mLineChartView.setValue(value, xValue, yValue, xString, yDegreeValue, pointColor, yDegreemap);
    }

    private void initLineChartView() {

        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        int width = wm.getDefaultDisplay().getWidth();

        for (int i = 0; i < 24; i++) {
            //横坐标轴显示文字
            if (getCurrentTime() == i) {
                xString.add("现在");
            } else {
                xString.add((i) + "时");
            }
            xValue.add((i) + "");
            //横纵坐标 点
            value.put((i) + "", yinitValue + (int) (Math.random() * 20));//60--240
            yDegreemap.put(i + "", "");
        }

        for (int i = 0; i < 10; i++) {
            //纵坐标轴显示文字
            yValue.add(i * 50);
        }

        mLineChartView.setValue(value, xValue, yValue, xString, yDegreeValue, pointColor, yDegreemap);
        mLineChartView.setInterval(width / 8);
        mHandler.postDelayed(new Runnable() {

            public void run() {
                if (getCurrentTime() > 3) {
                    mLineChartView.setCurrentPosition(getCurrentTime());
                }
            }

        }, 150);
    }

    private int getCurrentTime() {
        Time t = new Time();
        t.setToNow(); // 取得系统时间。
        int hour = t.hour; // 0-23
        now = hour;
        return hour;
    }

    //获取天气数据
    private void getPMData(String location) {

        Log.d("test", "--" + location);

        Subscriber<WeatherData> subscriber = new Subscriber<WeatherData>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.d("WeatherDetailActivity", "请检查网络连接deviceList");
                String weatherData = SharePreferenceUtils.getWeatherData();
                if (!"".equals(weatherData)) {
                    handleWeatherData((WeatherData) JsonUtils.jsonToBean(weatherData, WeatherData.class));
                }
                waitingDialog.dismiss();
            }

            @Override
            public void onNext(WeatherData weatherData) {
                Log.e("test", "" + JsonUtils.objectToJson(weatherData));

                SharePreferenceUtils.setWeatherData(JsonUtils.objectToJson(weatherData));
                handleWeatherData(weatherData);
                mHandler.sendEmptyMessage(todayMsg);
                waitingDialog.dismiss();
            }
        };
        FirstPageMethods.getInstance().getPMData(subscriber, location);
    }

    private void handleWeatherData(WeatherData weatherData) {
        PmWeatherData pmWeatherData = weatherData.getWeather();
        tv_max_temp.setText(pmWeatherData.getTemperature_high());
        tv_min_temp.setText(pmWeatherData.getTemperature_low());
        tv_rain.setText(pmWeatherData.getText());
        tv_humidity.setText(pmWeatherData.getHumidity() + "%");
        tv_wind.setText(pmWeatherData.getWind_scale() + "级");
        tv_wind_direction.setText(pmWeatherData.getWind_direction() + "风");

        List<PmWeatherTimeData> hoursDatas = weatherData.getTimeData();


        todayListData = changeListToMap(hoursDatas.get(0).getTodayData());
        tomorrowListData = changeListToMap(hoursDatas.get(1).getTodayData());
        lastedListData = changeListToMap(hoursDatas.get(2).getTodayData());
        tv_sport.setText(weatherData.getSports().getBrief());

        tv_today.setText("今天");
        String tomorrow = hoursDatas.get(1).getDay();

        tv_tomorrow.setText(tomorrow.replaceAll("-", " / ").substring(6));

        String lasted = hoursDatas.get(2).getDay();
        tv_lasted.setText(lasted.replaceAll("-", " / ").substring(6));
        setTvString(weatherData.getAir().getPm25() + " μg/m³", " μg/m³");
        tv_pm25.setText(weatherData.getLife().getUv().getBrief());
        todayAirData = weatherData.getAir();
    }

    public Map<Integer, PMHoursData> changeListToMap(List<PMHoursData> mlist) {
        Map<Integer, PMHoursData> map = new HashMap<>();
        for (int m = 0; m < mlist.size(); m++) {
            map.put(changeTime(mlist.get(m).getTime()), mlist.get(m));
        }
        return map;
    }

    public int changeTime(String hourStr) {
        widgth = hourStr.indexOf("T");
        hour = hourStr.substring(widgth + 1, widgth + 3);
        return Integer.valueOf(hour);
    }

    private void getScreenWH() {
        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int screenWidth = metrics.widthPixels;//屏幕宽度（像素）
        int screenHight = metrics.heightPixels;//屏幕高度（像素）
        screen_width = screenWidth;
        screen_hight = screenHight;

        Log.d("aa", screen_width + "--" + screen_hight);
        density = metrics.density;//屏幕密度（0.75 / 1.0 / 1.5）
    }
}
