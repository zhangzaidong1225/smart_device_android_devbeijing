package com.smarthome.lilos.lilossmarthome.utils;

import android.util.Log;

import com.google.gson.Gson;
import com.lianluo.lianluoIM.MQTTMessage;

import java.util.HashMap;

/**
 * Created by Tristan on 2016/11/8.
 */

public class MessageUtls
{
    public static String convertMessage(String clientID, String content)
    {
        String outString = "";
        MQTTMessage message = new MQTTMessage();

        message.notification_builder_id = 1;
        message.desc = "";
        message.notification_basic_style = 0x01;
        message.open_type = 0;
        message.title = "测试消息";
        message.url = "";
        message.from = clientID;
        message.custom_content = new HashMap();

//        message.custom_content.put("clientID",clientID);
        message.custom_content = JsonUtils.jsonToMap(content);

        Gson gson = new Gson();
        outString = gson.toJson(message) + "\n";
        Log.d("LianluoIM", "convertMessage: >>>>>>>>>>>组织字符串" + outString);

        return outString;
    }

    ;

}
