package com.smarthome.lilos.lilossmarthome.network.bean.pm25;

import java.util.List;

/**
 * Created by Administrator on 2017/5/9.
 * 天气数据
 */

public class WeatherData {

    private PmCityData city;

    private PmSportData sport;

    private PmAirData air;

    private PmWeatherData weather;

    private List<PmWeatherTimeData> data;
    private LifeData life;

    public LifeData getLife() {
        return life;
    }

    public void setLife(LifeData life) {
        this.life = life;
    }



    public PmCityData getCity() {
        return city;
    }

    public void setCity(PmCityData city) {
        this.city = city;
    }

    public PmSportData getSports() {
        return sport;
    }

    public void setSports(PmSportData sports) {
        this.sport = sports;
    }

    public PmAirData getAir() {
        return air;
    }

    public void setAir(PmAirData air) {
        this.air = air;
    }

    public PmWeatherData getWeather() {
        return weather;
    }

    public void setWeather(PmWeatherData weather) {
        this.weather = weather;
    }

    public List<PmWeatherTimeData> getTimeData() {
        return data;
    }

    public void setTimeData(List<PmWeatherTimeData> timeData) {
        this.data = timeData;
    }

    @Override
    public String toString() {
        return "WeatherData{" +
                "city=" + city +
                ", sports=" + sport +
                ", air=" + air +
                ", weather=" + weather +
                ", timeData=" + data +
                '}';
    }
}
