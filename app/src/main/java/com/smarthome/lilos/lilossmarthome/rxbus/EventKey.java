package com.smarthome.lilos.lilossmarthome.rxbus;

/**
 * RxBus的事件Key类型列表
 * Created by Joker on 2017/1/13.
 */

public class EventKey
{
    public static final String DEVICE_ADDRESS = "Device_Address";

    public final static String EXTRA_DEVICE_NAME = "device_name";
    public final static String EXTRA_DEVICE_ADDR = "device_addr";
    public final static String EXTRA_DEVICE_TYPE = "device_type";
    public final static String EXTRA_CONNECT_STATE = "connect_status";
    public final static String EXTRA_COMPANY_ID = "company_id";
    public final static String EXTRA_DEF_VERSION = "def_version";
    public final static String EXTRA_DEV_DATA = "dev_data";
    public final static String EXTRA_SERVICES = "services";
    public final static String EXTRA_CHARACTERISTICS = "characteristics";

    public final static String MQTT_DEVICE_ID = "dev_id";
    public final static String MQTT_ONLINE_STATUS = "online_status";

    public final static String PM = "pm";
    public final static String LOCATION = "location";
    public final static String AREAID = "areaid";


}
