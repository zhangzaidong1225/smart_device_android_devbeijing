package com.smarthome.lilos.lilossmarthome.activity.user;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Selection;
import android.text.Spannable;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.body.SMSBody;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.body.UserBody;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.entity.RegisterEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.entity.SMSEntity;
import com.smarthome.lilos.lilossmarthome.network.retrofit.WaitingSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.UserCenterMethods;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.utils.CountlyUtil;
import com.smarthome.lilos.lilossmarthome.utils.RegexpUtils;
import com.umeng.analytics.MobclickAgent;

import rx.Subscriber;

/**
 * 注册界面
 * 用于用户注册
 */
public class RegisterActivity extends BaseActivity
{
    private ImageView mIvBack;
    private EditText mEtAccount;
    private EditText mEtVerifyCode;
    private EditText mEtPwd;
    private Button mBtnGetCode;
    private Button mBtnRegister;
    private ImageView mIvClear;
    private ImageView mIvPswSwitch;
    private CheckBox mCbUserProtocol;
    private TextView mTvUserProtocol;

    private String mAccount;
    private String mVerifyCode;
    private String mPwd;

    private boolean isShowPsw = false;
    private boolean isAgree = false;

    private int count = 60;
    private Handler verifyHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case 0:
                    if (count == 0)
                    {
                        mBtnGetCode.setEnabled(true);
                        mBtnGetCode.setText(getResources().getText(R.string.get_code));
                        count = 60;
                    }
                    else
                    {
                        String msgStr = String.format(getResources().getString(R.string.verify_waiting), count);
                        mBtnGetCode.setText(msgStr);
                        verifyHandler.sendEmptyMessageDelayed(0, 1000);
                        count--;
                    }
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        CountlyUtil.recordEvent(CountlyUtil.EVENT.EVENT_RESET_PWD, CountlyUtil.KEY.ACCESS_TIME);
        init();
    }

    private void init()
    {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mEtAccount = (EditText) findViewById(R.id.et_account);
        mEtVerifyCode = (EditText) findViewById(R.id.et_verify_code);
        mEtPwd = (EditText) findViewById(R.id.et_password);
        mBtnGetCode = (Button) findViewById(R.id.btn_get_code);
        mBtnRegister = (Button) findViewById(R.id.btn_register);
        mCbUserProtocol = (CheckBox) findViewById(R.id.cb_user_protocol);
        mIvClear = (ImageView) findViewById(R.id.iv_clear_psw);
        mIvPswSwitch = (ImageView) findViewById(R.id.iv_psw_switch);
        mTvUserProtocol = (TextView) findViewById(R.id.tv_user_protocol);

        mEtPwd.setTypeface(Typeface.DEFAULT);
        mEtPwd.setTransformationMethod(new PasswordTransformationMethod());

        mIvBack.setOnClickListener(v -> {
            MobclickAgent.onEvent(this, "RegisterActivity_back");
            finish();
        });
        mBtnGetCode.setOnClickListener(v -> getVerifyCode());
        mBtnRegister.setOnClickListener(v -> register());
        mIvClear.setOnClickListener(v -> clearVerifyCode());
        mIvPswSwitch.setOnClickListener(v -> switchPsw());
        mCbUserProtocol.setOnCheckedChangeListener((buttonView, isChecked) -> isAgree = isChecked);
        mTvUserProtocol.setOnClickListener(v -> gotoUserProtocol());

        mEtPwd.setOnEditorActionListener(((v, actionId, event) -> {
            MobclickAgent.onEvent(this, "RegisterActivity_mEtPwd");
            if (actionId == EditorInfo.IME_ACTION_DONE)
            {
                register();
            }
            return false;
        }));
    }

    private void gotoUserProtocol()
    {
        MobclickAgent.onEvent(this, "RegisterActivity_gotoUserProtocol");
        Intent intent = new Intent(getBaseContext(), ProtocolActivity.class);
        startActivity(intent);
    }

    private void getVerifyCode()
    {
        MobclickAgent.onEvent(this, "RegisterActivity_getVerifyCodel");
        mAccount = mEtAccount.getText().toString();
        if (mAccount.equals(""))
        {
            AppToast.showToast(R.string.phone_empty);
            return;
        }

        if (!RegexpUtils.isPhoneNum(mAccount))
        {
            AppToast.showToast(R.string.phone_error);
            return;
        }

        mBtnGetCode.setEnabled(false);
        verifyHandler.sendEmptyMessage(0);

        Subscriber<SMSEntity> subscriber = new WaitingSubscriber<SMSEntity>(RegisterActivity.this)
        {
            @Override
            public void onNext(SMSEntity smsEntity)
            {
                AppToast.showToast(R.string.verify_send_success);
            }
        };
        SMSBody body = new SMSBody(mAccount);
        UserCenterMethods.getInstance().sendSMS(subscriber, body);
    }

    private void register()
    {
        MobclickAgent.onEvent(this, "RegisterActivity_register");
        if (isAgree)
        {
            mAccount = mEtAccount.getText().toString();
            mPwd = mEtPwd.getText().toString();
            mVerifyCode = mEtVerifyCode.getText().toString();
            if (mAccount != null && mPwd != null && !mAccount.equals("") && !mVerifyCode.equals("") && !mPwd.equals(""))
            {
                if (!RegexpUtils.isPhoneNum(mAccount))
                {
                    AppToast.showToast(R.string.phone_error);
                    return;
                }

                if (mPwd.length() <= 5)
                {
                    AppToast.showToast(R.string.psw_unfilled);
                    return;
                }

                if (!RegexpUtils.isMixPassword(mPwd))
                {
                    AppToast.showToast(R.string.password_must_mix);
                    return;
                }

                Subscriber<RegisterEntity> subscriber = new WaitingSubscriber<RegisterEntity>(RegisterActivity.this)
                {
                    @Override
                    public void onNext(RegisterEntity registerEntity)
                    {
                        AppToast.showToast(R.string.register_success);
                        Intent intent = new Intent();
                        intent.putExtra("account", mAccount);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }
                };
                UserBody body = new UserBody(mAccount, mPwd, mVerifyCode);
                UserCenterMethods.getInstance().register(subscriber, body);
            }
            else
            {
                AppToast.showToast(R.string.info_unfilled);
            }
        }
        else
        {
            AppToast.showToast(R.string.read_protocol);
        }
    }

    private void clearVerifyCode()
    {
        MobclickAgent.onEvent(this, "RegisterActivity_clearVerifyCode");
        mEtVerifyCode.setText("");
    }

    private void switchPsw()
    {
        MobclickAgent.onEvent(this, "RegisterActivity_switchPsw");
        isShowPsw = !isShowPsw;
        if (isShowPsw)
        {
            mIvPswSwitch.setImageResource(R.drawable.ic_hide_psw);
            mEtPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        else
        {
            mIvPswSwitch.setImageResource(R.drawable.ic_show_psw);
            mEtPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        //移动光标到最后
        CharSequence text = mEtPwd.getText();
        Spannable spanText = (Spannable) text;
        Selection.setSelection(spanText, text.length());
    }
}
