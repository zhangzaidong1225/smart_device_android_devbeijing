package com.smarthome.lilos.lilossmarthome.activity.user;

import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.umeng.analytics.MobclickAgent;

import rx.Subscriber;

/**
 * 序列号检查页
 * 用于检查设备是否为正版
 * 当前无入口,暂停使用
 */
public class SecurityCheckActivity extends BaseActivity
{
    private ImageView mIvBack;
    private EditText mEtSecurityCode;
    private ImageView mIvClear;
    private TextView mTvTotalTime;
    private TextView mTvCurrentTime;
    private Button mBtnCommit;
    private LinearLayout mLlCheckResult;

    private String mKey;
    private String mSecurityCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security_check);
        init();
    }

    private void init()
    {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mEtSecurityCode = (EditText) findViewById(R.id.et_security_code);
        mIvClear = (ImageView) findViewById(R.id.iv_clear_input);
        mBtnCommit = (Button) findViewById(R.id.btn_commit);
        mTvTotalTime = (TextView) findViewById(R.id.tv_total_time);
        mTvCurrentTime = (TextView) findViewById(R.id.tv_current_time);
        mLlCheckResult = (LinearLayout) findViewById(R.id.ll_check_result);

        mIvBack.setOnClickListener(v -> {
            MobclickAgent.onEvent(this, "SecurityCheckActivity_back");
            finish();
        });
        mBtnCommit.setOnClickListener(v -> securityCheck());
        mIvClear.setOnClickListener(v -> clearInput());

        mEtSecurityCode.setOnEditorActionListener((v, actionId, event) ->
        {
            MobclickAgent.onEvent(this, "SecurityCheckActivity_mEtSecurityCode");
            if (actionId == EditorInfo.IME_ACTION_DONE)
            {
                securityCheck();
                return true;
            }
            return false;
        });

        mLlCheckResult.setVisibility(View.INVISIBLE);
    }

    private void clearInput()
    {
        MobclickAgent.onEvent(this, "SecurityCheckActivity_clearInput");
        mEtSecurityCode.setText("");
    }

    private void securityCheck()
    {
        MobclickAgent.onEvent(this, "SecurityCheckActivity_securityCheck");
        mSecurityCheck = mEtSecurityCode.getText().toString();

        if (mKey == null || mKey.equals(""))
        {
            AppToast.showToast(R.string.key_not_exist);
        }

        if (mSecurityCheck != null && !mSecurityCheck.equals(""))
        {
            // TODO: 2017/2/14 接入检查设备号的接口

        }
        else
        {
            AppToast.showToast(R.string.security_code_unfilled);
        }
    }
}
