package com.smarthome.lilos.lilossmarthome.device;

import android.bluetooth.BluetoothDevice;
import android.net.Uri;
import android.util.Log;

import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.activity.MainActivity;
import com.smarthome.lilos.lilossmarthome.bluetooth.DfuProgressListener;
import com.smarthome.lilos.lilossmarthome.bluetooth.DfuService;

import java.io.File;

import no.nordicsemi.android.dfu.DfuServiceInitiator;
import no.nordicsemi.android.dfu.DfuServiceListenerHelper;

/**
 * 忻风设备类
 * Created by Administrator on 2016/5/16.
 * 描述忻风设备的专有属性
 */
public class XinfengDevice extends BaseBluetoothDevice
{

    private BluetoothDevice bluetoothDevice;
    //滤芯使用时长
    private int use_time;
    //本次虑棉使用时长
    private int use_time_this;

    //风扇等级
    private int speed;
    //电量
    private int battery_power;

    //百分比
    private int percentage;
    //Pm2.5标准浓度
    private int pm25;

    //充电标志
    private int battery_charge;

    //本次风扇使用时长
    private int wind_use_time_this;

    //滤芯在位
    private int filter_in;

    public XinfengDevice()
    {
        typeId = DeviceFactory.XINFENG;
        update_state = 100;
    }

    public BluetoothDevice getBluetoothDevice()
    {
        return bluetoothDevice;
    }

    public void setBluetoothDevice(BluetoothDevice bluetoothDevice)
    {
        this.bluetoothDevice = bluetoothDevice;
    }

    public int getUse_time()
    {
        return use_time;
    }

    public void setUse_time(int use_time)
    {
        this.use_time = use_time;
    }

    public int getUse_time_this()
    {
        return use_time_this;
    }

    public void setUse_time_this(int use_time_this)
    {
        this.use_time_this = use_time_this;
    }

    public int getSpeed()
    {
        return speed;
    }

    public void setSpeed(int speed)
    {
        this.speed = speed;
    }

    public int getBattery_power()
    {
        return battery_power;
    }

    public void setBattery_power(int battery_power)
    {
        this.battery_power = battery_power;
    }

    public int getPercentage()
    {
        return percentage;
    }

    public void setPercentage(int percentage)
    {
        this.percentage = percentage;
    }

    public int getPm25()
    {
        return pm25;
    }

    public void setPm25(int pm25)
    {
        this.pm25 = pm25;
    }

    public int getBattery_charge()
    {
        return battery_charge;
    }

    public void setBattery_charge(int battery_charge)
    {
        this.battery_charge = battery_charge;
    }

    public int getWind_use_time_this()
    {
        return wind_use_time_this;
    }

    public void setWind_use_time_this(int wind_use_time_this)
    {
        this.wind_use_time_this = wind_use_time_this;
    }

    public int getFilter_in()
    {
        return filter_in;
    }

    public void setFilter_in(int filter_in)
    {
        this.filter_in = filter_in;
    }

    public void shutDown()
    {
        Log.d("XinfengDevice", "开始关闭忻风设备");
        if (!isConnected())
        {
            Log.d("XinfengDevice", "断开连接");
            return;
        }

        byte head = (byte) 0xAA;
        byte command = 0x01;
        byte check = (byte) (head + command);
        byte[] data = new byte[]{head, command, check};

//        SmartHomeApplication.mBluetoothService.writeRXCharacteristic(getAddress(),data);
        SmartHomeApplication.mBluetoothService.writeUartDeviceCharacteristic(getAddress(), data);
    }

    public void setWindSpeed(int speed)
    {
        Log.d("XinfengDevice", "设置风速为" + speed);


        if (!isConnected())
        {

            Log.d("XinfengDevice", "连接已断开");

            return;
        }

        byte head = (byte) 0xAA;
        byte command = 0x10;
        byte sp = (byte) speed;
        byte time = 0;
        byte check = (byte) (head + command + sp + time);
        byte[] data = new byte[]{head, command, sp, time, check};
        SmartHomeApplication.mBluetoothService.writeUartDeviceCharacteristic(getAddress(), data);
    }

    public void devCheck()
    {

        Log.d("XinfengDevice", "设备检查");

        byte head = (byte) 0xAA;
        byte command = 0x13;
        byte check = (byte) (head + command);
        byte[] data = new byte[]{head, command, check};

        if (!isConnected())
        {

            Log.d("XinfengDevice", "连接已断开");

            return;
        }
//        SmartHomeApplication.mBluetoothService.writeRXCharacteristic(address, data);
        SmartHomeApplication.mBluetoothService.writeUartDeviceCharacteristic(getAddress(), data);
    }

    public void devRename(String newName)
    {

        Log.d("XinfengDevice", "设备重命名");

        byte head = (byte) 0xAA;
        byte command = 0x12;
        byte[] buf1 = new byte[19];
        byte[] buf2 = new byte[19];
        byte[] buf_name = new byte[30];
        try
        {
            int buf_length = 0;
            for (int i = 0; i < newName.length(); i++)
            {
                String tmp = newName.substring(i, i + 1);

                Log.d("XinfengDevice", "设备新名称为");

                byte[] tmp_buf = tmp.getBytes("UTF8");
                if (buf_length + tmp_buf.length <= 30)
                {
                    for (int j = 0; j < tmp_buf.length; j++)
                    {
                        buf_name[buf_length] = tmp_buf[j];
                        buf_length++;
                    }
                }
            }
            for (int i = 0; i < 30; i++)
            {

                Log.d("XinfengDevice", String.format("%x", buf_name[i]));

                if (i < 15)
                {
                    buf1[i + 3] = buf_name[i];
                }
                else
                {
                    buf2[i + 3 - 15] = buf_name[i];
                }
            }
        } catch (Exception e)
        {
        }
        buf1[0] = head;
        buf1[1] = command;
        buf1[2] = 0x00;

        buf2[0] = head;
        buf2[1] = command;
        buf2[2] = 0x01;

        for (int i = 0; i < 18; i++)
        {
            buf1[18] += buf1[i];
            buf2[18] += buf2[i];
        }
        for (int i = 0; i < 19; i++)
        {
        }

        for (int i = 0; i < 19; i++)
        {

        }
        if (!isConnected())
        {
            return;
        }
        SmartHomeApplication.mBluetoothService.writeUartDeviceCharacteristic(getAddress(), buf1);

        try
        {
            Thread.sleep(1000);
        } catch (Exception e)
        {
        }
        SmartHomeApplication.mBluetoothService.writeUartDeviceCharacteristic(getAddress(), buf2);
        SmartHomeApplication.myDeviceManager.mData.get(address).setName(newName);
    }

    public void devUpdate(String path)
    {
        DfuServiceListenerHelper.registerProgressListener(MainActivity.mContext, new DfuProgressListener());
        DfuServiceInitiator starter = new DfuServiceInitiator(getAddress())
                .setDeviceName(getAddress())
                .setKeepBond(true);
        Uri uri = Uri.fromFile(new File(path));
        starter.setZip(uri, path);
        starter.start(MainActivity.mContext, DfuService.class);
    }

    @Override
    public String toString()
    {
        return "XinfengDevice{" +
                "bluetoothDevice=" + bluetoothDevice +
                ", use_time=" + use_time +
                ", use_time_this=" + use_time_this +
                ", speed=" + speed +
                ", battery_power=" + battery_power +
                ", version=" + version +
                ", percentage=" + percentage +
                ", pm25=" + pm25 +
                ", battery_charge=" + battery_charge +
                ", wind_use_time_this=" + wind_use_time_this +
                ", filter_in=" + filter_in +
                "} " + super.toString();
    }
}