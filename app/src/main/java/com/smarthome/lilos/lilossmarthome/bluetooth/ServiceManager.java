package com.smarthome.lilos.lilossmarthome.bluetooth;

import java.util.UUID;

/**
 * BLE支持服务管理
 * 管理当前支持的全部服务的UUID
 * Created by Joker on 2016/12/6.
 */

public class ServiceManager
{
    /**
     * Blood Pressure service UUID 血压服务
     */
    public final static UUID BP_SERVICE_UUID = UUID.fromString("00001810-0000-1000-8000-00805f9b34fb");
    /**
     * Blood Pressure Measurement characteristic UUID 血压测量特征
     */
    public static final UUID BPM_CHARACTERISTIC_UUID = UUID.fromString("00002A35-0000-1000-8000-00805f9b34fb");
    /**
     * Intermediate Cuff Pressure characteristic UUID 中间压测量特征
     */
    public static final UUID ICP_CHARACTERISTIC_UUID = UUID.fromString("00002A36-0000-1000-8000-00805f9b34fb");

    /**
     * Cycling Speed and Cadence service UUID 自行车服务
     */
    public final static UUID CYCLING_SPEED_AND_CADENCE_SERVICE_UUID = UUID.fromString("00001816-0000-1000-8000-00805f9b34fb");
    /**
     * Cycling Speed and Cadence Measurement characteristic UUID 自行车测量特征
     */
    public static final UUID CSC_MEASUREMENT_CHARACTERISTIC_UUID = UUID.fromString("00002A5B-0000-1000-8000-00805f9b34fb");

    /**
     * Glucose service UUID 血糖服务
     */
    public final static UUID GLS_SERVICE_UUID = UUID.fromString("00001808-0000-1000-8000-00805f9b34fb");
    /**
     * Glucose Measurement characteristic UUID 血糖测量特征
     */
    public final static UUID GM_CHARACTERISTIC = UUID.fromString("00002A18-0000-1000-8000-00805f9b34fb");
    /**
     * Glucose Measurement Context characteristic UUID 血糖测量值特征
     */
    public final static UUID GM_CONTEXT_CHARACTERISTIC = UUID.fromString("00002A34-0000-1000-8000-00805f9b34fb");
    /**
     * Glucose Feature characteristic UUID 血糖测量参数特征
     */
    public final static UUID GF_CHARACTERISTIC = UUID.fromString("00002A51-0000-1000-8000-00805f9b34fb");
    /**
     * Record Access Control Point characteristic UUID 报告记录内容特征
     */
    public final static UUID RACP_CHARACTERISTIC = UUID.fromString("00002A52-0000-1000-8000-00805f9b34fb");

    /**
     * Heart Rate service UUID 心率服务
     */
    public final static UUID HR_SERVICE_UUID = UUID.fromString("0000180D-0000-1000-8000-00805f9b34fb");
    /**
     * Heart Rate Sensor Location characteristic UUID 心率测量部位特征
     */
    public static final UUID HR_SENSOR_LOCATION_CHARACTERISTIC_UUID = UUID.fromString("00002A38-0000-1000-8000-00805f9b34fb");
    /**
     * Heart Rate characteristic 心率测量值特征
     */
    public static final UUID HR_CHARACTERISTIC_UUID = UUID.fromString("00002A37-0000-1000-8000-00805f9b34fb");

    /**
     * Health Thermometer service UUID 体温服务
     */
    public final static UUID HT_SERVICE_UUID = UUID.fromString("00001809-0000-1000-8000-00805f9b34fb");
    /**
     * Health Thermometer Measurement characteristic UUID 体温测量值特征
     */
    public static final UUID HT_MEASUREMENT_CHARACTERISTIC_UUID = UUID.fromString("00002A1C-0000-1000-8000-00805f9b34fb");

    /**
     * Running Speed and Cadence Measurement service UUID 跑步机服务
     */
    public final static UUID RUNNING_SPEED_AND_CADENCE_SERVICE_UUID = UUID.fromString("00001814-0000-1000-8000-00805f9b34fb");
    /**
     * Running Speed and Cadence Measurement characteristic UUID 跑步机测量值特征
     */
    public static final UUID RSC_MEASUREMENT_CHARACTERISTIC_UUID = UUID.fromString("00002A53-0000-1000-8000-00805f9b34fb");

    /**
     * Weight Scale service UUID 体重秤服务
     */
    public static final UUID WS_SERVICE_UUID = UUID.fromString("0000181D-0000-1000-8000-00805f9b34fb");
    /**
     * Weight Scale Feature characteristic UUID 体重秤参数特征
     */
    public static final UUID WS_FEATURE_CHAR_UUID = UUID.fromString("00002A9E-0000-1000-8000-00805f9b34fb");
    /**
     * Weight Scale Measurement characteristic UUID 体重秤测量值特征
     */
    public static final UUID WS_MEASURE_CHAR_UUID = UUID.fromString("00002A9D-0000-1000-8000-00805f9b34fb");

    /**
     * Client Characteristic Configuration 客户端配置特征
     */
    public static final UUID CCCD = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    /**
     * 忻风服务
     */
    public static final UUID RX_SERVICE_UUID = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e");
    /**
     * 忻风写入信息特征
     */
    public static final UUID RX_CHAR_UUID = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e");
    /**
     * 忻风上报信息特征
     */
    public static final UUID TX_CHAR_UUID = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");

    /**
     * 电池服务
     */
    public final static UUID BATTERY_SERVICE = UUID.fromString("0000180F-0000-1000-8000-00805f9b34fb");
    /**
     * 电池等级特征
     */
    public final static UUID BATTERY_LEVEL_CHARACTERISTIC = UUID.fromString("00002A19-0000-1000-8000-00805f9b34fb");

    /**
     * 通用属性服务
     */
    public final static UUID GENERIC_ATTRIBUTE_SERVICE = UUID.fromString("00001801-0000-1000-8000-00805f9b34fb");
    /**
     * 服务变更特征
     */
    public final static UUID SERVICE_CHANGED_CHARACTERISTIC = UUID.fromString("00002A05-0000-1000-8000-00805f9b34fb");
}
