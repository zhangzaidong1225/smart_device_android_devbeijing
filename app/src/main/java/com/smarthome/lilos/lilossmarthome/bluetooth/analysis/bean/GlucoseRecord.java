package com.smarthome.lilos.lilossmarthome.bluetooth.analysis.bean;

import java.util.Calendar;

/**
 * Created by Joker on 2016/11/22.
 */

public class GlucoseRecord
{
    public static final int UNIT_kgpl = 0;
    public static final int UNIT_molpl = 1;

    public int sequenceNumber;
    public Calendar time;
    public int timeOffset;
    public float glucoseConcentration;
    public int unit;
    public int type;
    public int sampleLocation;
    public int status;

    public MeasurementContext context;

    public static class MeasurementContext
    {
        public static final int UNIT_kg = 0;
        public static final int UNIT_l = 1;

        public int carbohydrateId;
        public float carbohydrateUnits;
        public int meal;
        public int tester;
        public int health;
        public int exerciseDuration;
        public int exerciseIntensity;
        public int medicationId;
        public float medicationQuantity;
        public int medicationUnit;
        public float HbA1c;
    }
}
