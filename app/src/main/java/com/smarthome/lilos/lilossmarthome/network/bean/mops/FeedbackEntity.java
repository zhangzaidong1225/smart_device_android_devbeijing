package com.smarthome.lilos.lilossmarthome.network.bean.mops;

/**
 * Created by Joker on 2016/12/30.
 */

public class FeedbackEntity
{
    /**
     * contact : 17002108009
     * content : 反馈内容
     */

    private String contact;
    private String content;

    public String getContact()
    {
        return contact;
    }

    public void setContact(String contact)
    {
        this.contact = contact;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }
}
