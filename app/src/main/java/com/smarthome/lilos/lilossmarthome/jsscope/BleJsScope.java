package com.smarthome.lilos.lilossmarthome.jsscope;

import android.app.AlertDialog;
import android.util.Log;
//import android.webkit.WebView;

import com.smarthome.lilos.lilossmarthome.jsscope.bean.CallbackBean;
import com.smarthome.lilos.lilossmarthome.jsscope.bean.LowestAppVersionBean;
import com.smarthome.lilos.lilossmarthome.jsscope.method.Audio;
import com.smarthome.lilos.lilossmarthome.jsscope.method.BlueTooth;
import com.smarthome.lilos.lilossmarthome.jsscope.method.IM;
import com.smarthome.lilos.lilossmarthome.jsscope.method.Location;
import com.smarthome.lilos.lilossmarthome.jsscope.method.Login;
import com.smarthome.lilos.lilossmarthome.jsscope.method.NetWork;
import com.smarthome.lilos.lilossmarthome.jsscope.method.Peripheral;
import com.smarthome.lilos.lilossmarthome.jsscope.method.Storage;
import com.smarthome.lilos.lilossmarthome.jsscope.method.UIDialog;
import com.smarthome.lilos.lilossmarthome.jsscope.method.Webview;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.smarthome.lilos.lilossmarthome.utils.SystemUtils;
import com.tencent.smtt.sdk.WebView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * 用于JSSDK指令接入
 * Created by Joker on 2016/8/15.
 */
public class BleJsScope
{
    public static void alert(WebView webView, String message)
    {
        Log.d("BleJsScope", "尝试调用alert");
        AlertDialog.Builder builder = new AlertDialog.Builder(webView.getContext());
        builder.setTitle("JS消息");
        builder.setMessage(message);
        builder.setPositiveButton("OK", (dialog, which) -> dialog.dismiss());
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    public static boolean execute(WebView webView, String ctrl, String method, String params,
                                  String callBackFuncName, String lowestAppVersion)
    {
        LowestAppVersionBean versionBean = (LowestAppVersionBean) JsonUtils.jsonToBean(lowestAppVersion, LowestAppVersionBean.class);
        int appVersion = SystemUtils.getVersionCode(webView.getContext());
        int lowestVersion = Integer.parseInt(versionBean.android);
        if (appVersion < lowestVersion)
        {
            CallbackBean bean = new CallbackBean();
            Map<String, Object> resultMap = new HashMap<>();
            bean.callBackName = callBackFuncName;
            resultMap.put("status", "failure");
            resultMap.put("msg", "app版本过低，不支持该功能，请升级app");
            bean.callBackParams = JsonUtils.objectToJson(resultMap);
            webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");
            return false;
        }
        else
        {

            String clazzName = "";
            switch (ctrl)
            {
                case "BlueTooth":
                    clazzName = BlueTooth.class.getName();
                    break;
                case "Storage":
                    clazzName = Storage.class.getName();
                    break;
                case "NetWork":
                    clazzName = NetWork.class.getName();
                    break;
                case "UIDialog":
                    clazzName = UIDialog.class.getName();
                    break;
                case "Location":
                    clazzName = Location.class.getName();
                    break;
                case "Webview":
                    clazzName = Webview.class.getName();
                    break;
                case "IM":
                    clazzName = IM.class.getName();
                    break;
                case "Audio":
                    clazzName = Audio.class.getName();
                    break;
                case "Peripheral":
                    clazzName = Peripheral.class.getName();
                    break;
                case "Login":
                    clazzName = Login.class.getName();
                    break;
                default:
                    return false;
            }
            try
            {
                Map<String, String> map = (Map<String, String>) JsonUtils.jsonToMap(params);
                Class<?> clazz = Class.forName(clazzName);
                Method methodM = clazz.getMethod(method, WebView.class, Map.class, String.class);
                return (boolean) methodM.invoke(null, webView, map, callBackFuncName);
            } catch (ClassNotFoundException e)
            {
                e.printStackTrace();
                return false;
            } catch (NoSuchMethodException e)
            {
                e.printStackTrace();
                return false;
            } catch (InvocationTargetException e)
            {
                e.printStackTrace();
                return false;
            } catch (IllegalAccessException e)
            {
                e.printStackTrace();
                return false;
            }
        }
    }
}
