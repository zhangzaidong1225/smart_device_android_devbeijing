package com.smarthome.lilos.lilossmarthome.greendao.utils;

import android.util.Log;

import com.smarthome.lilos.lilossmarthome.greendao.GreenDaoHelper;
import com.smarthome.lilos.lilossmarthome.greendao.entity.SyncManager;
import com.smarthome.lilos.lilossmarthome.greendao.gen.SyncManagerDao;

/**
 * Created by louis on 2017/4/13.
 */

public class GreenDaoUtils {

    public static SyncManagerDao mSyncManagerDao = GreenDaoHelper.getDaoSession().getSyncManagerDao();

    public static boolean isInitDataBase(String version) {
        SyncManager syncManager = mSyncManagerDao.queryBuilder().where(SyncManagerDao.Properties.Key.eq("InitDataBase")).unique();
        if (syncManager != null && version.equals(syncManager.getValue())) {
            return true;
        }

        return false;
    }

    public static void setInitDataBase(String version) {
        SyncManager syncManager = mSyncManagerDao.queryBuilder().where(SyncManagerDao.Properties.Key.eq("InitDataBase")).unique();

        if (syncManager == null) {
            syncManager = new SyncManager("InitDataBase", version);
            mSyncManagerDao.insert(syncManager);
        } else {
            if (version.equals(syncManager.getValue())){
                return;
            } else {
                mSyncManagerDao.deleteAll();
                SyncManager syncMangerNew = new SyncManager("InitDataBase", version);
                mSyncManagerDao.insert(syncMangerNew);
            }
        }
    }
}
