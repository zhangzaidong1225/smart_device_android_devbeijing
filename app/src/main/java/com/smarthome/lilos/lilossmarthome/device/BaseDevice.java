package com.smarthome.lilos.lilossmarthome.device;

import com.orhanobut.logger.Logger;

/**
 * 基础设备类
 * 用于存放设备的全部基础信息
 * Created by Kevin on 2016/7/28.
 */
public abstract class BaseDevice
{

    protected int id;
    protected String name;
    protected String icon;
    protected int typeId;
    protected String devId;
    protected String address;



    protected int is_demo;
    //是否已连接 未连接 --0，已连接--- 连接中---
    protected int isconnectd;
    private boolean isInitialized = false;


    protected boolean isDelete = false;

    private int Pm25;
    public int getIs_demo() {
        return is_demo;
    }

    public void setIs_demo(int is_demo) {
        this.is_demo = is_demo;
    }
    public int getPm25() {
        if (Pm25 > 500) {
            Pm25 = 500;
        }
        return Pm25;
    }

    public void setPm25(int pm25) {
        Pm25 = pm25;
    }

/*    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }*/

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getIcon()
    {
        return icon;
    }

    public void setIcon(String icon)
    {
        this.icon = icon;
    }

    public int getTypeId()
    {
        return typeId;
    }

    public void setTypeId(int typeId)
    {
        this.typeId = typeId;
    }

    public String getDevId()
    {
        return devId;
    }

    public void setDevId(String devId)
    {
        this.devId = devId;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public int getIsconnectd()
    {
        return isconnectd;
    }

    public void setIsconnectd(int isconnectd)
    {
        this.isconnectd = isconnectd;
        Logger.init("设备状态改变");
        Logger.i("当前设备" + address + "(" + name + ")连接状态为 " + isconnectd);
        Logger.init("SMART HOME");
    }

    public void initialize()
    {
        this.isInitialized = true;
    }

    public boolean isInitialized()
    {
        return isInitialized;
    }

    public abstract void connect();

    public abstract void connect(boolean isConnecting);

    public abstract boolean isConnected();

    public abstract void stopConnect();

    public abstract void disconnect();

    @Override
    public String toString()
    {
        return "BaseDevice{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", icon='" + icon + '\'' +
                ", typeId=" + typeId +
                ", devId='" + devId + '\'' +
                ", address='" + address + '\'' +
                ", isconnectd=" + isconnectd + '\'' +
                ", is_demo=" + is_demo +
                '}';
    }
}
