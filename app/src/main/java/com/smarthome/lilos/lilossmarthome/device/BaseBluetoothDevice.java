package com.smarthome.lilos.lilossmarthome.device;

import android.bluetooth.BluetoothAdapter;
import android.util.Log;

import com.orhanobut.logger.Logger;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.activity.MainActivity;
import com.smarthome.lilos.lilossmarthome.bluetooth.BLEService;
import com.smarthome.lilos.lilossmarthome.bluetooth.BleConnectingTask;

import java.util.Timer;

/**
 * 基础蓝牙设备类
 * 用于存放蓝牙设备的基础数据
 * 实现连接等方法
 * Created by Joker on 2016/8/8.
 */
public class BaseBluetoothDevice extends BaseDevice
{
    protected int companyId;
    protected int def_version;
    protected boolean ismatchd;
    protected boolean autoConnect;
    protected boolean autoConnect1;
    protected int version;
    //失败信息
    //101---升级成功
    //102---升级失败
    //103---升级中
    //104---升级开始
    protected int update_state;
    protected boolean subscribe = false;
    private Timer timer;
/*    private int is_demo;

    @Override
    public int getIs_demo() {
        return is_demo;
    }

    @Override
    public void setIs_demo(int is_demo) {
        this.is_demo = is_demo;
    }*/

    public boolean ismatchd()
    {
        return ismatchd;
    }

    public void setIsmatchd(boolean ismatchd)
    {
        this.ismatchd = ismatchd;
    }

    public boolean isAutoConnect()
    {
        return autoConnect;
    }

    public void setAutoConnect(boolean autoConnect)
    {
        this.autoConnect = autoConnect;
        Logger.i("当前" + address + "（" + name + ") 的自动连接状态为：" + autoConnect);
    }

    public boolean isAutoConnect1()
    {
        return autoConnect1;
    }

    public void setAutoConnect1(boolean autoConnect1)
    {
        this.autoConnect1 = autoConnect1;
    }

    public int getUpdate_state()
    {
        return update_state;
    }

    public void setUpdate_state(int update_state)
    {
        this.update_state = update_state;
    }

    public boolean isSubscribe()
    {
        return subscribe;
    }

    public void setSubscribe(boolean subscribe)
    {
        this.subscribe = subscribe;
    }

    public void cancelBond()
    {
        setIsmatchd(false);
        if (getIsconnectd() != 1)
        {
            SmartHomeApplication.mBluetoothService.removeData(getAddress());
        }

    }

    @Override
    public boolean isConnected()
    {
        if (getIsconnectd() == BLEService.STATE_DISCONNECTED || getIsconnectd() == BLEService.STATE_CONNECTING)
        {
            return false;
        }
        else if (getIsconnectd() == BLEService.STATE_CONNECTED)
        {
            return true;
        }
        return false;
    }

    @Override
    public void stopConnect()
    {

        if (!isConnected())
        {
            Log.d("BaseBluetoothDevice", "断开连接……");
            return;
        }

        if (getIsconnectd() == 1)
        {
            setAutoConnect(false);
        }

        SmartHomeApplication.mBluetoothService.disconnect(getAddress());
    }

    @Override
    public void disconnect()
    {
        SmartHomeApplication.mBluetoothService.disconnect(getAddress());
    }

    public void connect()
    {
        connect(true);
    }

    public void connect(boolean connecting)
    {
        Log.d("BaseBluetoothDevice", "开始连接地址为" + getAddress() + "的设备");
        if (BluetoothAdapter.getDefaultAdapter().isEnabled())
        {
            if (SmartHomeApplication.getInstance().myDeviceManager.mData.isEmpty() && SmartHomeApplication.getInstance().myDeviceManager.mData.containsKey(getAddress()))
            {
                if (getIsconnectd() == BLEService.STATE_DISCONNECTED)
                {
                    if (connecting)
                    {
                        Log.d("BaseBluetoothDevice", "手动点击连接");
                        setIsconnectd(BLEService.STATE_MANUAL_CONNECT);
                    }
                    else
                    {
                        Log.d("BaseBluetoothDevice", "自动连接开始……");
                        setIsconnectd(BLEService.STATE_AUTO_CONNECT);
                    }
                }
                else if (getIsconnectd() == BLEService.STATE_AUTO_CONNECT)
                {
                    if (connecting)
                    {
                        SmartHomeApplication.mBluetoothService.close(getAddress());
                        setIsconnectd(BLEService.STATE_MANUAL_CONNECT);
                    }
                }
            }
            SmartHomeApplication.mBluetoothService.connect(getAddress());

            if (connecting)
            {
                timer = new Timer();
                timer.schedule(new BleConnectingTask(getAddress(), timer), 10000);
            }
        }
        else
        {
            Log.d("RequestBluetooth", "来自BaseBluetoothDevice的connect");
            MainActivity.mContext.requestBluetooth(BLEService.REQUEST_CONNECT, address);
        }
    }

    public int getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(int companyId)
    {
        this.companyId = companyId;
    }

    public int getDef_version()
    {
        return def_version;
    }

    public void setDef_version(int def_version)
    {
        this.def_version = def_version;
    }

    public int getVersion()
    {
        return version;
    }

    public void setVersion(int version)
    {
        this.version = version;
    }

    @Override
    public String toString()
    {
        return "BaseBluetoothDevice{" +
                "companyId=" + companyId +
                ", def_version=" + def_version +
                ", ismatchd=" + ismatchd +
                ", autoConnect=" + autoConnect +
                ", autoConnect1=" + autoConnect1 +
                ", update_state=" + update_state +
                "} " + super.toString();
    }
}
