/**
 * Copyright 2016 aTool.org
 */
package com.smarthome.lilos.lilossmarthome.network.bean.amap;

import java.util.List;

/**
 * Auto-generated: 2016-08-29 10:38:47
 *
 * @author aTool.org (i@aTool.org)
 * @website http://www.atool.org/json2javabean.php
 */
public class Addresscomponent
{

    //private String country;
    private String province;
    private String district;
//    private String city;
    //private String citycode;
    //private String adcode;
    //private String township;
    //private String towncode;
    //private Neighborhood neighborhood;
   // private Building building;



    //private Streetnumber streetNumber;
    //private List<Businessareas> businessAreas;

/*   public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }*/

    public String getProvince()
    {
        return province;
    }

    public void setProvince(String province)
    {
        this.province = province;
    }
    public String getDistrict()
    {
        return district;
    }

    public void setDistrict(String district)
    {
        this.district = district;
    }

/*    public String getCitycode()
    {
        return citycode;
    }*/
/*    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }*/
/*    public void setCitycode(String citycode)
    {
        this.citycode = citycode;
    }

    public String getAdcode()
    {
        return adcode;
    }

    public void setAdcode(String adcode)
    {
        this.adcode = adcode;
    }

    public String getTownship()
    {
        return township;
    }

    public void setTownship(String township)
    {
        this.township = township;
    }

    public String getTowncode()
    {
        return towncode;
    }

    public void setTowncode(String towncode)
    {
        this.towncode = towncode;
    }

    public Neighborhood getNeighborhood()
    {
        return neighborhood;
    }

    public void setNeighborhood(Neighborhood neighborhood)
    {
        this.neighborhood = neighborhood;
    }

    public Building getBuilding()
    {
        return building;
    }

    public void setBuilding(Building building)
    {
        this.building = building;
    }

    public Streetnumber getStreetNumber()
    {
        return streetNumber;
    }

    public void setStreetNumber(Streetnumber streetNumber)
    {
        this.streetNumber = streetNumber;
    }

    public List<Businessareas> getBusinessAreas()
    {
        return businessAreas;
    }

    public void setBusinessAreas(List<Businessareas> businessAreas)
    {
        this.businessAreas = businessAreas;
    }*/

}