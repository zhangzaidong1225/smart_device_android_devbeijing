package com.smarthome.lilos.lilossmarthome.network;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.ErrorMessage;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.ErrorMessage2;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;

import java.io.IOException;
import java.util.List;

import retrofit2.adapter.rxjava.HttpException;

/**
 * 网络错误解析类
 * 用于解析不同返回值的错误信息并显示
 * Created by Joker on 2016/7/29.
 */
public class ErrorManager
{
    public static void parseError(HttpException error)
    {
        if (error == null)
        {
            return;
        }

        String message;
        try
        {
            message = error.response().errorBody().string();
            if (message == null || message.equals(""))
            {
                return;
            }
        } catch (IOException e)
        {
            e.printStackTrace();
            return;
        }

        try
        {
            switch (error.code())
            {
                case 400:
                case 401:
                case 403:
                case 404:
                case 410:
                case 500:
                    ErrorMessage2 error2 = (ErrorMessage2) JsonUtils.jsonToBean(message, ErrorMessage2.class);
                    AppToast.showToast(error2.getMessage());
                    break;
                case 422:
                    List<ErrorMessage> errors = new JsonUtils<ErrorMessage>().jsonToList(message, ErrorMessage.class);
                    for (ErrorMessage errorM : errors)
                    {
                        AppToast.showToast(errorM.getMessage());
                    }
                    break;
            }
        }
        catch (Exception e)
        {

        }
    }
}
