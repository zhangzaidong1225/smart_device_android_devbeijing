package com.smarthome.lilos.lilossmarthome.network.bean.pm25;

/**
 * Created by louis on 2017/5/12.
 */

public class LifeData {
    public UVData getUv() {
        return uv;
    }

    public void setUv(UVData uv) {
        this.uv = uv;
    }

    private UVData uv;
}
