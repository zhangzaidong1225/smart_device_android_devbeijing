package com.smarthome.lilos.lilossmarthome.network.retrofit.request;

import android.util.Log;

import com.smarthome.lilos.lilossmarthome.network.URLManager;
import com.smarthome.lilos.lilossmarthome.network.bean.FirstPageEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.location.CityListEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.pm25.PMDayData;
import com.smarthome.lilos.lilossmarthome.network.bean.pm25.WeatherData;
import com.smarthome.lilos.lilossmarthome.network.retrofit.SchedulersTransformer;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Subscriber;

/**
 * Created by songyu on 2017/3/2.
 */

public class FirstPageMethods {
    private static final String BASE_URL = new URLManager().getMopsApiServerUrl();//"https://mops-xinfeng-develop.lianluo.com:883/";//"https://mops-dev.lianluo.com:883/";

    private static final String KEY = "db50eba84b76f8ccccb87a0c2c0b034a";
    private static final String COORDSYS = "gps";
    private static final int RADIUS = 0;
    private static final String EXTENSIONS = "base";
    private static final String BATCH = "false";
    private static final int ROADLEVEL = 1;

    private static final int DEFAULT_TIMEOUT = 10;

    private Retrofit retrofit;
    private FirstPageService service;

    private FirstPageMethods()
    {
    }

    public static FirstPageMethods getInstance()
    {
        return FirstPageMethods.SingletonHolder.INSTANCE;
    }

    public void init()
    {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);//超时设置

        HttpLoggingInterceptor.Level level = HttpLoggingInterceptor.Level.BODY;
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(message -> Log.i("+++3","11"));
        loggingInterceptor.setLevel(level);

        client.addInterceptor(loggingInterceptor);

        retrofit = new Retrofit.Builder()
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .build();

        service = retrofit.create(FirstPageService.class);
    }

    public void getFirst(Subscriber<FirstPageEntity> subscriber)
    {
        service.getFirst()
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }
    public void getPMData(Subscriber<WeatherData> subscriber, String locate)
    {
        service.getPMData(locate)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }
    public void searchCity(Subscriber<CityListEntity> subscriber,String location)
    {
        service.searchCity(location)
                .compose(new SchedulersTransformer<>())
                .subscribe(subscriber);
    }

    private static class SingletonHolder
    {
        private static final FirstPageMethods INSTANCE = new FirstPageMethods();
    }
}
