package com.smarthome.lilos.lilossmarthome.jsscope.method;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.smarthome.lilos.lilossmarthome.activity.user.LoginActivity;
import com.smarthome.lilos.lilossmarthome.jsscope.bean.CallbackBean;
import com.smarthome.lilos.lilossmarthome.rxbus.EventKey;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.login.LoginInterceptor;
import com.tencent.smtt.sdk.WebView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by louis on 2017/5/8.
 */

public class Login {
    private static CallbackBean bean = new CallbackBean();
    private static Map<String, Object> resutlMap = new HashMap<>();

    public static boolean login(WebView webView, Map<String, String> params,
                              String callBackFuncName)
    {
        Log.d("Login", "Webview login method.");
        bean.callBackName = callBackFuncName;
        resutlMap.clear();
        resutlMap.put("status", "success");
        bean.callBackParams = JsonUtils.objectToJson(resutlMap);
        webView.loadUrl("javascript:LL.callBack(" + JsonUtils.objectToJson(bean) + ")");

        //跳转到登陆页面
        gotoLoginActivity(webView.getContext());
        return true;
    }

    private static void gotoLoginActivity(Context context) {
        if (!SharePreferenceUtils.getLoginStatus())
        {
            Intent intent = new Intent(context, LoginActivity.class);
            context.startActivity(intent);
        }
    }

}
