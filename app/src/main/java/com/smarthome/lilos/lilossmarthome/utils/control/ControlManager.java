package com.smarthome.lilos.lilossmarthome.utils.control;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.orhanobut.logger.Logger;
import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.device.DeviceScanActivity;
import com.smarthome.lilos.lilossmarthome.activity.device.WebControlActivity;
import com.smarthome.lilos.lilossmarthome.activity.device.XinfengControlActivity;
import com.smarthome.lilos.lilossmarthome.activity.location.LocationManagerActivity;
import com.smarthome.lilos.lilossmarthome.activity.tool.QRScanActivity;
import com.smarthome.lilos.lilossmarthome.activity.tool.WebArticleActivity;
import com.smarthome.lilos.lilossmarthome.activity.weather.WeatherDetailActivity;
import com.smarthome.lilos.lilossmarthome.bluetooth.BLEService;
import com.smarthome.lilos.lilossmarthome.device.BaseDevice;
import com.smarthome.lilos.lilossmarthome.device.DeviceFactory;
import com.smarthome.lilos.lilossmarthome.device.XinfengDevice;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceTypeEntity;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 控制管理器
 * 用于管理不同设备的一个控制模式
 * 并且可以通过该管理器正确的进入不同的管理页面
 * Created by Joker on 2017/1/20.
 */

public class ControlManager
{
    private static final String CONTROL_NATIVE = "1";
    private static final String CONTROL_HTML5 = "2";
    private static final String CONTROL_HTML5_OFFLINE = "3";

    private static List<DeviceTypeEntity> mTypeList = new ArrayList<>();
    private static Map<Integer, DeviceTypeEntity> mTypeMap = new HashMap<>();

    public static void init()
    {
        if(SharePreferenceUtils.getTypeList() != null)
        {
            mTypeList.addAll(SharePreferenceUtils.getTypeList());
        }
    }

    public static void setTypeList(List<DeviceTypeEntity> data)
    {
        if (data != null)
        {
            mTypeList.clear();
            mTypeList.addAll(data);
            SharePreferenceUtils.setTypeList(mTypeList);
        }

        mTypeMap.clear();
        for (DeviceTypeEntity type : mTypeList)
        {
            mTypeMap.put(type.getId(), type);
        }
    }

    public static int getTypeStatus(int typeId)
    {
        if (mTypeMap.containsKey(typeId))
        {
            DeviceTypeEntity type = mTypeMap.get(typeId);
            return type.getStatus();
        }
        else
        {
            return -1;
        }
    }

    public static boolean isNativeControl(int typeId)
    {
        if(mTypeMap.size() == 0)
        {
            mTypeMap.clear();
            for (DeviceTypeEntity type : SharePreferenceUtils.getTypeList())
            {
                mTypeMap.put(type.getId(), type);
            }
        }

        if (mTypeMap.containsKey(typeId))
        {
            DeviceTypeEntity type = mTypeMap.get(typeId);
            if (TextUtils.equals(type.getControl_model(), CONTROL_NATIVE))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public static String getHtml5Control(int typeId)
    {
        if (mTypeMap.containsKey(typeId))
        {
            DeviceTypeEntity type = mTypeMap.get(typeId);
            return type.getControl_url();
        }
        else
        {
            return "";
        }
    }

    public static boolean isHtml5OfflineControl(int typeId)
    {
        if(mTypeMap.size() == 0)
        {
            mTypeMap.clear();
            for (DeviceTypeEntity type : SharePreferenceUtils.getTypeList())
            {
                mTypeMap.put(type.getId(), type);
            }
        }

        if (mTypeMap.containsKey(typeId))
        {
            DeviceTypeEntity type = mTypeMap.get(typeId);
            if (TextUtils.equals(type.getControl_model(), CONTROL_HTML5_OFFLINE))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public static String getHtmlOfflineVersion(int typeId)
    {
        if(mTypeMap.containsKey(typeId))
        {
            DeviceTypeEntity type = mTypeMap.get(typeId);
            return type.getControl_version();
        }
        else
        {
            return "";
        }
    }

    public static void gotoLocationManagerActivity(Context context) {
        Intent intent = new Intent(context, LocationManagerActivity.class);
        context.startActivity(intent);
    }

    public static void gotoWeatherDetailActivity(Context context,String locate,String areaId) {
        Intent intent = new Intent(context, WeatherDetailActivity.class);
        intent.putExtra("location",locate);
        intent.putExtra("areaId",areaId);
        context.startActivity(intent);
    }
    public static void goDeviceScanActivity(Context context) {
        Intent intent = new Intent(context,DeviceScanActivity.class);
        context.startActivity(intent);
    }
    public static void gotoControlActivity(Context context, BaseDevice device)
    {
        if (ControlManager.getTypeStatus(device.getTypeId()) == 0)
        {
            Intent intent = new Intent(context, QRScanActivity.class);
            Bundle deviceInfo = new Bundle();
            deviceInfo.putString(BLEService.EXTRA_DEVICE_NAME, device.getName());
            deviceInfo.putString(BLEService.EXTRA_DEVICE_ADDR, device.getAddress());
            deviceInfo.putInt(BLEService.EXTRA_CONNECT_STATE, device.getIsconnectd());
            intent.putExtras(deviceInfo);
            context.startActivity(intent);
            Logger.i("进入二维码扫描界面");
        }
        else
        {
            if (ControlManager.isNativeControl(device.getTypeId()))
            {
                if (device instanceof XinfengDevice || device.getTypeId() == DeviceFactory.XINFENG)
                {
                    Intent intent = new Intent(context, XinfengControlActivity.class);
                    Bundle deviceInfo = new Bundle();
                    deviceInfo.putString(BLEService.EXTRA_DEVICE_NAME, device.getName());
                    deviceInfo.putString(BLEService.EXTRA_DEVICE_ADDR, device.getAddress());
                    deviceInfo.putInt(BLEService.EXTRA_CONNECT_STATE, device.getIsconnectd());
                    intent.putExtras(deviceInfo);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    Logger.i("进入忻风控制界面");
                }
            }
            else if(ControlManager.isHtml5OfflineControl(device.getTypeId()))
            {
                if (device.getIsconnectd() == BLEService.STATE_CONNECTED || device.getIs_demo() == 1)
                {
                    Intent intent = new Intent(context, WebControlActivity.class);
                    Bundle deviceInfo = new Bundle();
                    deviceInfo.putString(WebControlActivity.WEB_PATH, "file:"+OfflinePackageManage.getPackagePath(device.getTypeId())+"/app.html");
                    deviceInfo.putString(BLEService.EXTRA_DEVICE_NAME, device.getName());
                    deviceInfo.putString(BLEService.EXTRA_DEVICE_ADDR, device.getAddress());
                    deviceInfo.putInt(BLEService.EXTRA_CONNECT_STATE, device.getIsconnectd());
                    intent.putExtras(deviceInfo);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    Logger.i("进入离线Web控制界面");
                }
                else
                {
                    AppToast.showToast(R.string.dev_disconnect);
                }
            }
            else
            {
                if (device.getIsconnectd() == BLEService.STATE_CONNECTED || device.getIs_demo() == 1)
                {
                    Intent intent = new Intent(context, WebControlActivity.class);
                    Bundle deviceInfo = new Bundle();
                    deviceInfo.putString(WebControlActivity.WEB_PATH, ControlManager.getHtml5Control(device.getTypeId()));
                    deviceInfo.putString(BLEService.EXTRA_DEVICE_NAME, device.getName());
                    deviceInfo.putString(BLEService.EXTRA_DEVICE_ADDR, device.getAddress());
                    deviceInfo.putInt(BLEService.EXTRA_CONNECT_STATE, device.getIsconnectd());
                    intent.putExtras(deviceInfo);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    Logger.i("进入Web控制界面");
                }
                else
                {
                    AppToast.showToast(R.string.dev_disconnect);
                }
            }
        }
    }
}
