package com.smarthome.lilos.lilossmarthome.utils;

import android.app.Application;
import android.support.annotation.StringRes;
import android.widget.Toast;

import java.lang.ref.WeakReference;

/**
 * Toast显示工具,用于自定义的Toast展示
 * Created by Joker on 2016/12/20.
 */

public class AppToast
{
    private static Toast mToast = null;
    private static WeakReference<Application> app;

    public static void init(Application application)
    {
        app = new WeakReference<>(application);
    }

    public static void showToast(String message)
    {
        if (mToast != null)
        {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(app.get(), message, Toast.LENGTH_LONG);
        mToast.show();
    }

    public static void showToast(@StringRes int resId)
    {
        if (mToast != null)
        {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(app.get(), resId, Toast.LENGTH_LONG);
        mToast.show();
    }
}
