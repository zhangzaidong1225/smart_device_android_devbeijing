package com.smarthome.lilos.lilossmarthome.activity.user;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Selection;
import android.text.Spannable;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.body.SMSBody;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.body.UserBody;
import com.smarthome.lilos.lilossmarthome.network.bean.usercenter.entity.SMSEntity;
import com.smarthome.lilos.lilossmarthome.network.retrofit.WaitingSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.UserCenterMethods;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.utils.CountlyUtil;
import com.smarthome.lilos.lilossmarthome.utils.RegexpUtils;
import com.umeng.analytics.MobclickAgent;

import rx.Subscriber;

/**
 * 忘记密码界面
 * 用于在不登录情况下修改密码
 */
public class ForgetPasswordActivity extends BaseActivity
{

    private ImageView mIvBack;
    private EditText mEtAccount;
    private EditText mEtVerifyCode;
    private EditText mEtPwd;
    private EditText mEtPwdRe;
    private Button mBtnGetCode;
    private Button mBtnCommit;
    private ImageView mIvClear;
    private ImageView mIvPswSwitch;
    private ImageView mIvPswSwitchRe;

    private String mAccount;
    private String mVerifyCode;
    private String mPwd;
    private String mPwdRe;

    private boolean isShowPsw = false;
    private boolean isShowPswRe = false;

    private int count = 60;
    private Handler verifyHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case 0:
                    if (count == 0)
                    {
                        mBtnGetCode.setEnabled(true);
                        mBtnGetCode.setText(getResources().getText(R.string.get_code));
                        count = 60;
                    }
                    else
                    {
                        String msgStr = String.format(getResources().getString(R.string.verify_waiting), count);
                        mBtnGetCode.setText(msgStr);
                        verifyHandler.sendEmptyMessageDelayed(0, 1000);
                        count--;
                    }
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_froget_password);
        CountlyUtil.recordEvent(CountlyUtil.EVENT.EVENT_FORGOT_PWD, CountlyUtil.KEY.ACCESS_TIME);
        init();
    }

    private void init()
    {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mEtAccount = (EditText) findViewById(R.id.et_account);
        mEtVerifyCode = (EditText) findViewById(R.id.et_verify_code);
        mEtPwd = (EditText) findViewById(R.id.et_password);
        mEtPwdRe = (EditText) findViewById(R.id.et_password_re);
        mBtnGetCode = (Button) findViewById(R.id.btn_get_code);
        mBtnCommit = (Button) findViewById(R.id.btn_commit);
        mIvClear = (ImageView) findViewById(R.id.iv_clear_psw);
        mIvPswSwitch = (ImageView) findViewById(R.id.iv_psw_switch);
        mIvPswSwitchRe = (ImageView) findViewById(R.id.iv_psw_switch_re);

        mEtPwd.setTypeface(Typeface.DEFAULT);
        mEtPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        mEtPwdRe.setTypeface(Typeface.DEFAULT);
        mEtPwdRe.setTransformationMethod(PasswordTransformationMethod.getInstance());

        mIvBack.setOnClickListener(v -> {
            MobclickAgent.onEvent(this, "ForgetPasswordActivity_back");
            finish();
        });
        mBtnGetCode.setOnClickListener(v -> getVerifyCode());
        mBtnCommit.setOnClickListener(v -> rePass());
        mIvClear.setOnClickListener(v -> clearVerifyCode());
        mIvPswSwitch.setOnClickListener(v -> switchPsw());
        mIvPswSwitchRe.setOnClickListener(v -> switchPswRe());

        mEtPwdRe.setOnEditorActionListener((v, actionId, event) -> {
            MobclickAgent.onEvent(this, "ForgetPasswordActivity_mEtPwdRe_OnEditorAction");
            if (actionId == EditorInfo.IME_ACTION_DONE)
            {
                rePass();
                return true;
            }
            return false;
        });
    }

    private void getVerifyCode()
    {
        MobclickAgent.onEvent(this, "ForgetPasswordActivity_getVerifyCode");
        mAccount = mEtAccount.getText().toString();
        if (mAccount.equals(""))
        {
            AppToast.showToast(R.string.phone_empty);
            return;
        }

        if (!RegexpUtils.isPhoneNum(mAccount))
        {
            AppToast.showToast(R.string.phone_error);
            return;
        }

        mBtnGetCode.setEnabled(false);
        verifyHandler.sendEmptyMessage(0);

        Subscriber<SMSEntity> subscriber = new WaitingSubscriber<SMSEntity>(ForgetPasswordActivity.this)
        {
            @Override
            public void onNext(SMSEntity smsEntity)
            {
                AppToast.showToast(R.string.verify_send_success);
            }
        };
        SMSBody body = new SMSBody(mAccount);
        UserCenterMethods.getInstance().sendSMS(subscriber, body);
    }

    private void rePass()
    {
        MobclickAgent.onEvent(this, "ForgetPasswordActivity_rePass");
        mAccount = mEtAccount.getText().toString();
        mPwd = mEtPwd.getText().toString();
        mPwdRe = mEtPwdRe.getText().toString();
        mVerifyCode = mEtVerifyCode.getText().toString();
        if (mAccount != null && mPwd != null && mPwdRe != null && !mAccount.equals("") && !mPwd.equals("") && !mPwdRe.equals("") && !mVerifyCode.equals(""))
        {
            if (!RegexpUtils.isPhoneNum(mAccount))
            {
                AppToast.showToast(R.string.phone_error);
                return;
            }

            if (!mPwd.equals(mPwdRe))
            {
                AppToast.showToast(R.string.psw_not_equal);
                return;
            }

            if (mPwd.length() <= 5)
            {
                AppToast.showToast(R.string.psw_unfilled);
                return;
            }

            if (!RegexpUtils.isMixPassword(mPwd))
            {
                AppToast.showToast(R.string.password_must_mix);
                return;
            }

            Subscriber<UserBody> subscriber = new WaitingSubscriber<UserBody>(ForgetPasswordActivity.this)
            {
                @Override
                public void onNext(UserBody userBody)
                {
                    AppToast.showToast(R.string.modify_password);
                    finish();
                }
            };
            UserBody body = new UserBody(mAccount, mPwd, mVerifyCode);
            UserCenterMethods.getInstance().resetPassword(subscriber, body);
        }
        else
        {
            AppToast.showToast(R.string.info_unfilled);
        }
    }

    private void clearVerifyCode()
    {
        MobclickAgent.onEvent(this, "ForgetPasswordActivity_clearVerifyCode");
        mEtVerifyCode.setText("");
    }

    private void switchPsw()
    {
        MobclickAgent.onEvent(this, "ForgetPasswordActivity_switchPsw");
        isShowPsw = !isShowPsw;
        if (isShowPsw)
        {
            mIvPswSwitch.setImageResource(R.drawable.ic_hide_psw);
            mEtPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        else
        {
            mIvPswSwitch.setImageResource(R.drawable.ic_show_psw);
            mEtPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        //移动光标到最后
        CharSequence text = mEtPwd.getText();
        Spannable spanText = (Spannable) text;
        Selection.setSelection(spanText, text.length());
    }

    private void switchPswRe()
    {
        MobclickAgent.onEvent(this, "ForgetPasswordActivity_switchPswRe");
        isShowPswRe = !isShowPswRe;
        if (isShowPswRe)
        {
            mIvPswSwitchRe.setImageResource(R.drawable.ic_hide_psw);
            mEtPwdRe.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        else
        {
            mIvPswSwitchRe.setImageResource(R.drawable.ic_show_psw);
            mEtPwdRe.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        //移动光标到最后
        CharSequence text = mEtPwdRe.getText();
        Spannable spanText = (Spannable) text;
        Selection.setSelection(spanText, text.length());
    }
}
