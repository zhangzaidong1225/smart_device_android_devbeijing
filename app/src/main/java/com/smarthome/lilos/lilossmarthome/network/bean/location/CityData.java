package com.smarthome.lilos.lilossmarthome.network.bean.location;

/**
 * Created by songyu on 2017/4/12.
 */

public class CityData {
	
    private int id;
    private String name;
    private int parent_id;
    private String short_name;
    private String pinyin;
    private String pinyin_first;
    private float lng;
    private float lat;
    private int level;
    private String position;
    private int sort;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getPinyin() {
        return pinyin;
    }

    public void setPinyin(String pinyin) {
        this.pinyin = pinyin;
    }

    public String getPinyin_first() {
        return pinyin_first;
    }

    public void setPinyin_first(String pinyin_first) {
        this.pinyin_first = pinyin_first;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    @Override
    public String toString() {
        return "CityData{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", parent_id=" + parent_id +
                ", short_name='" + short_name + '\'' +
                ", pinyin='" + pinyin + '\'' +
                ", pinyin_first='" + pinyin_first + '\'' +
                ", lng=" + lng +
                ", lat=" + lat +
                ", level=" + level +
                ", position='" + position + '\'' +
                ", sort=" + sort +
                '}';
    }
}
