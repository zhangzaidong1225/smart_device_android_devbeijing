package com.smarthome.lilos.lilossmarthome.activity.device;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.lianluo.lianluoIM.LianluoIM;
import com.lianluo.lianluoIM.OnDeviceListOptionListener;
import com.smarthome.lilos.lilossmarthome.APConfig.UdpSendUtils;
import com.smarthome.lilos.lilossmarthome.APConfig.WaitingDialog_Message;
import com.smarthome.lilos.lilossmarthome.APConfig.WifiFunction;
import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.activity.MainActivity;
import com.smarthome.lilos.lilossmarthome.bluetooth.BLEService;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.AddDeviceEntity;
import com.smarthome.lilos.lilossmarthome.network.retrofit.WaitingSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.MopsV2Methods;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.control.ControlManager;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;

/**
 * AP连接模式控制界面
 * 当前用户控制Wifi连接设备
 */
public class APConfigActivity extends BaseActivity implements View.OnClickListener
{
    public static final String DEVICE_AP_PREFIX = "lianluo_";
    private static final String TAG = "APConfigActivity";

    private static final String SSID_MARK = "SSID";
    private static final String PASS_MARK = "PASS";
    private static final String DEVID_MARK = "DEVID";
    private static final int MESSAGE_SHOW_OK_DIALOG = 0x1200003;
    private static final int MESSAGE_SHOW_ERRORDIALOG = 0x1200002;
    private static final int MESSAGE_SHOW_WIFILIST = 0x1200001;
    private static final int MESSAGE_NONE = 0x1200000;
    private static final int MESSAGE_GET_TARGET_AP = 0x1200004;
    private static final int MESSAGE_STOP_GET_TARGET_AP = 0x1200005;
    private static final int MESSAGE_SEND_UDP_PACKAGE = 0x1200006;
    private static final int MESSAGE_SEND_CONNECT_HOME = 0x1200007;
    private static final int MESSAGE_AUTOCONFIG_FAILED = 0x1200008;
    Button btn_adddevices = null;
    EditText et_ssid = null;
    EditText et_wifipass = null;
    ImageView iv_apconfig_back = null;
    WifiFunction myWifiFunction = null;
    UdpSendUtils myUdpSender = null;
    WaitingDialog_Message myWaitingDialog = null;
    boolean isTargetGot = false;
    boolean bHasStarted = false;
    String mClientID = null;
    String mSSID = null;
    String mCurrentBindDevice = null;
    String mCurrentBindDevicename = null;
    String mHotspot_ssid_prefix = null;
    int mCurrentBindDeviceType = -1;
    String preSSid = null;
    private List<ScanResult> meWifiList;
    Handler mHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case MESSAGE_SHOW_WIFILIST:
//                    Log.d(TAG, "handleMessage: MESSAGE_SHOW_IFILIST---->>");
//                    showWifiList();
                    break;
                case MESSAGE_SHOW_ERRORDIALOG:
//                    Log.e(TAG, "handleMessage: MESSAGE_SHOW_ERRORDIALOG ");
                    String errorString = (String) msg.obj;
                    AppToast.showToast(errorString);
                    MobclickAgent.onEvent(APConfigActivity.this, "APConfigActivity_startErrorActivity");
                    startErrorActivity();
                    break;
                case MESSAGE_GET_TARGET_AP:
                    MobclickAgent.onEvent(APConfigActivity.this, "APConfigActivity_getTargetWifiAndConnect");
                    getTargetWifiAndConnect();
                    break;
                case MESSAGE_STOP_GET_TARGET_AP:
                    MobclickAgent.onEvent(APConfigActivity.this, "APConfigActivity_onGetTargetApError");
                    onGetTargetApError();
//                    Log.d(TAG, "handleMessage: 连接AP超时， 跳转到错误页面 ");
                    break;
                case MESSAGE_SEND_UDP_PACKAGE:
                    MobclickAgent.onEvent(APConfigActivity.this, "APConfigActivity_sendUdpBroadcast");
                    sendUdpBroadcast();
                    break;
                case MESSAGE_SEND_CONNECT_HOME:
                    MobclickAgent.onEvent(APConfigActivity.this, "APConfigActivity_connectToHomeWifi");
                    connectToHomeWifi();
                    break;
                case MESSAGE_AUTOCONFIG_FAILED:
                    MobclickAgent.onEvent(APConfigActivity.this, "APConfigActivity_stopAutoConfig");
                    stopAutoConfig();
                    break;
//
//                    break;
            }
            super.handleMessage(msg);
        }
    };
    private final BroadcastReceiver wifiResultChange = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {

            String action = intent.getAction();

            if (action.equals("android.net.wifi.STATE_CHANGE"))
            {

                NetworkInfo ninfo = (NetworkInfo) intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                NetworkInfo.DetailedState state = ninfo.getDetailedState();

                //简单的处理一下：
                if ((state == NetworkInfo.DetailedState.DISCONNECTED) && (bHasStarted == true))
                {
                    if ((preSSid != null) && preSSid.startsWith(mHotspot_ssid_prefix))
                    {
                        myWaitingDialog.updateText(getResources().getString(R.string.ap_bindtip_isconntehome));
                        mHandler.sendEmptyMessage(MESSAGE_SEND_CONNECT_HOME);
                    }
                }
                else if ((state == NetworkInfo.DetailedState.CONNECTED && (bHasStarted == true)))
                {

                    if ((myWifiFunction.isWifiEnabled()))
                    {
                        String curSSid = myWifiFunction.getSSID();
                        curSSid = curSSid.replace("\"", "");
                        if ((preSSid == null) || preSSid.equals(curSSid) == false)
                        {
                            preSSid = curSSid;
                        }
//                        Log.d(TAG, "【APConfigActivity】onReceive: curSSid = "+curSSid+" mSSID = "+mSSID);
                        if (curSSid.startsWith(mHotspot_ssid_prefix))
                        {
//                            Log.d(TAG, "onReceive: 连接上智能设备的AP");
                            mHandler.sendEmptyMessage(MESSAGE_SEND_UDP_PACKAGE);
                        }
                    }

                }
            }
            else if (action.equals(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION))
            {

                NetworkInfo.DetailedState state = WifiInfo.getDetailedStateOf((SupplicantState)
                        intent.getParcelableExtra(WifiManager.EXTRA_SUPPLICANT_CONNECTED));
                SupplicantState state1 = (SupplicantState) intent.getParcelableExtra(WifiManager.EXTRA_NEW_STATE);
//               Log.d(TAG, "【APConfigActivity】 onReceive: "+WifiManager.SUPPLICANT_STATE_CHANGED_ACTION+"  DetailedState :: "+state+" SupplicantState state1 :: "+state1);

            }
            else if (action.equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION))
            {

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mqttconfig);

        initViews();
        initData();
        initRxBus();
    }

    @Override
    protected void onDestroy()
    {
        unregisterReceiver(wifiResultChange);
        if (myWaitingDialog != null)
        {
            myWaitingDialog.dismiss();
            myWaitingDialog = null;
        }
        if (myUdpSender != null)
        {
            myUdpSender.stopUdp();
            myUdpSender = null;
        }

        super.onDestroy();
    }

    private void stopAutoConfig()
    {
        if (myWaitingDialog != null)
        {
            myWaitingDialog.dismiss();
            myWaitingDialog = null;
        }
        if (myUdpSender != null)
        {
            myUdpSender.stopUdp();
            myUdpSender = null;
        }

        startErrorActivity();

    }

    private void binddevice(String string, String devicID)
    {
//        Log.d(TAG, "handleMessage: MESSAGE_STRING_DOWNLOADED == "+string);
        try
        {
            JSONObject obj = new JSONObject(string);
            String status = obj.getString("msg");
            int code = obj.getInt("code");
            if ((code == 0))
            {
//                if (status.equalsIgnoreCase("OK")){
                LianluoIM.addDevice(devicID);

//                }
            }
            else
            {
                startErrorActivity();
                AppToast.showToast(status);
            }
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void startErrorActivity()
    {

        mHandler.removeMessages(MESSAGE_AUTOCONFIG_FAILED);
        if (myWaitingDialog != null)
        {
            myWaitingDialog.dismiss();
            myWaitingDialog = null;
        }
        if (myUdpSender != null)
        {
            myUdpSender.stopUdp();
            myUdpSender = null;
        }
//        mCurrentBindDevice = null;
//        mCurrentBindDevicename =null;
        finish();
        Log.d(TAG, "startErrorActivity: mCurrentBindDevicename = " + mCurrentBindDevicename);

        Intent intent = new Intent();
        intent.setClass(APConfigActivity.this, WifiDeviceBindErrorActivity.class);
        intent.putExtra("clientid", mClientID);
        intent.putExtra("devname", mCurrentBindDevicename);
        startActivity(intent);

    }

    private void connectToHomeWifi()
    {
        String ssid = et_ssid.getText().toString().trim();
        String pass = et_wifipass.getText().toString().trim();

        for (ScanResult result : meWifiList)
        {
            if (result.SSID.equals(ssid))
            {
                connectTarget(result, pass);
                break;
            }
        }

    }

    private void startControlActivity()
    {
        Log.d(TAG, "startControlActivity: 打开控制页面");
        Intent intent = new Intent(getBaseContext(), WebControlActivity.class);
        Bundle deviceInfo = new Bundle();
        Log.d(TAG, "startControlActivity: 打开页面URL： Url = " + ControlManager.getHtml5Control(mCurrentBindDeviceType) + "mCurrentBindDevicename =" + mCurrentBindDevicename + " mCurrentBindDevice = " + mCurrentBindDevice);
        deviceInfo.putString(WebControlActivity.WEB_PATH, ControlManager.getHtml5Control(mCurrentBindDeviceType));
        deviceInfo.putString(BLEService.EXTRA_DEVICE_NAME, mCurrentBindDevicename);
        deviceInfo.putString(BLEService.EXTRA_DEVICE_ADDR, mCurrentBindDevice);
        deviceInfo.putInt(BLEService.EXTRA_CONNECT_STATE, BLEService.STATE_CONNECTED);
        intent.putExtras(deviceInfo);
        startActivity(intent);

        finish();
    }

    public String getApMessage()
    {

        String apMessage = null;

        Map apMap = new HashMap();
        apMap.put(SSID_MARK, et_ssid.getText().toString().trim());
        apMap.put(DEVID_MARK, mClientID);
        apMap.put(PASS_MARK, et_wifipass.getText().toString().trim());

        Gson gson = new Gson();
        apMessage = gson.toJson(apMap);

        Log.d(TAG, "getApMessage: apMessage = " + apMessage);

        return apMessage;
    }

    private void sendUdpBroadcast()
    {
        mHandler.removeMessages(MESSAGE_STOP_GET_TARGET_AP);

        myWaitingDialog.updateText(getResources().getString(R.string.ap_bindtip_isconfig));
        if (myUdpSender == null)
        {
            myUdpSender = new UdpSendUtils(getApMessage());
            myUdpSender.startUdp();
        }
    }

    private void onGetTargetApError()
    {
//        Toast.makeText(APConfigActivity.this,"连接AP慢，请检查设备的AP",Toast.LENGTH_SHORT).show();
        mHandler.removeMessages(MESSAGE_GET_TARGET_AP);
        mHandler.removeMessages(MESSAGE_STOP_GET_TARGET_AP);

        startErrorActivity();

    }

    /**
     * 初始化数据,获取wifi列表等工作.
     */

    private void initData()
    {
        regWifiReceiver();
        mCurrentBindDevice = null;
        mCurrentBindDevicename = null;
        mHotspot_ssid_prefix = null;

        //ClientID
        mClientID = getIntent().getStringExtra("curclientid");
        mCurrentBindDevicename = getIntent().getStringExtra("devicename");
        mCurrentBindDeviceType = getIntent().getIntExtra("devicetype", 10);
        mHotspot_ssid_prefix = getIntent().getStringExtra("hotspot_ssid_prefix");
        if (mHotspot_ssid_prefix == null)
        {
            mHotspot_ssid_prefix = DEVICE_AP_PREFIX;
        }

        //当前的ssid
        myWifiFunction = new WifiFunction(getApplicationContext());
        myWifiFunction.startScan();
        mSSID = myWifiFunction.getSSID();

        if (mSSID.startsWith("0x"))
        {
            mSSID = "";
        }
        else
        {
            mSSID = mSSID.replace("\"", "");
        }

        if (mSSID != null && mSSID.length() > 0)
        {
            et_ssid.setText(mSSID);
            et_wifipass.requestFocus();
        }
        else
        {
            et_ssid.requestFocus();
        }
//        Log.d(TAG, "初始化时设备的Wifi信息 >>initData: mSSID = "+mSSID);
    }

    /**
     * 初始化界面.
     */
    private void initViews()
    {
        et_ssid = (EditText) findViewById(R.id.et_ssid);
        et_wifipass = (EditText) findViewById(R.id.et_pass);
        btn_adddevices = (Button) findViewById(R.id.btn_adddevices);
        btn_adddevices.setOnClickListener(this);

        iv_apconfig_back = (ImageView) findViewById(R.id.iv_apconfig_back);
        iv_apconfig_back.setOnClickListener(this);
    }

    private void initRxBus()
    {
        RxBus.with(this)
                .setEvent(Events.MQTT_ADD_DEVICE)
                .onNext(events -> {
                    Log.d(TAG, "onReceive: 开始绑定设备 ");
                    String binddeviceID = (String) events.content;
                    Log.d(TAG, "onReceive: binddeviceID = " + binddeviceID);
                    startBindDevice(binddeviceID);
                })
                .create();
    }

//    private void updateWaitingTips(String tips){
//        if (myWaitingDialog!=null && myWaitingDialog.isShowing()){
//            myWaitingDialog.updateText(tips);
//        }
//    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btn_adddevices:
                MobclickAgent.onEvent(APConfigActivity.this, "APConfigActivity_startAutoConfig");
                startAutoConfig();
                break;
            case R.id.iv_apconfig_back:
                MobclickAgent.onEvent(APConfigActivity.this, "APConfigActivity_startMainActivity");
                finish();
                startMainActivity();
                break;

        }
    }

    public void startMainActivity()
    {
        Intent intent = new Intent();
        intent.setClass(APConfigActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void startAutoConfig()
    {
        //显示等待框
        if (myWaitingDialog == null)
        {
            myWaitingDialog = new WaitingDialog_Message(APConfigActivity.this);
        }
        myWaitingDialog.show();
        bHasStarted = true;

        //连接AP Wifi网络
        mHandler.sendEmptyMessageDelayed(MESSAGE_GET_TARGET_AP, 1000);
        mHandler.sendEmptyMessageDelayed(MESSAGE_STOP_GET_TARGET_AP, 30000);
        mHandler.sendEmptyMessageDelayed(MESSAGE_AUTOCONFIG_FAILED, 60000);
        myWaitingDialog.updateText(getResources().getString(R.string.ap_bindtip_isconnectap));
//        getTargetWifiAndConnect();

    }

    private void regWifiReceiver()
    {

        IntentFilter labelIntentFilter = new IntentFilter();
        labelIntentFilter.addAction("android.net.wifi.STATE_CHANGE");
        labelIntentFilter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        labelIntentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        labelIntentFilter.setPriority(1000); // 设置优先级，最高为1000
        registerReceiver(wifiResultChange, labelIntentFilter);

    }

    private void startBindDevice(String devid)
    {
        if (mCurrentBindDevice == null || mCurrentBindDevice.equals(devid) == false)
        {
            mCurrentBindDevice = devid;
//        URLManager urlmng = new URLManager();
//        String url =urlmng.getDevAddUrl(uid,,devid,10);
//        Log.d(TAG, "startBindDevice: url = "+url);

            mHandler.removeMessages(MESSAGE_STOP_GET_TARGET_AP);
            mHandler.removeMessages(MESSAGE_AUTOCONFIG_FAILED);
            mHandler.removeMessages(MESSAGE_SEND_CONNECT_HOME);
            mHandler.removeMessages(MESSAGE_SEND_UDP_PACKAGE);
            // TODO: 2016/8/18 生成url
//        new Downloader(url,mHandler).start();
            Log.d(TAG, "startBindDevice: 》》》》》  mCurrentBindDevice " + mCurrentBindDevice);
            addDevice(mCurrentBindDevicename, mCurrentBindDevice, mCurrentBindDeviceType);
        }
    }

    public void getTargetWifiAndConnect()
    {

        if (myWifiFunction == null)
        {

            myWifiFunction = new WifiFunction(APConfigActivity.this);

        }
        meWifiList = myWifiFunction.getWifiList();
        for (ScanResult result : meWifiList)
        {
            Log.d(TAG, "getTargetWifiAndConnect: result = " + result.SSID);
            if (result.SSID.startsWith(mHotspot_ssid_prefix))
            {
                Log.d(TAG, "【找到了目标SSID】getTargetWifiAndConnect: result.SSID " + result.SSID);
                isTargetGot = true;
                connectTarget(result, "");
                break;
            }
        }
        if (isTargetGot == false)
        {
            mHandler.sendEmptyMessageDelayed(MESSAGE_GET_TARGET_AP, 1000);
        }
    }


    private void connectTarget(ScanResult result, String pass)
    {
        WifiConfiguration config = myWifiFunction.CreateWifiInfo2(result, pass);
        myWifiFunction.addNetWork(config);
    }

    private void addDevice(String name, String devId, int typeId)
    {
        Subscriber<AddDeviceEntity> subscriber = new WaitingSubscriber<AddDeviceEntity>(APConfigActivity.this)
        {
            @Override
            public void onNext(AddDeviceEntity addDeviceEntity)
            {
                onAddDeviceResult(addDeviceEntity);
            }

            @Override
            public void onError(Throwable e)
            {
                super.onError(e);
                if(e instanceof HttpException)
                {
                    if(((HttpException) e).code() == 422)
                    {
                        mHandler.removeMessages(MESSAGE_STOP_GET_TARGET_AP);
                        mHandler.removeMessages(MESSAGE_AUTOCONFIG_FAILED);
                        startControlActivity();
                    }
                    else
                    {
                        Message msg = mHandler.obtainMessage();
                        msg.what = MESSAGE_SHOW_ERRORDIALOG;
                        msg.obj = ((HttpException) e).message();
                        mHandler.sendMessage(msg);
                    }
                }
            }
        };
        MopsV2Methods.getInstance().addDevice(subscriber,name,devId,typeId);
    }

    private void onAddDeviceResult(AddDeviceEntity entity)
    {
        LianluoIM.addDevice(mCurrentBindDevice);
        LianluoIM.setDevOptionLister(new OnDeviceListOptionListener()
        {
            @Override
            public void onDeviceListChanged(String s)
            {
                if (s.equalsIgnoreCase("add"))
                {
                    if (myWaitingDialog != null)
                    {
                        myWaitingDialog.dismiss();
                        myWaitingDialog = null;
                    }
                    if (myUdpSender != null)
                    {
                        myUdpSender.stopUdp();
                        myUdpSender = null;
                    }
                    mHandler.removeMessages(MESSAGE_STOP_GET_TARGET_AP);
                    mHandler.removeMessages(MESSAGE_AUTOCONFIG_FAILED);
                    AppToast.showToast(R.string.ap_config_bindok);
                    startControlActivity();
                }
            }

            @Override
            public void onDeviceOptionFailed(String s)
            {
                if (myWaitingDialog != null)
                {
                    myWaitingDialog.dismiss();
                    myWaitingDialog = null;
                }
                if (myUdpSender != null)
                {
                    myUdpSender.stopUdp();
                    myUdpSender = null;
                }
                AppToast.showToast(R.string.ap_config_binderror);
                startErrorActivity();
            }
        });
    }
}
