package com.smarthome.lilos.lilossmarthome.wigdet;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.Window;

import com.smarthome.lilos.lilossmarthome.R;

/**
 * 等待Dialog,用于耗时操作时前台显示
 * Created by Joker on 2016/5/26.
 */
public class WaitingDialog
{
    Context context;
    Dialog dialog;

    /**
     * 构造方法
     *
     * @param context 上下文对象
     */
    public WaitingDialog(Context context)
    {
        this.context = context;
        //初始化控件
        initViews();
    }

    /**
     * 初始化控件
     */
    private void initViews()
    {
        dialog = new Dialog(context, R.style.LoadingStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_waiting);
    }

    /**
     * 显示Dialog
     */
    public void show()
    {
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setGravity(Gravity.CENTER);
        dialogWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));

        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    /**
     * 释放Dialog
     */
    public void dismiss()
    {
        dialog.dismiss();
    }

    /**
     * 设置是否可以取消
     *
     * @param flag 是否可以取消
     */
    public void setCancelable(boolean flag)
    {
        dialog.setCancelable(flag);
    }

    /**
     * 设置点击其他区域是否可以取消
     *
     * @param flag 是否可以取消
     */
    public void setCanceledOnTouchOutside(boolean flag)
    {
        dialog.setCanceledOnTouchOutside(false);
    }

    public void setOnCancelListener(DialogInterface.OnCancelListener listener)
    {
        dialog.setOnCancelListener(listener);
    }

    public boolean isShowing()
    {
        return dialog.isShowing();
    }
}
