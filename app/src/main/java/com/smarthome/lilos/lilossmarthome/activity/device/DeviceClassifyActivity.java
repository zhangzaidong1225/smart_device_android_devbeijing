package com.smarthome.lilos.lilossmarthome.activity.device;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.adapter.DeviceTypeAdapter;
import com.smarthome.lilos.lilossmarthome.bluetooth.BLEService;
import com.smarthome.lilos.lilossmarthome.network.bean.DeviceTypeListEntity;
import com.umeng.analytics.MobclickAgent;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 *设备类型选择页面
 * 用于在手动搜索链接时，选择对应的设备类型
 */
public class DeviceClassifyActivity extends BaseActivity
        implements View.OnClickListener, AdapterView.OnItemClickListener
{

    private ImageView mIvBack;
    private ListView mLvDevicesClassification;
    private DeviceTypeAdapter mDeviceTypeAdapter;
    private List<DeviceTypeListEntity.DevTypeListData> mDeviceTypeList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_classify);
        init();
        getData();
    }

    private void getData()
    {
        String data = getIntent().getStringExtra("dev_type_list");

        Type type = new TypeToken<List<DeviceTypeListEntity.DevTypeListData>>() {}.getType();
        List<DeviceTypeListEntity.DevTypeListData> devTypeListData = new Gson().fromJson(data, type);
//        Log.d("deviceClassifyActivity","   "+devTypeListData.toString());
        mDeviceTypeList.clear();
        mDeviceTypeList.addAll(devTypeListData);
        mDeviceTypeAdapter.notifyDataSetChanged();
    }

    private void init()
    {
        mLvDevicesClassification = (ListView) findViewById(R.id.lv_device_classification);
        mIvBack = (ImageView) findViewById(R.id.ib_back);

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnFail(R.drawable.load_error)
                .showImageOnLoading(R.drawable.load_error)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        mDeviceTypeAdapter = new DeviceTypeAdapter(getBaseContext(), mDeviceTypeList, R.layout.listitem_device_type, options);
        mLvDevicesClassification.setAdapter(mDeviceTypeAdapter);

        mLvDevicesClassification.setOnItemClickListener(this);
        mIvBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.ib_back:
                MobclickAgent.onEvent(this, "DeviceClassifyActivity_back");
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        gotoDevicePowerOnPromptActivity(position);
    }

    private void gotoDevicePowerOnPromptActivity(int position)
    {
        MobclickAgent.onEvent(this, "DeviceClassifyActivity_gotoDevicePowerOnPromptActivity");
        Intent intent = new Intent(this, DevicePowerOnPromptActivity.class);
        intent.putExtra(BLEService.EXTRA_DEVICE_TYPE, mDeviceTypeAdapter.getItem(position).getTypeId());
        intent.putExtra(BLEService.EXTRA_DEVICE_NAME, mDeviceTypeAdapter.getItem(position).getTypeName());
        startActivity(intent);
    }
}
