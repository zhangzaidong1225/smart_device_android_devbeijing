package com.smarthome.lilos.lilossmarthome.activity.user;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.device.BaseBluetoothDevice;
import com.smarthome.lilos.lilossmarthome.device.BaseDevice;
import com.smarthome.lilos.lilossmarthome.device.DeviceManager;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceTypeEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.TokenEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.UpdateDeviceEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.UserDeviceEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.synchro.SynchroData;
import com.smarthome.lilos.lilossmarthome.network.retrofit.BaseSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.WaitingSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.MopsV2Methods;
import com.smarthome.lilos.lilossmarthome.rxbus.EventKey;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.utils.JsonUtils;
import com.smarthome.lilos.lilossmarthome.utils.RegexpUtils;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.control.ControlManager;
import com.smarthome.lilos.lilossmarthome.utils.control.OfflinePackageManage;
import com.smarthome.lilos.lilossmarthome.utils.login.LoginCarrier;
import com.smarthome.lilos.lilossmarthome.utils.login.LoginInterceptor;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import rx.Subscriber;

/**
 * 登录页面
 * 用于账户登录
 * 包含有登录过滤器,用于在各个需要登录的界面在未登录时进入登录界面进行登录
 * Edited by Joker on 2016/7/19.添加登录过滤器
 */
public class LoginActivity extends BaseActivity
{
    private static final int REGISTER_ID = 10000;
    private static final int REPASS_ID = 10001;

    private LoginCarrier invoker;
    private int[] flags;

    private ImageView mIvBack;
    private EditText mEtAccount;
    private EditText mEtPassword;
    private ImageView mIvClearNum;
    private ImageView mIvClear;
    private TextView mTvRegister;
    private TextView mTvForgetPwd;
    private Button mBtnLogin;

    private Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case 0:
                    invoker = getIntent().getParcelableExtra(LoginInterceptor.mINVOKER);
                    flags = getIntent().getIntArrayExtra(LoginInterceptor.mFLAG);

                    if (invoker != null)
                    {
                        invoker.invoke(LoginActivity.this, flags);
                    }
                    finish();
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
    }

    private void init()
    {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mEtAccount = (EditText) findViewById(R.id.et_account);
        mEtPassword = (EditText) findViewById(R.id.et_password);
        mIvClearNum = (ImageView) findViewById(R.id.iv_clear_num);
        mIvClear = (ImageView) findViewById(R.id.iv_clear_psw);
        mTvRegister = (TextView) findViewById(R.id.tv_register);
        mTvForgetPwd = (TextView) findViewById(R.id.tv_forget_pwd);
        mBtnLogin = (Button) findViewById(R.id.btn_login);

        mEtPassword.setTypeface(Typeface.DEFAULT);
        mEtPassword.setTransformationMethod(new PasswordTransformationMethod());

        mIvBack.setOnClickListener(v -> {
            MobclickAgent.onEvent(this, "LoginActivity_back");
            finish();
        });
        mIvClearNum.setOnClickListener(v -> clearNum());
        mIvClear.setOnClickListener(v -> clearPsw());
        mTvRegister.setOnClickListener(v -> gotoRegisterActivity());
        mTvForgetPwd.setOnClickListener(v -> gotoForgetPasswordActivity());
        mBtnLogin.setOnClickListener(v -> login());

        mEtPassword.setOnEditorActionListener((v, actionId, event) -> {
            MobclickAgent.onEvent(this, "LoginActivity_mEtPassword_EditorAction");
            if (actionId == EditorInfo.IME_ACTION_DONE)
            {
                login();
                return true;
            }
            return false;
        });
    }

    private void login()
    {
        MobclickAgent.onEvent(this, "LoginActivity_login");
        String account = mEtAccount.getText().toString();
        String password = mEtPassword.getText().toString();
        if (!account.equals("") && !password.equals(""))
        {
            if (!RegexpUtils.isPhoneNum(account))
            {
                AppToast.showToast(R.string.phone_error);
                return;
            }

            Subscriber<TokenEntity> subscriber = new WaitingSubscriber<TokenEntity>(LoginActivity.this) {
                @Override
                public void onNext(TokenEntity tokenEntity) {
                    onLoginResult(tokenEntity);
                }
            };
            String clint = SharePreferenceUtils.getMqttClient();
            if (SmartHomeApplication.myClientID != null) {
                MopsV2Methods.getInstance().login(subscriber, account, password, SmartHomeApplication.myClientID);
            } else if (clint != null) {
                MopsV2Methods.getInstance().login(subscriber, account, password, clint);
            } else {
                MopsV2Methods.getInstance().login(subscriber, account, password, "86847661-8957-32e6-96fe-96daaa74d171");
            }
        }
        else
        {
            AppToast.showToast(R.string.info_unfilled);
        }
    }

    private void gotoRegisterActivity()
    {
        MobclickAgent.onEvent(this, "LoginActivity_gotoRegisterActivity");
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivityForResult(intent, REGISTER_ID);
    }

    private void gotoForgetPasswordActivity()
    {
        MobclickAgent.onEvent(this, "LoginActivity_gotoForgetPasswordActivity");
        Intent intent = new Intent(this, ForgetPasswordActivity.class);
        startActivityForResult(intent, REPASS_ID);
    }

    private void clearPsw()
    {
        MobclickAgent.onEvent(this, "LoginActivity_clearPsw");
        mEtPassword.setText("");
    }

    private void clearNum()
    {
        MobclickAgent.onEvent(this, "LoginActivity_clearNum");
        mEtAccount.setText("");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == RESULT_OK)
        {
            switch (requestCode)
            {
                case REGISTER_ID:
                    String mAccount = data.getStringExtra("account");
                    mEtAccount.setText(mAccount);
                    break;
            }
        }
    }

    private void getDeviceTypeList()
    {
        Subscriber<List<DeviceTypeEntity>> subscriber = new BaseSubscriber<List<DeviceTypeEntity>>()
        {
            @Override
            public void onNext(List<DeviceTypeEntity> entities)
            {
                onDeviceTypeListResult(entities);
            }
        };
        MopsV2Methods.getInstance().types(subscriber);
    }

    private void getDeviceList()
    {
        Subscriber<List<UserDeviceEntity>> subscriber = new BaseSubscriber<List<UserDeviceEntity>>()
        {
            @Override
            public void onNext(List<UserDeviceEntity> userDeviceEntities)
            {
                onDeviceListResult(userDeviceEntities);
                handler.sendEmptyMessage(0);
            }

            @Override
            public void onError(Throwable e)
            {
                super.onError(e);
                handler.sendEmptyMessage(0);
            }
        };
        MopsV2Methods.getInstance().devices(subscriber);
    }

    private void onDeviceListResult(List<UserDeviceEntity> entities)
    {
        SmartHomeApplication.mBluetoothService.getDeviceList(entities);
        OfflinePackageManage.updateOfflinePackage();
    }

    private void onLoginResult(TokenEntity entity)
    {
        MopsV2Methods.getInstance().setToken(entity.getAccess_token());
        AppToast.showToast(R.string.login_success);
        SharePreferenceUtils.setMobileNum(mEtAccount.getText().toString().trim());
        SharePreferenceUtils.setLoginStatus(true);
        Log.d("===","token--" + entity.getAccess_token());

        SharePreferenceUtils.setAccessToken(entity.getAccess_token());
        SharePreferenceUtils.setRefreshToken(entity.getRefresh_token());

        RxBus.getInstance().send(Events.LOGIN_SUCCESS,null);
        synchroDevice(setData());

//        if (SharePreferenceUtils.getTypeList() != null)
//        {
//            getDeviceList();
//        } else
//        {
//            getDeviceTypeList();
//        }
//        for (String address : SmartHomeApplication.myDeviceManager.mDataOff.keySet())
//        {
//            if (SmartHomeApplication.myDeviceManager.mDataOff.get(address) instanceof BaseBluetoothDevice)
//            {
//                SmartHomeApplication.mBluetoothService.disconnect(address);
//            }
//        }
    }

    private void onDeviceTypeListResult(List<DeviceTypeEntity> entities)
    {
        ControlManager.setTypeList(entities);
        getDeviceList();
    }

    private List setData(){
        List<SynchroData> synchroDatas = new ArrayList<>();

        Collection values = DeviceManager.getInstance().mDataOff.values();
        for (Iterator iterator = values.iterator(); iterator.hasNext(); ) {
            Object obj = iterator.next();
            BaseDevice baseDevice = (BaseDevice) obj;
            SynchroData synchroData = new SynchroData();
            synchroData.setName(baseDevice.getName());
            synchroData.setDevice_id(baseDevice.getAddress());
            synchroData.setDevice_type_id(baseDevice.getTypeId());
            synchroDatas.add(synchroData);
        }
        return synchroDatas;
    }

    /**
     * 同步设备
     * @param synchroDatas
     */
    private void synchroDevice(List<SynchroData> synchroDatas)
    {

        Subscriber<List<UserDeviceEntity>> subscriber = new WaitingSubscriber<List<UserDeviceEntity>>(this)
        {
            @Override
            public void onNext(List<UserDeviceEntity> userDeviceEntity)
            {
//                Log.d("```", "login-1--"+JsonUtils.objectToJson(userDeviceEntity));
//                Log.d("```","login-1--"+SharePreferenceUtils.getTypeList()+"");
//                DeviceManager.getInstance().mDataOff.clear();

//                getDeviceList();
                onDeviceListResult(userDeviceEntity);
                handler.sendEmptyMessage(0);
//                if (SharePreferenceUtils.getTypeList() != null)
//                {
//                    getDeviceList();
//                } else
//                {
//                    getDeviceTypeList();
//                }
            }

            @Override
            public void onError(Throwable e)
            {
                super.onError(e);
                Log.d("```", "login-2--"+e.getMessage());

            }
        };
        MopsV2Methods.getInstance().synchroDevices(subscriber,synchroDatas);
    }
}
