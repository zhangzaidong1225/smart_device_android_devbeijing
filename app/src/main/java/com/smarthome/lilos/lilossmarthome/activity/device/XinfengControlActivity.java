package com.smarthome.lilos.lilossmarthome.activity.device;

import android.Manifest;
import android.animation.Animator;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.text.style.SuperscriptSpan;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.logger.Logger;
import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.bluetooth.BLEService;
import com.smarthome.lilos.lilossmarthome.bluetooth.DfuService;
import com.smarthome.lilos.lilossmarthome.bluetooth.analysis.UartUtil;
import com.smarthome.lilos.lilossmarthome.device.BaseBluetoothDevice;
import com.smarthome.lilos.lilossmarthome.device.DeviceFactory;
import com.smarthome.lilos.lilossmarthome.device.DeviceManager;
import com.smarthome.lilos.lilossmarthome.device.XinfengDevice;
import com.smarthome.lilos.lilossmarthome.jsscope.method.Login;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.DeviceFirmwareEntity;
import com.smarthome.lilos.lilossmarthome.network.retrofit.WaitingSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.MopsV2Methods;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.utils.CountlyUtil;
import com.smarthome.lilos.lilossmarthome.utils.DevUpdateUtil;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.SystemUtils;
import com.smarthome.lilos.lilossmarthome.utils.XinfengUtlis;
import com.smarthome.lilos.lilossmarthome.utils.control.WaveTransitionManager;
import com.smarthome.lilos.lilossmarthome.utils.login.LoginInterceptor;
import com.smarthome.lilos.lilossmarthome.wigdet.ColorArcProgressBar;
import com.smarthome.lilos.lilossmarthome.wigdet.ProgressBarDialog;
import com.smarthome.lilos.lilossmarthome.wigdet.SurfaceWaveView;
import com.umeng.analytics.MobclickAgent;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;

import no.nordicsemi.android.dfu.DfuProgressListener;
import no.nordicsemi.android.dfu.DfuProgressListenerAdapter;
import no.nordicsemi.android.dfu.DfuServiceInitiator;
import no.nordicsemi.android.dfu.DfuServiceListenerHelper;
import okhttp3.internal.Util;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * 忻风控制界面
 * 专门用于控制忻风设备
 */
public class XinfengControlActivity extends BaseActivity implements UartUtil.UartListener ,SeekBar.OnSeekBarChangeListener
{

    private static int FORCED_UPDATE_MIN_VERSION = 40;
    DeviceManager myDeviceManager = null;
    private String mDeviceAddress;


    private String mDeviceName;

    private FrameLayout mFlLoading;
    private TextView mTvTitle;
    private ImageView mIvBack;
    private ImageView mIvUpload;
    private ImageView mIvShare;
    private ColorArcProgressBar mCaprFilterValue;
    private ColorArcProgressBar mCaprFilterValueBg;
    private TextView mTvFilterValue;
    private TextView mTvFilterUnit;
    private TextView mTvFilterUnitOff;
    private ImageView mIvPower;
    private TextView mTvFilterConnotValue;
    private ImageView mIvPowerSwitchOff;
//    private WaveView mWvWvae;
    private SurfaceWaveView mWvWvae;
    private TextView mTxtFanSpeed;

    private RelativeLayout mRlDevicePowerOn;
    private RelativeLayout mRlDevicePowerOff;
    private ImageView mIvPowerSwitchOn;

    private ImageView mIvFanLeft;
    private ImageView mIvFanRight;
    private WaveTransitionManager waveTransitionManager;


    private XinfengDevice mXinfengDevice;
    private long mReference;
    private int mFileType = DfuService.TYPE_AUTO;
    private Uri mFileStreamUri;
    private int lastSpeed = -1;
    private int defSpeed = 80;
    private int defSpeedLevel = 2;
    private int[] speedLevels = {30, 50, 100};
    private boolean aniRunning = false;
    private DeviceFirmwareEntity mEntity;
    private DevUpdateUtil mDevUpdateUtil;
    private ProgressBarDialog progressBarDialog;
    private boolean isUploading = false;
    private boolean isUILock = false;
    private boolean isSleep = true;
    private UILockHandler uiLockHandler;
    private SpannableString dm3SpStr;
    private SpannableString m3SpStr;

    private long startTime;
    private long endTime;
    private SeekBar mSeekBar;
    private int setSpeed = -1;


    private boolean inControlActivity = true;
    private boolean isForcedUpdate = false;

    private BroadcastReceiver mDownloadReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            if (action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
            {
                long reference = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                if (reference == mReference)
                {
                    AppToast.showToast(R.string.upload_download_complete);
                    progressBarDialog.setContent(getResources().getString(R.string.upload_preparing));
                }
                if (mDevUpdateUtil.checkFileMd5(mEntity.getMd5()))
                {
                    startDfuService();
                }
            }
        }
    };

    private BroadcastReceiver mDfuUpdateReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            if (action.equals(DfuService.BROADCAST_PROGRESS))
            {
                int progress = intent.getIntExtra(DfuService.EXTRA_PROGRESS, 0);
            }
        }
    };

    private DfuProgressListener mDfuProgressListener = new DfuProgressListenerAdapter()
    {
        @Override
        public void onDeviceConnecting(final String deviceAddress)
        {
            Log.d("---","---onDeviceConnecting");
            progressBarDialog.setContent(getResources().getString(R.string.upload_connecting));
        }

        @Override
        public void onDfuProcessStarting(final String deviceAddress)
        {
            Log.d("---","---onDfuProcessStarting");

            progressBarDialog.setContent(getResources().getString(R.string.upload_init));
        }

        @Override
        public void onEnablingDfuMode(final String deviceAddress)
        {
            Log.d("---","---onEnablingDfuMode");

        }

        @Override
        public void onFirmwareValidating(final String deviceAddress)
        {
            Log.d("---","---onFirmwareValidating");

        }

        @Override
        public void onDeviceDisconnecting(final String deviceAddress)
        {
            Log.d("---","---onDeviceDisconnecting");

//            final NotificationManager manager = (NotificationManager) XinfengControlActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
////            manager.cancel(DfuService.NOTIFICATION_ID);
//            manager.cancelAll();
        }

        @Override
        public void onDfuCompleted(final String deviceAddress)
        {
            Log.d("---","---onDfuCompleted");

            progressBarDialog.setProgress(100);

            RxBus.getInstance().send(Events.DEVICE_UPDATE_COMPLETE,deviceAddress);
            if (LoginInterceptor.getLogin()) {
                ((BaseBluetoothDevice) myDeviceManager.mData.get(mXinfengDevice.getAddress())).setAutoConnect(true);
            } else {
                ((BaseBluetoothDevice) myDeviceManager.mDataOff.get(mXinfengDevice.getAddress())).setAutoConnect(true);
            }
            isUploading = false;
            progressBarDialog.dismiss();
            AppToast.showToast(R.string.update_success);
            //升级成功，重置设备版本号
            mXinfengDevice.setVersion(Integer.valueOf(mEntity.getVersion()));
            final NotificationManager manager = (NotificationManager) XinfengControlActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
//            manager.cancel(DfuService.NOTIFICATION_ID);
            manager.cancelAll();

            finish();
        }

        @Override
        public void onDfuAborted(final String deviceAddress)
        {
            Log.d("---","---onDfuAborted");

            isUploading = false;
            progressBarDialog.dismiss();
        }

        @Override
        public void onProgressChanged(final String deviceAddress, final int percent,
                                      final float speed, final float avgSpeed,
                                      final int currentPart, final int partsTotal)
        {
            Log.d("---","---onProgressChanged");

            progressBarDialog.setContent(getResources().getString(R.string.upload_uploading));
            progressBarDialog.setProgress(percent);
        }

        @Override
        public void onError(final String deviceAddress, final int error, final int errorType,
                            final String message)
        {
            Log.d("---","---onError");

            if (LoginInterceptor.getLogin()) {
                ((BaseBluetoothDevice) myDeviceManager.mData.get(mXinfengDevice.getAddress())).setAutoConnect(true);
            } else {
                ((BaseBluetoothDevice) myDeviceManager.mDataOff.get(mXinfengDevice.getAddress())).setAutoConnect(true);
            }
            AppToast.showToast(message);
            isUploading = false;
            progressBarDialog.dismiss();
            final NotificationManager manager = (NotificationManager) XinfengControlActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
//            manager.cancel(DfuService.NOTIFICATION_ID);
            manager.cancelAll();
            finish();
        }
    };

    private static IntentFilter makeDfuUpdateIntentFilter()
    {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DfuService.BROADCAST_PROGRESS);
        intentFilter.addAction(DfuService.BROADCAST_ERROR);
        intentFilter.addAction(DfuService.BROADCAST_LOG);
        return intentFilter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.e("波浪=====ee="," ==setSpeed=1==："+setSpeed);
        listenClose = false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xinfeng_control);
        Bundle deviceInfo = getIntent().getExtras();
        mDeviceName = deviceInfo.getString(BLEService.EXTRA_DEVICE_NAME);
        mDeviceAddress = deviceInfo.getString(BLEService.EXTRA_DEVICE_ADDR);
        init();
        initRxBus();
//        initAnimations();
        getData();
        RxBus.getInstance().send(Events.RETURN_MAIN_ACTIVITY);
        UartUtil.setListener(this);
        startTime = System.currentTimeMillis();
        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(mDownloadReceiver, filter);

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mDfuUpdateReceiver, makeDfuUpdateIntentFilter());
        DfuServiceListenerHelper.registerProgressListener(getApplicationContext(), mDfuProgressListener);

    }

    private void init()
    {
        dm3SpStr = new SpannableString("dm3");
        dm3SpStr.setSpan(new RelativeSizeSpan(0.5f),2,3,Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        dm3SpStr.setSpan(new SuperscriptSpan(),2,3, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        m3SpStr = new SpannableString("m3");
        m3SpStr.setSpan(new RelativeSizeSpan(0.5f),1,2,Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        m3SpStr.setSpan(new SuperscriptSpan(),1,2,Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        mDevUpdateUtil = new DevUpdateUtil(getApplicationContext());
        mFlLoading = (FrameLayout) findViewById(R.id.fl_loading);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvUpload = (ImageView) findViewById(R.id.iv_upload);
        mIvShare = (ImageView) findViewById(R.id.iv_share);
        mCaprFilterValue = (ColorArcProgressBar) findViewById(R.id.capb_filter_value);
        mCaprFilterValueBg = (ColorArcProgressBar) findViewById(R.id.capb_filter_value_bg);
        mTvFilterValue = (TextView) findViewById(R.id.tv_filter_value);
        mTvFilterUnit = (TextView) findViewById(R.id.tv_filter_unit);
        mTvFilterUnitOff = (TextView) findViewById(R.id.tv_filter_unit_off);
        mIvPower = (ImageView) findViewById(R.id.iv_power);
        mTvFilterConnotValue = (TextView) findViewById(R.id.tv_filter_connot_value);
        mIvPowerSwitchOff = (ImageView) findViewById(R.id.iv_power_switch);
       // mTxtFanSpeed = (TextView) findViewById(R.id.lb_fan_speed);
/*        mIvFanLevel1 = (ImageView) findViewById(R.id.iv_fan_level_1);
        mIvFanLevel2 = (ImageView) findViewById(R.id.iv_fan_level_2);
        mIvFanLevel3 = (ImageView) findViewById(R.id.iv_fan_level_3);*/
        mRlDevicePowerOn = (RelativeLayout) findViewById(R.id.rl_device_power_on);
        mRlDevicePowerOff = (RelativeLayout) findViewById(R.id.rl_device_power_off);
        mIvPowerSwitchOn = (ImageView) findViewById(R.id.iv_power_switch_on);

        mTvFilterUnitOff.setText(dm3SpStr);
        mSeekBar = (SeekBar) findViewById(R.id.timeline);
        mSeekBar.setOnSeekBarChangeListener(this);
        mIvFanLeft = (ImageView)findViewById(R.id.iv_fan_left);
        mIvFanRight = (ImageView)findViewById(R.id.iv_fan_right);
//        mWvWvae = (WaveView) findViewById(R.id.wv_wave);
        mWvWvae = (SurfaceWaveView) findViewById(R.id.wv_wave);

//        ViewGroup.LayoutParams lp = mWvWvae.getLayoutParams();
//        lp.height = SystemUtils.dip2px(getApplicationContext(), 188.4f) - SystemUtils.getBottomStatusHeight(getApplicationContext());
//        mWvWvae.setLayoutParams(lp);
        int bottomHeight = SystemUtils.getBottomStatusHeight(getApplicationContext());
        mWvWvae.setWaterDepth(140 - SystemUtils.px2dip(getApplicationContext(), bottomHeight));

        LinearLayout container = (LinearLayout) findViewById(R.id.container);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) container.getLayoutParams();
        layoutParams.bottomMargin =  SystemUtils.dip2px(getApplicationContext(), 180) - bottomHeight;
        container.setLayoutParams(layoutParams);
/*        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mTxtFanSpeed.getLayoutParams();
        layoutParams.bottomMargin =  SystemUtils.dip2px(getApplicationContext(), 230) - bottomHeight;
        mTxtFanSpeed.setLayoutParams(layoutParams);*/


//        mWvWvae.setWaterLevelRatio(0.8848f);
//        mWvWvae.setAmplitudeRatio(0.0001f);
//        waveHelper = new WaveHelper(mWvWvae);
//        waveHelper.setSpeed(new int[]{2000, 1500, 1200, 1000});
//        waveHelper.setDuration(3000);
//        waveHelper.setAmplitude(new float[]{0.0001f, 0.0384f, 0.0768f, 0.1152f});

        waveTransitionManager = new WaveTransitionManager(mWvWvae);
        waveTransitionManager.startThread();

        mIvBack.setOnClickListener(v -> {
            MobclickAgent.onEvent(this, "XinfengControlActivity_back");
            finish();
        });
        mIvUpload.setOnClickListener(v -> gotoUpload());
        mIvShare.setOnClickListener(v -> gotoShare());
        mIvPowerSwitchOn.setOnClickListener(v -> gotoPowerOn());
        mIvPowerSwitchOff.setOnClickListener(v -> gotoPowerOff());
        container.setOnClickListener(v ->isShowToast());
/*        mIvFanLevel1.setOnClickListener(v -> controlSpeed(v));
        mIvFanLevel2.setOnClickListener(v -> controlSpeed(v));
        mIvFanLevel3.setOnClickListener(v -> controlSpeed(v));*/

        progressBarDialog = new ProgressBarDialog(this);

        uiLockHandler = new UILockHandler(this);

        myDeviceManager = DeviceManager.getInstance();
    }

    private void initRxBus()
    {
        RxBus.with(this)
                .setEvent(Events.GATT_DISCONNECTED)
                .onNext(events -> {
                    String disAddress = (String) events.content;
                    if (TextUtils.equals(disAddress, mDeviceAddress))
                    {
                        if (mXinfengDevice == null)
                        {
                            finish();
                            return;
                        }
                        mXinfengDevice.disconnect();
                        if (!isUploading)
                        {
                            AppToast.showToast(R.string.device_disconnect);
                            finish();
                        }
                    }
                })
                .create();

        RxBus.with(this)
                .setEvent(Events.GATT_DATA_CHANGED)
                .onNext(events -> {
                    if (LoginInterceptor.getLogin()) {
                        mXinfengDevice = (XinfengDevice) myDeviceManager.mData.get(mDeviceAddress);
                        Log.e("-------","uiShutdown1");
                        updateUI(mXinfengDevice);
                    } else {
                        mXinfengDevice = (XinfengDevice) myDeviceManager.mDataOff.get(mDeviceAddress);
                        Log.e("-------","uiShutdown2"+mXinfengDevice.getSpeed());
                        updateUI(mXinfengDevice);
                    }
                })
                .create();

        RxBus.with(this)
                .setEvent(Events.DEVICE_NOT_SUPPORT_UART)
                .onNext(events -> SmartHomeApplication.mBluetoothService.disconnect(mDeviceAddress))
                .create();
    }

    private void getData()
    {
        mTvTitle.setText(mDeviceName);
        Log.e("分割 ==== add","  " +mDeviceAddress);
        // 分
        if (LoginInterceptor.getLogin()) {

            mXinfengDevice = (XinfengDevice) myDeviceManager.mData.get(mDeviceAddress);
            Log.e("分割 ==== add", mXinfengDevice + "  " + mDeviceAddress + "  " + myDeviceManager.mData.get(mDeviceAddress));
            if (mXinfengDevice.isInitialized()) {
                Log.e("-------","uiShutdown3");
                updateUI((XinfengDevice) myDeviceManager.mData.get(mDeviceAddress));
            }
        } else {
            mXinfengDevice = (XinfengDevice) myDeviceManager.mDataOff.get(mDeviceAddress);
            Log.e("分割 ==== add", mXinfengDevice + "  " + mDeviceAddress + "  " + myDeviceManager.mDataOff.get(mDeviceAddress));
            if (mXinfengDevice.isInitialized()) {
                Log.e("-------","uiShutdown4");
                updateUI((XinfengDevice) myDeviceManager.mDataOff.get(mDeviceAddress));
            }
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
        mXinfengDevice.setSpeed(-1);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
//        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
//        registerReceiver(mDownloadReceiver, filter);
//
//        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mDfuUpdateReceiver, makeDfuUpdateIntentFilter());
//        DfuServiceListenerHelper.registerProgressListener(getApplicationContext(), mDfuProgressListener);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
//        unregisterReceiver(mDownloadReceiver);
//        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mDfuUpdateReceiver);
//        DfuServiceListenerHelper.unregisterProgressListener(getApplicationContext(), mDfuProgressListener);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        UartUtil.unbind();
        endTime = System.currentTimeMillis();

        unregisterReceiver(mDownloadReceiver);
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mDfuUpdateReceiver);
        DfuServiceListenerHelper.unregisterProgressListener(getApplicationContext(), mDfuProgressListener);


        long durationTime = (endTime - startTime)/1000;
        if (mXinfengDevice != null){
            CountlyUtil.recordControlDurationTime(mXinfengDevice.getTypeId(),durationTime);
        }

        //关闭全部动画
//        if (waveHelper != null)
//        {
//            waveHelper.cancelAnim();
//        }
        waveTransitionManager.stopThread();

//        if (fanRotateAnim1 != null)
//        {
//            fanRotateAnim1.cancel();
//            fanRotateAnim1 = null;
//        }
//        if (fanRotateAnim2 != null)
//        {
//            fanRotateAnim2.cancel();
//            fanRotateAnim2 = null;
//        }
//        if (fanRotateAnim3 != null)
//        {
//            fanRotateAnim3.cancel();
//            fanRotateAnim3 = null;
//        }
        mCaprFilterValue.cancelAnimation();
        mCaprFilterValueBg.cancelAnimation();
        mXinfengDevice = null;
        progressBarDialog = null;
        mDfuUpdateReceiver = null;
        uiLockHandler.removeCallbacksAndMessages(null);
        uiLockHandler = null;

        System.gc();
    }

//    private void controlSpeed(View v)
//    {
//        MobclickAgent.onEvent(this, "XinfengControlActivity_controlSpeed");
//        if (isSleep)
//        {
//            AppToast.showToast(R.string.msg_wake_up_xinfeng);
//            return;
//        }
///*        switch (v.getId())
//        {
//            case R.id.iv_fan_level_1:
//                defSpeed = 50;
//                defSpeedLevel = 1;
//                break;
//            case R.id.iv_fan_level_2:
//                defSpeed = 80;
//                defSpeedLevel = 2;
//                break;
//            case R.id.iv_fan_level_3:
//                defSpeed = 100;
//                defSpeedLevel = 3;
//                break;
//        }*/
//        setWindSpeed(defSpeed);
////        waveHelper.changeLevel(defSpeedLevel);
//        waveTransitionManager.changeLevel(speedLevels[defSpeedLevel - 1]);
//        startAnim(defSpeedLevel);
//        lockUI();
//    }

    private void gotoUpload()
    {
        MobclickAgent.onEvent(this, "XinfengControlActivity_gotoUpload");
        if (mXinfengDevice.getVersion() == 0)
        {
            AppToast.showToast(R.string.wait_for_version);
            return;
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 10001);
        }
        else
        {
            isUploading = true;
            Subscriber<DeviceFirmwareEntity> subscriber = new WaitingSubscriber<DeviceFirmwareEntity>(XinfengControlActivity.this)
            {
                @Override
                public void onNext(DeviceFirmwareEntity deviceFirmwareEntity)
                {
                    updateResult(deviceFirmwareEntity);
                }

                @Override
                public void onError(Throwable e)
                {
                    super.onError(e);
                    isUploading = false;
                }
            };
            MopsV2Methods.getInstance().firmware(subscriber,mXinfengDevice.getTypeId());
        }
    }

    private void startDfuService()
    {
        mXinfengDevice.disconnect();
        if (LoginInterceptor.getLogin()) {
            ((BaseBluetoothDevice) myDeviceManager.mData.get(mXinfengDevice.getAddress())).setAutoConnect(false);
        } else {
            ((BaseBluetoothDevice) myDeviceManager.mDataOff.get(mXinfengDevice.getAddress())).setAutoConnect(false);
        }
        final DfuServiceInitiator starter = new DfuServiceInitiator(mXinfengDevice.getAddress())
                .setDeviceName(mXinfengDevice.getName())
                .setKeepBond(true);
        if (mFileType == DfuService.TYPE_AUTO)
        {
            starter.setZip(mFileStreamUri, mDevUpdateUtil.mPath);
        }
        else
        {
            Log.d("XinfengControlActivity", "不是自动模式");
            return;
        }
        starter.start(this, DfuService.class);
    }

    private void gotoShare()
    {
        // TODO: 2016/7/21 分享功能，暂时不需要
    }

    private void gotoPowerOn()
    {
        MobclickAgent.onEvent(this, "XinfengControlActivity_gotoPowerOn");
        mCaprFilterValue.setCurrentZero();
        mCaprFilterValueBg.setCurrentZero();
        Log.e("------","1");
        setWindSpeed(defSpeed);
        mSeekBar.setProgress(defSpeed - 30);
        uiPowerOn(mXinfengDevice);
//        waveHelper.changeLevel(defSpeedLevel);
        waveTransitionManager.changeLevel(speedLevels[defSpeedLevel - 1]);
//        startAnim(defSpeedLevel);
        lockUI();
    }

    private void gotoPowerOff()
    {
        MobclickAgent.onEvent(this, "XinfengControlActivity_gotoPowerOff");
        Log.e("------","2");
        setWindSpeed(0);
        mSeekBar.setProgress(0);
        uiShutdown();
        lockUI();
    }

    private void setWindSpeed(int value)
    {
        Log.e("------",""+value);
        mXinfengDevice.setWindSpeed(value);
        lastSpeed = value;
    }

//    private void initAnimations()
//    {
//        LinearInterpolator lin = new LinearInterpolator();
//
////        fanRotateAnim1 = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
////        fanRotateAnim1.setInterpolator(lin);
////        fanRotateAnim1.setDuration(2000);
////        fanRotateAnim1.setRepeatCount(Animation.INFINITE);
////        fanRotateAnim1.setRepeatMode(Animation.RESTART);
////
////        fanRotateAnim2 = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
////        fanRotateAnim2.setInterpolator(lin);
////        fanRotateAnim2.setDuration(1500);
////        fanRotateAnim2.setRepeatCount(Animation.INFINITE);
////        fanRotateAnim2.setRepeatMode(Animation.RESTART);
////
////        fanRotateAnim3 = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
////        fanRotateAnim3.setInterpolator(lin);
////        fanRotateAnim3.setDuration(1000);
////        fanRotateAnim3.setRepeatCount(Animation.INFINITE);
////        fanRotateAnim3.setRepeatMode(Animation.RESTART);
////
////
////        //待确定是否有效
////        if (fanRotateAnim1 != null){
////            fanRotateAnim1.cancel();
////        }
////        if (fanRotateAnim2 != null){
////            fanRotateAnim2.cancel();
////        }
////        if (fanRotateAnim3 != null){
////            fanRotateAnim3.cancel();
////        }
//    }

//    private void startAnim(int index)
//    {
//        //待确定是否有效
//        if (fanRotateAnim1 != null){
//            fanRotateAnim1.cancel();
//        }
//        if (fanRotateAnim2 != null){
//            fanRotateAnim2.cancel();
//        }
//        if (fanRotateAnim3 != null){
//            fanRotateAnim3.cancel();
//        }
///*        switch (index)
//        {
//            //7.0 动画不旋转,改变启动方式
//            case 1:
//                mIvFanLevel1.startAnimation(fanRotateAnim1);
//                break;
//            case 2:
//                mIvFanLevel2.startAnimation(fanRotateAnim2);
//                break;
//            case 3:
//                mIvFanLevel3.startAnimation(fanRotateAnim3);
//                break;
//        }*/
//    }

    private void uiShutdown()
    {
        mRlDevicePowerOn.setVisibility(View.GONE);
        mRlDevicePowerOff.setVisibility(View.VISIBLE);
//        waveHelper.changeLevel(0);
        waveTransitionManager.powerOff();
        mSeekBar.setProgress(0);
        Log.e("------","3");
        setWindSpeed(0);
//        startAnim(0);
        aniRunning = false;
        isSleep = true;
        mSeekBar.setEnabled(false);
        mSeekBar.setThumb(getResources().getDrawable(R.drawable.ic_thumb_off));
        mIvFanLeft.setImageResource(R.drawable.ic_fan_small_off);
        mIvFanRight.setImageResource(R.drawable.ic_fan_big_off);
    }

    private void uiPowerOn(XinfengDevice device)
    {
        mSeekBar.setEnabled(true);
        mSeekBar.setThumb(getResources().getDrawable(R.drawable.ic_thumb_open));
        mIvFanLeft.setImageResource(R.drawable.ic_fan_small_open);
        mIvFanRight.setImageResource(R.drawable.ic_fan_big_open);
        mRlDevicePowerOn.setVisibility(View.VISIBLE);
        mRlDevicePowerOff.setVisibility(View.GONE);
        isSleep = false;

        int usedTimeThis = device.getUse_time_this();
        int usedTime = device.getUse_time();
        final float usedLevel = XinfengUtlis.getFilterLevel(usedTimeThis);
        mCaprFilterValueBg.setCurrentValues(100, new Animator.AnimatorListener()
        {
            @Override
            public void onAnimationStart(Animator animation)
            {

            }

            @Override
            public void onAnimationEnd(Animator animation)
            {
                mCaprFilterValue.setCurrentValues(usedLevel);
            }

            @Override
            public void onAnimationCancel(Animator animation)
            {

            }

            @Override
            public void onAnimationRepeat(Animator animation)
            {

            }
        });

        if (usedTimeThis < 100)
        {
            DecimalFormat df = new DecimalFormat("00");
            mTvFilterValue.setText(df.format(usedTimeThis));
            mTvFilterUnit.setText(dm3SpStr);
        }
        else
        {
            DecimalFormat df = new DecimalFormat("0.0");
            mTvFilterValue.setText(df.format(usedTimeThis / 1000.0));
            mTvFilterUnit.setText(m3SpStr);
        }

        if (device.getFilter_in() == 0)
        {
            mTvFilterConnotValue.setText(getResources().getText(R.string.device_connet_not_in));
        }
        else
        {
            float usedPercent = (float) (usedTime / (55 * 60 * 60.0));
            if (usedPercent > 1)
            {
                usedPercent = 1;
            }
            DecimalFormat df2 = new DecimalFormat("#0.0%");
            mTvFilterConnotValue.setText(getResources().getText(R.string.filter_cotton_status) + " " + df2.format(1 - usedPercent));
        }

        if (device.getBattery_charge() == 1)
        {
            mIvPower.setImageLevel(110);
        }
        else
        {
            mIvPower.setImageLevel(device.getBattery_power());
        }
    }

    //强制升级
    private boolean forcedUpdate() {
        gotoUpload();
        return false;
    }

    private void updateUI(XinfengDevice device)
    {
        //Logger.d(device.toString());
        if (device == null)
        {
            finish();
            return;
        }
        if (isUILock)
        {
            return;
        }

        if (device.getVersion() > 20 && device.getVersion() < FORCED_UPDATE_MIN_VERSION && !isForcedUpdate) {
            isForcedUpdate = true;
            forcedUpdate();
        }
        mFlLoading.setVisibility(View.GONE);


        if (device.getSpeed() == 0)
        {
            uiShutdown();
        }
        else if (device.getVersion() != 0)
        {
            uiPowerOn(device);
            if (lastSpeed != device.getSpeed() || !aniRunning)
            {
                int speed = device.getSpeed();
                defSpeed = speed;
                if (speed <= 50)
                {
                    defSpeedLevel = 1;
                }
                else if (speed <= 80)
                {
                    defSpeedLevel = 2;
                }
                else
                {
                    defSpeedLevel = 3;
                }
//                waveHelper.changeLevel(defSpeedLevel);
                waveTransitionManager.changeLevel(speedLevels[defSpeedLevel - 1]);
//                startAnim(defSpeedLevel);
                aniRunning = true;
            }
            if(device.getSpeed() == 90 || device.getSpeed() == 50){
                mSeekBar.setProgress(device.getSpeed() - 30);
                inControlActivity = false;
                defSpeed = device.getSpeed();
            }
            if(inControlActivity){
                mSeekBar.setProgress(device.getSpeed() - 30);
            }
        }
        lastSpeed = device.getSpeed();
    }
    private void isShowToast(){
        if(!mSeekBar.isEnabled()){
            AppToast.showToast(R.string.msg_wake_up_xinfeng);
        }
    }
    private void lockUI()
    {
        isUILock = true;
        uiLockHandler.removeMessages(0);
        uiLockHandler.sendEmptyMessageDelayed(0, 1500);
    }

    @Override
    public void onDataChanged(String address,byte[] values)
    {
        if(address.equals(mXinfengDevice.getAddress()))
        {
            boolean needChanged = false;
            if (!mXinfengDevice.isInitialized())
            {
                DeviceFactory.parseData(values, mXinfengDevice);
                SmartHomeApplication.mBluetoothService.updateData(mXinfengDevice.getAddress(), mXinfengDevice);
                needChanged = true;
            }
            else
            {
                if (DeviceFactory.parseData(values, mXinfengDevice))
                {
                    SmartHomeApplication.mBluetoothService.updateData(mXinfengDevice.getAddress(), mXinfengDevice);
                    needChanged = true;
                }
            }

            if (needChanged)
            {
                Observable.empty()
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .doOnCompleted(() -> {Log.e("-------","uiShutdown5");updateUI(mXinfengDevice);})
                        .subscribe();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 10001)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                gotoUpload();
            }
            else
            {
                AppToast.showToast(R.string.msg_dfu_write);
            }
        }
    }

    private void updateResult(DeviceFirmwareEntity entity) {

        if (isForcedUpdate) {
            onForcedUpdateResult(entity);
        } else {
            onUpdateResult(entity);
        }

    }

    private void onForcedUpdateResult(DeviceFirmwareEntity entity) {

        if (LoginInterceptor.getLogin()) {
            mXinfengDevice = (XinfengDevice) myDeviceManager.mData.get(mDeviceAddress);
        } else {
            mXinfengDevice = (XinfengDevice) myDeviceManager.mDataOff.get(mDeviceAddress);
        }

        mEntity = entity;
        if (mXinfengDevice != null && mEntity != null) {
            progressBarDialog.show();
            progressBarDialog.setCanceledOnTouchOutside(false);
            progressBarDialog.setCancelable(false);
            progressBarDialog.setTitle(getResources().getString(R.string.upload_dialog_title));
            progressBarDialog.setContent(getResources().getString(R.string.upload_force));
            progressBarDialog.setLeftStr(getResources().getString(R.string.upload_cancel));
            progressBarDialog.setRightStr(getResources().getString(R.string.upload_confirm));
            progressBarDialog.setLeftClickListener(v -> progressBarDialog.dismiss());
            progressBarDialog.setLeftEnable(false);
            progressBarDialog.setRightClickListener(v ->
            {
                if (mXinfengDevice.getBattery_power() < 50) {
                    AppToast.showToast(R.string.upload_dialog_battery_title);
                    progressBarDialog.setContent(getResources().getString(R.string.upload_dialog_battery_title));
                    progressBarDialog.setLeftStr(getResources().getString(R.string.upload_cancel));
                    progressBarDialog.setRightStr(getResources().getString(R.string.upload_confirm));
                    progressBarDialog.setLeftClickListener(w -> progressBarDialog.dismiss());
                    progressBarDialog.setLeftEnable(false);
                    progressBarDialog.setRightClickListener(w ->
                    {
                        isUploading = false;
                        progressBarDialog.dismiss();
                        finish();
                    });
                } else {
                    mReference = mDevUpdateUtil.downloadFirmware(mEntity.getUrl());
                    progressBarDialog.setContent(getResources().getString(R.string.upload_downloading));
                    progressBarDialog.setLeftEnable(false);
                    progressBarDialog.setRightEnable(false);
                    progressBarDialog.setCancelable(false);
                    progressBarDialog.setCanceledOnTouchOutside(false);
                }
            });


        } else {
            isUploading = false;
            finish();
        }
    }

    private void onUpdateResult(DeviceFirmwareEntity entity)
    {
        if (LoginInterceptor.getLogin()) {
            mXinfengDevice = (XinfengDevice) myDeviceManager.mData.get(mDeviceAddress);
        } else {
            mXinfengDevice = (XinfengDevice) myDeviceManager.mDataOff.get(mDeviceAddress);
        }

        mEntity = entity;if (mXinfengDevice != null && mEntity != null)
        {
            if(Integer.parseInt(mXinfengDevice.getVersion()+"") < Integer.parseInt(mEntity.getVersion()))
            {
                progressBarDialog.show();
                progressBarDialog.setCanceledOnTouchOutside(false);
                progressBarDialog.setTitle(getResources().getString(R.string.upload_dialog_title));

                String content;
                if (SystemUtils.isWifi(getBaseContext()))
                {
                    content = getResources().getString(R.string.upload_content);
                } else
                {
                    content = getResources().getString(R.string.upload_content_no_wifi);
                }
                content = String.format(content, mXinfengDevice.getVersion() + "", mEntity.getVersion());
                progressBarDialog.setContent(content);
                progressBarDialog.setLeftStr(getResources().getString(R.string.upload_cancel));
                progressBarDialog.setRightStr(getResources().getString(R.string.upload_confirm));
                progressBarDialog.setLeftClickListener(v -> progressBarDialog.dismiss());
                progressBarDialog.setRightClickListener(v ->
                {
                    if (mXinfengDevice.getBattery_power() < 50) {
                        AppToast.showToast(R.string.upload_dialog_battery_title);
                        progressBarDialog.setContent(getResources().getString(R.string.upload_dialog_battery_title));
                        progressBarDialog.setLeftStr(getResources().getString(R.string.upload_cancel));
                        progressBarDialog.setRightStr(getResources().getString(R.string.upload_confirm));
                        progressBarDialog.setLeftClickListener(w -> progressBarDialog.dismiss());
                        progressBarDialog.setLeftEnable(false);
                        progressBarDialog.setRightClickListener(w ->
                        {
                            isUploading = false;
                            progressBarDialog.dismiss();
                        });
                    } else {
                        mReference = mDevUpdateUtil.downloadFirmware(mEntity.getUrl());
                        progressBarDialog.setContent(getResources().getString(R.string.upload_downloading));
                        progressBarDialog.setLeftEnable(false);
                        progressBarDialog.setRightEnable(false);
                        progressBarDialog.setCancelable(false);
                        progressBarDialog.setCanceledOnTouchOutside(false);
                    }

                });


            } else
            {
                isUploading = false;
                AppToast.showToast(R.string.upload_no_version);
            }
        }
    }
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        inControlActivity = false;
        defSpeed = setSpeed;
        setWindSpeed(setSpeed);
        waveTransitionManager.changeLevel(setSpeed);

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress,
                                  boolean fromUser) {
        setSpeed = seekBar.getProgress() +30;
        Log.e("issetspeed","speed:  "+mXinfengDevice.getSpeed()+
                "    fromUser:"  +fromUser +" :   "+ seekBar.getProgress()+
                "inControlActivity :"+inControlActivity);
        if(!fromUser && seekBar.getProgress() == 0 && !inControlActivity){
            setWindSpeed(0);
        }
    }

    private static class UILockHandler extends Handler
    {

        private WeakReference<XinfengControlActivity> ref;

        public UILockHandler(XinfengControlActivity activity)
        {
            if (activity != null)
            {
                ref = new WeakReference<>(activity);
            }
        }

        @Override
        public void handleMessage(Message msg)
        {
            if (ref == null)
            {
                return;
            }
            XinfengControlActivity v = ref.get();
            if (v == null)
            {
                return;
            }

            switch (msg.what)
            {
                case 0:
                    v.isUILock = false;
                    break;
            }
        }
    }
}