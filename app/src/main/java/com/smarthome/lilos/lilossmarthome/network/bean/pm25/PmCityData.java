package com.smarthome.lilos.lilossmarthome.network.bean.pm25;

/**
 * Created by amibition on 2017/4/25.
 */

public class PmCityData {

    private String country;
    private String province;
    private String city;
    private String area;
    private String area_id;

    public String getAreaId() {
        return area_id;
    }

    public void setAreaId(String areaId) {
        this.area_id = areaId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return "PmCityData{" +
                "country='" + country + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", area='" + area + '\'' +
                '}';
    }
}
