package com.smarthome.lilos.lilossmarthome.network.bean;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Created by Kevin on 2016/7/28.
 */
public class DeviceListEntity extends ResponseEntity
{
    private ArrayList<DevListData> data;

    public ArrayList<DevListData> getData()
    {
        return data;
    }

    public void setData(ArrayList<DevListData> data)
    {
        this.data = data;
    }

    public class DevListData
    {
        private int id;
        private String name;
        private String icon;
        private int typeId;
        private String devId;

        public String getDevId()
        {
            return devId;
        }

        public void setDevId(String devId)
        {
            this.devId = devId;
        }

        public int getId()
        {
            return id;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public String getName()
        {
            try
            {
                name = new String(URLDecoder.decode(name, "utf-8").getBytes());
            } catch (UnsupportedEncodingException e)
            {
                e.printStackTrace();
            }
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getIcon()
        {
            return icon;
        }

        public void setIcon(String icon)
        {
            this.icon = icon;
        }

        public int getTypeId()
        {
            return typeId;
        }

        public void setTypeId(int typeId)
        {
            this.typeId = typeId;
        }
    }
}
