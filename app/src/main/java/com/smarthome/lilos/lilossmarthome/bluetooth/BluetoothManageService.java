package com.smarthome.lilos.lilossmarthome.bluetooth;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.orhanobut.logger.Logger;
import com.smarthome.lilos.lilossmarthome.SmartHomeApplication;
import com.smarthome.lilos.lilossmarthome.device.BaseBluetoothDevice;
import com.smarthome.lilos.lilossmarthome.device.BaseDevice;
import com.smarthome.lilos.lilossmarthome.device.DeviceFactory;
import com.smarthome.lilos.lilossmarthome.device.DeviceManager;
import com.smarthome.lilos.lilossmarthome.device.DeviceMap;
import com.smarthome.lilos.lilossmarthome.fragment.TabHomeFragment;
import com.smarthome.lilos.lilossmarthome.network.bean.DeviceListEntity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.UserDeviceEntity;
import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;
import com.smarthome.lilos.lilossmarthome.utils.login.LoginInterceptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 蓝牙设备列表管理
 * 管理已配对的蓝牙设备,包括添加删除修改等操作
 */
public class BluetoothManageService extends BLEService
{

    public static final String SHAREPREFERENCE_NAME = "XINGFENG";
    public final IBinder mBluetoothLocalBinder = new BluetoothLocalBinder();
    public DeviceMap deviceMap;

    public TabHomeFragment.ConnectTimeOutListener mListener = null;

    public void setConnectTimeOutListener(TabHomeFragment.ConnectTimeOutListener listener)
    {
        this.mListener = listener;
    }

    /***
     * 初始化 data 数据
     */
    public void initData()
    {
        Logger.v("初始化设备数据");
        deviceMap = new DeviceMap();
        deviceMap.map = new HashMap<>();
        deviceMap.mapOff = new HashMap<>();
    }

    public void clearData(int mark)
    {
        Logger.v("清除蓝牙服务中的设备数据");
        if (SharePreferenceUtils.getLoginStatus()) {
            if (mark == 0) {
                DeviceManager.getInstance().mData.clear();
                //DeviceManager.getInstance().mDataOff.clear();
                SharedPreferences sp = getSharedPreferences(SHAREPREFERENCE_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.clear();
                editor.apply();
            }
            if (mark == 1) {
                if (DeviceManager.getInstance().mData.size() > 0) {
                    for (Iterator iter = DeviceManager.getInstance().mData.entrySet().iterator(); iter.hasNext(); ) {
                        Map.Entry element = (Map.Entry) iter.next();
                        Object strObj = element.getValue();
                        if (element.getValue() instanceof BaseBluetoothDevice) {
                            final BaseBluetoothDevice device = (BaseBluetoothDevice) strObj;
                            if (device.getIsconnectd() == 0 && !device.ismatchd()) {
                                iter.remove();
                            }
                        }
                    }
                }
            }
        } else {
            if (mark == 0) {
                //DeviceManager.getInstance().mDataOff.clear();
                SharedPreferences sp = getSharedPreferences(SHAREPREFERENCE_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.clear();
                editor.commit();
            }
            if (mark == 1) {
                if (DeviceManager.getInstance().mDataOff.size() > 0) {
                    for (Iterator iter = DeviceManager.getInstance().mData.entrySet().iterator(); iter.hasNext(); ) {
                        Map.Entry element = (Map.Entry) iter.next();
                        Object strObj = element.getValue();
                        if (element.getValue() instanceof BaseBluetoothDevice) {
                            final BaseBluetoothDevice device = (BaseBluetoothDevice) strObj;
                            if (device.getIsconnectd() == 0 && !device.ismatchd()) {
                                iter.remove();
                            }
                        }
                    }
                }
            }
        }
    }

    public void removeData(String address)
    {
        Log.d("===","removeData---BLEMANSer");

        Logger.v("删除蓝牙服务内的设备信息，MAC地址为：" + address);
        if (SharePreferenceUtils.getLoginStatus()) {
            Iterator iter = DeviceManager.getInstance().mData.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                Object val = entry.getValue();
                BaseDevice device = (BaseDevice) val;
                if (device.getAddress().equals(address)) {
                    device.disconnect();
                    iter.remove();
                    break;
                }
            }

            if (DeviceManager.getInstance().mData.isEmpty()) {
                clearData(0);
            } else {
                DeviceManager.getInstance().saveData();
            }
        } else {
            Iterator iter = DeviceManager.getInstance().mDataOff.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                Object val = entry.getValue();
                BaseDevice device = (BaseDevice) val;
                if (device.getAddress().equals(address)) {
                    device.disconnect();
                    iter.remove();
                    break;
                }
            }

            if (DeviceManager.getInstance().mDataOff.isEmpty()) {
                clearData(0);
            } else {
                DeviceManager.getInstance().saveData();
            }

        }
    }

    public void addData(String address, BaseDevice item)
    {
        Log.d("===","addData--BLEMANSer");
        if (SharePreferenceUtils.getLoginStatus()) {

            DeviceManager.getInstance().mData.put(address, item);
            broadcastUpdate(Events.DEVICE_ADDED, address);
            DeviceManager.getInstance().saveData();
        } else {
            Log.e("put----addData","  :"+address);
            DeviceManager.getInstance().mDataOff.put(address, item);
            broadcastUpdate(Events.DEVICE_ADDED, address);
            DeviceManager.getInstance().saveData();
        }
    }

    public void updateData(String address, Object item)
    {
        Log.d("===","updateData---bluetoothManageService--");
        if (SharePreferenceUtils.getLoginStatus()) {

            DeviceManager.getInstance().mData.put(address, (BaseDevice) item);
            DeviceManager.getInstance().saveData();
            RxBus.getInstance().send(Events.GATT_DATA_CHANGED);
        } else {
            Log.e("put----updateData","  :"+address);
            DeviceManager.getInstance().mDataOff.put(address, (BaseDevice) item);
            DeviceManager.getInstance().saveData();
            RxBus.getInstance().send(Events.GATT_DATA_CHANGED);
        }
    }

    public void getDeviceList(List<UserDeviceEntity> data)
    {
        Log.d("===","login--1-getDeviceList");

        DeviceManager.getInstance().mData.clear();
        if (!data.isEmpty())
        {
            for (UserDeviceEntity dev : data)
            {
                BaseDevice device = DeviceFactory.createDevice(dev);
                if (device != null)
                {
                    if (DeviceManager.getInstance().mDataOff.containsKey(device.getAddress())){
                        Log.d("===","login--2-getDeviceList" +DeviceManager.getInstance().mDataOff.get(device.getAddress()).getIsconnectd() );
                        if (DeviceManager.getInstance().mDataOff.get(device.getAddress()).getIsconnectd() != 0){
                            device.setIsconnectd(DeviceManager.getInstance().mDataOff.get(device.getAddress()).getIsconnectd());
                        }
                    }
                    Log.d("===","login--2-getDeviceList---" + device.getIsconnectd());

                    addData(device.getAddress(), device);
                }
            }
            SmartHomeApplication.myDeviceManager.refreshWiFiDevices();
        }
        DeviceManager.getInstance().mDataOff.clear();
        DeviceManager.getInstance().ClearOff();

    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return mBluetoothLocalBinder;
    }

    @Override
    public boolean onUnbind(Intent intent)
    {
        return super.onUnbind(intent);
    }

    public void scannedDevice(BaseBluetoothDevice device)
    {
        RxBus.getInstance().send(Events.DEVICE_SCANNED,device);
    }

    @Override
    protected void broadcastUpdate(@Events.EventCode int code, String address)
    {
        if (SharePreferenceUtils.getLoginStatus()) {
            final BaseDevice baseDevice = DeviceManager.getInstance().mData.get(address);
            if (baseDevice instanceof BaseBluetoothDevice) {
                final BaseBluetoothDevice device = (BaseBluetoothDevice) DeviceManager.getInstance().mData.get(address);
                switch (code) {
                    case Events.DEVICE_ADDED:
                        Logger.d("添加设备");
                        sendAddress(code, address);
                        break;
                    case Events.GATT_CONNECTING:
                        Logger.d("连接设备中");
                        device.setIsconnectd(BLEService.STATE_CONNECTING);
                        Log.e("**updateData"," :1");
                        updateData(address, device);
                        sendAddress(code, address);
                        break;
                    case Events.GATT_CONNECTED:
                        Logger.d("连接成功");
                        if (device != null) {
                            device.setIsconnectd(BLEService.STATE_CONNECTED);
                            device.setIsmatchd(true);
                            device.setAutoConnect(true);
                            Log.e("**updateData"," :2");
                            updateData(address, device);
                        }
                        sendAddress(code, address);
                        break;
                    case Events.GATT_DISCONNECTED:
                        Logger.d("断开连接-->取消配对");
                        Log.d("===","GATT_DISCONNECTED-1--bluetoothManageService--");
                        close(address);
                        if (device == null) {
                            break;
                        }
                        if (device.ismatchd()) {
                            Log.d("===","GATT_DISCONNECTED-2--bluetoothManageService--");

                            device.setIsconnectd(BLEService.STATE_DISCONNECTED);
                            if (device.isAutoConnect()) {
                                Logger.d("断开连接-->取消配对-->device.isAutoConnect-->true");
                                device.setAutoConnect(false);
                                device.setAutoConnect1(true);
                                Timer timer = new Timer();
                                TimerTask timerTask = new TimerTask() {
                                    @Override
                                    public void run() {
                                        device.setAutoConnect(true);
                                    }
                                };
                                timer.schedule(timerTask, 5000);
                            }
                            Log.e("**updateData"," :3");
                            updateData(address, device);
                        }
                        if (!device.ismatchd() && device.getIsconnectd() == 1) {
                            removeData(address);
                        }
                        sendAddress(code, address);
                        break;
                    case Events.GATT_SERVICES_DISCOVERED:
                        sendAddress(code, address);
                        break;
                    case Events.DEVICE_NOT_SUPPORT_UART:
                        Logger.d("设备不支持UART");
                        sendAddress(code, address);
                        break;
                    default:
                        break;
                }
            }
        } else {
            final BaseDevice baseDevice = DeviceManager.getInstance().mDataOff.get(address);
            if (baseDevice instanceof BaseBluetoothDevice) {
                final BaseBluetoothDevice device = (BaseBluetoothDevice) DeviceManager.getInstance().mDataOff.get(address);
                switch (code) {
                    case Events.DEVICE_ADDED:
                        Logger.d("添加设备");
                        sendAddress(code, address);
                        break;
                    case Events.GATT_CONNECTING:
                        Logger.d("连接设备中");
                        device.setIsconnectd(BLEService.STATE_CONNECTING);
                        Log.e("**updateData"," :4");
                        updateData(address, device);
                        sendAddress(code, address);
                        break;
                    case Events.GATT_CONNECTED:
                        Logger.d("连接成功");
                        if (device != null) {
                            device.setIsconnectd(BLEService.STATE_CONNECTED);
                            device.setIsmatchd(true);
                            device.setAutoConnect(true);
                            Log.e("**updateData"," :5");
                            updateData(address, device);
                        }
                        sendAddress(code, address);
                        break;
                    case Events.GATT_DISCONNECTED:
                        Log.e("**updateData"," : "+device.ismatchd());
                        Logger.d("断开连接-->取消配对");
                        Log.d("===","GATT_DISCONNECTED-1--bluetoothManageService--");
                        close(address);
                        if (device == null) {
                            break;
                        }
                        if (device.ismatchd()) {
                            device.setIsconnectd(BLEService.STATE_DISCONNECTED);
                            Log.d("===","GATT_DISCONNECTED-2--bluetoothManageService--");

                            if (device.isAutoConnect()) {

                                Logger.d("断开连接-->取消配对-->device.isAutoConnect-->true");
                                device.setAutoConnect(false);
                                device.setAutoConnect1(true);
                                Timer timer = new Timer();
                                TimerTask timerTask = new TimerTask() {
                                    @Override
                                    public void run() {
                                        device.setAutoConnect(true);
                                    }
                                };
                                timer.schedule(timerTask, 5000);
                            }
                            Log.e("**updateData"," :6");
                            updateData(address, device);
                            sendManualDelete(Events.GATT_MANUAL_DISCONNECT,device);
                            //RxBus.getInstance().send(Events.GATT_MANUAL_DISCONNECT,device);
                        }
                        if (!device.ismatchd() && device.getIsconnectd() == 1) {
                            removeData(address);
                        }
                        sendAddress(code, address);
                        break;
                    case Events.GATT_SERVICES_DISCOVERED:
                        sendAddress(code, address);
                        break;
                    case Events.DEVICE_NOT_SUPPORT_UART:
                        Logger.d("设备不支持UART");
                        sendAddress(code, address);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public class BluetoothLocalBinder extends Binder
    {
        public BluetoothManageService getService()
        {
            return BluetoothManageService.this;
        }
    }
}
