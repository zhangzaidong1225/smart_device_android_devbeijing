package com.smarthome.lilos.lilossmarthome.network.bean.pm25;

import java.util.List;

/**
 * Created by Administrator on 2017/5/9.
 */

public class PmWeatherTimeData {

    private String day;

    private List<PMHoursData> hours;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public List<PMHoursData> getTodayData() {
        return hours;
    }

    public void setTodayData(List<PMHoursData> todayData) {
        this.hours = todayData;
    }

    @Override
    public String toString() {
        return "PmWeatherTimeData{" +
                "day='" + day + '\'' +
                ", todayData=" + hours +
                '}';
    }
}
