package com.smarthome.lilos.lilossmarthome.network.bean.usercenter;

/**
 * Created by Joker on 2016/12/16.
 */

public class ErrorMessage2
{
    private String name;
    private String message;
    private int code;
    private int status;
    private String type;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "ErrorMessage2{" +
                "name='" + name + '\'' +
                ", message='" + message + '\'' +
                ", code=" + code +
                ", status=" + status +
                ", type='" + type + '\'' +
                '}';
    }
}
