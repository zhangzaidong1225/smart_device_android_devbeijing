package com.smarthome.lilos.lilossmarthome.activity.user;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.network.bean.mops.FeedbackEntity;
import com.smarthome.lilos.lilossmarthome.network.retrofit.WaitingSubscriber;
import com.smarthome.lilos.lilossmarthome.network.retrofit.request.MopsV2Methods;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;
import com.smarthome.lilos.lilossmarthome.utils.CountlyUtil;
import com.smarthome.lilos.lilossmarthome.utils.RegexpUtils;
import com.umeng.analytics.MobclickAgent;

import rx.Subscriber;

/**
 * 反馈页面
 * 用于反馈APP的问题
 */
public class FeedbackActivity extends BaseActivity
{
    private ImageView mIvBack;
    private EditText mEtContactNum;
    private ImageView mIvClear;
    private EditText mEtFeedbackContent;

    private Button mBtnCommit;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        CountlyUtil.recordEvent(CountlyUtil.EVENT.EVENT_FEEDBACK, CountlyUtil.KEY.ACCESS_TIME);
        init();
    }

    private void init()
    {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mEtContactNum = (EditText) findViewById(R.id.et_contact_num);
        mIvClear = (ImageView) findViewById(R.id.iv_clear_input);
        mEtFeedbackContent = (EditText) findViewById(R.id.et_content);
        mBtnCommit = (Button) findViewById(R.id.btn_commit);

        mIvBack.setOnClickListener(v ->{
            MobclickAgent.onEvent(this, "FeedbackActivity_back");
            finish();
        } );
        mIvClear.setOnClickListener(v -> clearInput());
        mBtnCommit.setOnClickListener(v -> commitFeedBack());
    }

    private void clearInput()
    {
        MobclickAgent.onEvent(this, "FeedbackActivity_clearInput");
        mEtContactNum.setText("");
    }

    private void commitFeedBack()
    {
        MobclickAgent.onEvent(this, "FeedbackActivity_commitFeedBack");
        String contact = mEtContactNum.getText().toString();
        String feedbackContent = mEtFeedbackContent.getText().toString();

        if (!RegexpUtils.isPhoneNum(contact))
        {
            AppToast.showToast(R.string.phone_error);
            return;
        }

        if (feedbackContent.length() < 1)
        {
            AppToast.showToast(R.string.feedback_empty);
            return;
        }

        Subscriber<FeedbackEntity> subscriber = new WaitingSubscriber<FeedbackEntity>(FeedbackActivity.this)
        {
            @Override
            public void onNext(FeedbackEntity feedbackEntity)
            {
                onFeedbackResult(feedbackEntity);
            }
        };
        MopsV2Methods.getInstance().feedback(subscriber,mEtContactNum.getText().toString(),mEtFeedbackContent.getText().toString());
    }

    private void onFeedbackResult(FeedbackEntity entity)
    {
        AppToast.showToast(R.string.commit_success);
        finish();
    }
}
