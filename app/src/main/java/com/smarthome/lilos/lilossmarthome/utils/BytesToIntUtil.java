package com.smarthome.lilos.lilossmarthome.utils;

/**
 * byte[]转换为int类型
 * Created by louis on 16/6/16.
 */
public class BytesToIntUtil
{
    public static int hexArrayToInt(byte byte1)
    {
        return 0x000000FF & ((int) byte1);
    }

    public static int hexArrayToInt(byte byteh, byte bytel)
    {

        int inth = hexArrayToInt(byteh);//(0x000000FF & ((int) byteh));
        int intl = hexArrayToInt(bytel);//(0x000000FF & ((int) bytel));

        return ((int) (inth << 8 | intl)) & 0xFFFFFFFF;
    }

    public static int hexArrayToInt(byte bytehh, byte byteh, byte bytel, byte bytell)
    {
        int inthh = hexArrayToInt(bytehh);
        int inth = hexArrayToInt(byteh);
        int intl = hexArrayToInt(bytel);
        int intll = hexArrayToInt(bytell);
        return (((int) (intll | intl << 8 | inth << 16 | inthh << 24)) & 0xFFFFFFFF);
    }
}
