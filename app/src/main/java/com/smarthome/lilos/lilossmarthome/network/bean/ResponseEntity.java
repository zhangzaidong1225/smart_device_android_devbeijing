package com.smarthome.lilos.lilossmarthome.network.bean;

/**
 * Created by Kevin on 2016/7/20.
 */
public class ResponseEntity
{
    private int code;
    private String msg;

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }
}
