package com.smarthome.lilos.lilossmarthome.network.bean;

/**
 * Created by Kevin on 2016/7/20.
 */
public class SecurityCheckEntity extends ResponseEntity
{

    private SecurityCheckData data;

    public SecurityCheckData getData()
    {
        return data;
    }

    public void setData(SecurityCheckData data)
    {
        this.data = data;
    }

    public class SecurityCheckData
    {
        private int all_count;
        private int user_count;

        public int getAllCount()
        {
            return all_count;
        }

        public void setAllCount(int all_count)
        {
            this.all_count = all_count;
        }

        public int getUser_count()
        {
            return user_count;
        }

        public void setUser_count(int user_count)
        {
            this.user_count = user_count;
        }
    }
}
