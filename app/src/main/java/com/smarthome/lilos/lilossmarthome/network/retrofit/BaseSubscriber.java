package com.smarthome.lilos.lilossmarthome.network.retrofit;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.network.ErrorManager;
import com.smarthome.lilos.lilossmarthome.utils.AppToast;

import java.net.UnknownHostException;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;

/**
 * 基础的Subscriber
 * 预处理onError方法
 * Created by Joker on 2016/12/21.
 */

public abstract class BaseSubscriber<T> extends Subscriber<T>
{
    @Override
    public void onCompleted()
    {

    }

    @Override
    public void onError(Throwable e)
    {
        if(e instanceof UnknownHostException)
        {
            AppToast.showToast(R.string.network_error);
        }
        else if (e instanceof HttpException)
        {
            HttpException he = (HttpException) e;
            ErrorManager.parseError(he);
        }
        else
        {
            e.printStackTrace();
        }
    }
}
