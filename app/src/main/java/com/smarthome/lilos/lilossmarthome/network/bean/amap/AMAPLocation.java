package com.smarthome.lilos.lilossmarthome.network.bean.amap;

/**
 * Created by Joker on 2016/8/28.
 */
public class AMAPLocation
{
    public String status;
    public String info;
    public String infocode;
    public String locations;

    @Override
    public String toString()
    {
        return "AMAPLocation{" +
                "status='" + status + '\'' +
                ", info='" + info + '\'' +
                ", infocode='" + infocode + '\'' +
                ", locations='" + locations + '\'' +
                '}';
    }
}
