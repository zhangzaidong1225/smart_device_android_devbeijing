package com.smarthome.lilos.lilossmarthome.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.widget.RadioGroup;

import com.smarthome.lilos.lilossmarthome.R;
import com.umeng.analytics.MobclickAgent;

import java.util.List;

/**
 * FragmentTab适配器
 * Created by lilos on 2016/6/21.
 */
public class FragmentTabAdapter implements RadioGroup.OnCheckedChangeListener
{

    private List<Fragment> mFragments;
    private RadioGroup mRgtabs;
    private FragmentActivity mFragmentActivity;
    private int mFmContainerId;
    /**
     * 用于让调用者在切换tab时候增加新的功能
     */
    private OnRgsExtraCheckedChangedListener onRgsExtraCheckedChangedListener;

    private int mCurrentId;

    public FragmentTabAdapter(FragmentActivity activity, List<Fragment> fragments,
                              RadioGroup rgtabs, int containerId)
    {
        this.mFragmentActivity = activity;
        this.mFragments = fragments;
        this.mRgtabs = rgtabs;
        this.mFmContainerId = containerId;

        // 默认显示第一页
        FragmentTransaction ft = mFragmentActivity.getSupportFragmentManager().beginTransaction();
        ft.add(mFmContainerId, fragments.get(0));
        ft.commit();
        mRgtabs.check(R.id.tab_home);
        mRgtabs.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId)
    {
        for (int i = 0; i < mRgtabs.getChildCount(); i++)
        {
            if (mRgtabs.getChildAt(i).getId() == checkedId)
            {
                Fragment fragment = mFragments.get(i);
                FragmentTransaction ft = obtainFragmentTransaction(i);

                getCurrentFragment().onPause(); // 暂停当前tab

                if (fragment.isAdded())
                {
                    fragment.onResume(); // 启动目标tab的onResume()
                }
                else
                {
                    ft.add(mFmContainerId, fragment);
                }

                showTab(i);
                ft.commit();

                // 如果设置了切换tab额外功能功能接口
                if (null != onRgsExtraCheckedChangedListener)
                {
                    onRgsExtraCheckedChangedListener.OnRgsExtraCheckedChanged(mRgtabs, checkedId, i);
                }

            }
        }
    }

    /**
     * 切换tab
     *
     * @param idx
     */
    private void showTab(int idx)
    {
        for (int i = 0; i < mFragments.size(); i++)
        {
            Fragment fragment = mFragments.get(i);
            FragmentTransaction ft = obtainFragmentTransaction(idx);

            if (idx == i)
            {
                MobclickAgent.onEvent(mFragmentActivity, "show_fragment"+i);
                ft.show(fragment);
            }
            else
            {
                ft.hide(fragment);
            }
            ft.commit();
        }
        mCurrentId = idx; // 更新目标tab为当前tab
    }

    /**
     * 获取一个带动画的FragmentTransaction
     *
     * @param index
     * @return
     */
    private FragmentTransaction obtainFragmentTransaction(int index)
    {
        FragmentTransaction ft = mFragmentActivity.getSupportFragmentManager().beginTransaction();
        // 设置切换动画
        if (index > mCurrentId)
        {
            ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        }
        else
        {
            ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        }
        return ft;
    }

    public int getCurrentTab()
    {
        return mCurrentId;
    }

    public Fragment getCurrentFragment()
    {
        return mFragments.get(mCurrentId);
    }

    public OnRgsExtraCheckedChangedListener getOnRgsExtraCheckedChangedListener()
    {
        return onRgsExtraCheckedChangedListener;
    }

    public void setOnRgsExtraCheckedChangedListener(
            OnRgsExtraCheckedChangedListener onRgsExtraCheckedChangedListener)
    {
        this.onRgsExtraCheckedChangedListener = onRgsExtraCheckedChangedListener;
    }

    /**
     * 切换tab额外功能功能接口
     */
    public static class OnRgsExtraCheckedChangedListener
    {
        public void OnRgsExtraCheckedChanged(RadioGroup radioGroup, int checkedId, int index)
        {

        }
    }
}
