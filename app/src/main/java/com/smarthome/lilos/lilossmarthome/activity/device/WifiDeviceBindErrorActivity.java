package com.smarthome.lilos.lilossmarthome.activity.device;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.smarthome.lilos.lilossmarthome.R;
import com.smarthome.lilos.lilossmarthome.activity.BaseActivity;
import com.smarthome.lilos.lilossmarthome.activity.MainActivity;
import com.umeng.analytics.MobclickAgent;

/**
 * Wifi设备绑定错误提示页面
 * 用于提示Wifi设备绑定失败
 */
public class WifiDeviceBindErrorActivity extends BaseActivity implements View.OnClickListener
{
    //    Button btn_morehelper = null;
    Button btn_retry = null;
    String curClientID = null;
    String curDeviceName = null;
    ImageView iv_back = null;
    TextView tv_add_error_title = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_device_bind_error);
        initViews();
    }

    private void initViews()
    {
//        btn_morehelper = (Button) findViewById(R.id.btn_errortipmore);
//        btn_morehelper.setOnClickListener(this);

        btn_retry = (Button) findViewById(R.id.btn_retry);
        btn_retry.setOnClickListener(this);
        iv_back = (ImageView) findViewById(R.id.ap_error_ib_back);
        iv_back.setOnClickListener(this);
        curClientID = getIntent().getStringExtra("clientid");
        curDeviceName = getIntent().getStringExtra("devname");

        tv_add_error_title = (TextView) findViewById(R.id.tv_add_error_title);
        tv_add_error_title.setText("设备名称：" + curDeviceName);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
//            case R.id.btn_errortipmore:
//                Toast.makeText(WifiDeviceBindErrorActivity.this,"更多帮助",Toast.LENGTH_LONG).show();
//                break;
            case R.id.btn_retry:
                MobclickAgent.onEvent(this, "WifiDeviceBindErrorActivity_startAutoConfigActivity");
                startAutoConfigActivity();
                break;
            case R.id.ap_error_ib_back:
                MobclickAgent.onEvent(this, "WifiDeviceBindErrorActivity_startMainActivity");
                startMainActivity();
                break;

        }
    }

    private void startMainActivity()
    {
        Intent intent = new Intent();
        intent.setClass(WifiDeviceBindErrorActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void startAutoConfigActivity()
    {
        Intent intent = new Intent();
        intent.putExtra("curclientid", curClientID);
        intent.setClass(WifiDeviceBindErrorActivity.this, APConfigActivity.class);
        startActivity(intent);
    }


}
