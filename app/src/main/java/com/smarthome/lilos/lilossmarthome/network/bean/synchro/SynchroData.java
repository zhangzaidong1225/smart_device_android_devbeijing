package com.smarthome.lilos.lilossmarthome.network.bean.synchro;

/**
 * Created by songyu on 2017/4/24.
 */
public class SynchroData {
    private int device_type_id;
    private String name;
    private String device_id;

    public int getDevice_type_id() {
        return device_type_id;
    }

    public void setDevice_type_id(int device_type_id) {
        this.device_type_id = device_type_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    @Override
    public String toString() {
        return "SynchroData{" +
                "device_type_id='" + device_type_id + '\'' +
                ", name='" + name + '\'' +
                ", device_id='" + device_id + '\'' +
                '}';
    }
}
