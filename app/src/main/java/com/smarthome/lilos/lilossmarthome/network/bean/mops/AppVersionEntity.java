package com.smarthome.lilos.lilossmarthome.network.bean.mops;

/**
 * Created by Joker on 2016/12/30.
 */

public class AppVersionEntity
{
    /**
     * version : 1.0.10
     * is_forced : 1
     * url : https://demo.com/uploads/20161221/585a3725eff62.zip
     * md5 : 3314c12f0e9ff01ac8f6b4ff4a83f46a
     * description : 忻风便携式智能空气净化器
     */

    private String version;
    private int is_forced;
    private String url;
    private String md5;
    private String description;

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public int getIs_forced()
    {
        return is_forced;
    }

    public void setIs_forced(int is_forced)
    {
        this.is_forced = is_forced;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getMd5()
    {
        return md5;
    }

    public void setMd5(String md5)
    {
        this.md5 = md5;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
