package com.smarthome.lilos.lilossmarthome.activity;

import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.smarthome.lilos.lilossmarthome.rxbus.Events;
import com.smarthome.lilos.lilossmarthome.rxbus.RxBus;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;
import com.umeng.analytics.MobclickAgent;

import ly.count.android.sdk.Countly;

/**
 * 基础Activity
 * 实现以下功能:
 * 1.设置界面强制竖屏
 * 2.设置界面的沉浸式
 * 3.初始化RxBus,监听界面退出事件,用于在界面启动时,可以结束掉MainActivity与目标Activity之间的全部Activity,提升操作过程的流畅度
 * 4.设置Conutly的生命周期,是全部Activity集成Countly
 */
public class BaseActivity extends RxAppCompatActivity
{
    protected boolean listenClose = true;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        ViewGroup contentFrameLayout = (ViewGroup) findViewById(Window.ID_ANDROID_CONTENT);
        View parentView = contentFrameLayout.getChildAt(0);
        if (parentView != null && Build.VERSION.SDK_INT >= 14)
        {
            parentView.setFitsSystemWindows(true);
        }

        Countly.onCreate(this);

        initRxBus();
    }

    private void initRxBus()
    {
        RxBus.with(this)
                .setEvent(Events.RETURN_MAIN_ACTIVITY)
                .onNext(events -> {
                    if (listenClose)
                    {
                        finish();
                    }
                })
                .create();
    }
    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }
    @Override
    protected void onStart()
    {
        super.onStart();
        Countly.sharedInstance().onStart(this);
    }

    @Override
    protected void onStop()
    {
        Countly.sharedInstance().onStop();
        super.onStop();
    }
}