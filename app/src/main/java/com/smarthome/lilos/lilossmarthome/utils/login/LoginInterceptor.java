package com.smarthome.lilos.lilossmarthome.utils.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.smarthome.lilos.lilossmarthome.activity.user.LoginActivity;
import com.smarthome.lilos.lilossmarthome.fragment.TabHomeFragment;
import com.smarthome.lilos.lilossmarthome.utils.SharePreferenceUtils;

/**
 * 登录过滤器
 * Created by Joker on 2016/7/19.
 */
public class LoginInterceptor
{
    public static final String mINVOKER = "INTERCEPTOR_INVOKER";
    public static final String mFLAG = "INTERCEPTOR_FLAG";

    public static void interceptor(Context ctx, String target, Bundle bundle, Intent intent,
                                   int[] flags)
    {
        if (target != null && target.length() > 0)
        {
            LoginCarrier invoker = new LoginCarrier(target, bundle);
            if (getLogin()) {//获取登录状态
                invoker.invoke(ctx, flags);
            } else {
                if (intent == null)
                {
                    intent = new Intent(ctx, LoginActivity.class);
                }
                login(ctx, invoker, intent, flags);
            }
        }
    }

    public static void interceptor(Context ctx, String target, Bundle bundle)
    {
        interceptor(ctx, target, bundle, null, null);
    }

    public static void interceptor(Context ctx, String target, Bundle bundle, int[] flags)
    {
        interceptor(ctx, target, bundle, null, flags);
    }

    public static boolean getLogin()
    {
        return SharePreferenceUtils.getLoginStatus();
    }

    private static void login(Context context, LoginCarrier invoker, Intent intent, int[] flags)
    {
        intent.putExtra(mINVOKER, invoker);
        if (flags != null && flags.length > 0)
        {
            intent.putExtra(mFLAG, flags);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }
}

